package core;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import utilitys.ConstantPath;
import utilitys.util;


public class SpecificElementScreenShot {

	
	static BufferedImage eleScreenshot;
	WebDriver driver;
	static File screenshotLocation;
	private String currentDir = System.getProperty("user.dir");
	public static String status;

	
	public SpecificElementScreenShot(WebDriver driver) {
		this.driver = driver;
	}

	public void SpecificElementScrenShot(WebElement ele, String uAgent, int value, String FileName) throws IOException {

		try {
			// Get entire page screenshot
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			BufferedImage fullImg = ImageIO.read(screenshot);

			// Get the location of element on the page
			Point point = ele.getLocation();
			System.out.println("Check1");
			// Get width and height of the element
			int eleWidth = ele.getSize().getWidth();
			int eleHeight = ele.getSize().getHeight();

			/*
			 * int wordYCoordinate =point.getY(); int currentPointYCoordinate
			 * =wordYCoordinate - (screenHeight * (int)
			 * Math.round(Math.floor(wordYCoordinate / screenHeight))-1);
			 */

			if (uAgent.contains("chrome")) {
				point.move(point.getX(), value);
			}

			if (uAgent.contains("firefox")) {

				point.move(point.getX(), value);
			}

			System.out.println("Check X Y Cordinate size-->" + point.getX() + " : " + point.getY() + " : " + eleWidth
					+ " : " + eleHeight);

			// Crop the entire page screenshot to get only element screenshot
			eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);

			try {
				ImageIO.write(eleScreenshot, "png", screenshot);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// Copy the element screenshot to disk
			screenshotLocation = new File(
					"D:\\Edit_GenieWorkSpace\\Casper2.0\\SikulaImage\\" + uAgent + FileName + ".png");
			FileUtils.copyFile(screenshot, screenshotLocation);
System.out.println("Check2");
		} catch (WebDriverException e) {
			e.printStackTrace();
		}
	}

	public String imageverification(String path) throws Exception {
	
		String status = "";


		BufferedImage expectedImage = ImageIO.read(new File(currentDir + path + ".png"));

		ImageDiffer imgDiff = new ImageDiffer();
		ImageDiff diff = imgDiff.makeDiff(eleScreenshot, expectedImage);
		
		if (diff.hasDiff() == true) {
			status = "Fail";

			MyScreenRecorder.stopRecording();
			String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
			util.MoveRecFile(RecVideo);

			System.out.println("Images are Not Same");

		} else {

			status = "Pass";

			MyScreenRecorder.stopRecording();
			String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
			util.deleteRecFile(RecVideo);

			System.out.println("Images are Same");
		}

		return status;
	}
}
