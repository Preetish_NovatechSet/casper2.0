package core;

import java.awt.AWTException;
import java.awt.Robot;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;


public class MoveToElement {
	
	public MoveToElement(final WebElement element) {
	}
	
	public static MoveToElement byId(final WebDriver driver, final String id) {
		WaitFor.presenceOfElementById(driver, id);
		Actions builder = new Actions(driver); 
		builder.moveToElement(driver.findElement(By.id(id)))
		.build().perform();
		return new MoveToElement(driver.findElement(By.id(id)));
	}
	
	public static MoveToElement byXpath(final WebDriver driver, final String xpath) {
		WaitFor.presenceOfElementByXpath(driver, xpath);
		Actions builder = new Actions(driver); 
		builder.moveToElement(driver.findElement(By.xpath(xpath)))
		.build().perform();
		return new MoveToElement(driver.findElement(By.xpath(xpath)));
	}
	
	public static MoveToElement byClassName(final WebDriver driver, final String className) {
		WaitFor.presenceOfElementByClassName(driver, className);
		Actions builder = new Actions(driver); 
		builder.moveToElement(driver.findElement(By.className(className)))
		.build().perform();
		return new MoveToElement(driver.findElement(By.className(className)));
	}
	
	public static MoveToElement byCssSelector(final WebDriver driver, final String className) {
		
		WaitFor.presenceOfElementByCSSSelector(driver,className);
		Actions builder = new Actions(driver); 
		builder.moveToElement(driver.findElement(By.cssSelector(className)))
		.build().perform();
	
		return new MoveToElement(driver.findElement(By.cssSelector(className)));
	
	}
	public static MoveToElement byLinkText(final WebDriver driver, final String linkText) {
		WaitFor.presenceOfElementByLinkText(driver, linkText);
		Actions builder = new Actions(driver); 
		builder.moveToElement(driver.findElement(By.linkText(linkText)))
		.build().perform();
		return new MoveToElement(driver.findElement(By.linkText(linkText)));
	}
	
	public static MoveToElement byWebElement(final WebDriver driver, final WebElement element) throws AWTException {
		   Robot robot = new Robot();
		   
		 Point point = element.getLocation();
		 int xcord = point.getX();
		 System.out.println("Position of the webelement from left side is "+xcord +" pixels");
		 int ycord = point.getY();
		 System.out.println("Position of the webelement from top side is "+ycord +" pixels");
		
		 robot.mouseMove(xcord, ycord);
		
		/*Actions builder = new Actions(driver); 
		builder
		.moveToElement(element,xcord,ycord)
		.moveToElement(element,xcord,ycord)
		.build().perform();*/
		return new MoveToElement(element);
	}
	
	
	
	public static MoveToElement byKey(final WebDriver driver, final WebElement element, final Keys key) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.click();
		actions.sendKeys(key);
		actions.build().perform();
		return new MoveToElement(element);
	}
	
	public static MoveToElement Click_sendkey(final WebDriver driver, final WebElement element, final String key) {
		try {
			Actions actions = new Actions(driver);
			    Thread.sleep(2000);
			actions.moveToElement(element).click().sendKeys(key);
			actions.build().perform();
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}
		return new MoveToElement(element);
	}
	
	
	public static MoveToElement ClickByX_Ycord (final WebDriver driver, final WebElement element){
		try {
			Point point = element.getLocation();
			int xcord = point.getX();
			System.out.println("Element's Position from left side Is "+xcord +" pixels.");
			int ycord = point.getY();
			System.out.println("Element's Position from top side Is "+ycord +" pixels.");
			Actions build = new Actions(driver);
			build.moveToElement(element).moveByOffset(xcord,0).click().build().perform();			
		} catch (java.lang.Exception e) {
					e.printStackTrace();
		    }
		return new MoveToElement(element);
		}
	
	
	
	public static MoveToElement bysendkeyWithoutclick(final WebDriver driver, final WebElement element, final String key) {
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(element);
			actions.sendKeys(key);
			actions.build().perform();
		    }catch(java.lang.Exception e){
			e.printStackTrace();
		}
		return new MoveToElement(element);
	}
	
	
	public static MoveToElement sendkeybyinsidevalue(final WebDriver driver, final WebElement element, final Keys key) {

	      
			try {
				Actions obj = new Actions(driver);
				  
				  obj.moveToElement(element).sendKeys(key).build().perform();
		
			} catch (StaleElementReferenceException e) {			
				e.getStackTrace();
			}catch(java.lang.Exception e){
				e.printStackTrace();
			}
	  
	      return new MoveToElement(element);
	   }
	
	
	public static MoveToElement byclick(final WebDriver driver, final WebElement element) {
		
		Actions builder = new Actions(driver); 
		builder.moveToElement(element).click();
		builder.build().perform();
	 return new MoveToElement(element);
	 }
	
	
	public static MoveToElement dobleclick(final WebDriver driver, final WebElement element) {

	Actions action = new Actions(driver).doubleClick(element);
    action.build().perform();
    return new MoveToElement(element);
	 }
	
	
	public static MoveToElement selectEleMovecurser(final WebDriver driver, final WebElement element,final WebElement element1,final Keys key) {
		 Actions builder = new Actions(driver);

		   Action selection = builder.clickAndHold(element)
				   .moveToElement(element)
		           .release(element1)
		           .sendKeys(key)
		           .build();
		       selection.perform();		
		
	    return new MoveToElement(element1);
		 }
	
	public static MoveToElement selectMovecurser(final WebDriver driver, final WebElement element,final WebElement element1) {
		 Actions builder = new Actions(driver);

		   Action selection = builder.clickAndHold(element)   
				   .moveToElement(element)
		           .release(element1)
		           .build();
		       selection.perform();		
		
	    return new MoveToElement(element1);
		 }
	
	
public static void selectsingleelement(final WebDriver driver,final Keys key) {
	
		Actions action = new Actions(driver);
	    action.sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME)
	            .keyUp(Keys.SHIFT).sendKeys(key).build().perform();
	   
   
}

public static void Rightclick(final WebDriver driver,final WebElement element) throws InterruptedException {
	
	
	try {
		Actions actions = new Actions(driver);
		Action action=actions.moveToElement(element).contextClick(element).build(); 
        action.perform();
		
	} catch (java.lang.Exception e) {
		
		e.printStackTrace();
	}

	
}
	
	public static void copyelement(final WebDriver driver) {
		
	Actions action = new Actions(driver);
	    action.sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME)
	            .keyUp(Keys.SHIFT).keyDown(Keys.CONTROL).sendKeys("c").keyUp(Keys.CONTROL)
	            .build().perform();
	   
	}
	
public static void copy(final WebDriver driver) {
		
		Actions action = new Actions(driver);
	    action.keyDown(Keys.CONTROL).sendKeys("c").keyUp(Keys.CONTROL)
	            .build().perform();
	         
	}

public static void cut(final WebDriver driver) {
	
	Actions action = new Actions(driver);
    action.keyDown(Keys.CONTROL).sendKeys("x").keyUp(Keys.CONTROL)
            .build().perform();
         
}

public static void paste(final WebDriver driver) {
		
		Actions action = new Actions(driver);
	    action.keyDown(Keys.CONTROL).sendKeys("v").keyUp(Keys.CONTROL)
        .build().perform();
		   
}
	   



public static void selectelementShitHomeBykeys(final WebDriver driver,final Keys key) {
	
	Actions action = new Actions(driver);
         action.keyDown(Keys.SHIFT).sendKeys(Keys.HOME)
            .keyUp(Keys.SHIFT).sendKeys(key).build().perform();
   
}

public static void ShitHomecopy(final WebDriver driver) {
	
	Actions action = new Actions(driver);
         action.keyDown(Keys.SHIFT).sendKeys(Keys.HOME)
            .keyUp(Keys.SHIFT).keyDown(Keys.LEFT_CONTROL).sendKeys("c").keyUp(Keys.LEFT_CONTROL).click().build().perform();
   
}


public static void selectElement(final WebDriver driver) {
	
	Actions action = new Actions(driver);
    action.sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME)
            .keyUp(Keys.SHIFT).build().perform();
   

  }


public static void Shitselect_Ele_Left_RightArrow(final WebDriver driver,final int value,final Keys Left_Right){
	
	Actions action = new Actions(driver);
       action.keyDown(Keys.SHIFT);
   for(int i = 0;i<value;i++) {
    action.sendKeys(Left_Right);
    }
   action.keyUp(Keys.SHIFT).build().perform();
   
    }

}