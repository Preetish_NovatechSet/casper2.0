package com.RightPanel;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.SpecificElementScreenShot;
import core.WaitFor;
import utilitys.Excel;
import utilitys.Switch;

public class RightPanelCitation03 extends CUP_BaseClass {

	static String remark;
	static String className = "RightPanelCitation03";
	static String category;
	static String area;
	static String description;
	private static String status="";
	public static Excel Ex;
	Pom PomObj;

	@Test(alwaysRun = true)
	public void citationFigure03() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			RightPanelCitation03 obj = new RightPanelCitation03();
			obj.citation();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {

			System.out.println(className);

			String remark = status.equalsIgnoreCase("fail")
					? "Element didn't scroll to the actual element/Highlight didn't apper on the element"
					: "";

			Ex.testdata(description, className, remark, category, area, status, uAgent);

		}
	}
	
	

	public void citation() throws Exception {
		

		try {
			
			Ex = new Excel(description, className, remark, category, area, status, uAgent);
			description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option,then move to citation and then click, Go to the Table section and click on cited table, then click on \"next\" button check the Element has scroll to next and highlight or not";
			area = "Citaion-Table";
			category = "Ce-Utilities";
			
			

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele111 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());
			ele111.click();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			Actions act = new Actions(driver);
			act.moveToElement(element).click(element).build().perform();

			Thread.sleep(1000);
			WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2());
			WebElement ele = driver.findElement(By.xpath(PomObj.Table_2()));
			act.moveToElement(ele).click(ele).build().perform();

			for (int i = 0; i <1; i++) {
				Thread.sleep(1000);
				WaitFor.presenceOfElementByXpath(driver, PomObj.Next());
				WebElement ele1 = driver.findElement(By.xpath(PomObj.Next()));

				act.moveToElement(ele1).click(ele1).build().perform();
			}

			switc.SwitchCase(uAgent);

			WaitFor.presenceOfElementByXpath(driver, PomObj.EDS());
			WebElement Eds = driver.findElement(By.xpath(PomObj.EDS()));

			SpecificElementScreenShot obj = new SpecificElementScreenShot(driver);

			if (uAgent.contains("chrome")) {
				obj.SpecificElementScrenShot(Eds, uAgent, 315, "citati");
				  status = obj.imageverification("/screenshots/ChromeCitati");
			}

			if (uAgent.contains("firefox")) {
				obj.SpecificElementScrenShot(Eds, uAgent, 295, "citati");
				  status = obj.imageverification("/screenshots/FirefoxCitati");
			}
			
			 
						

		    } catch (Exception e) {
			  e.printStackTrace();
		}

	}

}