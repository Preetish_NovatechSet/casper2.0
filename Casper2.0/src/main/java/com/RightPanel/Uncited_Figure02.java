package com.RightPanel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class Uncited_Figure02 extends CUP_BaseClass {

	static String remark;
	static String className = "Uncited_Figure02";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	public void Uncited_Ref() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option, then click on CE-UTILITIES then scroll to UNCITED FIGURE then click, Check the Uncited figure List if Present else Go to any Figure cited, then left click and unlink and save, Check Whether Figure citation has Added on UNCITED FIGURE or not";
		area = "Right Panel";
		category = "UNCITED FIGURE";

		try {
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			WaitFor.presenceOfElementByXpath(driver, PomObj.fig1a());
			WebElement ele11 = driver.findElement(By.xpath(PomObj.fig1a()));

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", ele11);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

			ele11.click();

			try {
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.refFig());
				List<WebElement> ele1 = driver.findElements(By.cssSelector(PomObj.refFig()));

				for (WebElement ele : ele1) {

					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", ele11);
					((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

					Thread.sleep(3000);
					MoveToElement.byCssSelector(driver, PomObj.refFig());

					Actions act = new Actions(driver);
					MoveToElement.byCssSelector(driver, PomObj.refFig());
					act.contextClick(ele).build().perform();

					Thread.sleep(4000);

					act.sendKeys(Keys.ENTER).build().perform();

				}
			} catch (Exception e) {

				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.refFig());
				List<WebElement> ele1 = driver.findElements(By.cssSelector(PomObj.refFig()));

				for (WebElement ele : ele1) {

					Thread.sleep(3000);
					MoveToElement.byCssSelector(driver, PomObj.refFig());

					Actions act = new Actions(driver);
					MoveToElement.byCssSelector(driver, PomObj.refFig());
					act.contextClick(ele).build().perform();

					Thread.sleep(4000);

					act.sendKeys(Keys.ENTER).build().perform();

				}
			}

			UpperToolBar obj1 = new UpperToolBar(driver);
			obj1.save_btn_method();

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele111 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());

			Thread.sleep(3000);

			ele111.click();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.uncited_figure()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			Actions act = new Actions(driver);
			act.moveToElement(element).click(element).build().perform();

			String value = "";
			List<WebElement> element1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));

			for (WebElement ele : element1) {

				value = value + ele.getText();
				// System.out.println("Check-->"+value);

			}

			if (value.contains("Uncited Figure are listed!")) {

				status = "Pass";

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Uncited Figure Popup didn't appear";

			}

			WebElement element11 = driver.findElement(By.cssSelector(PomObj.Ok()));

			Thread.sleep(3000);
			element11.click();

			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			WaitFor.presenceOfElementByXpath(driver, PomObj.uncitedFigureList());
			List<WebElement> uncitedRefList = driver.findElements(By.xpath(PomObj.uncitedFigureList()));

			String value1 = "";
			for (WebElement ele : uncitedRefList){
				value1 = value1 + ele.getText();
			}

			// System.out.println("Check Figure "+value1);

			if (value1.contains("Figure 4")) {

				status = "Pass";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				Thread.sleep(3000);
				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				Thread.sleep(3000);
				remark = "After unlink save Uncited Figure didnt displayed";

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(alwaysRun = true)
	public void CopyEditorUncited_fig02() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			Uncited_Figure02 obj = new Uncited_Figure02();
			obj.Uncited_Ref();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}
}
