package com.RightPanel;



import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class Uncited_References02 extends CUP_BaseClass{
	
	
	static String remark;
	static String className = "Uncited_References02"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	 public void Uncited_Ref() throws Exception {
			
try {
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option, then click on CE-UTILITIES then scroll to UNCITED REFRERENCES then click, Check the Uncited References List if Present else Go to any number references cited, then left click and unlink and save, Check Whether nbr Ref has Added on UNCITED REFRERENCES or not";			    
				area = "Right Panel";
					category = "UNCITED REFERENCES";
	
		 
					
		 driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
	                System.out.println("BrowerName->"+uAgent);  
					  
	                Switch switc = new Switch(driver);
	                
	                Actions act = new Actions(driver);
					   
	                
	                           switc.SwitchCase(uAgent);
					
	                
					  PomObj= new Pom();	
					   
					  WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Ref25());
			        WebElement ele1= driver.findElement(By.cssSelector(PomObj.Ref25()));
			        
			        
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
				((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
					    
				Thread.sleep(3000);
				
			            
			       
			           MoveToElement.byCssSelector(driver, PomObj.Ref25());
			               act.contextClick(ele1).build().perform();
			               		             
			               Thread.sleep(6000);
			                
			          act.sendKeys(Keys.ENTER).build().perform();            
			                
					
					
			          UpperToolBar obj1 = new UpperToolBar(driver);
						     obj1.save_btn_method();  
						     
						     
						     driver.switchTo().defaultContent();
							   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele11= driver.findElement(By.xpath(PomObj.Edit_Option()));  
								   
					 MoveToElement.byXpath(driver,  PomObj.Edit_Option());
								            ele11.click();
								                                                 
								                   Thread.sleep(3000);
								                   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
				WebElement Add= driver.findElement(By.xpath(PomObj.Ce_Utilities()));  
								            	           				            	           
				MoveToElement.byXpath(driver,  PomObj.Ce_Utilities());
								 Add.click();
								            
							Thread.sleep(3000);
		 WebElement element = driver.findElement(By.cssSelector(PomObj.uncited_references()));		                   
								                  
				     JavascriptExecutor jse = (JavascriptExecutor)driver;	
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
								                	
					
			Thread.sleep(3000);
	
	      
	 act.moveToElement(element).click(element).build().perform();
			               
			               
			               String value="";                  
		List<WebElement> element1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));
			           	
			       for(WebElement ele:element1) {
			           	
			                    value = value+ele.getText();
			           	    //System.out.println("Check-->"+value);
			           	    
			           	   }
			           	
			  if(value.contains("Uncited references are listed!")){
			           		
			           		status ="Pass";
			           	 			           	 	           		       
			           }else{
			           	        	 
			           	      status ="Fail";		           	        	 
			           	        	 
			           	   MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			           	        	util.MoveRecFile(RecVideo);	
			           	        	 
			           	        	 
			           	remark="Uncited Reference Pop didn't appear"; 
			           	          
			           	         }
			  
			 WebElement element11 = driver.findElement(By.cssSelector(PomObj.Ok()));
				  			  
			 
				 Thread.sleep(3000);
				         element11.click();				  
					
				         jse.executeScript("arguments[0].scrollIntoView(true);", element);
	
			 WaitFor.presenceOfElementByCSSSelector(driver, PomObj.uncitedrefList());
	WebElement uncitedRefList = driver.findElement(By.cssSelector(PomObj.uncitedrefList()));
	         
			        String value1 = uncitedRefList.getText();
			        
			        //System.out.println("Check ref List-->"+value);
			  
		 if(value1.contains("Ref 25")){
			          	
			        	status ="Pass";
		           	 	
		           	 	
		           		 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		           		       util.deleteRecFile(RecVideo);
		           		       
		          }else{
		           	        	 
		           	      status ="Fail";
		           	        	 
		           	   Thread.sleep(3000);  
		           	   MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		           	        	util.MoveRecFile(RecVideo);	
		           	        	 
		           	         Thread.sleep(3000); 	  	 
		           	 remark="After unlink save Uncited ref didnt displayed"; 
		           	          
		           	         }
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
		        }
                    
		
		               
		          

@Test(alwaysRun = true)	
public void CopyEditorUncited_Ref02() throws Throwable {
		


	try{
		
		  MyScreenRecorder.startRecording(uAgent,className);			
		
		  Uncited_References02 obj = new Uncited_References02();
		               obj.Uncited_Ref();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }finally{				  
		     	    System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
		     }
	   }				
  }
