package com.RightPanel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class Uncited_Table02 extends CUP_BaseClass{
	
	
	static String remark;
	static String className = "Uncited_Table02"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	 public void Uncited_Ref() throws Exception {
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option, then click on CE-UTILITIES then scroll to UNCITED TABLE then click, Check the Uncited table List if Present else Go to table cited, then left click and unlink and save, Check Whether nbr Ref has Added on UNCITED TABLE or not";			    
			area = "Right Panel";
				category = "UNCITED FIGURE";

	 
				
	 try {
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
		            
		      System.out.println("BrowerName->"+uAgent);  
					  
		               Switch switc = new Switch(driver);
					   switc.SwitchCase(uAgent);
					
		            
					  PomObj= new Pom();	
					  
					    
			    WaitFor.presenceOfElementByCSSSelector(driver, PomObj.table1());
		         WebElement ele1= driver.findElement(By.cssSelector(PomObj.table1()));			  
		        
		 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
			((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	  
					  		                 				
			        					    
				                 Thread.sleep(3000);
				
			             Actions act = new Actions(driver);
			        MoveToElement.byCssSelector(driver, PomObj.table1());
			               act.contextClick(ele1).build().perform();
			               		             
			               Thread.sleep(4000);
			                
			          act.sendKeys(Keys.ENTER).build().perform();            
			                
			          
			          UpperToolBar obj1 = new UpperToolBar(driver);
						     obj1.save_btn_method();  
						  
						     
						     driver.switchTo().defaultContent();
							   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele111= driver.findElement(By.xpath(PomObj.Edit_Option()));  
								   
					 MoveToElement.byXpath(driver,  PomObj.Edit_Option());
								            
							Thread.sleep(3000);
							
							ele111.click();		
							
								                   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
				WebElement Add= driver.findElement(By.xpath(PomObj.Ce_Utilities()));  
								            	           				            	           
				MoveToElement.byXpath(driver,  PomObj.Ce_Utilities());
								 Add.click();
								            
							Thread.sleep(3000);
		 WebElement element = driver.findElement(By.cssSelector(PomObj.uncited_Table()));		                   
								                  
				     JavascriptExecutor jse = (JavascriptExecutor)driver;	
			jse.executeScript("arguments[0].scrollIntoView(true);", element);
								                	
			Thread.sleep(3000);
			
			                     
			          act.moveToElement(element).click(element).build().perform();
			               
			   String value="";                  
List<WebElement> element1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));
			           	
			       for(WebElement ele:element1){
			           	
			                    value = value+ele.getText();
			           	    //System.out.println("Check-->"+value);
			           	 }
			           	
			  if(value.contains("Uncited Tables are listed!")){
			           		
			           		status ="Pass";
			           	 			           	 	           		       
			           }else{
			           	        	 
			           	      status ="Fail";		           	        	 
			           	        	 
			           	   MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			           	        	util.MoveRecFile(RecVideo);	
			           	        	 
			           	        	 
			           	remark="Uncited Table Pop didn't appear"; 
			           	          
			           	         }
			  
			 WebElement element11 = driver.findElement(By.cssSelector(PomObj.Ok()));
				  			  
			 
				 Thread.sleep(3000);
				         element11.click();		
				         
				         
     jse.executeScript("arguments[0].scrollIntoView(true);", element);

		WaitFor.presenceOfElementByXpath(driver, PomObj.uncitedTableList());
List<WebElement> uncitedRefList = driver.findElements(By.xpath(PomObj.uncitedTableList()));
		     
String value1="";
for(WebElement ele:uncitedRefList) {
			        value1 = value1+ele.getText();
}
			  
   //System.out.println("Check Figure "+value1);
			  
		 if(value1.contains("Table 1")){
			          	
			        	status ="Pass";
		           	 	
		           	 	
		           		 MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		           		       util.deleteRecFile(RecVideo);
		           		       
		          }else{
		           	        	 
		           	      status ="Fail";
		           	        	 
		           	   Thread.sleep(3000);  
		           	   MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		           	        	util.MoveRecFile(RecVideo);	
		           	        	 
		           	         Thread.sleep(3000); 	  	 
		           	 remark="After unlink save Uncited Figure didnt displayed"; 
		           	          
		           	         }
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		        }
                    
		
		               
		          

@Test(alwaysRun = true)	
public void CopyEditorUncited_Table02() throws Throwable {
		


	try{
		
		  MyScreenRecorder.startRecording(uAgent,className);			
		
		  Uncited_Table02 obj = new Uncited_Table02();
		               obj.Uncited_Ref();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }finally{				  
		     	    System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
		     }
	   }				
  }
