package com.RightPanel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class RightPanelRefCitation06 extends CUP_BaseClass {

	static String remark;
	static String className = "RightPanelRefCitation06";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	@Test(alwaysRun = true)
	public void CitationRef06() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			RightPanelRefCitation06 obj = new RightPanelRefCitation06();
			obj.Citation();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}

	public void Citation() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Casper-CUP_CopyEditor-Click on Right Panal Edit-Option,select the CE-Utilities then click the citation, Go to the Ref section,Select the any ref, then click on \"Prev\" button check the Element has scroll to Prev or not, if not highlight then alert message should be present \"There is no previous citation\"";
		area = "Citaion-Ref";
		category = "Ce-Utilities";

		try {

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			Actions actions = new Actions(driver);

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement Edit_Opti = driver.findElement(By.xpath(PomObj.Edit_Option()));

			actions.moveToElement(Edit_Opti).click().build().perform();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));
			actions.moveToElement(Add).click().build().perform();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			actions.moveToElement(element).click(element).build().perform();

			for (int i = 0; i < 2; i++) {
				Thread.sleep(1000);
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.RefNxt());
				WebElement nxt = driver.findElement(By.cssSelector(PomObj.RefNxt()));
				actions.moveToElement(nxt).click().build().perform();
			}

			Thread.sleep(2000);

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Ref_29());
			WebElement ref29 = driver.findElement(By.cssSelector(PomObj.Ref_29()));
			actions.moveToElement(ref29).click().build().perform();

		


			Thread.sleep(1000);
	
			WaitFor.presenceOfElementByXpath(driver, PomObj.Next());
			WebElement ele1 = driver.findElement(By.xpath(PomObj.Next()));
			actions.moveToElement(ele1).click(ele1).build().perform();

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.ValidationPopUP());
			List<WebElement> el = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));

			String value1 = "";

			for (WebElement ele11 : el) {
				value1 = value1 + ele11.getText();
				System.out.println("-->" + value1);

			}

			WaitFor.presenceOfElementByXpath(driver, PomObj.ClickOK());
			WebElement clickok = driver.findElement(By.xpath(PomObj.ClickOK()));
			      MoveToElement.byclick(driver, clickok);
			      Thread.sleep(5000);
			
			
			      for(int i=0;i<2;i++) {
			         WaitFor.presenceOfElementByXpath(driver, PomObj.Prev());
					 WebElement prev = driver.findElement(By.xpath(PomObj.Prev()));
					 actions.moveToElement(prev).click(prev).build().perform();
			      }
			      
			      
			      driver.switchTo().defaultContent();
					WaitFor.presenceOfElementByCSSSelector(driver, PomObj.ValidationPopUP());
					List<WebElement> el1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));

					String value = "";

					for (WebElement ele11 : el1) {
						value = value + ele11.getText();
						System.out.println("-->" + value);

					}
					
					MoveToElement.byclick(driver, clickok);
			      
			
			switc.SwitchCaselessTime(uAgent);
			WebElement obj = null;
			try {
				obj = driver.findElement(By.xpath(PomObj.Highlight()));

			} catch (Exception e) {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Highlight Xpath class is not Present in the node";
				Assert.assertTrue(false);
			}

			
			
			
			
			
			String   value11 = null;
			for (int i = 0; i < 2; i++) {
				value11 = obj.getAttribute("innerHTML");
			}
			String cssValue = obj.getCssValue("background-color");
			System.out.println("-->" +   value11 + "\n" + "Css_value" + cssValue);
			
		if (value.contains("There is no previous citation.")==true  &&  value11.contains("29") == true && cssValue.contains("255, 140, 0")){

				status = "Pass";
				
				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Element didn't scroll to the actual element/Highlight didn't apper on the element";
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
