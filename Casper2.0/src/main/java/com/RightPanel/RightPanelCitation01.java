package com.RightPanel;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class RightPanelCitation01 extends CUP_BaseClass {

	static String remark;
	static String className = "RightPanelCitation01";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	public void Citation() throws Exception {

		try {
			Ex = new Excel(description, className, remark, category, area, status, uAgent);
			description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option,then move to citation and click, Go to the Table section and click on cited table any number, Verify whether Table citation was move to the correct place and highlight the citation or not";
			area = "Citaion-Table";
			category = "Ce-Utilities";

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele1 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());
			ele1.click();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			Actions act = new Actions(driver);
			act.moveToElement(element).click(element).build().perform();

			Thread.sleep(1000);
			WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2());
			WebElement ele = driver.findElement(By.xpath(PomObj.Table_2()));
			act.moveToElement(ele).click(ele).build().perform();

			switc.SwitchCase(uAgent);

			WebElement obj = null;
			try {
				obj = driver.findElement(By.xpath(PomObj.Highlight()));

			} catch (Exception e) {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Highlight Xpath class is not Present in the node";
				Assert.assertTrue(false);
			}

			String value = obj.getAttribute("innerHTML");
			String Cssvalue = obj.getCssValue("background-color");
			System.out.println("Highlight Attribute-->" + value + "\n" + "Css value-->" + Cssvalue);

			if (value.contains("Table II")==true && Cssvalue.contains("255, 140, 0")==true) {
				status = "Pass";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Highlight is not Present";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test(alwaysRun = true)
	public void CitationFigure01() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			RightPanelCitation01 obj = new RightPanelCitation01();
			obj.Citation();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}
}
