package com.RightPanel;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class Uncited_Figure03 extends CUP_BaseClass {

	static String remark;
	static String className = "Uncited_Figure03";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	public void Uncited_Ref() throws Exception {

		try {

			Ex = new Excel(description, className, remark, category, area, status, uAgent);
			description = "Casper-CUP_CopyEditor-Click on Right Panel Edit-Option go to CE-UTILITIES , click on UNCITED FIGURE, click on Uncited figure and Cross check whether uncited Figure citation is display or not";
			area = "Right Panel";
			category = "UNCITED FIGURE";

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			

			PomObj = new Pom();
					
			
		     switc.SwitchCase(uAgent);
			     
		     
		     Actions act = new Actions(driver);
		     
			/*
			 * WaitFor.presenceOfElementByXpath(driver, PomObj.S20()); WebElement el =
			 * driver.findElement(By.xpath(PomObj.S20()));
			 * 
			 * ((JavascriptExecutor)driver).executeScript(
			 * "arguments[0].scrollIntoView(false);", el);
			 * ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
			 * 
			 * MoveToElement.byXpath(driver, PomObj.S20());
			 * 
			 *  WaitFor.presenceOfElementByXpath(driver,
			 * PomObj.S20()); act.contextClick(el).build().perform();
			 * 
			 * Robot r= new Robot(); Thread.sleep(3000); r.keyPress(KeyEvent.VK_ENTER);
			 */
					
			
			driver.switchTo().defaultContent();

			
			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele11 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());
			ele11.click();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);

			WebElement element = driver.findElement(By.cssSelector(PomObj.uncited_figure()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			
			// act.sendKeys(Keys.PAGE_DOWN).build().perform();
			act.moveToElement(element).click(element).build().perform();

			String value = "";
			List<WebElement> element1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));

			for (WebElement ele : element1) {

				value = value + ele.getText();
			}

			System.out.println("Check-->" + value);

			if (value.contains("Uncited Figure has been listed!")) {

				status = "Pass";

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Uncited Figure Pop didn't appear";
			}

			WebElement element11 = driver.findElement(By.cssSelector(PomObj.Ok()));

			Thread.sleep(3000);

			element11.click();

			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			WaitFor.presenceOfElementByXpath(driver, PomObj.uncitedFigureList());
			List<WebElement> uncitedRefList = driver.findElements(By.xpath(PomObj.uncitedFigureList()));

			String value1 = "";
			for (WebElement ele : uncitedRefList) {
				value1 = value1 + ele.getText();
			}

			System.out.println("Check-->" + value);

			if (value1.contains("Figure 4")) {

				status = "Pass";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				Thread.sleep(3000);
				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				Thread.sleep(3000);
				remark = "After unlink save Uncited Figure didnt displayed";

			}
		} catch (Exception e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(alwaysRun = true)

	public void CopyEditorUncited_Ref03() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			Uncited_Figure03 obj = new Uncited_Figure03();
			obj.Uncited_Ref();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}
}
