package com.RightPanel;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class RightPanelCitation02 extends CUP_BaseClass{

	
	static String remark;
	static String className = "RightPanelCitation02"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	


	 public void Citation() throws Exception {

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
  description = "Casper-CUP_CopyEditor-Go to para select Any Word then left click, mouse hover to link then click,Select the table from drop-down, then select the citation and save Check whether the Edit option Citation Table click cited word has been highlight or not";			    
		area = "Citaion-Table";
			category = "Ce-Utilities";
					
					
	try {
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
		         System.out.println("BrowerName->"+uAgent);  
					  
		          Switch switc = new Switch(driver);
					  switc.SwitchCase(uAgent);
				
					          PomObj= new Pom();	
						
					    Actions actions = new Actions(driver);	
					  
					WaitFor.presenceOfElementByXpath(driver,PomObj.ParaCitationTable());
			      WebElement ele1= driver.findElement(By.xpath(PomObj.ParaCitationTable()));  
						
			  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
			      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
			      
			      
					   MoveToElement.byXpath(driver,PomObj.ParaCitationTable());
					             
					           MoveToElement.byclick(driver, ele1);
			 
			 MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_DOWN);
			 
			 MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
		     
				if(uAgent.equals("chrome")) {
					 MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_UP);
				}
				 
				 MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 6, Keys.ARROW_LEFT);

				   

Robot r = new Robot();
		TimeUnit.SECONDS.sleep(2);
		r.mouseMove(430,350);
		
		   TimeUnit.SECONDS.sleep(2);
		
		 r.mouseRelease(InputEvent.BUTTON3_MASK);	
		 
		 for(int i=0;i<2;i++) {
			   TimeUnit.SECONDS.sleep(2);
		    r.keyPress(KeyEvent.VK_DOWN);	
		        
		     }
		 r.keyRelease(KeyEvent.VK_DOWN);
		 
		 
		 actions.sendKeys(Keys.ENTER).build().perform();
		 
		 driver.switchTo().defaultContent();
		 
		    Thread.sleep(3000);
		    driver.switchTo().frame("iframeinsertlinkeditorcke_1"); 
		   
		    
		    WaitFor.presenceOfElementById(driver,PomObj.linkTypes());
			   Select dropdown = new Select(driver.findElement(By.id(PomObj.linkTypes())));      			                    
			           dropdown.selectByVisibleText("Table");
		    
	
			           
			         WaitFor.presenceOfElementById(driver,PomObj.CitationdropDown());
		 Select dropdown1 = new Select(driver.findElement(By.id(PomObj.CitationdropDown())));      			                    
					           dropdown1.selectByVisibleText("Table II");   
		    
		    
					           driver.switchTo().defaultContent();
					  WaitFor.presenceOfElementByXpath(driver,PomObj.AddLinkSubmit());
				  WebElement ele11= driver.findElement(By.xpath(PomObj.AddLinkSubmit()));	 
							        MoveToElement.byclick(driver, ele11);
							        
							        
							  UpperToolBar obj1 = new UpperToolBar(driver); 
						        	obj1.save_btn_method();
							         
					           
							        
				
	WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
	  WebElement ele111= driver.findElement(By.xpath(PomObj.Edit_Option()));  
										   
		MoveToElement.byXpath(driver,  PomObj.Edit_Option());
					 ele111.click();
										                                                 
				   Thread.sleep(3000);
										                   
	WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
		 WebElement Add= driver.findElement(By.xpath(PomObj.Ce_Utilities()));  
										            	           				            	           
			MoveToElement.byXpath(driver,  PomObj.Ce_Utilities());
					Add.click();
					 
				Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));		                   
										              				                  
					JavascriptExecutor jse = (JavascriptExecutor)driver;	
		   jse.executeScript("arguments[0].scrollIntoView(true);", element);
										              				                	
										   	Thread.sleep(3000);
				
							 Actions act = new Actions(driver);
					act.moveToElement(element).click(element).build().perform();	
					
					 					
					     Thread.sleep(1000);
		      WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2());
			WebElement ele = driver.findElement(By.xpath(PomObj.Table_2()));
			
			for(int i=0;i<2;i++) {
					act.moveToElement(ele).click(ele).build().perform();
			}	        
							        						        
							 switc.SwitchCase(uAgent);
				
			WaitFor.presenceOfElementByXpath(driver,PomObj.EDS());
          WebElement Eds= driver.findElement(By.xpath(PomObj.EDS()));	 
									        										        
          String value1 = Eds.getAttribute("innerHTML");
		  System.out.println("-->" + value1);
	      
   
              
                if(value1.contains("(EDS).")==true){
		               status="Pass";
		               
		               MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		               		   util.deleteRecFile(RecVideo);          
		               
		                        }else{
		                
		             	status="Fail";
		                		                		                
		                MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		                	util.MoveRecFile(RecVideo);	
		                	
		                	
		            remark="Unable to Highlight the element"; 
		       }     
                
                
	        } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	          }
	 
	 }
	 
	 
	
	 
	 
	   @Test(alwaysRun = true)	
	 public void CitationFigure02() throws Throwable {
	 			

	 	 
	 		try{
	 			
	 			    MyScreenRecorder.startRecording(uAgent,className);			
	 			
	 			  RightPanelCitation02 obj = new RightPanelCitation02();
	 			               obj.Citation();
	 		
	 				 }catch (Exception e){
	 				   e.getStackTrace();
	 				       }finally{				  
	 			     	    System.out.println(className);
	 	  Ex.testdata(description, className, remark, category, area, status, uAgent);
	 			     }
	 		    }
	 	  }
	 
