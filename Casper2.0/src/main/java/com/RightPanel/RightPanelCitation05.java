package com.RightPanel;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.SpecificElementScreenShot;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class RightPanelCitation05 extends CUP_BaseClass {

	
	static String remark;
	static String className = "RightPanelCitation05";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	
	
	
	@Test(alwaysRun = true)
	public void CitationFigure04() throws Throwable {

		try {

			  MyScreenRecorder.startRecording(uAgent,className);

			RightPanelCitation05 obj = new RightPanelCitation05();
			obj.Citation();

		} catch (Exception e) {
			  e.getStackTrace();
		       } finally {
		    	   
		String remark = status.equalsIgnoreCase("fail")
					? "Element didn't scroll to the actual element/Highlight didn't apper on the element"
					: "";
			
			           System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}

	
	
	
	
	public void Citation() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option,then move to citation and click, Go to the Table section and click on cited table, then click on \"Prev\" button check the Element has scroll to Prev or not";
		area = "Citaion-Table";
		category = "Ce-Utilities";

		try {
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele111 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());
			ele111.click();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			Actions act = new Actions(driver);
			act.moveToElement(element).click(element).build().perform();
			
			Thread.sleep(1000);
			WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2());
			WebElement ele = driver.findElement(By.xpath(PomObj.Table_2()));
			act.moveToElement(ele).click(ele).build().perform();

			for (int i = 0; i<1; i++) {
				Thread.sleep(1000);
				WaitFor.presenceOfElementByXpath(driver, PomObj.Next());
				WebElement ele1 = driver.findElement(By.xpath(PomObj.Next()));

				act.moveToElement(ele1).click(ele1).build().perform();
			}
			
			        		
			 
			 for (int i = 0; i<2; i++) {
					Thread.sleep(300);
					
					WaitFor.presenceOfElementByXpath(driver, PomObj.Prev());
					WebElement ele1 = driver.findElement(By.xpath(PomObj.Prev()));

					act.moveToElement(ele1).click(ele1).build().perform();
				}
			 
			 
			 
			 switc.SwitchCaselessTime(uAgent);	
		
		WebElement obj = null;
				try {
					obj = driver.findElement(By.xpath(PomObj.Highlight()));
				  
				  } catch (Exception e) {
					
					   status="Fail";
			           
			               MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			           	util.MoveRecFile(RecVideo);	
			           	
			           	
			            remark="Highlight Xpath class is not Present in the node";
			                    Assert.assertTrue(false);
			                    
				}
				
						
				
				String value1 = obj.getAttribute("innerHTML");
                
                System.out.println("-->"+value1);
                
                
        if(value1.contains("Table II")){
               status="Pass";
               
               MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
               		   util.deleteRecFile(RecVideo);          
               
                        }else{
                
             	status="Fail";
                		                		                
                MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
                	util.MoveRecFile(RecVideo);	
                	
                	
            remark="Highlight is not Present"; 
               Assert.assertTrue(false);
       }	
        
        
        
        WaitFor.presenceOfElementByXpath(driver, PomObj.TableII());
		WebElement TableII = driver.findElement(By.xpath(PomObj.TableII()));
				
  SpecificElementScreenShot obj1 =new SpecificElementScreenShot(driver);
		
		if(uAgent.contains("chrome")) {
		       obj1.SpecificElementScrenShot(TableII, uAgent, 315, "TableIICitation");
		       
		       status =    obj1.imageverification("/screenshots/ChromeTableIICitation");
		}
		
		 if(uAgent.contains("firefox")) {
		       obj1.SpecificElementScrenShot(TableII, uAgent, 295, "TableIICitation");	
		       
		       status =    obj1.imageverification("/screenshots/FirefoxTableIICitation");
		 }
		
								    
		 
			 
	 
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}