package com.RightPanel;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class RightPanelFigureCitation06 extends CUP_BaseClass {

	static String remark;
	static String className = "RightPanelFigureCitation06";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	@Test(alwaysRun = true)
	public void CitationFigure06() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			RightPanelFigureCitation06 obj = new RightPanelFigureCitation06();
			obj.Citation();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}

	public void Citation() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option,then move to citation and click, Go to the Figure section and click on cited Figure, then click on \"Prev\" button check the Element has scroll to Prev or not, if not highlight then alert message should be present \"There is no previous citation\"";
		area = "Citaion-Table";
		category = "Ce-Utilities";

		try {
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement ele111 = driver.findElement(By.xpath(PomObj.Edit_Option()));

			MoveToElement.byXpath(driver, PomObj.Edit_Option());
			ele111.click();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));

			MoveToElement.byXpath(driver, PomObj.Ce_Utilities());
			Add.click();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			Actions act = new Actions(driver);
			act.moveToElement(element).click(element).build().perform();

			Thread.sleep(1000);

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Fig_4());
			WebElement ele = driver.findElement(By.cssSelector(PomObj.Fig_4()));
			act.moveToElement(ele).click(ele).build().perform();

			for (int i = 0; i < 3; i++) {
				Thread.sleep(700);
				WaitFor.presenceOfElementByXpath(driver, PomObj.Next());
				WebElement ele1 = driver.findElement(By.xpath(PomObj.Next()));

				act.moveToElement(ele1).click(ele1).build().perform();
			}

			for (int i = 0; i <= 4; i++) {
				Thread.sleep(300);

				WaitFor.presenceOfElementByXpath(driver, PomObj.Prev());
				WebElement ele1 = driver.findElement(By.xpath(PomObj.Prev()));

				act.moveToElement(ele1).click(ele1).build().perform();
			}

			driver.switchTo().defaultContent();
			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.ValidationPopUP());
			List<WebElement> el = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));

			String value = "";

			for (WebElement ele1 : el) {
				value = value + ele1.getText();
				System.out.println("-->" + value);

			}

			WaitFor.presenceOfElementByXpath(driver, PomObj.ClickOK());
			WebElement clickok = driver.findElement(By.xpath(PomObj.ClickOK()));

			if (value.contains("There is no previous citation.") == true) {

				status = "Pass";

				MoveToElement.byclick(driver, clickok);

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Popup didn't apper";

				Assert.assertTrue(false);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
