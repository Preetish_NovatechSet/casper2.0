package com.RightPanel;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class RightPanelRefCitation02 extends CUP_BaseClass {

	static String remark;
	static String className = "RightPanelRefCitation02";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

	@Test(alwaysRun = true)
	public void CitationFigure02() throws Throwable {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			RightPanelRefCitation02 obj = new RightPanelRefCitation02();
			obj.Citation();

		} catch (Exception e) {
			e.getStackTrace();
		} finally {
			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}

	public void Citation() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on para and add one new reference,then go to right panel click Edit option go to CE-Utilities then select the citation and check whether the added Reference is displayed on ref area or not";
		area = "Citaion-Table";
		category = "Ce-Utilities";

		try {

			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			System.out.println("BrowerName->" + uAgent);

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			Actions actions = new Actions(driver);

			WaitFor.presenceOfElementByXpath(driver, PomObj.ParaCitationref());
			WebElement ele1 = driver.findElement(By.xpath(PomObj.ParaCitationref()));

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

			MoveToElement.byXpath(driver, PomObj.ParaCitationref());
			MoveToElement.byclick(driver, ele1);

			Thread.sleep(2000);
			driver.switchTo().defaultContent();

			WaitFor.presenceOfElementByXpath(driver, PomObj.UpperInsertrefBar());
			WebElement upperRefMenuBar = driver.findElement(By.xpath(PomObj.UpperInsertrefBar()));

			MoveToElement.byXpath(driver, PomObj.UpperInsertrefBar());
			MoveToElement.byclick(driver, upperRefMenuBar);

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.RefType());
			WebElement reftyp = driver.findElement(By.cssSelector(PomObj.RefType()));

			MoveToElement.byCssSelector(driver, PomObj.RefType());
			MoveToElement.byclick(driver, reftyp);

			// click on Add Reference

			Thread.sleep(3000);
			WebElement dropdown = driver.findElement(By.id("selectbox"));

			actions.moveToElement(dropdown).build().perform();
			Select sel = new Select(dropdown);
			sel.selectByVisibleText("Journal");

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Year());
			WebElement year = driver.findElement(By.cssSelector(PomObj.Year()));

			actions.moveToElement(year).click().sendKeys("2018").build().perform();

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.SaveRef());
			WebElement save = driver.findElement(By.cssSelector(PomObj.SaveRef()));
			
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", save);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

			actions.moveToElement(save).click().build().perform();

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Ok());
			WebElement okbutton = driver.findElement(By.cssSelector(PomObj.Ok()));
			actions.moveToElement(okbutton).click().build().perform();

			UpperToolBar obj1 = new UpperToolBar(driver);
			obj1.save_btn_method();

			WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			WebElement Edit_Opti = driver.findElement(By.xpath(PomObj.Edit_Option()));

			actions.moveToElement(Edit_Opti).click().build().perform();

			Thread.sleep(3000);

			WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
			WebElement Add = driver.findElement(By.xpath(PomObj.Ce_Utilities()));
			actions.moveToElement(Add).click().build().perform();

			Thread.sleep(3000);
			WebElement element = driver.findElement(By.cssSelector(PomObj.citation()));

			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].scrollIntoView(true);", element);

			Thread.sleep(3000);

			actions.moveToElement(element).click(element).build().perform();

			for (int i = 0; i < 2; i++) {
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.RefNxt());
				WebElement nxt = driver.findElement(By.cssSelector(PomObj.RefNxt()));
				actions.moveToElement(nxt).click().build().perform();
			}

			Thread.sleep(2000);

			WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Ref_29());
			WebElement ref29 = driver.findElement(By.cssSelector(PomObj.Ref_29()));
			actions.moveToElement(ref29).click().build().perform();

			switc.SwitchCaselessTime(uAgent);

			WebElement obj = null;
			try {
				obj = driver.findElement(By.xpath(PomObj.Highlight()));

			} catch (Exception e) {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "Highlight Xpath class is not Present in the node";
				Assert.assertTrue(false);
			}

			String value = obj.getAttribute("innerHTML");
			String cssValue = obj.getCssValue("background-color");
			System.out.println("-->" + value + "\n" + "Css_value" + cssValue);

			if (value.contains("29")==true &&cssValue.contains("255, 140, 0")) {
				status = "Pass";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.deleteRecFile(RecVideo);

			} else {

				status = "Fail";

				MyScreenRecorder.stopRecording();
				String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
				util.MoveRecFile(RecVideo);

				remark = "element add but right pane";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
