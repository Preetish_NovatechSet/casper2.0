package com.RightPanel;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class Uncited_References01 extends CUP_BaseClass{
	

		
		static String remark;
		static String className = "Uncited_References01"; 
		static String category;
		static String area;
		static String description;
	    static String status;
	    public static  Excel Ex;
		Pom PomObj;
	
	
		 public void Uncited_Ref() throws Exception {
			
 Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Click on Right Panal Edit-Option, then click on CE-UTILITIES then scroll to UNCITED REFRERENCES then click, Check the Uncited References List if Present else check the POP is displayed aur not";			    
			area = "Right Panel";
				  category = "UNCITED REFERENCES";
		
	   driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
                System.out.println("BrowerName->"+uAgent);  
				  
                Switch switc = new Switch(driver);
				  switc.SwitchCase(uAgent);
				
                
				  PomObj= new Pom();	
				  
				  
                driver.switchTo().defaultContent();
				   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
             WebElement ele1= driver.findElement(By.xpath(PomObj.Edit_Option()));  
				   
				   MoveToElement.byXpath(driver,  PomObj.Edit_Option());
				                   ele1.click();
				                                                 
				                      Thread.sleep(3000);
				                   
				     WaitFor.presenceOfElementByXpath(driver, PomObj.Ce_Utilities());
				   WebElement Add= driver.findElement(By.xpath(PomObj.Ce_Utilities()));  
				            	           				            	           
				     MoveToElement.byXpath(driver,  PomObj.Ce_Utilities());
				                   Add.click();
				            
				                   Thread.sleep(3000);
	 WebElement element = driver.findElement(By.cssSelector(PomObj.uncited_references()));		                   
				                  
     JavascriptExecutor jse = (JavascriptExecutor)driver;	
	jse.executeScript("arguments[0].scrollIntoView(true);", element);
				                	
	Thread.sleep(3000);

	                 Actions act = new Actions(driver);
	          act.moveToElement(element).click(element).build().perform();
	     
	   String value="";                  
	List<WebElement> element1 = driver.findElements(By.cssSelector(PomObj.ValidationPopUP()));
	
	for(WebElement ele:element1) {
	
         value = value+ele.getText();
	    System.out.println("PKM-->"+value);
	    
	    	     }
	
	if(value.contains("All references are already cited!")){
		
		status ="Pass";
	 	
	 	
		 MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		       util.deleteRecFile(RecVideo);
		       
	         }else{
	        	 
	        	 status ="Fail";
	        	 
	        	 
	   MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	        	util.MoveRecFile(RecVideo);	
	        	 
	        	 
	        	remark="All References are cited Pop didn't apper"; 
	          
	         }
	    } 
				                  
		 
		 
	
		 	 
		 
		 @Test(alwaysRun = true)	
public void CopyEditorUncited_Ref01() throws Throwable {
			

	 
		try{
			
			  MyScreenRecorder.startRecording(uAgent,className);			
			
			  Uncited_References01 obj = new Uncited_References01();
			               obj.Uncited_Ref();
		
				 }catch (Exception e){
					e.getStackTrace();
				 }finally{				  
			     	    System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
			     }
			}				
      }
