package com.FigureContain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import core.CreateNewFile;


public class WritePropertiesFile {
	
	String url=null;


	
	public static void FuntionWritePropertiesFile(String key,String url,String Path) throws InterruptedException {
		try {
			Thread.sleep(12000);
		Properties properties = new Properties();
		properties.setProperty(key,url);
		String scr = Path;
		File file = new File(scr);
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
     }
	
	
	
	
	//TandF
	public static String WriteUrlChromeInPropertiesFile(String url) throws InterruptedException{
		
	String FilePath="./Configfolder/UrlFigureContain/ChromeFigureContainUrl.properties";	
 CreateNewFile.CreateNewPropertiesFile(FilePath);		
WritePropertiesFile.FuntionWritePropertiesFile("ChromeFigureContainUrl", url, FilePath);
		return url;
		
	}
	
	public static String WritePEUrlChromeInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/ChromePEFigureContainUrl.properties";
		CreateNewFile.CreateNewPropertiesFile(FilePath);
WritePropertiesFile.FuntionWritePropertiesFile("ChromePEFigureContainUrl", url, FilePath);
		return url;
		
	}
	
	
	
	
	
	public static String WriteUrlFireFoxInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/FireFoxFigureContainUrl.properties";	
		CreateNewFile.CreateNewPropertiesFile(FilePath);
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxFigureContainUrl", url, FilePath);
		return url;
		
    }
	
	
	
	public static String WritePEUrlFireFoxInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/FireFoxPEFigureContainUrl.properties";	
		CreateNewFile.CreateNewPropertiesFile(FilePath);
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxPEFigureContainUrl", url, FilePath);
		return url;		
		
   }
	
	
			
	public static String WriteUrlIEInPropertiesFile(String url) throws InterruptedException {		
		String FilePath="./Configfolder/UrlFigureContain/IEFigureContainUrl.properties";	
		CreateNewFile.CreateNewPropertiesFile(FilePath);
WritePropertiesFile.FuntionWritePropertiesFile("IEFigureContainUrl", url, FilePath);
		return url;		
	}
	
	public static String WritePEUrlIEInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/IEPEFigureContainUrl.properties";	
		   CreateNewFile.CreateNewPropertiesFile(FilePath);		
WritePropertiesFile.FuntionWritePropertiesFile("IEPEFigureContainUrl", url, FilePath);
		return url;	
}
	
		
	
	
	public static String WriteUrlEdgeInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/EdgeFigureContainUrl.properties";	
		   CreateNewFile.CreateNewPropertiesFile(FilePath);		
WritePropertiesFile.FuntionWritePropertiesFile("EdgeFigureContainUrl", url, FilePath);
	return url;			
}
	
	
	public static String WritePEUrlEdgeInPropertiesFile(String url) throws InterruptedException {
		String FilePath="./Configfolder/UrlFigureContain/EdgePEFigureContainUrl.properties";	
		   CreateNewFile.CreateNewPropertiesFile(FilePath);		
WritePropertiesFile.FuntionWritePropertiesFile("EdgePEFigureContainUrl", url, FilePath);
return url;			
		

	}
	
	
	//Ers
		public static String WriteErsUrlChromeInPropertiesFile(String url) throws InterruptedException {
			
	WritePropertiesFile.FuntionWritePropertiesFile("ErsChrome", url, "./ErsUrl/FigureContain/ChromeFigureContainUrl.properties");
	return url;			
			

		}
		
		public static String WriteErsUrlFirefoxInPropertiesFile(String url) throws InterruptedException {
			
	WritePropertiesFile.FuntionWritePropertiesFile("ErsFirefox", url, "./ErsUrl/FigureContain/FirefoxFigureContainUrl.properties");
	return url;			
			

		}
		
		
		public static String WritePEErsUrlChromeInPropertiesFile(String url) throws InterruptedException {
			
	WritePropertiesFile.FuntionWritePropertiesFile("ErsChrome", url, "./ErsUrl/FigureContain/ChromePEFigureContainUrl.properties");
	return url;			
			

		}
		
		public static String WritePEErsUrlFirefoxInPropertiesFile(String url) throws InterruptedException {
			
	WritePropertiesFile.FuntionWritePropertiesFile("ErsFirefox", url, "./ErsUrl/FigureContain/FirefoxPEFigureContainUrl.properties");
	return url;			
			

		}
		
		
		//PPL
		
		public static String WritePPlUrlChrome(String url) throws InterruptedException {
			String path ="./Configfolder/PPLUrl/CasperChromeUrl.properties";
			   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperPPLChrome", url, path);		
			return url;			
		   }
		
		
		public static String WritePPlUrlFirefox(String url) throws InterruptedException {
			String path ="./Configfolder/PPLUrl/CasperFirefoxUrl.properties";
			   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperPPLfirefox", url, path);		
			return url;			
		   }		
		
/***********************************************************************************************/
	
//CUP-CopyEditor chrome

public static String WriteCUPUrlChrome(String url) throws InterruptedException {
	String path ="./Configfolder/CUPUrl/CasperChromeUrl.properties";
	   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperCUPChrome", url, path);		
	return url;			
   }



//CUP-CopyEditor Firefox

public static String WriteCUPUrlFirefox(String url) throws InterruptedException {
	String path ="./Configfolder/CUPUrl/CasperFirefoxUrl.properties";
	   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperCUPfirefox", url, path);		
	return url;			
   }	



/********************************************************************************************/



//CUP-AuthorEnd

public static String WriteCUPAEUrlChrome(String url) throws InterruptedException {
	String path ="./Configfolder/CUPUrl/CasperAEChromeUrl.properties";
	   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperCUPChrome", url, path);		
	return url;			
}


public static String WriteCUPAEUrlFirefox(String url) throws InterruptedException {
	String path ="./Configfolder/CUPUrl/CasperAEFirefoxUrl.properties";
	   CreateNewFile.CreateNewPropertiesFile(path);
WritePropertiesFile.FuntionWritePropertiesFile("CasperCUPfirefox", url, path);		
	return url;			
   }

}
	
