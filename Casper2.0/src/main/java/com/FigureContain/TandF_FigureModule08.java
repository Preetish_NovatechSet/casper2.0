package com.FigureContain;



/********
 *Preetish*
  ********/


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class TandF_FigureModule08 extends TandFBaseClass{

	static WebElement ele=null;
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
		
    
	/*********
	 *8. Single word select and use backspace key for delete, check tracking and data lose
	 *********/

	public  void selectSingle_word_bckspace() throws Exception {
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	  description = "Casper-TandF_AuthorEnd-Single word select and use backspace key for delete, check tracking and data lose";
		    className = "TandF_FigureModule08";    
		         area = "Figure Contain";
		           category = "Select and delete";
		             		    
	try{
		   driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);      
                  System.out.println("BrowerName->"+uAgent);
                  
            
                  Cookies cokies =new Cookies(driver);
		             cokies.cookies(); 
                  
                  
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent);
		
		PomObj= new Pom();
		   
		WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
		
         ele=driver.findElement(By.xpath(PomObj.figure2()));
        
        
        String value = ele.getText(); 	
        
			if(value.contains(value)) {
				
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		    	
     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)"); 


MoveToElement.byXpath(driver, PomObj.figure2());
								
				WaitFor.clickableOfElementByXpath(driver, PomObj.figure2());
				
		for(int i=0;i<3;i++) {		         
				ele.click();
				}	 
		
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	
	for(int i=0;i<10;i++) {
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
	}
															
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
											
	MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 9,Keys.ARROW_LEFT);
				
	  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
							
		UpperToolBar ob = new UpperToolBar(driver);
		  	   ob.save_btn_method();

		  	   
		   }	
		
			Thread.sleep(3000);
			
		    
                    
         Switch switc1 = new Switch(driver);
            switc1.SwitchCase(uAgent);
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
			
	      WebElement ele1=driver.findElement(By.xpath(PomObj.figure2()));
				  
				    String afterSave =  ele1.getAttribute("innerHTML");		    
					String expectedValue1 = "procedure</del>";
					String expectedValue2 = "ice-del ice-cts";			

				 System.out.println("After save - " +afterSave);

	if(afterSave.contains(expectedValue1)&&afterSave.contains(expectedValue2)){

					 status="Pass";
				
				 }else {
					 
	//	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);	 
					 					 					 				 		 
			 	     status = "Fail";
	remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
	
	Thread.sleep(10000);
	
	       utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				
				 if(afterSave.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				  if(afterSave.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	}		   
				 }
		    }catch (Exception e){
		    	
				e.getStackTrace();
	        	}finally {
	        		System.out.println(className);
	    	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	        	}
		   }
	
	
	@Parameters({ "browser" })	
	   @Test(alwaysRun = true)
	
		public void test8() throws Throwable {
		

	try{
				 		 			 				 					
			TandF_FigureModule08 obj = new TandF_FigureModule08();
					obj.selectSingle_word_bckspace();			
					
				}catch (Exception e) {
					e.getStackTrace();
				               }
				         }
			       }
	

