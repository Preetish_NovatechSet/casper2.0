package com.FigureContain;



import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_FigureCaption07 extends CUP_AuthorEnd_BaseCalss{

	
	
	static WebElement ele=null;
	static String remark;
	static String className = "CUP_AuthorEnd_FigureCaption07";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
	
	
	/*****
	 *7. Single word select and use delete key for delete, 
	 *      check tracking and data lose
	 * @throws Exception 
	 *****/




public  void selectSingle_word_delete() throws Exception {
	
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "Casper-CUP_AuthorEditor-Single word select and use delete key for delete, check tracking and data lose";    
	            area = "Figure Contain";
	                category = "Select and delete";
		                				
		   try {
			   
                  System.out.println("BrowerName->"+uAgent);
                                                                        
			           Switch switc = new Switch(driver);
			           
			           CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
			                   C1_C2.CasperCUP(uAgent); 
			           
			              switc.SwitchCase(uAgent);
                        
			   
			   PomObj= new Pom();
			   
				WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
				
		       ele=driver.findElement(By.xpath(PomObj.figure1()));
					
		        driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS); 
		        
		        String value = ele.getText();
					    
					
					if(value.contains(value)){
						 
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		
 
	      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
						
						MoveToElement.byXpath(driver, PomObj.figure1());
		
		for(int i=0;i<2;i++) {
				             ele.click();
						}  
		
		
		for(int i=0;i<2;i++){			  
	  		 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
	  			  }
		
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				      			
			
			    	for(int i=0;i<54;i++){
			    		   
			    		if(uAgent.contains("IE")) {
				    			Thread.sleep(300);
				    			}
			    		
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
			    	}
																
			MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 11, Keys.ARROW_RIGHT);
					
			      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
						
					//driver.switchTo().defaultContent();
					
			 UpperToolBar ob = new UpperToolBar(driver);
		 	  	   ob.save_btn_method();
			 	  	   			 	 
					    }
					}catch (Exception e){
	   					e.printStackTrace();
	    	       }
		
		
		//Figure 3 Effect						
		   try
		      {
			   Thread.sleep(3000);
				        
	                      
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent);
				
			   			   
		   	WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
		                  
		   	WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
		
			
		    String afterSave =  ele1.getAttribute("innerHTML");
			 
			String expectedValue1 = "diffraction</del>";
			String expectedValue2 = "ice-del ice-cts";

		 System.out.println("After save - " + afterSave );

 if(afterSave .contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true){
				 
				status="Pass";
				 	
				 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				 					util.deleteRecFile(RecVideo);
				
				 }else {			
					    
	//	 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
			 		 
		 
		 status = "Fail";
		 
		    MyScreenRecorder.stopRecording();
		 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		 							util.MoveRecFile(RecVideo);
		
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
	       
	 Thread.sleep(12000);
	 
			 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				
				 	if(afterSave .contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				    if(afterSave .contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	}
			     }
	        }catch (Exception e) {
	        	e.getStackTrace();
	           }finally {
	        	   System.out.println(className);
	       Ex.testdata(description, className, remark, category, area, status, uAgent);
	           }
		   }
	
		

	 @Test(alwaysRun = true)
		public void CUPtest7() throws Throwable {
		

	try{
		 MyScreenRecorder.startRecording(uAgent,className);		 	
		
		 CUP_AuthorEnd_FigureCaption07 obj = new CUP_AuthorEnd_FigureCaption07();
					obj.selectSingle_word_delete();
					
								
				}catch (Exception e) {
					e.getStackTrace();
				        }
				  }
	        }