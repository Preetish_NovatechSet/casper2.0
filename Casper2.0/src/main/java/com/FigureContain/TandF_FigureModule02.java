package com.FigureContain;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;





public class TandF_FigureModule02 extends TandFBaseClass{
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
    
    
	
	
	/********
	*2. Delete one character with delete key press and insert special character, 
	*	         tracking should be present
	*********/
	
	
	
public void  delete_oneChar_WithdelKeyAnd_insSplChar() throws Exception {
		
		
		try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
  description = "Casper-TandF_AuthorEnd-Delete one character with delete key press and insert special character, tracking should be present";
		className = "TandF_FigureModule02";    
			  area = "Figure Contain";
			      category = "insert and deletion";
			      
		
				
			       driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			          
			              System.out.println("BrowerName->"+uAgent);	
			              
			              Cookies cokies =new Cookies(driver);
				             cokies.cookies();
			              
	            Switch switc = new Switch(driver);
	                switc.SwitchCase(uAgent);           
			              
			              		              
			           PomObj= new Pom();	
			                 
	WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	
			    WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
			    
	
  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
         ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
		        	

   
  String value = ele.getText();
		        	
						if(value.contains(value)) 
						 {    	
					    
					   MoveToElement.byXpath(driver, PomObj.figure1());
					   
					   for(int i=0;i<3;i++) {					    	
					         ele.click();
					   }
					   
	         MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	
	
					for(int i=0;i<57;i++) {
					    	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
					    }
					    
					
			   MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
					         
					  	    
					  SymbolActionFuction obj = new SymbolActionFuction(driver);
				               obj.SymbolTest();
					  	
					  	  
					UpperToolBar obj1 = new UpperToolBar(driver);
					  	   obj1.save_btn_method();
					  		
					  	 				  	 
					         }
						  } catch (Exception e) {
						    e.printStackTrace();
						  }
		try {	
			Thread.sleep(3000);
            
   Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
						
				 WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
		    		        
				WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));

				  
						
					     String actualValue =  ele1.getAttribute("innerHTML");
						 String expectedValue1 = "e</del>";
						 String expectedValue2 = "ice-del ice-cts";
						 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
						 String expectedValue4="ice-ins ice-cts";
				
			
				 
				 System.out.println("After save - " + actualValue);
				 
				 

if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){

			status = "Pass";
						 
				 }else {
				
					 
	//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);					 
					
							    
		
					  
			 			status="Fail";
		remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
			 			
			 Thread.sleep(10000);
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				   	  
				   	  
				    if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	}
				    if(actualValue.contains(expectedValue3)==true) {
				 	   System.out.println("E3: No issues");	
				   }
				    if(actualValue.contains(expectedValue4)==true) {
					 	 System.out.println("E4: No issues");		 	 
					   }
			        }
		       }catch (Exception e){
		    	   e.getStackTrace();
		         }finally {
		        	 System.out.println(className);
		    Ex.testdata(description, className, remark, category, area, status, uAgent);
		       }
           }
	
	
	
	
@Test(alwaysRun = true)

	public void test2() throws Throwable {

        try {					 
	           
			TandF_FigureModule02 obj = new TandF_FigureModule02();
			     obj.delete_oneChar_WithdelKeyAnd_insSplChar();
			     
			     
		    }catch (Exception e){
		    	  
			e.getStackTrace();
	    	  }
    	   }
       }