package com.FigureContain;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUP_AuthorEnd_FigureCaption09 extends CUP_AuthorEnd_BaseCalss{

	    static WebElement ele =null;
		static String remark;
		static String className = "CUP_AuthorEnd_FigureCaption09";
		static String category;
		static String area;
		static String description;
	    static String status;
	    public static  Excel Ex;	
	    Pom PomObj;
		
		/*****
		 **9. Single word select and "Control + C" then "Control + v" in next next word, check tracking and data lose
		 * @throws Exception 
		 *****/
		
		
		public void SelectSingle_Word_CopyPaste_nxtword() throws Exception {
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "Casper-CUP_AuthorEditor-Single word select and Control + C then Control + v in next next word, check tracking and data lose";		      
			   area = "Figure Contain";
			        category = "Copy and Paste";
			                  
		
			     try {
			    	 
			  System.out.println("BrowerName->"+uAgent);	
			  			
	                  
	       Switch switc = new Switch(driver);
	       
	       CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                    C1_C2.CasperCUP(uAgent); 
           
	          switc.SwitchCase(uAgent);
			    	 
			    	 PomObj= new Pom();
					   
			 WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
			 		
			      ele=driver.findElement(By.xpath(PomObj.figure1()));
					
			     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 

			     ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
			
					
			     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
						    								
				
				for(int i =0;i<2;i++) {	
								ele.click();
				 }					
				
				
				for(int i=0;i<2;i++){			  
			  		 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
			  	 }
				
						  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
								
						    	
						    		 for(int i =0;i<27;i++) {
						    			 
						     if(uAgent.contains("IE")) {
								    Thread.sleep(300);
								    }   			 
						    			 
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
						    		 }
												
						
				MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 7,Keys.ARROW_LEFT);
							
				  
			if(uAgent.equals("IE")){	
				 
				 Robot robot = new Robot();
			     robot.keyPress(KeyEvent.VK_CONTROL);
			     robot.keyPress(KeyEvent.VK_C); 
			     robot.keyRelease(KeyEvent.VK_C); 
			     robot.keyRelease(KeyEvent.VK_CONTROL); 
			}else{
				
				   MoveToElement.copy(driver);
				
			}
				
						     
								 for(int i =0;i<12;i++) {
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);  
								    }
								    	
								 if(uAgent.equals("IE")){		
									
			/*	((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");	
				             		
				           core.HandleAlert.isAlertPresentAccept(driver);	*/	
									 
									 
									 Robot robot = new Robot();
								     robot.keyPress(KeyEvent.VK_CONTROL);
								     robot.keyPress(KeyEvent.VK_V); 
								     robot.keyRelease(KeyEvent.VK_V); 
								     robot.keyRelease(KeyEvent.VK_CONTROL); 				 
									 
									 
								 }else{     	
					  MoveToElement.paste(driver);
								 }
								 
								 
								 
							   UpperToolBar ob = new UpperToolBar(driver);  
							               ob.save_btn_method();
							              
			      }catch(Exception e){
			    	 
			    	 e.getStackTrace(); 
			     }
			    	 

	try {
		
		Thread.sleep(3000);  
	            
	 Switch switc = new Switch(driver);
	    switc.SwitchCase(uAgent);
		
					 WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
					 		
					WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
				
				
					     String actualValue =  ele1.getAttribute("innerHTML");
						    String expectedValue1 = "showing</ins>";
						 		 

				 System.out.println("After save - " + actualValue);


			if(actualValue.contains(expectedValue1)==true){
				
				status="Pass";
				
				 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				 		 util.deleteRecFile(RecVideo);
				
				 }else {
					 
		//	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
					 									    					 					 		 
			 	     status = "Fail";		 	     
			 	     
			 	    MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			 	   		util.MoveRecFile(RecVideo);
			 	    
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	 
			 	    
			 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				 		
			
				  if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	   }		  	 
				   }    
	          }catch(Exception e){
			 		
					    e.getStackTrace();
					    
				      }finally {
				    	  System.out.println(className);
	        Ex.testdata(description, className, remark, category, area, status, uAgent);
		         }
			 }
			
		
		
		   @Test(alwaysRun = true)
		
			public void CUPtest09() throws Throwable {
			

		try{
			
			MyScreenRecorder.startRecording(uAgent,className);	
					 		 			 		
			CUP_AuthorEnd_FigureCaption09 obj = new CUP_AuthorEnd_FigureCaption09();
						obj.SelectSingle_Word_CopyPaste_nxtword();
					
					}catch (Exception e) {
						e.getStackTrace();
						
					              }
		                 }
		        }
