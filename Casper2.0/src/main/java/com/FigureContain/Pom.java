package com.FigureContain;




public class Pom {

	
	 private static String element = null;
//	 private static List<WebElement> ele = null;

	 
	 
	 
	 
	 public String EqLogo(){
			
			element = "//*[@equation-mode='inserted']";		
	     return element;
	}
	 
	 public String DelEQ(){
			
			element = "//input[@id='btn-del-equation']";		
	     return element;
	} 
	
	 
	 
	 public String Insertbotton(){
			
			element = ".//*[@class='cke_dialog_ui_hbox_first']//following::*[contains(text(),'Insert')]";		
	     return element;
	}
	 
	 
	 public String Ersfigure3(){
			
			element = "//div[@id='F3']//span[@class='p']";		
	     return element;
	}
	
	 
	 public String Ersfigure5(){
			
			element = "//div[@id='F5']//span[@class='p']";		
	     return element;
	}
	 
	 public String Q1(){
			
			element = "//a[contains(text(),'[Q1]')]";		
return element;
	}
	 
	 public String Ersfigure4(){
			
			element = "//div[@id='F4']//span[@class='p']";		
	     return element;
	}

	 
	 public String Comment() {
		 
	
		 element="//span[contains(text(),'Comment')]";		 
		       return element;
	 }
	 
	 
	 public String SumbitDilogBox() {
		 element="//*[@class='cke_dialog_ui_button']";
		 return element;
	 }
	 
	 
	 
	 public String EditorEditor() {
	 
		 element="#commentBoxDiv";
	 
	 return element;
}

public String Letícia_Marin() {
	element = "//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[1]/span/span[1]";
      return element;
}
	 
	 public String ValidationFig1() {
	
		 element="//*[@id='F0001']/p/span[2]";
		 
	 return element;
	}
	
	 public String DeleteFigValidation() {
			
		  element="//*[@class='ice-del ice-cts']";
		 
	 return element;
	}
	
	 public String DeleteFig1Cup() {
			
		  element="//span[contains(text(),'We can tell the tool to wait only till the Conditi')]";
		 
	 return element;
	}
	
	 	 
	 public String CommentVal() {
		 
	      element="//*[@class='comment hover']";
	
	 return element;
	 }
	
	 
	 
	 public String cookies() {
		 
		 element="//*[@id='cookie-gotIt']";
				 return element;
	 }
	 
	 
	 public String AQ1(){
			
			element = "//a[contains(text(),'[AQ1]')]";		
      return element;
	}
	 
	 
	 public String EditAQ(){
		 
		 
		 element ="//div[@class='checkbox']//input[@id='changeCheck']";
		 return element;
	 }
	 
	 
 public String AQnxtBottom(){
		 		 
		 element =".//*[@id='btn_aq_next']";
		 return element;
	 
 }
 
 public String EDS(){
	 //element =	"//a[contains(text(),'JSM-840A')]";
	 element ="//a[contains(text(),'(EDS).')]";
	 return element;
 
}
 
 public String xrefDinamicValue(){

	 element ="//a[@id='xref']";
	 return element;
 
}
 public String FigureIV(){

	 element ="//a[contains(text(),'Figure 4(c)')]";
	 return element;
 
}
 
 
 public String Figure2(){

	 element ="//a[contains(text(),'Figure 4(c)')]";
	 return element;
 
}

 
 public String S20(){
	 
	 element ="//a[contains(text(),'20 s.')]";
	 return element;
 
}

 public String TableII(){
	 
	 element ="//a[contains(text(),'Table II')]";
	 return element;
  }

 public String AuthorName2(){
		element ="//*[text()='Pihlaja']";
		  return element;
	}
 
 public String AuthorName4(){
	 element ="//*[text()='Maier']";
	    return element;
 }

 
 public String AddAffiliationCitationdropDown() {
		element="#aff_list";
		return element;
	}
 
 public String RemoveAffiliationCitationdropDown() {
		element="#delete_aff_list";
		return element;
	}

 public String Aff_DeleteButtom() {
		element="//input[@value='Delete']";
		return element;
	}


 public String AQAll(){
	 	 
	 element ="//*[@class='show']";
	 return element;
 
}

 public String btn_aq_save(){
	 element ="//button[text()='SAVE & SUBMIT']|//button[@id='btn_aq_save']";
	 return element;
 }
 
 public String Yes_Submit(){
	 element ="//*[text()='Yes, Submit!']";
	 return element;
 }
 
 
 public String Submit(){
	 element ="//span[@class='cke_button_icon cke_button__submit_icon']";
	 return element;
 }
 
 
 public String AddLinkSubmit(){
	 element ="//span[@class='cke_dialog_ui_button'][contains(text(),'Submit')] ";
	 return element;
 }
 
 
 public String EditorLink(){
	 
	 element ="//a[text()='Editor Link']";
	 return element;
 }
 
	 
	public String figure1(){
	
			element = "//div[@id='F0001']//span[@class='p']";		
         return element;
	}
	
	public String Edit_Option(){
		
		element = "//p[contains(text(),'Edit Option')]";		
     return element;
     
}
	
public String ParaCitationTable(){
		
		element = "//*[@sec-type='materialsandmethods']/div[3]/div[3]";		
     return element;
     
}


public String ParaCitationfigure(){
	
	element = "//*[@sec-type='materialsandmethods']/div[3]/div[4]";		
 return element;
 
}


public String RefType(){
	
	element = "[id='selectbox']";		
 return element;
 
}

public String Ref_Position(){
	
	element = "[id='ref_num_insert']";		
 return element;
 
}

public String Year(){
	
	element = "[name='year_text']";		
 return element;
 
}

public String SaveRef(){
	
	element = "[onclick='save_exe_reference()']";		
 return element;
 
}

public String RefNxt(){
	
	element = "[class='page-link next']";		
 return element;
 
}



public String ParaCitationref(){
	
	element = "//div[contains(text(),'shows the results of the viability tests performed')]";		
 return element;
 
}


public String UpperInsertrefBar(){
	
	element = "//a[@title='Insert Reference']";		
 return element;
 
}



public String linkTypes(){
	
	element = "drpTypes";		
 return element;
 
}

public String CitationdropDown(){
	
	element = "drpLink";		
 return element;
 
}


public String DropDowncitation(){
	
	element = "//select[@id='drpLink']";		
 return element;
 
}

	
public String LeftPanelAdd(){
		
		element = "//a[@data-toggle='collapse'][contains(text(),'Add')]";		
     return element;
     
}
	

public String Ce_Utilities(){
	
	element = "//a[contains(text(),'Ce-Utilities')]";		
 return element;
 
}

public String citation(){
	
	element = "[for='check_element_Citation']";		
 return element;
 
}

public String Highlight(){
	
	element = "//*[@class='xref ElementHighlighter']";		
 return element;
 
}


public String HighlightLink(){
	
	element = "//*[@class='link ElementHighlighter']";		
 return element;
 
}


public String Fig_1(){
	
	element = "#titleIndexfigures1";		
 return element;
 
}


public String Ref_1(){
	
	element = "bbref#ref1";		
 return element;
 
}
  
public String Ref_29(){
	
	element = "bbref#ref29";		
 return element;
 
}

public String Fig_4(){
	
	element = "#titleIndexfigures4";		
 return element;
 
}

public String Fig_2(){
	
	element = "#titleIndexfigures2";		
 return element;
 
}


public String Table_1(){
	
	element = "//label[contains(text(),'Tab.. 1')]";		
 return element;
 
}

public String Table_2(){
	
	element = "//label[contains(text(),'Tab.. 2')]";		
 return element;
 
}

public String Next(){
	
	element = "//span[@class='dynamicBtn']//a[@class='btn btn-warining caspBtn'][contains(text(),'Next')]";		
 return element;
 
}

public String Prev(){
	
	element = "//span[@class='dynamicBtn']//a[@class='btn btn-warining caspBtn'][contains(text(),'Prev')]";		
 return element;
 
}




public String CitatedTable_1(){
	
	element = "[currentindex='titleIndexTables01_01']";		
 return element;
 
}



public String citaTable1(){
	
	element = "[href='#T0001']";		
 return element;
 
}



public String ValidationPopUP(){
	
	element = "[class='sweet-alert showSweetAlert visible']";		
 return element;
 
}

public String ValidationCitationPopup(){
	
	element = "[class='sweet-alert visible showSweetAlert']";		
 return element;
 
}


public String uncited_references(){
	
	element = "#id_uncited_references";		
 return element;
 
}

public String uncited_figure(){
	
	element = "#check_element_Figlist";		
 return element;
 
}

public String uncited_Table(){
	
	element = "#check_element_Tbllist";		
 return element;
 
}

public String uncitedrefList(){
	
	element = "[itemtype='UncitedRef']";		
 return element;
 
}

public String uncitedFigureList(){
	
	element = "//div[@id='FigPanel']/mref/p";		
 return element;
 
}

public String uncitedTableList(){
	
	element = "//*[@id='TblPanel']/nref/p";		
 return element;
 
}



public String Ok(){
	
	element = "[class='confirm']";		
 return element;
 
}

public String Ref25(){
	
	element = "[href='#ref25']";		
 return element;
 
}


public String refFig(){
	
	element = "[href='#F0004']";		
 return element;
 
}

public String refFig1(){
	
	element = "[href='#F0001']";		
 return element;
 
}

public String fig4(){
	
	element = "//a[contains(text(),'4(b)')]";		
 return element;
 
}

public String fig1a(){
	
	element = "//a[contains(text(),'4(b)')]";		
 return element;
 
}

public String table1(){
	
	element = "[href='#T0001']";		
 return element;
 
}


public String ListtheEquationType(){
	
	element = "//div/label";		
 return element;
 
}


public String Figurecited(){
	
	element = "//a[contains(text(),'Figure 1(e)')]";		
 return element;
}

	
public String Equation(){
		
		element = "//a[contains(text(),'Equation')]";		
     return element;
}
	

public String InlineEquation(){
	
	element = "//label[contains(text(),'Inline Equation')]";		
 return element;
}


public String MathFormula1(){
	
	element = "//*[@class='get-latex sqrtn']";		
 return element;
}

public String MathFormula2(){
	
	element = "//*[@class='get-latex fracB']";		
 return element;
}


public String MathMultiply(){
	
	element = "//*[@class='get-latex times']";		
 return element;
}

public String MathLims(){
	
	element = "//*[@class='get-latex lim']";		
 return element;
}

	
public String Para(){
		
		element = "//*[@xmltag='body']/div[2]/div[1]/div[1]";		
     return element;
}
	
	
	
public String Add(){
		
		element = "//div[3]//div[1]//h4[1]//a[1]";		
     return element;
}
	

public String Addfigure(){
	
	element = "//div[@id='edited-para']//ul[@class='edito-option']//li[3]//a[1]";		
 return element;
}


public String UploadImage(){
	
	element = "//input[@id='FigBrowse']";		
 return element;
}




public String CaptionText(){
	
	 element = "//*[@id='txt_fig_caption']";		
 return element;
}

public String AddSaveCaption(){
	
	element = "//div[@class='modal-dialog']//input[@value='Save']";		
 return element;
}

public String ClickOK(){
	
	element = "//button[@class='confirm']";		
 return element;
}



public String ValidationPop(){
	
	element = "//*[@class='sweet-alert showSweetAlert visible']";		
 return element;
}


public String Image_validation() {
	
	element="//img[@content-type='black-white']";
	return element;
}

public String Imagefig1() {
	
	element="//img[@id='F0001']";
	return element;
}





	public String figure2() {
	
			element = ".//*[@id='F0002']/p/span[2]";
	     return element;
	}
	
	
	public String figure3() {
	
			element = ".//*[@id='F0003']/p/span[2]";	
	     return element;
	}
	
	public String pplfigure3() {
		
		element = ".//*[@id='F0003']/p/span[3]";	
     return element;
}
	
public String pplfigure5() {
		
		element = ".//span[contains(text(),'The thermal denaturation curve of WT, V1, V2, V3 a')]";	
     return element;
}

public String pplfigure6() {
	
	element = "#F0006 > p > span.p";	
 return element;
}

	
	public String figure4() {
		
			element = ".//*[@id='F0004']/p/span[2]";	
	     return element;
	}
	
	
	
	public String figure5() {
		
			element = ".//*[@id='F0005']/p/span[2]";	
	     return element;
	}
	
	
	public String click_On_Symbol(){
	
			element = ".//*[@class='cke_button_icon cke_button__symbol_icon']";			
	     return element;
	} 	
	
	
	public String click_On_Letter(){
		
			element = ".//*[@class='cke_dialog_ui_input_select']/select";
	     return element;
	}
	
	public String click_On_Splchar(){
		element = "[aria-posinset='224']";
	     return element;
	}
	
	
public String Affilation_saveButton() {		
		element = "//div[@id='Div1']//div//input[@value='Save']";
	     return element;			
}

public String Save_Equation() {		
	element = "//input[@id='btn-equation']";
     return element;			
}

	
public String save_btn_method() {		
			element = ".//*[@title='Save (Ctrl+S)']";
		     return element;
				
    }
	
public String AuthorName() {		
	element = ".//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[1]/span[2]/span[1]";
	  return element;
			
}	


public String LetÃ­cia_Marin() {
	element = "//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[1]/span/span[1]";
      return element;
}


	
public String GivenTextBox(){
		element="#up_contrib_name";
		  return element;
	}
	
	
	public String SurnameTextBox(){
		element="#up_contrib_surname";
		  return element;
	}
	
	
	public String AuthorName1(){
		element="//*[@class='name']//following::*[contains(text(),'Sevcan')]|//*[text()='Hakyemez-Paul']";
		  return element;
	}
	
	public String Carlo(){
		element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[2]/span[2]/span[1]";
		  return element;
	}
	
	
	public String PPLAuthorName1(){
		element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[2]/span/span[1]";
		  return element;
	}
	

	public String AuthornameTrack() {
		element="//*[@class='name nova-edited']/ins";
          return element;
	}
	
	
	
	public String Save(){
		element="#up_contrib_more > div:nth-child(6) > input";
		  return element;
	}
	
	
	public String CasperUpdate(){
		element="//*[@id='btnAuthorName']";
		  return element;
	}
	

	
	
	
	public String givenNameTrc(){
		element=".given-names";
		  return element;
	}
	
	
	
	public String SurNameTrc(){
		element=".surname";
		  return element;
	}
	
	
	public String PPLFigure(){
		element=".surname";
		  return element;
	}
	
	public String AuthorNameSahar(){
		element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[1]/span/span[1]";
		  return element;
	}

	public String CasperTable2() {
		element="//div[@id='T0002']//p[@class='caption']";
		  return element;
		
	}
	
	public String B3() {
		element="//td[contains(text(),'B3')]";
		  return element;
		
	}
	
	public String Para1() {
		element="//*[@id='S001']/div[3]";
		  return element;
		
	}
	
	
}