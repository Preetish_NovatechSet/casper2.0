package com.FigureContain;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.page.SymbolActionFuction;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_FigureContain03 extends PPL_BaseClass{
	
	
	
	
	/*
	public TandF_FigureModule03(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	/****** 
	* 3. Insert special character and press delete key, tracking should be present
	*******/ 
		
	public void insert_spl_chr_del() throws Exception {
		
		
		
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-PPL_AuthorEnd-Insert special character and press delete key, tracking should be present";
		className = "PPL_FigureModule03";    
		     area = "Figure Caption";
			     category = "insert and deletion";
			           
			     System.out.println("BrowerName->"+uAgent);       
			         
			     Cookies cokies =new Cookies(driver);
	                  cokies.cookies();
                
                  Switch switc = new Switch(driver);
                      switc.SwitchCase(uAgent);
			     
			     
			     
			            PomObj= new Pom();
			    
try {
		         	
	         WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
			WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
		
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
			 			
	
			 			
			 	String value = ele.getText();
			 
			 	
			 	if(value.contains(value)) 
			 	
			 {			
			 		
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	 		
		 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		 
				
				
		MoveToElement.byXpath(driver,PomObj.figure1());
			 		
		  for(int i=0;i<3;i++){
				          ele.click();
		     }
		  
		  for(int i=0;i<4;i++) {			  
 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
	     }
		  
			 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
			 			 	
			 
			 	for(int i=0;i<40;i++) {
			 		 Thread.sleep(300);
			  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			     }

			 	
		SymbolActionFuction obj = new SymbolActionFuction(driver);
	                  obj.SymbolTest();
			      	              	                  
	                  switc.SwitchCase(uAgent); 
	                  	                                   
	               WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	             WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
	      			
	                             
switch(uAgent) {

	  case "edge":
	             	     
		  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT);             	  
	             break;
	  }
	             
	               
		 MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);
	                		 		   
			 		   
		 		UpperToolBar ob = new UpperToolBar(driver);
		 		              ob.save_btn_method();
		 		              
			 			  }} catch (Exception e) {
			 				  
							e.printStackTrace();
				           }
			 	
try {	
	
	
	Thread.sleep(3000);
	   
Switch switc1 = new Switch(driver);
     switc1.SwitchCase(uAgent); 			
			 									 
			WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
				    		        
					WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));

						  							
							     String afterSave =  ele1.getAttribute("innerHTML");
								 String expectedValue1 = "s</del>";
								 String expectedValue2 = "ice-del ice-cts";
								 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
								 String expectedValue4="ice-ins ice-cts";
				
								 
			System.out.println("After save - " + afterSave);


if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true && afterSave.contains(expectedValue4)==true){
		
				 
				 	status = "Pass";
			 }else {
				
				    
	//	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
		 		 
		 			status="Fail";
		 
 remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";

 
 Thread.sleep(10000);
 
		 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			   	 	 
				 	
				   if(afterSave.contains(expectedValue1)==true) {
				 	    System.out.println("E1: No issues");
				 	}
				    if(afterSave.contains(expectedValue2)==true){
				 	    System.out.println("E2: No issues");
				 	}
				    if(afterSave.contains(expectedValue3)==true){
					 	System.out.println("E3: No issues");
					 	}
				    if(afterSave.contains(expectedValue4)==true){
				 	    System.out.println("E4: No issues");
				 	         }
			          
				         }
		
			     } catch (Exception e){
			    	 e.printStackTrace();
			     }finally{
			    		 System.out.println(className);
				   Ex.testdata(description, className, remark, category, area, status, uAgent);
		       }
            }
	
	
		
	   @Test(alwaysRun = true)
		 public void PPLtest3() throws Throwable {
		
  try {				
				
		    PPL_FigureContain03 nw = new PPL_FigureContain03();
		           nw.insert_spl_chr_del();
		    
		  } catch (Exception e) {
			e.getStackTrace();
			}
        }
    }
	
	
