package com.FigureContain;
/**
 *Preetish
 **/




import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;




public class TandF_FigureModule01 extends TandFBaseClass{
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	
	/*public TandF_FigureModule01(WebDriver ldriver) {
		TandF_FigureModule01.driver=ldriver;
	}*/
	
	
	
	   
	
	   /*****
	    * 1. Delete one character with Back space key press and insert special character, 
	    *       tracking should be present
	    *****/
	   
	  
	  
	   public void deleteOne_Char_with_backspace_insert_specialChar() throws Exception {
		  
		   try {
			   
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
    description = "Casper-TandF_AuthorEnd-Delete one character with Back space key press and insert special character, tracking should be present";
			 className = "TandF_FigureModule01";    
			         area = "Figure Caption";
			                category = "insert and deletion";
			                      
		
			      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			                     
			              System.out.println("BrowerName->"+uAgent);
			              
			      Cookies cokies =new Cookies(driver);
			             cokies.cookies();
			              
			           Switch switc = new Switch(driver);
			              switc.SwitchCase(uAgent);
			   
			             
			                     PomObj= new Pom();		  
			                    
             WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
			    		        
	
	         WebElement ele= driver.findElement(By.xpath(PomObj.figure1()));  
  					
			        
			        
			     String value = ele.getAttribute("innerHTML");
			       			//System.out.println("check 1 : "+value);
			       			
		
			      if(value.contains(value)){
		  
		     	    	
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
        ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	     	


	           MoveToElement.byXpath(driver,  PomObj.figure1());
	          
	           for(int i=0;i<3;i++) {
			       		ele.click();
			       	 } 
			       
			       			 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
			       						   
			       		
			       			 
for(int i=0;i<60;i++) {
         MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
}
			     

			        MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
			    
			        
			   SymbolActionFuction obj = new SymbolActionFuction(driver);
	                      obj.SymbolTest();
	              
			       					 
	               
	               
			       	 	UpperToolBar obj1 = new UpperToolBar(driver);
			       				  obj1.save_btn_method();
			       				  
			       				
		     	     }		
			 }catch (Exception e){
					e.getStackTrace();
				 }
			    
try {
	Thread.sleep(3000);

    
Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
    
	
		     	 WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
    		    WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));		  
				
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "c</del>";
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue4="ice-ins ice-cts";
			 
				 System.out.println("After save - " + actualValue);



if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){

				 	status ="Pass";
				 	
			      }else{
			    	  
			   
			// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  
			    	  	    
				 			status="Fail";
				
		remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";		 
		Thread.sleep(10000);
				 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				 	   
			      }    
	       if(actualValue.contains(expectedValue1)==true) {
				 	        System.out.println("E1: No issues");
				 	
				 	 }
				  
				 	    if(actualValue.contains(expectedValue2)==true){
				 	        System.out.println("E2: No issues");
				 	  
				 	 }
				   
				 	    if(actualValue.contains(expectedValue3)==true) {
				 	         System.out.println("E3: No issues");
				 	  
				      }
				 	    if(actualValue.contains(expectedValue4)==true) {
						 	  System.out.println("E4: No issues");
						 	  
						      }
			         
				     }catch(Exception e){
				    	 e.getStackTrace();
				    	   }finally{
	  
	     	   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	    }
     }
	  
	  

	  
	   @Test(alwaysRun = true)	
	 public void test1() throws Throwable {
		

 
	try{
				 		 			 				 	    	     
     	 TandF_FigureModule01 obj = new TandF_FigureModule01();
		obj.deleteOne_Char_with_backspace_insert_specialChar();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }
		 }	
	  
}
	