package com.FigureContain;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_FigureCaption14 extends CUP_AuthorEnd_BaseCalss {

	

    static WebElement ele = null;
	static String remark;
	static String className = "CUP_AuthorEnd_FigureCaption14";  
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
	
	
	/***
	 * 14. Insert one character and place cursor to next character then press backspace key, tracking should be present
	 * @throws Exception 
	 ***/


public void insert1Char_palcecursor_nxtChar_backspce() throws Exception {
		
		try {
			
Ex =  new Excel(description, className, remark, category, area, status,uAgent);
  description = "Casper-CUP_AuthorEditor-Insert one character and place cursor to next character then press backspace key, tracking should be present";  
	   area = "Figure Contain";
		   category = "Insert and Delete";
			       		 		
		   
			       System.out.println("BrowerName->"+uAgent);
			                
			
	       Switch switc = new Switch(driver);
	       
	       CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
               C1_C2.CasperCUP(uAgent); 
           
	           switc.SwitchCase(uAgent);  
						  
						  PomObj= new Pom();	
						  
			        WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
							
				  ele=driver.findElement(By.xpath(PomObj.figure2()));
			     			
					       
					     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
					              				
			
			String value = ele.getText();
			     				
			         if(value.contains(value)){
			        	 
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			
		((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");      	 
			        	 
			  MoveToElement.byXpath(driver, PomObj.figure2());
			 
for(int i=0;i<2;i++){	     			
			       ele.click();	       
     }
			  
				if(uAgent.equals("IE")){
						MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
			}
			  					 
	  
			  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
														 
			      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
			
		
	for(int i=0;i<55;i++){
		   if(uAgent.contains("IE")) {
    			Thread.sleep(300);
    			}
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
	}
			    	 
			     			     								     					
			    MoveToElement.bysendkeyWithoutclick(driver, ele, "P");

			     					
			     		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
			     		
			     		
			     MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
			     								     									     						
			     			UpperToolBar ob = new UpperToolBar(driver);
			     				  	 ob.save_btn_method();
			     				  		     				  	    
			     				      }
			                  }catch (Exception e){
			     		        e.getStackTrace();
					     }
			     			      
		try 
		{		
			 Thread.sleep(3000);

    Switch switc = new Switch(driver);
        switc.SwitchCase(uAgent);  
			 
			     WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
								
				    WebElement ele1=driver.findElement(By.xpath(PomObj.figure2()));
			    
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "P</ins>";
				 String expectedValue2 = "r</del>";
				 String expectedValue3 = "ice-del ice-cts";
				 String expectedValue4="ice-del ice-cts";
				 System.out.println("After save - " + actualValue);



if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
status="Pass";
 
 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				 		 util.deleteRecFile(RecVideo);

      }else {
    	 				     				 		 		 
status = "Fail";
	 	     
MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	 				 	 util.MoveRecFile(RecVideo);
	 				 	 
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
	 		 	   
     utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				
				 }
				 	if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	       }
				    if(actualValue.contains(expectedValue3)==true) {
					 	   System.out.println("E3: No issues");
					 	}
				    if(actualValue.contains(expectedValue4)==true) {
					 	   System.out.println("E4: No issues");
					 	}
				   
				       }catch (Exception e) {
			    	
				    	   
				  e.printStackTrace();
								 	   
							        }finally{
			System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
		      }
	      }




@Test(alwaysRun = true)

	public void CUPtest14() throws Throwable {
	


try{
			 		 			 				 	
	 MyScreenRecorder.startRecording(uAgent,className);		
	 
	 CUP_AuthorEnd_FigureCaption14 obj = new CUP_AuthorEnd_FigureCaption14();
      obj.insert1Char_palcecursor_nxtChar_backspce();

  
  
			}catch (Exception e) {
				e.getStackTrace();
			              }
			       }
          }
