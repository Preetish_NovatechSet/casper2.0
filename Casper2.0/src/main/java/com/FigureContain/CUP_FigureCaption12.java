package com.FigureContain;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_FigureCaption12 extends CUP_BaseClass{

	
	
	static WebElement ele =null;
	static String remark;
	static String className = "CUP_FigureCaption12";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
   
    Pom PomObj;
    
	/***
	 * PE - Delete one character with delete key press and insert text, tracking should be present
	 * 
	 ***/

	public void delonechr_DeleteKey_insertTXT() throws Exception{
	
		  try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "Casper-CUP_CopyEditor-Delete one character with delete key press and insert text, tracking should be present";    
		       area = "Figure Contain";
		          category = "Delete and Insert";    
		               System.out.println("BrowerName->"+uAgent);
		                       
		               
		          Cookies cokies =new Cookies(driver);
		               cokies.cookies();    
		               
		      Switch switc = new Switch(driver);
		          switc.SwitchCase(uAgent);   
		               
		               
		               
		                PomObj= new Pom();	
		  
		           WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
						
			           ele=driver.findElement(By.xpath(PomObj.figure2()));
				
				  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
				 
				
                          String value = ele.getText();
		       
				 if(value.contains(value)){
					 					 
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
	((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
	
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());	
	
	for(int i=0;i<2;i++){
			                  ele.click();
	} 
	
	       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);   
	
			  
		  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
		  	
	
	
	   /* if(uAgent.equals("firefox")) {
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
	     }*/
     
				for(int i=0;i<59;i++){
					   if(uAgent.contains("IE")) {
			    			Thread.sleep(300);
			    			}
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				}
					     
						
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
					
					    MoveToElement.bysendkeyWithoutclick(driver, ele, "Iso");
				 }
				 
				 
	       UpperToolBar ob =new UpperToolBar(driver);
			  	      ob.save_btn_method();
			  	      
			  	      //core.HandleAlert.isAlertPresentAccept(driver);
			  	      
		          }catch (Exception e) {
						e.getStackTrace();
	              }
		
	try 
		   {
Thread.sleep(3000);
		

Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
		
		
		  WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
			
	     WebElement ele1=driver.findElement(By.xpath(PomObj.figure2()));
			    
	         String actualValue =  ele1.getAttribute("innerHTML");
			 String expectedValue1 = "Iso</ins>";
			 String expectedValue2 = "B</del>";
			 String expectedValue3 = "ice-del ice-cts";
			 String expectedValue4="ice-del ice-cts";

			 
				 System.out.println("After save - " + actualValue);


if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
	status="Pass";
	 
	 MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
					 		 util.deleteRecFile(RecVideo);

            }else {
          	 				     				 		 		 
 status = "Fail";
		 	     
	MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		 				 	 util.MoveRecFile(RecVideo);
		 	
	 remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
		 		 	  
		 		 	 Thread.sleep(10000);
		 		 	 
		 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				 }
				 	 
				 	if(actualValue.contains(expectedValue1)==true){
				 	   System.out.println("E1: No issues");
				 	}
				 	
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	 }
				    
				    if(actualValue.contains(expectedValue3)==true){
					 	   System.out.println("E3: No issues");
					 	  
					         }
				    if(actualValue.contains(expectedValue4)==true){
					 	   System.out.println("E4: No issues");
					 	  
					         }				  
				}catch(Exception e){
		
			          e.printStackTrace();
		           }finally{
		       System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);	 
	  }
 }
	
	
	
	
	   @Test(alwaysRun = true)
	
		 public void CUPtest12() throws Throwable {
		


	try{
		    MyScreenRecorder.startRecording(uAgent,className);			 				 
			         
			CUP_FigureCaption12 obj = new CUP_FigureCaption12();
					   obj.delonechr_DeleteKey_insertTXT();
				
					      
				}catch (Exception e) {
					e.getStackTrace();
				             }
				      }
	          }