package com.FigureContain;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class TandF_FigureModule16 extends TandFBaseClass{

	
    static WebElement ele =null; 
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	
	Pom PomObj;
	
	
	/****
	 * 16. Insert one character and place cursor to before character then press backspace key, tracking should be present
	 * @throws Exception
	 ****/
	
	public void insert1Char_palcecursor_beforeChar_backspce() throws Exception {
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
    description = "Casper-TandF_AuthorEnd-Insert one character and place cursor to before character then press backspace key, tracking should be present";
		  className = "TandF_FigureModule16";    
		      area = "Figure Contain";
		          category = "Insert and Delete";
		              
		  
		  try {
			  Cookies cokies =new Cookies(driver);
		        cokies.cookies();  
               
     Switch switc = new Switch(driver);
         switc.SwitchCase(uAgent);  
			
			  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
			  
			      PomObj= new Pom();	
			  
			  WaitFor.presenceOfElementByXpath(driver, PomObj.figure4());
				
		     ele=driver.findElement(By.xpath(PomObj.figure4()));
     				

		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
     	 			
		   ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
		
String value = ele.getText();
     				
         if(value.contains(value)) {
     					
  MoveToElement.byXpath(driver, PomObj.figure4());
 
  for(int i=0;i<2;i++){ 			
         ele.click();	       
  }
  
  if(uAgent.equals("IE")){
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
}
  
MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);

MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DOWN);

for(int i=0;i<5;i++){
MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
		}
						
				MoveToElement.bysendkeyWithoutclick(driver, ele, "B");
						  
					
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
										 			
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
				
		 }	
					
					
						UpperToolBar ob = new UpperToolBar(driver);
					  	          ob.save_btn_method();
						  	    
						  	      //    HandleAlert.isAlertPresentAccept(driver);
						     }catch (Exception e) {
					e.getStackTrace();
				              }
			
			try {
			
				Thread.sleep(3000);        
	                
	      Switch switc = new Switch(driver);
	          switc.SwitchCase(uAgent);  
				 
				 WaitFor.presenceOfElementByXpath(driver, PomObj.figure4());
					
				    WebElement ele1=driver.findElement(By.xpath(PomObj.figure4()));
			    
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "B</ins>";
				 String expectedValue2 = "r</del>";
				 String expectedValue3 = "ice-del ice-cts";
				 String expectedValue4="ice-del ice-cts";
				 
				 
				 System.out.println("After save - " + actualValue);

				 
		
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true&& actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
				status="Pass";
				 	
				 
				 }else {
				
					 Thread.sleep(10000);
					    
					 
	 				// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
					    
	 		 	     status = "Fail";
	 		 	  
	remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
	 		 	  
	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	 		 	   
				  if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	       }
				    if(actualValue.contains(expectedValue3)==true){
					 	   System.out.println("E3: No issues");
					 	       }
				    if(actualValue.contains(expectedValue4)==true){
					 	   System.out.println("E4: No issues");
					 	     }
				        }
                 }catch(Exception e){
                	e.printStackTrace();
                	
				       }finally {
			System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);   
		         }
	         }
	
	
	   @Test(alwaysRun = true)
	
		public void test16() throws Throwable {
		


	try{
							 				    
				    
			      TandF_FigureModule16 obj = new TandF_FigureModule16();
					obj.insert1Char_palcecursor_beforeChar_backspce();
				
				}catch (Exception e) {
					e.getStackTrace();
				              }
                       }
            }
