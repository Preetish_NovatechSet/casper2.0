package com.FigureContain;


import utilitys.DataProviderFactory;


//Take the Url From Properties File

public class FigureContainUrl {
	
	
	//PPL
/************************************************************************************************/	
	public static class ChromePPLCasperFigureContainUrl{
			
		public static String url;
			 
				 public static String getPPLChromeUrl() {
				         url=  DataProviderFactory.getFigureContainUrl().getPPLurlForChrome();
				         

					 return url;
				    }
				}
	
	//PPL
/************************************************************************************************/	
		public static class FirefoxPPLCasperFigureContainUrl{
				
			public static String url;
				 
					 public static String getPPLFirefoxUrl() {
					         url=  DataProviderFactory.getFigureContainUrl().getPPLurlForFirefox();
					         

						 return url;
					    }
					}	
	
	
		//CUP
/************************************************************************************************/	
			public static class ChromeCUPCasperFigureContainUrl{
					
				public static String url;
					 
						 public static String getCUPChromeUrl() {
		 url=  DataProviderFactory.getFigureContainUrl().getCUPurlForChrome();
						         

							 return url;
						    }
						}
			
			//CUP
/************************************************************************************************/	
public static class FirefoxCUPCasperFigureContainUrl{
						
    public static String url;
						 
				 public static String getCUPFirefoxUrl() {
	
	url=  DataProviderFactory.getFigureContainUrl().getCUPurlForFirefox();
							         
		 return url;
							    }
							}	

		
//CUPAuthorEnd
/************************************************************************************************/	
		public static class ChromeCUPCasperAEFigureContainUrl{
				
			public static String url;
				 
					 public static String getCUPAEChromeUrl() {
	 url=  DataProviderFactory.getFigureContainUrl().getCUPAEurlForChrome();
					         

						 return url;
					    }
					}
		
//CUPAuthorEnd
/************************************************************************************************/	
public static class FirefoxCUPCasperAEFigureContainUrl{
					
public static String url;
					 
			 public static String getCUPAEFirefoxUrl() {

url=  DataProviderFactory.getFigureContainUrl().getCUPAEurlForFirefox();
						         
	 return url;
						    }
						}	
		
		
		
	
	
	
	//Ers
/************************************************************************************************/	
	public static class ChromeErsFigureContainUrl{
			
		public static String url;
			 
				 public static String getErsChromeUrl() {
				         url=  DataProviderFactory.getFigureContainUrl().getCUPurlForChrome();
				         

					 return url;
				    }
				}
	
	//Ers
/************************************************************************************************/	
	public static class FirefoxErsFigureContainUrl{
			
		public static String url;
			 
				 public static String getErsFirefoxUrl() {
				         url=  DataProviderFactory.getFigureContainUrl().getErsurlForFirefox();			         
					 return url;
				    }
				}
	
	//Ers
/************************************************************************************************/	
	public static class ChromeErsPEFigureContainUrl{
			
		public static String url;
			 
				 public static String getErsChromeUrl() {
				         url=  DataProviderFactory.getFigureContainUrl().getErsurlForPEChrome();
				         

					 return url;
				    }
				}
	
	//Ers
/************************************************************************************************/	
	public static class FirefoxErsPEFigureContainUrl{
			
		public static String url;
			 
				 public static String getErsFirefoxUrl() {
				         url=  DataProviderFactory.getFigureContainUrl().getErsurlForPEFirefox();			         
					 return url;
				    }
				}
	
	
	
	
	
	
/************************************************************************************************/	
public static class FireFoxFigureContainUrl{
		
	public static String url;
		 
			 public static String getFireFoxUrl() {
			         url=  DataProviderFactory.getFigureContainUrl().geturlForFireFox();
			         

				 return url;
			    }
			}








/************************************************************************************************/


public static class IEFigureContainUrl{
		  public static String url;
		 
			 public static String getIEUrl() {
			         url=  DataProviderFactory.getFigureContainUrl().geturlForIE();
			         

				 return url;
			    }
			}

/************************************************************************************************/	

public static class ChromeFigureContainUrl {	
			
	public static  String url;
							
			 	
		public static String getChromeUrl() {
				      url=  DataProviderFactory.getFigureContainUrl().geturlForChrome();
				      

			    return url;
			     }  
			
		    }
		

/************************************************************************************************/

public static class EdgeFigureContainUrl {	
	
	public static  String url;
	 	
	public static String getEdgeUrl() {
		      url=  DataProviderFactory.getFigureContainUrl().geturlForEdge();
		      
	    return url;
	     }  
	
    }


/************************************************************************************************/

static class EdgePEFigureContainUrl {	
	
	public static  String url;
	 	
	public static String getEdgePEUrl() {
		      url=  DataProviderFactory.getFigureContainUrl().geturlForEdgePE();
		       
	    return url;
	     }  
	
    }


	
/************************************************************************************************/

public static class ChromePEFigureContainUrl {	
				
				public static  String url;
				 	
				 public static String getChromePEUrl() {
					     url=  DataProviderFactory.getFigureContainUrl().geturlForChromePE();
					            
			                      		         
				     return url;
				     }  
				 }
				

/************************************************************************************************/

public static class FireFoxPEFigureContainUrl {	
					
					public static  String url;
					 	
					public static String getFireFoxPEUrl() {
						     url=  DataProviderFactory.getFigureContainUrl().geturlForFireFoxPE();
						     					     
						     return url;
					     
			           }
					
	           }	

/************************************************************************************************/	
	
public static class IE_PEFigureContainUrl {	
						
		public static  String url;
						 	
	 public static String getIEPEUrl() {
				url=  DataProviderFactory.getFigureContainUrl().geturlForIEPE();
							     
								                       
						    return url;
						     
				           }												
				       }
	               }
		         

/************************************************************************************************/

