package com.FigureContain;



import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

/********
 *Preetish*
  ********/


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;




public class TandF_FigureModule07 extends TandFBaseClass{

	static WebElement ele=null;
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
    
	
	/*public TandF_FigureModule07(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	
	
	/*****
	 *7. Single word select and use delete key for delete, 
	 *      check tracking and data lose
	 * @throws Exception 
	 *****/




public  void selectSingle_word_delete() throws Exception {
		
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	   description = "Casper-TandF_AuthorEnd-Single word select and use delete key for delete, check tracking and data lose";
		   className = "TandF_FigureModule07";    
		        area = "Figure Contain";
		           category = "Select and delete";
		                
				
		   try {			                  
                        System.out.println("BrowerName->"+uAgent);
                          
                        Cookies cokies =new Cookies(driver);
			                 cokies.cookies();       
                        
			           Switch switc = new Switch(driver);
			              switc.SwitchCase(uAgent);
                        
			   
			   PomObj= new Pom();
			   
				WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
				
		       ele=driver.findElement(By.xpath(PomObj.figure2()));
					
		        driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS); 
		        
		        String value = ele.getText();
					    
					
					if(value.contains(value)){
						 
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		
 
	      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
						
						MoveToElement.byXpath(driver, PomObj.figure2());
		
		for(int i=0;i<3;i++) {
				             ele.click();
						}  
		
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				      			
			
			    	for(int i=0;i<29;i++) {
						MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
			    	}
				
								
				
			MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 8, Keys.ARROW_RIGHT);
					
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
						
					//driver.switchTo().defaultContent();
					
			 UpperToolBar ob = new UpperToolBar(driver);
		 	  	   ob.save_btn_method();
			 	  	   
			 	 
					    }
					}catch (Exception e) {
	   					e.printStackTrace();
	    	           }
		
		
		//Figure 3 Effect
						
		   try
		      {
			   Thread.sleep(3000);
				        
	                      
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent);
				
			   			   
		   	WaitFor.presenceOfElementByXpath(driver, PomObj.figure2());
		                  
		   	WebElement ele1=driver.findElement(By.xpath(PomObj.figure2()));
		
			
		    String afterSave =  ele1.getAttribute("innerHTML");
			 
			String expectedValue1 = "Research</del>";
			String expectedValue2 = "ice-del ice-cts";

		 System.out.println("After save - " + afterSave );

 if(afterSave .contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true){
				 
				status="Pass";
				 	
				 }else {			
					    
	//	 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
			 		 
		 
		 status = "Fail";
		
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
	       
	 Thread.sleep(12000);
	 
			 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				
				 	if(afterSave .contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				    if(afterSave .contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	}
			     }
	        }catch (Exception e) {
	        	e.getStackTrace();
	           }finally {
	        	   System.out.println(className);
	       Ex.testdata(description, className, remark, category, area, status, uAgent);
	           }
		   }
	
		

	 @Test(alwaysRun = true)
		public void test7() throws Throwable {
		


	try{
				 		 			 				 							
		TandF_FigureModule07 obj = new TandF_FigureModule07();
					obj.selectSingle_word_delete();
					
								
				}catch (Exception e) {
					e.getStackTrace();
				        }
				 }
	      }
