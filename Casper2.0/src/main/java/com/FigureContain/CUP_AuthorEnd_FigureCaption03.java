package com.FigureContain;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_FigureCaption03 extends CUP_AuthorEnd_BaseCalss{
	
	
	
	
	/*
	public TandF_FigureModule03(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	
	

	static String remark;
	static String className = "CUP_AuthorEnd_FigureCaption03";  
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	/****** 
	* 3. Insert special character and press delete key, tracking should be present
	******/ 
		
	
	public void insert_spl_chr_del() throws Exception {
			
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_AuthorEditor-Insert special character and press delete key, tracking should be present"; 
		   area = "Figure Caption";
			   category = "insert and deletion";
			         
			   
			    System.out.println("BrowerName->"+uAgent);       
			         
			    
			             PomObj= new Pom();  			    			    
			    
                  Switch switc = new Switch(driver);                     
			    
			    CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                        C1_C2.CasperCUP(uAgent);  
			    			    
                    switc.SwitchCase(uAgent);
			    
try {
		         	
	         WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
			WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
		
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
			 						 			
			 	          String value = ele.getText();
			 			 	
			if(value.contains(value)){			
			 		
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	 		
		 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		 
								
		              MoveToElement.byXpath(driver,PomObj.figure1());
			 		
		  for(int i=0;i<3;i++){			  
				     ele.click();
		         }
		  
		  
		  for(int i=0;i<2;i++){			  
               MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
	     }
		  
			  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
			 			 	
			 
			 	for(int i=0;i<65;i++){
			 		
			 	   if(uAgent.contains("IE")){
		    			Thread.sleep(300);
		    			}
			 	   
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			     }

			 	
		SymbolActionFuction obj = new SymbolActionFuction(driver);
	                  obj.SymbolTest();
			      	              	                  
	                  switc.SwitchCase(uAgent); 
	                  	                                   
	               WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	             WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
	      			
	                             
switch(uAgent) {

	  case "edge":
	             	     
		  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT);             	  
	             break;
	  }
	             
	               
		 MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);
	                		 		   
			 		   
		 		UpperToolBar ob = new UpperToolBar(driver);
		 		              ob.save_btn_method();
		 		              
			 			  }} catch (Exception e) {
			 				  
							e.printStackTrace();
				           }
			 	
try {	
	
	
	Thread.sleep(3000);
	   
Switch switc1 = new Switch(driver);
     switc1.SwitchCase(uAgent); 			
			 									 
			WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
				    		        
					WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));

						  							
				String afterSave =  ele1.getAttribute("innerHTML");
				String expectedValue1 = "r</del>";
				String expectedValue2 = "ice-del ice-cts";
				String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				String expectedValue4="ice-ins ice-cts";
				
								 
			System.out.println("After save - " + afterSave);


if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true && afterSave.contains(expectedValue4)==true){
		
				 
				 	status = "Pass";
				 	
				 	MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
							util.deleteRecFile(RecVideo);
								  
			 }else {
				
				    
	//	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
		 		 
		 			status="Fail";
		 			
		 			MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		 					util.MoveRecFile(RecVideo);
		 
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";

   Thread.sleep(10000);
 
		 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			   	 	 
				 	
				if(afterSave.contains(expectedValue1)==true){
				 	    System.out.println("E1: No issues");
				 	               }
				    if(afterSave.contains(expectedValue2)==true){
				 	    System.out.println("E2: No issues");
				 	          }
				    if(afterSave.contains(expectedValue3)==true){
					 	System.out.println("E3: No issues");
					 	}
				    if(afterSave.contains(expectedValue4)==true){
				 	    System.out.println("E4: No issues");
				 	}
			          
	         }
		
			     } catch (Exception e){
			    	 e.printStackTrace();
			     }finally{
			    		 System.out.println(className);
				   Ex.testdata(description, className, remark, category, area, status, uAgent);
		       }
            }
	
	
	
	 @Test(alwaysRun = true)
	
	 public void CUPtest3() throws Throwable {
	
try {				
			
      MyScreenRecorder.startRecording(uAgent,className); 
  
      CUP_AuthorEnd_FigureCaption03 nw = new CUP_AuthorEnd_FigureCaption03();
	                  nw.insert_spl_chr_del();
	    
	  } catch (Exception e) {
		e.getStackTrace();
		}
    }
}