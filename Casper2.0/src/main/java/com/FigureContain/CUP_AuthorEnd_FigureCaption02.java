package com.FigureContain;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_FigureCaption02 extends CUP_AuthorEnd_BaseCalss{
	
	

	static String remark;
	static String className = "CUP_AuthorEnd_FigureCaption02";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
   
    

	
	/********
	*2. Delete one character with delete key press and insert special character, 
	*	         tracking should be present
	*********/
	
	
	
public void  delete_oneChar_WithdelKeyAnd_insSplChar() throws Exception {
		
		
		try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
  description = "Casper-CUP_AuthorEditor-Delete one character with delete key press and insert special character, tracking should be present";    
			  area = "Figure Caption";
			      category = "insert and deletion";
			      
		
				
			       driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			          
			              System.out.println("BrowerName->"+uAgent);	
			              
			   
			              
	            Switch switc = new Switch(driver);
	            
	            
	            
	            CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                        C1_C2.CasperCUP(uAgent);  
	            
	            
	                switc.SwitchCase(uAgent);           
			              
			              		              
			           PomObj= new Pom();	
			                 
	WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	
			    WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
			    
	
  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
        
          ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		        	
   
  String value = ele.getText();
		        	
		if(value.contains(value)) 
						 {    	
					    
					   MoveToElement.byXpath(driver, PomObj.figure1());
					   
					   for(int i=0;i<2;i++) {					    	
					         ele.click();
					   }
					   
					   for(int i=0;i<2;i++) {
						   Thread.sleep(300);
			  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
				     }
					   
	         MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	
	
			for(int i=0;i<67;i++){
					   MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			 }
					    
					
			   MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
					         
					  	    
					  SymbolActionFuction obj = new SymbolActionFuction(driver);
				               obj.SymbolTest();
					  	
					  	  
					UpperToolBar obj1 = new UpperToolBar(driver);
					  	   obj1.save_btn_method();
					  		
					  	 				  	 
					         }
						  } catch (Exception e) {
						    e.printStackTrace();
						  }
		try {	
			Thread.sleep(3000);
            
   Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
						
				 WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
		    		        
				WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));

				  
						
					     String actualValue =  ele1.getAttribute("innerHTML");
						 String expectedValue1 = "o</del>";
						 String expectedValue2 = "ice-del ice-cts";
						 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
						 String expectedValue4="ice-ins ice-cts";
				
			
				 
				 System.out.println("After save - " + actualValue);
				 
				 

if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){

			status = "Pass";
						 
			MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				       util.deleteRecFile(RecVideo);
			
				 }else {
									 					 
											    							  
			 		status="Fail";
			 		
			 		MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
							 	  		 util.MoveRecFile(RecVideo);	 		
			 					 		
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
			 	
		
			 Thread.sleep(10000);
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				   	  
				   	  
				    if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	}
				    if(actualValue.contains(expectedValue3)==true) {
				 	   System.out.println("E3: No issues");	
				   }
				    if(actualValue.contains(expectedValue4)==true) {
					 	 System.out.println("E4: No issues");		 	 
					   }
			        }
		       }catch (Exception e){
		    	   e.getStackTrace();
		         }
           }
	
	
	
	
@Test(alwaysRun = true)

	public void CUPtest2() throws Throwable {

        try {		
        	
        	  MyScreenRecorder.startRecording(uAgent,className);   
        	
        	  CUP_AuthorEnd_FigureCaption02 obj = new CUP_AuthorEnd_FigureCaption02();
			     obj.delete_oneChar_WithdelKeyAnd_insSplChar();
			     
			     
		       }catch (Exception e){
		    	  
			e.getStackTrace();
	    	     }finally{
	    	    	 
		        	     System.out.println(className);
		    Ex.testdata(description, className, remark, category, area, status, uAgent);
		    
		         
		       }
    	   }
      }
