package com.FigureContain;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

//readProperties File


	
	public class ConfigUrlForFigureContain	{
		
		static Properties pro;
		
		
		
	
		public static void ReadPath(String PathofXML)
		{
			
			String sourcePath = PathofXML;
			
			File src=new File(sourcePath);
			
			try 
			{
				FileInputStream fis=new FileInputStream(src);
				
				 pro=new Properties();
				
				   pro.load(fis);
				
			} catch (Exception e) 
			{
				System.out.println("Exception is "+e.getMessage());
			}
			
		   }
		
			
		
//TandF		
	public String geturlForChrome()
		{
			
		
		
	ConfigUrlForFigureContain.ReadPath("./Configfolder/UrlFigureContain/ChromeFigureContainUrl.properties");
		
			String Table2_Aspirations=pro.getProperty("ChromeFigureContainUrl");
			return Table2_Aspirations;
		}
	
	
	
	public String geturlForChromePE()
	{
		
		
		ConfigUrlForFigureContain.ReadPath("./Configfolder/UrlFigureContain/ChromePEFigureContainUrl.properties");
		
		String Table2_Aspirations=pro.getProperty("ChromePEFigureContainUrl");
		return Table2_Aspirations;
	}
	
	  
	
	
	
	public String geturlForFireFox()
	{
		
		ConfigUrlForFigureContain.ReadPath("./Configfolder/UrlFigureContain/FireFoxFigureContainUrl.properties");
			
		String url=pro.getProperty("FireFoxFigureContainUrl");
		  return url;
	}
	
	
	
	
public String geturlForFireFoxPE()
	{
		
	ConfigUrlForFigureContain.ReadPath("./Configfolder/UrlFigureContain/FireFoxPEFigureContainUrl.properties");
	

		
		String Table2_Aspirations=pro.getProperty("FireFoxPEFigureContainUrl");
		return Table2_Aspirations;
	   }



public String geturlForIE()
{
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\IEFigureContainUrl.properties");
			
	String url=pro.getProperty("IEFigureContainUrl");
	return url;
}



public String geturlForIEPE()
{
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\IEPEFigureContainUrl.properties");
		
	String url=pro.getProperty("IEPEFigureContainUrl");
	return url;
}



public String geturlForEdge()
{
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\EdgeFigureContainUrl.properties");
	
		
	String url=pro.getProperty("EdgeFigureContainUrl");
	return url;
}


public String geturlForEdgePE()
{
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\EdgePEFigureContainUrl.properties");
	

	String url=pro.getProperty("EdgePEFigureContainUrl");
	return url;
}


public String geturlForMac(){
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\MacFigureContainUrl.properties");
	
	String url=pro.getProperty("MacFigureContainUrl");
	return url;
}



public String geturlForMacPE()
{
	
	ConfigUrlForFigureContain.ReadPath("./Configfolder\\UrlFigureContain\\MacPEFigureContainUrl.properties");
	
	String url=pro.getProperty("MacPEFigureContainUrl");
	return url;
}


//Ers
public String getErsurlForChrome()
{
	
ConfigUrlForFigureContain.ReadPath("./ErsUrl/FigureContain/ChromeFigureContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("ErsChrome");
	return Table2_Aspirations;
}



public String getErsurlForFirefox()
{

ConfigUrlForFigureContain.ReadPath("./ErsUrl/FigureContain/FirefoxFigureContainUrl.properties");

String Table2_Aspirations=pro.getProperty("ErsFirefox");
return Table2_Aspirations;
}

public String getErsurlForPEChrome()
{

ConfigUrlForFigureContain.ReadPath("./ErsUrl/FigureContain/ChromePEFigureContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("ErsChrome");
	  return Table2_Aspirations;
}


public String getErsurlForPEFirefox(){
	
ConfigUrlForFigureContain.ReadPath("./ErsUrl/FigureContain/FirefoxPEFigureContainUrl.properties");

String Table2_Aspirations=pro.getProperty("ErsFirefox");
return Table2_Aspirations;
    }

//***PPl Publisher for Casper***//

public String getPPLurlForChrome(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/PPLUrl/CasperChromeUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperPPLChrome");
return Table2_Aspirations;
    }


public String getPPLurlForFirefox(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/PPLUrl/CasperFirefoxUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperPPLfirefox");
return Table2_Aspirations;
    }




//***CUP Publisher for Casper***//

public String getCUPurlForChrome(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/CUPUrl/CasperChromeUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperCUPChrome");
return Table2_Aspirations;
    }


public String getCUPurlForFirefox(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/CUPUrl/CasperFirefoxUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperCUPfirefox");
return Table2_Aspirations;
    }




//Editor-End
/*****************************************************************************************/


public String getCUPAEurlForChrome(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/CUPUrl/CasperAEChromeUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperCUPChrome");
return Table2_Aspirations;
    }


//Editor-End
public String getCUPAEurlForFirefox(){
	
ConfigUrlForFigureContain.ReadPath("./Configfolder/CUPUrl/CasperAEFirefoxUrl.properties");

String Table2_Aspirations=pro.getProperty("CasperCUPfirefox");
return Table2_Aspirations;
    }


}
	

