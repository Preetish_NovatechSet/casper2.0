package com.FigureContain;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import utilitys.BrowserFactory;

public class CUP_AuthorEnd_BaseCalss {

	public static WebDriver driver;
	public static  String uAgent;

	
	@Parameters({ "browser" })	
	@BeforeClass
	
	
		public void setupApplication(String browser) throws Exception
		{
			
			
switch (browser) {
				
			
			case "fireFox":
							
	driver = BrowserFactory.Setup_Grid(browser,FigureContainUrl.FirefoxCUPCasperAEFigureContainUrl.getCUPAEFirefoxUrl());

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return typeof InstallTrigger !== 'undefined'?'firefox':''");		
	break ;	
				
								    
			case "chrome":
			
	driver = BrowserFactory.Setup_Grid(browser,FigureContainUrl.ChromeCUPCasperAEFigureContainUrl.getCUPAEChromeUrl());

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return navigator.userAgent.match(/Chrom(e|ium)\\/([0-9]+)\\./)? 'chrome' : '';");			
						
			break ;
								    
								    
			}
			
			Reporter.log("=====Application Started=====", true);
			
		} 
		
			
		
	
@AfterClass

	public void closeApplication()
		{
		
				 try {
				Thread.sleep(3000);
				  driver.quit();	 
					
					
					switch (uAgent){
					case "opera":	 
		Runtime.getRuntime().exec("taskkill /f /im opera.exe");
				 }
					
					 System.out.println("Application Close");
				} catch (Exception e) {
					e.printStackTrace();
				           }
			           }
			       }
