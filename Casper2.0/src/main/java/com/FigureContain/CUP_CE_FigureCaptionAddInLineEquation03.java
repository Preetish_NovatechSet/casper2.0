package com.FigureContain;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;




public class CUP_CE_FigureCaptionAddInLineEquation03 extends CUP_BaseClass{
	
	
	static String remark;
	static String className = "CUP_CE_FigureCaptionAddInLineEquation03"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	
	
	public void AddInline01() throws InterruptedException{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_CopyEditor-Double click on added equation click on delete, then check whether added Equation has deleted from the figure Caption area";			    
	    area = "Figure Caption";
			category = "Equation deletion";
			
			
			try {
				
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
				
				System.out.println("BrowerName->"+uAgent);
				
   Cookies cokies =new Cookies(driver);
         cokies.cookies();
				
       Switch switc = new Switch(driver);
				switc.SwitchCase(uAgent);
				
				    PomObj= new Pom();		
				    
				    
		    WebElement web = driver.findElement(By.xpath(PomObj.EqLogo()));


		 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", web);           
				   ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
				               	
				  MoveToElement.byXpath(driver, PomObj.EqLogo());
				 
				  
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
	 	"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
  				      "{arguments[0].fireEvent('ondblclick');}";
								
                    driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
					((JavascriptExecutor)driver).executeScript(doubleClickJS,web);
					
		            Thread.sleep(5000);       
					driver.switchTo().defaultContent();
	      WaitFor.presenceOfElementByXpath(driver, PomObj.DelEQ());
		WebElement DeleteEQ = driver.findElement(By.xpath(PomObj.DelEQ()));	
				  MoveToElement.byclick(driver, DeleteEQ);
				  
				  
				  core.HandleAlert.isAlertPresentAccept(driver);
				  
				  
				  
				  UpperToolBar obj1 = new UpperToolBar(driver);
				         obj1.save_btn_method(); 
				  
				  
				         switc.SwitchCase(uAgent);
				         
				         
	 WebElement web1;
	 String Actual;
					try {
						
						web1 = driver.findElement(By.xpath(PomObj.EqLogo()));
						     Actual = web1.getAttribute("innerHTML"); 
						     
		String Expected="\\sqrt[]{}\\frac{\\partial }{\\partial }\\times\\lim_{ \\rightarrow }";
						     
	if(Actual.contains(Expected)==false){
		    	
		   status="Fail";
  			 				 	   
    	  		MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
    	  			 	    util.MoveRecFile(RecVideo);			
    	  			 			
    	  			
    	 remark="Unable to delete the equation";		 
    	               Thread.sleep(10000);
		   }
		
		}catch (Exception e){
												
	 status ="Pass";
		    	  		 			    	  		 	
				        MyScreenRecorder.stopRecording();
	String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				    	  util.deleteRecFile(RecVideo); 	
											
					     }
    	  		
			        }catch(Exception e){				  
				   
			  }
       }
	

	
@Test(alwaysRun = true)	

	public void FigureCaptionAddInline03() throws IOException{
		
		try {
			
	MyScreenRecorder.startRecording(uAgent,className);	
			
	CUP_CE_FigureCaptionAddInLineEquation03 obj= new CUP_CE_FigureCaptionAddInLineEquation03();
			                obj.AddInline01();
		   }catch (Exception e){
			    e.printStackTrace();
		     }finally{
		   	  
		     	   System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);
		    }
		 }
    }
