package com.FigureContain;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class CUP_AddFigure_caption02 extends CUP_BaseClass{

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	static WebElement ele=null;
	
	
	/*public TandF_FigureModule01(WebDriver ldriver) {
		TandF_FigureModule01.driver=ldriver;
	}*/
	
	
	
	   
	
	   /*****
	    * 1. Delete one character with Back space key press and insert special character, 
	    *       tracking should be present
	    *****/
	   
	  
	  
public void deleteOne_Char_with_backspace_insert_specialChar() throws Exception {
		  
		   try {
			   
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
    description = "Casper-CUP_AuthorEnd-Go to Right panel click on edit-option then click on Add then click on figure, add some text on caption and save, check whether without image it's getting saved or not.";
			 className = "CUP_AddFigure_caption02";    
			         area = "Right panel";
			                category = "Add figure";
			                      
		
			      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			                     
			              System.out.println("BrowerName->"+uAgent);
			              
			      Cookies cokies =new Cookies(driver);
			             cokies.cookies();
			              
			           Switch switc = new Switch(driver);
			              switc.SwitchCase(uAgent);
			   
			             
			                     PomObj= new Pom();	
			                     
			                     
			         ele= driver.findElement(By.xpath(PomObj.Para()));  
	    
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	  
			      MoveToElement.byXpath(driver,  PomObj.Para());	     
			      ele.click();
			      
			                     
			                     
			                    driver.switchTo().defaultContent();
             WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
			    		        
	
	         ele= driver.findElement(By.xpath(PomObj.Edit_Option()));  
  					
			        
			        
			     String value = ele.getAttribute("innerHTML");
			       			//System.out.println("check 1 : "+value);
			       			
		
			      if(value.contains(value)){
		  		     	    	

	           MoveToElement.byXpath(driver,  PomObj.Edit_Option());
	          
	           
			       		ele.click();
			   
			       		
			    WaitFor.presenceOfElementByXpath(driver, PomObj.Add());
	    ele= driver.findElement(By.xpath(PomObj.Add()));  	
			       		
			     
	    	       MoveToElement.byXpath(driver, PomObj.Add());
			       		MoveToElement.byclick(driver, ele);
			       		
			  
			       				  
			       		
			       ele= driver.findElement(By.xpath(PomObj.Addfigure()));  	
			       		
				     
		    	       MoveToElement.byXpath(driver, PomObj.Addfigure());
				       		MoveToElement.byclick(driver, ele);
			       		
				     
				       								   
						Thread.sleep(6000);
						   
			       			ele= driver.findElement(By.xpath(PomObj.CaptionText()));  	
						       		
								     
		MoveToElement.byXpath(driver, PomObj.CaptionText());
			MoveToElement.byclick(driver, ele);
			
MoveToElement.bysendkeyWithoutclick(driver, ele, "We can tell the tool to wait only till the Condition satisfy.");
						   
		ele= driver.findElement(By.xpath(PomObj.AddSaveCaption()));  	
       				     
			MoveToElement.byXpath(driver, PomObj.AddSaveCaption());
				MoveToElement.byclick(driver, ele);	   
				
				
		   
	 WaitFor.presenceOfElementByXpath(driver, PomObj.ValidationPop());
 		        
			ele= driver.findElement(By.xpath(PomObj.ValidationPop())); 
		         String Actualvalue = ele.getAttribute("innerHTML");
		                System.out.println(Actualvalue); 
		         String ExpectedVal="Figure Added Successfully !";
		         String ExceptedVal1="Please Upload image!";
		         
		         if(Actualvalue.contains(ExpectedVal)==false && Actualvalue.contains(ExceptedVal1)==true) {
		        	 
		        	 status ="Pass";
		        	 
					}else{
						     	   	    
					 status="Fail";
					
	remark="Without uploading image Caption has save, pop-up didn't appear";		 
			Thread.sleep(10000);
			
					utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
					 	   
				         }
		                
		                
	       if(Actualvalue.contains(ExpectedVal)==true) {
				 	        System.out.println("E1: No issues");
				 	
				 	           }
				  
			              }
			         
				     }catch(Exception e){
				    	 e.getStackTrace();
				    	   }finally{
	  
	     	   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	       }
     }
	  
	  

	  
	   @Test(alwaysRun = true)	
	 public void CUPtest2() throws Throwable {
		

 
	try{
				 		 			 				 	    	     
		CUP_AddFigure_caption02 obj = new CUP_AddFigure_caption02();
		  obj.deleteOne_Char_with_backspace_insert_specialChar();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }
		 }		  
     }
