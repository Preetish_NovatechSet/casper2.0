package com.FigureContain;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_FigureContain10 extends PPL_BaseClass{
	
	
	
	static WebElement ele =null;
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;   
    Pom PomObj;
  
    
	/****
	*10. Delete single word character and insert some text, check insert and delete tracking present
	*****/

    

	public void deleteSingleWordChar_AndInsert_SomeText() throws Exception {
		  try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_AuthorEnd-Delete single word character and insert some text, check insert and delete tracking present";
			className = "PPL_FigureModule10";    
			      area = "Figure Contain";
			           category = "Delete and insert";
			             				    
			           
  System.out.println("BrowerName->"+uAgent);	             

  
  Cookies cokies =new Cookies(driver);
        cokies.cookies();
     
        
Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
  
  
			 PomObj= new Pom();	 
			 
		 WaitFor.presenceOfElementByXpath(driver, PomObj.pplfigure3());
			  		 		
			   ele=driver.findElement(By.xpath(PomObj.pplfigure3()));
		
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
			 					
					String value =ele.getText();
					
					if(value.contains(value)) {


	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
									
		((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
		
		
			MoveToElement.byXpath(driver, PomObj.pplfigure3());
		
			for(int i=0;i<2;i++) {    	
				        ele.click();
			}  
			
			
			for(int i=0;i<=4;i++){			  
		  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
		  	}
				    	
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				    	
			
					for(int i=0;i<34;i++){
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				}
				  
												
		for(int i=0;i<3;i++) {
						 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
				}
						
						
		 MoveToElement.bysendkeyWithoutclick(driver, ele, "preetish");
						 
			
		    UpperToolBar ob = new UpperToolBar(driver);
			  	    ob.save_btn_method();
		 
		// core.HandleAlert.isAlertPresentAccept(driver);
			 }
				

Thread.sleep(3000);
  
        
Switch switc1 = new Switch(driver);
switc1.SwitchCase(uAgent);
				

				 WaitFor.presenceOfElementByXpath(driver, PomObj.pplfigure3());
	  		 		
	 		    WebElement ele1=driver.findElement(By.xpath(PomObj.pplfigure3()));
			
			 String actualValue =  ele1.getAttribute("innerHTML");
			 String expectedValue1 = "and</del>";
			 String expectedValue2 = "preetish</ins>";
			 String expectedValue3 = "ice-del ice-cts";
			 String expectedValue4 = "ice-ins ice-cts";
			 System.out.println("After save - " + actualValue);


if(actualValue.contains(expectedValue1)==true&&actualValue.contains(expectedValue2)==true&&actualValue.contains(expectedValue3)==true&&actualValue.contains(expectedValue4)==true){
	 status="Pass";
 
              }else {
            	 
				    
 				// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
 		 		 
 		 	     status = "Fail";
  remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
 		 	   
 		 	 Thread.sleep(10000);	   
 		 	   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
 		 	   
 		 	   
			 	if(actualValue.contains(expectedValue1)==true) {
			 	   System.out.println("E1: No issues");
			 	}
			    if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	   }
			     
			    if(actualValue.contains(expectedValue3)==true){
			       System.out.println("E3: No issues");
			        }
			    if(actualValue.contains(expectedValue4)==true){
				       System.out.println("E4: No issues");
				        }
			       }
		   }catch (Exception e){
	      e.getStackTrace();
			   
			       }finally {
			   System.out.println(className);
	Ex.testdata(description, className, remark, category, area, status, uAgent);
		      }
	      }
      

	
	
	
	   @Test(alwaysRun = true)
	
		public void PPLtest10() throws Throwable {
		


	try{
				 		 			 				      
		PPL_FigureContain10 obj = new PPL_FigureContain10();
					  obj.deleteSingleWordChar_AndInsert_SomeText();
					
				}catch (Exception e) {
					e.getStackTrace();
				              }
		 		       }	
	       }