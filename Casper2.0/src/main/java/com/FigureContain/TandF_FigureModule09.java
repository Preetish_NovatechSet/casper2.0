package com.FigureContain;



/********
 *Preetish*
  ********/

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class TandF_FigureModule09 extends TandFBaseClass{
	
	
    static WebElement ele =null;
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;	
    Pom PomObj;
	
	/*****
	 **9. Single word select and "Control + C" then "Control + v" in next next word, check tracking and data lose
	 * @throws Exception 
	 *****/
	
	
	public void SelectSingle_Word_CopyPaste_nxtword() throws Exception {
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-TandF_AuthorEnd-Single word select and Control + C then Control + v in next next word, check tracking and data lose";
		  className = "TandF_FigureModule09";    
		        area = "Figure Contain";
		             category = "Copy and Paste";
		                  
	
		     try {
		  System.out.println("BrowerName->"+uAgent);	
		  
		 
		  Cookies cookies = new Cookies(driver);
                cookies.cookies();         
                  
       Switch switc = new Switch(driver);
          switc.SwitchCase(uAgent);
		    	 
		    	 PomObj= new Pom();
				   
		 WaitFor.presenceOfElementByXpath(driver, PomObj.figure3());
		 		
		      ele=driver.findElement(By.xpath(PomObj.figure3()));
				
		     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 

		     ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
		
				
		     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
					    								
			
			for(int i =0;i<3;i++) {	
							ele.click();
			 }					
					  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
							
					    	
					    		 for(int i =0;i<107;i++) {
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
					    		 }
											
					
			MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 8,Keys.ARROW_LEFT);
						
			  
		if(uAgent.equals("IE")){	
			 
			 Robot robot = new Robot();
		     robot.keyPress(KeyEvent.VK_CONTROL);
		     robot.keyPress(KeyEvent.VK_C); 
		     robot.keyRelease(KeyEvent.VK_C); 
		     robot.keyRelease(KeyEvent.VK_CONTROL); 
		}else{
			
			   MoveToElement.copy(driver);
			
		}
			
					     
							 for(int i =0;i<19;i++) {
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);  
							    }
							    	
							 if(uAgent.equals("IE")){		
								
		/*	((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");	
			             		
			           core.HandleAlert.isAlertPresentAccept(driver);	*/	
								 
								 
								 Robot robot = new Robot();
							     robot.keyPress(KeyEvent.VK_CONTROL);
							     robot.keyPress(KeyEvent.VK_V); 
							     robot.keyRelease(KeyEvent.VK_V); 
							     robot.keyRelease(KeyEvent.VK_CONTROL); 				 
								 
								 
							 }else{     	
				  MoveToElement.paste(driver);
							 }
							 
							 
							 
						   UpperToolBar ob = new UpperToolBar(driver);  
						               ob.save_btn_method();
						              
		      }catch(Exception e){
		    	 
		    	 e.getStackTrace(); 
		     }
		    	 

try {
	
	Thread.sleep(3000);  
            
 Switch switc = new Switch(driver);
    switc.SwitchCase(uAgent);
	
				 WaitFor.presenceOfElementByXpath(driver, PomObj.figure3());
				 		
				WebElement ele1=driver.findElement(By.xpath(PomObj.figure3()));
			
			
				     String actualValue =  ele1.getAttribute("innerHTML");
					 String expectedValue1 = "reaction</ins>";
					 		 

			 System.out.println("After save - " + actualValue);


		if(actualValue.contains(expectedValue1)==true){
			status="Pass";
			 }else {
				 
	//	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
				 
				 Thread.sleep(10000);
				    
				 		
		 		 
		 	     status = "Fail";
		 	    
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	 
		 	    
		 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			 		
		
			  if(actualValue.contains(expectedValue1)==true) {
			 	   System.out.println("E1: No issues");
			 	   }		  	 
			   }    
          }catch(Exception e){
		 		
				    e.getStackTrace();
				    
			      }finally {
			    	  System.out.println(className);
        Ex.testdata(description, className, remark, category, area, status, uAgent);
	         }
		 }
		
	
	
	   @Test(alwaysRun = true)
	
		public void test09() throws Throwable {
		

	try{
				 		 			 		
		   TandF_FigureModule09 obj = new TandF_FigureModule09();
					obj.SelectSingle_Word_CopyPaste_nxtword();
				
				}catch (Exception e) {
					e.getStackTrace();
				              }
	                 }
	        }