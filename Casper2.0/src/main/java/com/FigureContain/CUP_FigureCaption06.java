package com.FigureContain;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.page.SymbolActionFuction;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_FigureCaption06 extends CUP_BaseClass{

	
	
	static WebElement ele=null;
	static String remark;
	static String className = "CUP_FigureCaption06";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
    
    
/*	public TandF_FigureModule06(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	

/*******
*6. Insert special character and place cursor to before character then press backspace key, tracking should be present
 * @throws Exception 
********/


public void insert_spl_chr_PlcCurserBefore_char_bckspce() throws Exception {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
  description = "Casper-CUP_CopyEditor-Insert special character and place cursor to before character then press backspace key, tracking should be present";	     
	     area = "Figure Contain";
	         category = "insert and deletion";
	             
		
try {
	
       System.out.println("BrowerName->"+uAgent);
       

       Cookies cokies =new Cookies(driver);
             cokies.cookies();
       
    Switch switc = new Switch(driver);
       switc.SwitchCase(uAgent);
        
	
	       PomObj= new Pom();
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	
          WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
              
          driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);    
          
              
              String value=ele.getText();
              
					if(value.contains(value)) {
						
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");			


	
	MoveToElement.byXpath(driver, PomObj.figure1());
	
	for(int i=0;i<2;i++){
						  ele.click();					       
	}	       
	
	
	for(int i=0;i<2;i++) {			  
  		 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
  			     }
	
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				
			
			for(int i=0;i<59;i++){
				  
				if(uAgent.contains("IE")) {
		    			Thread.sleep(300);
		    			}
				
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			}	
				
			
			SymbolActionFuction obj = new SymbolActionFuction(driver);
                obj.SymbolTest();
	 	          
                
                switc.SwitchCase(uAgent); 
                
	 	       
                WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
               WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
           	
 switch(uAgent) {

     	      case "edge":
     	        		      	             	     
     	       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);             	  
     	        		      	     break;
     	 }              
               
				   	MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_LEFT);
				                   
	 	                 
	 	          MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
	 	        	 	      
	 	         
	UpperToolBar ob = new UpperToolBar(driver);
	 	  	    ob.save_btn_method();
	 	  	    
	 	  	// core.HandleAlert.isAlertPresentAccept(driver);
	 	 	  	    
	                   }} catch (Exception e) {
	   					e.printStackTrace();
	    	           }
	               
try
{
	 Thread.sleep(3000);
		
             
  Switch switc = new Switch(driver);
     switc.SwitchCase(uAgent);
	
	 
	      WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
	   
	      
         WebElement val=driver.findElement(By.xpath(PomObj.figure1()));
		
               String afterSave =  val.getAttribute("innerHTML");
			     
				 String expectedValue1 = "n</del>";
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue4="ice-ins ice-cts";
		 
		 
		 System.out.println("After save - " + afterSave);
	
	
if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true&& afterSave.contains(expectedValue4)==true){
		 status="Pass";
		 
		 MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
					util.deleteRecFile(RecVideo);
		 	
		 }else{
			 
	//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", val);	 
			 
			   		 
	 	     status = "Fail";
	 	     
	 	    MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	 							util.MoveRecFile(RecVideo);
	 							
	 							
	 remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
  
	 	   Thread.sleep(10000);    
	 	    utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			
		  }
		 	if(afterSave.contains(expectedValue1)==true) {
		 	   System.out.println("E1: No issues");
		 	            }
		    if(afterSave.contains(expectedValue2)==true){
		 	   System.out.println("E2: No issues");
		 	       }
		    if(afterSave.contains(expectedValue3)==true) {
		 	   System.out.println("E3: No issues");
		     }
		    if(afterSave.contains(expectedValue4)==true) {
			 	System.out.println("E4: No issues");
		}
    }catch (Exception e) {
             e.getStackTrace();
    	   
	       }finally {
	    	   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
        }
  }
 

@Test(alwaysRun = true)

	public void CUPtest6() throws Throwable {
	


try{
	      MyScreenRecorder.startRecording(uAgent,className);
			 		 			 				 	
		   CUP_FigureCaption06 obj = new CUP_FigureCaption06();
			obj.insert_spl_chr_PlcCurserBefore_char_bckspce();
			
			
		}catch (Exception e) {
			e.getStackTrace();
		               }
		        }
       }
	

