package com.FigureContain;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_CE_FigureCaptionAddInLineEquation02 extends CUP_BaseClass {

	static String remark;
	static String className = "CUP_CE_FigureCaptionAddInLineEquation02";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;
	// private String currentDir = System.getProperty("user.dir");

	@Test(alwaysRun = true)
	public void FigureCaptionAddInline01() throws IOException {

		try {

			MyScreenRecorder.startRecording(uAgent, className);

			CUP_CE_FigureCaptionAddInLineEquation02 obj = new CUP_CE_FigureCaptionAddInLineEquation02();
			obj.AddInline02();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			System.out.println(className);
			Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
	}

	public void AddInline02() throws Exception {

		Ex = new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-Click on figure caption, then click on Edit Option go to the add section then click on the equation, Select the inline Equation and  add all symbol from Math and Save, check whether the inserted math symbol has displayed correctly or not";
		area = "Figure Caption";
		category = "Add inline Equtaion";

		
		try {

			
			driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);

			System.out.println("BrowerName->" + uAgent);

			Cookies cokies = new Cookies(driver);
			cokies.cookies();

			Switch switc = new Switch(driver);
			switc.SwitchCase(uAgent);

			PomObj = new Pom();

			WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());

			WebElement ele = driver.findElement(By.xpath(PomObj.figure1()));

			String value = ele.getAttribute("innerHTML");
			// System.out.println("check 1 : "+value);

			if (value.contains(value)) {

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", ele);
				((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

				MoveToElement.byXpath(driver, PomObj.figure1());

				for (int i = 0; i < 2; i++) {
					ele.click();
				}

				driver.switchTo().defaultContent();

				WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
				WebElement ele1 = driver.findElement(By.xpath(PomObj.Edit_Option()));

				MoveToElement.byXpath(driver, PomObj.Edit_Option());
				ele1.click();

				Thread.sleep(3000);

				WaitFor.presenceOfElementByXpath(driver, PomObj.LeftPanelAdd());
				WebElement Add = driver.findElement(By.xpath(PomObj.LeftPanelAdd()));

				MoveToElement.byXpath(driver, PomObj.LeftPanelAdd());
				Add.click();

				WaitFor.presenceOfElementByXpath(driver, PomObj.Equation());
				WebElement ele11 = driver.findElement(By.xpath(PomObj.Equation()));

				MoveToElement.byXpath(driver, PomObj.Equation());
				ele11.click();

				
				System.out.println("Check point for frame");
				driver.switchTo().frame("equation_iframe");
				System.out.println("Entered into frame");

				Actions act = new Actions(driver);
				driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
				WaitFor.presenceOfElementByXpath(driver, PomObj.InlineEquation());
				WebElement inline = driver.findElement(By.xpath(PomObj.InlineEquation()));

				new WebDriverWait(driver, 10).until(WebDriver -> ((JavascriptExecutor) WebDriver)
						.executeScript("return document.readyState").equals("complete"));

				act.moveToElement(inline).click(inline).build().perform();

				WebElement Math = driver.findElement(By.xpath(PomObj.MathFormula1()));
				act.moveToElement(Math).click(Math).build().perform();

				Thread.sleep(3000);
				WebElement Math1 = driver.findElement(By.xpath(PomObj.MathFormula2()));
				act.moveToElement(Math1).click(Math1).build().perform();

				WebElement MathMultyp = driver.findElement(By.xpath(PomObj.MathMultiply()));
				act.moveToElement(MathMultyp).click(MathMultyp).build().perform();

				Thread.sleep(3000);
				WebElement MathLim = driver.findElement(By.xpath(PomObj.MathLims()));
				act.moveToElement(MathLim).click(MathLim).build().perform();

				Thread.sleep(10000);

				driver.switchTo().defaultContent();

				WebElement SAveEQ = driver.findElement(By.xpath(PomObj.Save_Equation()));
				act.moveToElement(SAveEQ).click(SAveEQ).build().perform();

				Thread.sleep(3000);

				UpperToolBar ob = new UpperToolBar(driver);
				ob.save_btn_method();

				switc.SwitchCase(uAgent);

				WebElement web = driver.findElement(By.xpath(PomObj.EqLogo()));

				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", web);
				((JavascriptExecutor) driver).executeScript("window.scrollBy(250,350)");

				String Actual = web.getAttribute("innerHTML");
				System.out.println("Check1-->" + Actual);

				String Expected = "\\sqrt[]{}\\frac{\\partial }{\\partial }\\times\\lim_{ \\rightarrow }";

				/*
				 * SpecificElementScreenShot obj1 =new SpecificElementScreenShot(driver);
				 * 
				 * if(uAgent.contains("chrome")) { obj1.SpecificElementScrenShot(FigCitaion,
				 * uAgent, 315, "figCitation");
				 * 
				 * status = obj1.imageverification("/screenshots/chromefigCitation"); }
				 * 
				 * if(uAgent.contains("firefox")) { obj1.SpecificElementScrenShot(FigCitaion,
				 * uAgent, 295, "figCitation");
				 * 
				 * status = obj1.imageverification("/screenshots/firefoxfigCitation"); }
				 */

				if (Actual.contains(Expected) == true) {

					status = "Pass";

					MyScreenRecorder.stopRecording();
					String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
					util.deleteRecFile(RecVideo);

				} else {

					status = "Fail";

					MyScreenRecorder.stopRecording();
					String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);
					util.MoveRecFile(RecVideo);

					remark = "Add Equtaion didn't dislpayed properly";
					Thread.sleep(10000);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void isjQueryLoaded(WebDriver driver) {
		System.out.println("Waiting for ready state complete");
		(new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				JavascriptExecutor js = (JavascriptExecutor) d;
				String readyState = js.executeScript("return document.readyState").toString();
				System.out.println("Ready State: " + readyState);
				return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
			}
		});
	}
}