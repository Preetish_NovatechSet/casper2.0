package com.FigureContain;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class CUP_AddFigure_caption06 extends CUP_BaseClass{

	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	static WebElement ele=null;
	
	
	
	/*public TandF_FigureModule01(WebDriver ldriver) {
		TandF_FigureModule01.driver=ldriver;
	}*/
	
	
	
	   
	
	   /*****
	    * 1. Delete one character with Back space key press and insert special character, 
	    *       tracking should be present
	    *****/
	   
	  
	  
public void deleteOne_Char_with_backspace_insert_specialChar() throws Exception {
		  
		   try {
			   
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
    description = "Casper-CUP_AuthorEnd-Go to the new added figure caption, check the comment has added or not if not then add comment and save then click on comment icon press delete button check weather comment has been deleted or not";
			 className = "CUP_AddFigure_caption06";   
			         area = "Right panel Add figure";
			                category = "Delete comment";
			                      
		
			      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			                     
			              System.out.println("BrowerName->"+uAgent);
			              
			      Cookies cokies =new Cookies(driver);
			             cokies.cookies();
			              
			           Switch switc = new Switch(driver);
			              switc.SwitchCase(uAgent);
			   
			             
			                     PomObj= new Pom();	
			                     
			   ele= driver.findElement(By.xpath(PomObj.CommentVal()));
			   
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
		   
			      MoveToElement.byXpath(driver,PomObj.CommentVal());	     
			                 ele.click();
			                 
			                 
			           driver.switchTo().defaultContent();
			           

				   WaitFor.presenceOfElementByXpath(driver, PomObj.SumbitDilogBox());
			List<WebElement> delete= driver.findElements(By.xpath(PomObj.SumbitDilogBox()));		   		 
						   		 
			for(WebElement ele:delete)
			{
				String  value1 = ele.getText();
			
			
			 System.out.println("Checkval-->"+value1);
						 if(value1.contains("Delete")){
						   			
							 ele.click();
						   	}       
			}                     
			                     
			
			
			
			UpperToolBar s = new UpperToolBar(driver);
		       s.save_btn_method();
		 
		      
		             switc.SwitchCase(uAgent);
		       
		       
	   WaitFor.presenceOfElementByXpath(driver, PomObj.ValidationFig1());
		   ele = driver.findElement(By.xpath(PomObj.ValidationFig1()));
		  
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		  
			((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
		   
		   
		   MoveToElement.byXpath(driver, PomObj.ValidationFig1());
		   	
		          
		   		   String  Actualval = ele.getAttribute("innerHTML");
		             System.out.println("-->"+Actualval);
		 
		    String ExpectedVal="Hi how r u";
		             
		 if(Actualval.contains(ExpectedVal)==false) {
			 
			 status="Pass";
			 
		 }else {
		
			 status="Fail";
		 
			 remark="Unable to delete the comment";		 
				Thread.sleep(10000);
				
					utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			 
		 }
			                     
			         				     }catch(Exception e){
				    	 e.getStackTrace();
				    	   }finally{
	  
	     	   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	       }
     }
	  
	  

	  
	   @Test(alwaysRun = true)	
	 public void CUPAddfiguretest6() throws Throwable {
		
	try{
				 		 			 				 	    	     
		CUP_AddFigure_caption06 obj = new CUP_AddFigure_caption06();
		  obj.deleteOne_Char_with_backspace_insert_specialChar();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }
		 }		  
     }