package com.FigureContain;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.EditGenieTest.FuntionForLinkGenration;
import com.linkGenrationModule.LinkGerationTest;
import utilitys.BrowserFactory;
import utilitys.ConstantPath;



public class PPL_LinkGentration{
	


	
	WebDriver driver;
	final static String PublisherName="PPL";
	final static String PPLpath=FuntionForLinkGenration.getXmlprojectpath()+"/CasperPPL\\BCJ-2018-0685/";
	
	
	
	//**Chrome**//
@Test(alwaysRun=true)
		
public void PPLChromeFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,PPLpath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WritePPlUrlChrome(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					  ChromePPLFigureContain();
				}		
	        }
public void ChromePPLFigureContain() throws Exception {
			try {
				driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,PPLpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WritePPlUrlChrome(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				ChromePPLFigureContain();				
			}		
         }	

/***************************************************************************************************/
      //**Firefox**//

@Test(alwaysRun=true)
  public void PPLFirefoxFigureContain() throws Exception {
				try {
		driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
     		LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,PPLpath);
					
				String TestLinkUrl= driver.getCurrentUrl();
		com.FigureContain.WritePropertiesFile.WritePPlUrlFirefox(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					    firefoxPPLFigureContain();
				}		
	       }
public void firefoxPPLFigureContain() throws Exception {
			try {
			driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
	    		LinkGerationTest url = new LinkGerationTest(driver);
		    	url.LoginLinkGerationTest(PublisherName,PPLpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WritePPlUrlFirefox(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				 firefoxPPLFigureContain();				
			}		
         }			
/***************************************************************************************************/

@AfterMethod(alwaysRun=true)

public void TearDown(){
	
	try {
		Thread.sleep(6000);
		   driver.quit();
	} catch (InterruptedException e) {
		e.printStackTrace();
	       }
	
       }
  }	
   
