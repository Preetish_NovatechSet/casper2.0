package com.FigureContain;


import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class CUP_AddFigure_caption07 extends CUP_BaseClass{

	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	static WebElement ele=null;
	
	
	
	/*public TandF_FigureModule01(WebDriver ldriver) {
		TandF_FigureModule01.driver=ldriver;
	}*/
	
	
	
	   
	
	   /*****
	    * Go to the new added figure caption right click on added image then click on comment 
	    *         then add some text, check whether comment has been added aur not
	    *****/
	   
	  
	  
public void deleteOne_Char_with_backspace_insert_specialChar() throws Exception {
		  
		   try {
			   
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
    description = "Casper-CUP_AuthorEnd-Go to the new added figure caption right click on image then click on delete, check whether figure Caption & image has been deleted or not";
			 className = "CUP_AddFigure_caption07";    
			         area = "Right panel added figure";
			                category = "Delete the added figure & image";
			                      
		
			   driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			                     
			              System.out.println("BrowerName->"+uAgent);
			              
			      Cookies cokies =new Cookies(driver);
			             cokies.cookies();
			              
			           Switch switc = new Switch(driver);
			             switc.SwitchCase(uAgent);
			   
			             
			                     PomObj= new Pom();	
			                     
			                     
			      ele= driver.findElement(By.xpath(PomObj.Imagefig1()));  
	    
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			    
	     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,100)");	  
			      
			    
	     
	     driver.switchTo().defaultContent();
	     Robot robot = new Robot();
	     
	     robot.mouseMove(200, 200);

         // Press the mouse button #1.
         robot.mousePress(InputEvent.BUTTON1_MASK);
         robot.mouseRelease(InputEvent.BUTTON1_MASK);

	     
	     
         robot.mousePress(InputEvent.BUTTON3_MASK);
         robot.mouseRelease(InputEvent.BUTTON3_MASK);
         
            Thread.sleep(3000);	
           robot.keyPress(KeyEvent.VK_DOWN);
              Thread.sleep(3000);
           robot.keyRelease(KeyEvent.VK_DOWN);
         
           Thread.sleep(5000);	
         robot.keyPress(KeyEvent.VK_ENTER);
            
         
          Thread.sleep(5000);	
          
          
/*          System.out.println("Check -1");
       robot.mouseMove(780, 150);
      		 
       robot.mousePress(InputEvent.BUTTON1_MASK);
       robot.mouseRelease(InputEvent.BUTTON1_MASK);
       
       Thread.sleep(2000);*/
          
          
          
          if(isAlertPresent()){
        	  Alert alert = driver.switchTo().alert();
        	  System.out.println(alert.getText());
        	  alert.accept();
        	 }
          
          
          
       
       robot.mouseMove(1000, 140); 
       
       
	 
        robot.mousePress(InputEvent.BUTTON1_MASK);
       robot.mouseRelease(InputEvent.BUTTON1_MASK);
		
       
       Thread.sleep(2000);
			 driver.navigate().refresh();      
			       
			      switc.SwitchCase(uAgent);
			       
			      
			   WaitFor.presenceOfElementByXpath(driver, PomObj.DeleteFig1Cup());
				   ele = driver.findElement(By.xpath(PomObj.DeleteFig1Cup()));      
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");      
			      
			      
			       
		   WaitFor.presenceOfElementByXpath(driver, PomObj.DeleteFigValidation());
			   ele = driver.findElement(By.xpath(PomObj.DeleteFigValidation()));
			  	   				          
			   		   String  Actualval = ele.getAttribute("innerHTML");
			              System.out.println("-->"+Actualval);
			 
	String ExpectedVal="We can tell the tool to wait only till the Condition satisfy.";
			             
			 if(Actualval.contains(ExpectedVal)==true) {
				 status="Pass";
			 }else {
			
				 status="Fail";
				 
				 
				 remark="Unable to delete the figure caption and figure";		 
					Thread.sleep(10000);
					
			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			 
			 }
			                    
			              
			         
				     }catch(Exception e){
				    	 e.getStackTrace();
				    	   }finally{
	  
	     	   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	       }
     }
	  
	  

	  
	   private boolean isAlertPresent() {
		   try{
			
			   driver.switchTo().alert();
			
			   return true;
	
			           }catch(NoAlertPresentException ex){
		
			   return false;
		
			   }
	     }




	@Test(alwaysRun = true)	
	 public void CUPAddfiguretest7() throws Throwable {
		

 
	try{
				 		 			 				 	    	     
		CUP_AddFigure_caption07 obj = new CUP_AddFigure_caption07();
		  obj.deleteOne_Char_with_backspace_insert_specialChar();
	
			 }catch (Exception e){
				e.getStackTrace();
			 }
		 }		  
     }