package com.FigureContain;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.EditGenieTest.FuntionForLinkGenration;
import com.linkGenrationModule.LinkGerationTest;

import utilitys.BrowserFactory;
import utilitys.ConstantPath;

public class CUP_LinkGenration {

	WebDriver driver;
	final static String PublisherName="CUP";
	final static String CUPpath=  FuntionForLinkGenration.getXmlprojectpath()+"/CasperCUP/MRC1800202/";
	//final static String CUPpath=  FuntionForLinkGenration.getXmlprojectpath()+"/CasperCUP/HYG/";
	
	
  /***************************************************************************************/
                            /***Casper-CopyEditor-EndCUP***/
  /***************************************************************************************/
	
	
	//**Chrome**//
	
	
  @Test(alwaysRun=true)
		
public void CUPChromeFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CUPpath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WriteCUPUrlChrome(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					  ChromeCUPFigureContain();
				}		
	        }


public void ChromeCUPFigureContain() throws Exception {
			try {
				driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CUPpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WriteCUPUrlChrome(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				ChromeCUPFigureContain();				
			  }		
         }	
	
	
	
/************************************************************************************************/
    //**Firefox**//

@Test(alwaysRun=true)
public void CUPFirefoxFigureContain() throws Exception {
				try {
		driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
   		LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CUPpath);
					
				String TestLinkUrl= driver.getCurrentUrl();
		com.FigureContain.WritePropertiesFile.WriteCUPUrlFirefox(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					    firefoxCUPFigureContain();
				}		
	       }

public void firefoxCUPFigureContain() throws Exception {
			try {
			driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
	    		LinkGerationTest url = new LinkGerationTest(driver);
		    	url.LoginLinkGerationTest(PublisherName,CUPpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WriteCUPUrlFirefox(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				 firefoxCUPFigureContain();				
			}		
       }		

	
	
      
 /***************************************************************************************/
                              /***CasperAuthorEndCUP***/
 /***************************************************************************************/

//Chrome

   @Test(alwaysRun=true)
 
public void ChromeCUPAuthorEndFigureContain() throws Exception {
	try {
		driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
		LinkGerationTest url = new LinkGerationTest(driver);
		url.LoginLinkGerationTest(PublisherName,CUPpath);
		
		String TestLinkUrl= driver.getCurrentUrl();
		com.FigureContain.WritePropertiesFile.WriteCUPAEUrlChrome(TestLinkUrl);
	} catch (Exception e) {
		 driver.quit();
		    ChromeCUPAEFigureContain();				
	}		
 }



public void ChromeCUPAEFigureContain() throws Exception {
			try {
				driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CUPpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WriteCUPAEUrlChrome(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				    ChromeCUPAEFigureContain();				
			  }		
         }




//Firefox
@Test(alwaysRun=true)
public void CUPFirefoxAEFigureContain() throws Exception {
				try {
		driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
   		LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CUPpath);
					
				String TestLinkUrl= driver.getCurrentUrl();
		com.FigureContain.WritePropertiesFile.WriteCUPAEUrlFirefox(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					    firefoxCUPAEFigureContain();
				}		
	       }

public void firefoxCUPAEFigureContain() throws Exception {
			try {
			driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
	    		LinkGerationTest url = new LinkGerationTest(driver);
		    	url.LoginLinkGerationTest(PublisherName,CUPpath);
				
				String TestLinkUrl= driver.getCurrentUrl();
				com.FigureContain.WritePropertiesFile.WriteCUPAEUrlFirefox(TestLinkUrl);
			} catch (Exception e) {
				 driver.quit();
				 firefoxCUPAEFigureContain();				
			}		
       }		

/***************************************************************************************************/

@AfterMethod(alwaysRun=true)

public void TearDown(){
	
	try {
		Thread.sleep(6000);
		   driver.quit();
	} catch (InterruptedException e) {
		e.printStackTrace();
	       }
	
       }
  }	
   

