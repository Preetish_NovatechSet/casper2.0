package com.FigureContain;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class TandF_FigureModule18 extends TandFBaseClass {
	
	/**
	 *Single word select and "Control + x" then "Control + v" in next next word, check delete and insert tracking and data lose
	 **/
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	

	public void Cut_ThenPaste_nxt_nxtWord() throws Exception{
		
		    try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-TandF_AuthorEnd-Single word select and Control + x then Control + v in next next word, check delete and insert tracking and data lose";
		className = "TandF_FigureModule18";    
			area = "Figure Contain";
				category = "Cut and Paste";
				      
				
				  Cookies cokies =new Cookies(driver);
			          cokies.cookies();
	                
	      Switch switc = new Switch(driver);
	          switc.SwitchCase(uAgent);  		    
				      
				  PomObj= new Pom();	
		   			  
		 		     WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
		 						
		 				   WebElement ele=driver.findElement(By.xpath(PomObj.figure1()));
					
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
						
			((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
					
			String value = ele.getText();
			
			if(value.contains(value)) {
   
				MoveToElement.byXpath(driver, PomObj.figure1());
				
				for(int i=0;i<2;i++){    
				ele.click();
				}
				
				
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				
				
				for(int i=0;i<22;i++){
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			}
				     
				MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 4,Keys.ARROW_RIGHT);
				
				
				if(uAgent.equals("IE")) {
				
				 Robot robot = new Robot();
			     robot.keyPress(KeyEvent.VK_CONTROL);
			     robot.keyPress(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_CONTROL); 
				
				}else {
				
					MoveToElement.cut(driver);
				
				}
				
				for(int i=0;i<20;i++) {
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
				   }
				
				
				
				if(uAgent.equals("IE")) {
	/*((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");			
			       core.HandleAlert.isAlertPresentAccept(driver);		*/
					
					
					 Robot robot = new Robot();
				     robot.keyPress(KeyEvent.VK_CONTROL);
				     robot.keyPress(KeyEvent.VK_V); 
				     robot.keyRelease(KeyEvent.VK_V); 
				     robot.keyRelease(KeyEvent.VK_CONTROL); 	
										
				}else {
				
				MoveToElement.paste(driver);
				
		}
				
		UpperToolBar obj = new UpperToolBar(driver);
				obj.save_btn_method();
				
				//HandleAlert.isAlertPresentAccept(driver);
				
				}}catch (Exception e) {
					 e.getStackTrace();
				}
		
	try 
	{			
		Thread.sleep(3000);
		
Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent);  	
		 
				WaitFor.presenceOfElementByXpath(driver, PomObj.figure1());
					
				   WebElement ele1=driver.findElement(By.xpath(PomObj.figure1()));
				     
				     String afterSave =  ele1.getAttribute("innerHTML");
					
					
				
					//String beforeSave = "Effect of biomass concentration on plateau permeate flux in dynamic filtration of T. suecica.";
					
				     String expectedValue1 = "Task</ins>";
					 String expectedValue2 = "Task</del>";
					 String expectedValue3 = "ice-del ice-cts";
					 String expectedValue4="ice-del ice-cts";

					 System.out.println("After save - " + afterSave);



if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true && afterSave.contains(expectedValue4)==true){
					// if(!beforeSave.equals(actualValue) && afterSave.contains(expectedValue1) && afterSave.contains(expectedValue2)){
					
					 	status="Pass";
					 		 	 
					 }else{
						

//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		

status = "Fail";

remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";

Thread.sleep(10000);
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);

					 }
					 	 
					 	
					 if(afterSave.contains(expectedValue1)==true) {
					 	    System.out.println("E1: No issues");
					 	}
					 if(afterSave.contains(expectedValue2)==true){
					 	    System.out.println("E2: No issues");
					 	}
					 if(afterSave.contains(expectedValue3)==true){
						 	System.out.println("E3: No issues");
						 	}
					 if(afterSave.contains(expectedValue3)==true){
					 	    System.out.println("E4: No issues");
					 	   }
					    }catch (Exception e) {
			    	
					   e.printStackTrace();
					   
						    }finally {
				 System.out.println(className);
		Ex.testdata(description, className, remark, category, area, status, uAgent);
			}
     }
	
	
	
	
	   @Test(alwaysRun = true)
	
		public void test18() throws Throwable {
		
	try{
				 		 			 				 	
 
	   TandF_FigureModule18 obj = new TandF_FigureModule18();
	        obj.Cut_ThenPaste_nxt_nxtWord();
	  
	   
				}catch (Exception e) {
					e.getStackTrace();
				              }
	                   }
               }

