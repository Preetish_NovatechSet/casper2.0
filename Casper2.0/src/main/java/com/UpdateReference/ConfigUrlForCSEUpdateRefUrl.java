package com.UpdateReference;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigUrlForCSEUpdateRefUrl {

static Properties pro;
	
	
	public static void ReadPath(String PathofXML)
	{
		
		String sourcePath = PathofXML;
		
		File src=new File(sourcePath);
		
		try 
		{
		FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		 
		 {
			System.out.println("Exception is "+e.getMessage());
		 }
		
	   }
	
	
	
public String geturlForChrome()
	{
			
	ConfigUrlForCSEUpdateRefUrl.ReadPath("./Configfolder\\CseUpadteRef\\Chrome.properties");
	
		String Table2_Aspirations=pro.getProperty("Chrome");
	 	return Table2_Aspirations;
	}
 

public String geturlForFireFox()
{
	
	ConfigUrlForCSEUpdateRefUrl.ReadPath("./Configfolder\\CseUpadteRef\\Firefox.properties");
		
	  String url=pro.getProperty("FireFox");
	  return url;
}


public String geturlForIE()
{

	ConfigUrlForCSEUpdateRefUrl.ReadPath("./Configfolder\\CseUpadteRef\\IE.properties");
	
String url=pro.getProperty("IE");
return url;
}


public String geturlForOpera(){

	ConfigUrlForCSEUpdateRefUrl.ReadPath("./Configfolder\\CseUpadteRef\\Opera.properties");

String url=pro.getProperty("Opera");
return url;
}


public String geturlForEdge(){

	ConfigUrlForCSEUpdateRefUrl.ReadPath("./Configfolder\\CseUpadteRef\\Edge.properties");
	
String url=pro.getProperty("Edge");
return url;
     }
}
