package com.UpdateReference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseOtherReference04 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether the 2 author name can delete 1 author name in Others reference and check the style displayed correctly or not";
		 className = "UpdateCseOtherReference04";    
				area = "Update Other Reference";
					category = "CSE Style";
					                				                   
					
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		             switc.SwitchCase(uAgent);
				
		          PomObj = new Pom();
		          
		          
		          ele = driver.findElement(By.xpath(PomObj.ele_1()));
			      
			  		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			  					
			  					MoveToElement.byXpath(driver, PomObj.ele_1());    
			  					          ele.click();
		  						
		  						 driver.switchTo().defaultContent();
		  						 
		  			ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
	  					          
	  					   MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
	  				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
	  					          ele.click();
	  					     
	  					          
	  		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
	  		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
	                       dropdown.selectByVisibleText("Others");
	                                 
	              	   
	  				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
	  				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));	 
	  				     ele.click();ele.sendKeys("1957");
								    
		  						 
		  						 
		  						 
		  						 
								    ele=driver.findElement(By.xpath(PomObj.Surname1()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
										ele.click(); ele.sendKeys("Reddy");      
								          
										
			ele = driver.findElement(By.xpath(PomObj.given1()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
			ele.click();
			ele.sendKeys("N");
			
			
			ele=driver.findElement(By.xpath(PomObj.Surname2()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
						ele.click(); ele.sendKeys("AA"); 			
					
				
								
						ele=driver.findElement(By.xpath(PomObj.given2()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
				ele.click(); ele.sendKeys("Mahato"); 

			ele = driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
			ele.click();
			ele.sendKeys("1957");

			
			ele = driver.findElement(By.xpath(PomObj.Other_Info()));
		 WaitFor.clickableOfElementByXpath(driver, PomObj.Other_Info());
			ele.click(); ele.sendKeys("A World of Opportunities");
					
		  						
		   ele=driver.findElement(By.cssSelector(PomObj.Save()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
								ele.click();	
								
								
					
								
								 switc.SwitchCase(uAgent);			
	    ele=driver.findElement(By.cssSelector(PomObj.reference14()));
   WaitFor.clickableOfElementByCSSSelector(driver, PomObj.reference14());
						ele.click();							
								
								
						 driver.switchTo().defaultContent();
						
					ele=driver.findElement(By.xpath(PomObj.Surname2()));
				WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
							ele.click(); ele.clear(); 			
						
					
									
				ele=driver.findElement(By.xpath(PomObj.given2()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
					     ele.click(); ele.clear();

					     
    ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
						ele.click();	
		  							              
    	}catch(Exception e){
            e.printStackTrace();
              }

try {
	
	Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent);

								       
 
	String Actualvalue =null;
	String ListOfJournal = null;
	String ExpectedValue="Reddy N. 1957. A World of Opportunities."; 
	 
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.Ref14());
	List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref14()));

	
	             for(WebElement ele1:el){
	        	   Actualvalue =ele1.getText();
	                	
	             ListOfJournal+=Actualvalue + "\n";
	   	System.out.println("PKM--->"+ListOfJournal);
	         	   
				    }
	             
	     if(ListOfJournal.contains(ExpectedValue)==true) {
	        	  status ="Pass";
	        	  
	        }else{
	        	
remark="Cse Style is incorrect while Updating the Other reference";	
     		    	 
	          		 status ="Fail"; 
	        }
	     
} catch (Exception e) {
	e.printStackTrace();
}finally {
	        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
}
						

        }
    	
        
    	 
    	@Test(alwaysRun=true)
    	         	
    			public void test4() throws Exception {
    			         	
    		UpdateCseOtherReference04 bj = new UpdateCseOtherReference04();
    			         	         bj.CSE();
    			         	
    			         	   }
    	               }