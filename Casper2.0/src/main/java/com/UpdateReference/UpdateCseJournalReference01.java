package com.UpdateReference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class UpdateCseJournalReference01 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE -Check whether the 1 author name reference can add 10 author name and check the style displayed correctly or not";
		 className = "UpdateCseJournalReference01";    
				area = "Update Journal Reference";
					category = "CSE Style";
					                				                   
					
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		             switc.SwitchCase(uAgent);
				
		          PomObj = new Pom();
		          
		          
	 WebElement lwe = driver.findElement(By.cssSelector(PomObj.reference7()));
		  						  					
		  						lwe.click();
		  						
		  						 driver.switchTo().defaultContent();
		  						 
		  				 
								    
								    ele=driver.findElement(By.xpath(PomObj.Surname2()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
										ele.click(); ele.sendKeys("tu");      
								          
										
										
								ele=driver.findElement(By.xpath(PomObj.given2()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
									    ele.click(); ele.sendKeys("Narayan");  
								    
									    ele=driver.findElement(By.xpath(PomObj.Surname3()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
											ele.click(); ele.sendKeys("Ra");      
									          
											
											
					    ele=driver.findElement(By.xpath(PomObj.given3()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
						ele.click(); ele.sendKeys("sad");  
						
						
						for(int i=0;i<=7;i++) {
						 ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
			                              ele.click(); 
						
						}
						
						
						
						ele=driver.findElement(By.xpath(PomObj.Surname4()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
							ele.click(); ele.sendKeys("Rj");      
					          
							
							
		ele=driver.findElement(By.xpath(PomObj.given4()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
		ele.click(); ele.sendKeys("Pra"); 
						
					


		ele=driver.findElement(By.xpath(PomObj.Surname5()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
			ele.click(); ele.sendKeys("Rani");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given5()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
		ele.click(); ele.sendKeys("Pa"); 

		ele=driver.findElement(By.xpath(PomObj.Surname6()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname6());
			ele.click(); ele.sendKeys("Hj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given6()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given6());
		ele.click(); ele.sendKeys("Pra"); 


		ele=driver.findElement(By.xpath(PomObj.Surname7()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname7());
			ele.click(); ele.sendKeys("Dj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given7()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given7());
		ele.click(); ele.sendKeys("Pra"); 

		ele=driver.findElement(By.xpath(PomObj.Surname8()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname8());
			ele.click(); ele.sendKeys("Rj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given8()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given8());
		ele.click(); ele.sendKeys("A"); 


		ele=driver.findElement(By.xpath(PomObj.Surname9()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname9());
			ele.click(); ele.sendKeys("Rj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given9()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given9());
		ele.click(); ele.sendKeys("Pra"); 



		ele=driver.findElement(By.xpath(PomObj.Surname10()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
			ele.click(); ele.sendKeys("Rj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given10()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
		ele.click(); ele.sendKeys("P"); 


		ele=driver.findElement(By.xpath(PomObj.Surname11()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
			ele.click(); ele.sendKeys("Rj");      
		      
			
			
		ele=driver.findElement(By.xpath(PomObj.given11()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
		ele.click(); ele.sendKeys("R"); 					
		  						
		ele=driver.findElement(By.cssSelector(PomObj.Save()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
								ele.click();		
								
								
								
		  							              
    	}catch(Exception e){
            e.printStackTrace();
              }

try {
	
	Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent);

								       
 
	String Actualvalue =null;
	String ListOfJournal = null;
	String ExpectedValue="Karandish M., tu Narayan, Ra sad, Rj Pra, Rani Pa, Hj Pra, Dj Pra, Rj A, Rj Pra, Rj P, Rj R. 2014. The efficiency of laser application on the enamel surface: a systematic review. J Lasers Med Sci. 5:108�114."; 
	 
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.Ref07());
	List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref07()));

	
	             for(WebElement ele1:el){
	        	   Actualvalue =ele1.getText();
	                	
	             ListOfJournal+=Actualvalue + "\n";
	   	System.out.println("PKM--->"+ListOfJournal);
	         	   
				    }
	             
	     if(ListOfJournal.contains(ExpectedValue)==true) {
	        	  status ="Pass";
	        	  
	        }else{
	        	
remark="Cse Style is incorrect while Updating the Journal reference/ Cse Style Journal is not Working";	
     		    	 
	          		 status ="Fail"; 
	        }
	     
} catch (Exception e) {
	e.printStackTrace();
}finally {
	        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
}
						

        }
    	
        
    	 
    	@Test(alwaysRun=true)
    	         	
    			public void test1() throws Exception {
    			         	
    		   UpdateCseJournalReference01 bj = new UpdateCseJournalReference01();
    			         	         bj.CSE();
    			         	
    			         	   }
    	               }


