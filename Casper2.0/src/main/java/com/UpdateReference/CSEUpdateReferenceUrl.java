package com.UpdateReference;

import utilitys.DataProviderFactory;

public class CSEUpdateReferenceUrl {
	
	
	
/************************************************************************************************/	
	public static class FireFoxCseUpdateRefUrl{
			
		public static String url;
			 
				 public static String getFireFoxUrl() {
		         url=  DataProviderFactory.getCSCStyleUpdateReferanceUrl().geturlForFireFox();
				         

					 return url;
				    }
				}


/************************************************************************************************/

	public static class IECseUpdateRefUrl{
			  public static String url;
			 
				 public static String getIEUrl() {
				         url=  DataProviderFactory.getCSCStyleUpdateReferanceUrl().geturlForIE();
				         

					 return url;
				    }
				}

	/************************************************************************************************/	

	public static class ChromeCseUpdateRefUrl {	
				
		public static  String url;
								
				 	
			public static String getChromeUrl() {
					      url= DataProviderFactory.getCSCStyleUpdateReferanceUrl().geturlForChrome();
					      

				    return url;
				     }  
				
			    }
			
	/************************************************************************************************/
			
	public static class OperaCseUpdateRefUrl {	
				
				public static  String url;
				 	
				public static String getOperaUrl() {
					      url=  DataProviderFactory.getCSCStyleUpdateReferanceUrl().geturlForOpera();
					      
				    return url;
				     }  
				
			    }

	/************************************************************************************************/

	public static class EdgeCseUpdateRefUrl {	
		
		public static  String url;
		 	
		public static String getEdgeUrl() {
			      url=  DataProviderFactory.getCSCStyleUpdateReferanceUrl().geturlForEdge();
			      
		    return url;
		     }  
		
	    }


	/************************************************************************************************/

  }


