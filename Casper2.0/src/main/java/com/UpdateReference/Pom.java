package com.UpdateReference;

public class Pom {
	
	
		
	private static String element = null;

public String AddAuthor1 () {
        
    	element="//div[@class='shownexteditor btn col-sm-6']";
	
	 return element;
    }
	
	
	
public String ArticleTitle () {
        
    	element="//div[@class='col-sm-12']//div[@placeholder='Required']";
	
	 return element;
    }
	
public String JournalTitle() {
    
	element="//div[@class='col-sm-6']//div[@placeholder='Required']";

 return element;
}


public String ele_1() {
    
	element="//div[contains(text(),'There were some limitations associated with this s')]";

 return element;
}


public String Volume() {
    
	element="//input[@name='volume_text']";

 return element;
}

public String FirstPage() {
	element="//input[@name='fpage_text']";

	 return element;
	
  }

public String LastPage() {
	element="//input[@name='lpage_text']";

	 return element;
	
   }


public String Year() {
    
	element="//input[contains(@placeholder,'Example :1974 / In Press / Forthcoming')]";

 return element;
}

public String PopupValidation() {
	element="//div[@class='sweet-alert showSweetAlert visible']";
	
	return element;
}
 	 
		 public String Karandish() {
	  element="//surname[contains(text(),'Karandish')]";		 
			       return element;
		 }
		 
		 
		 public String Save() {
				element="#addButton";

				 return element;
				
			   }
		 
		 public String Comment() {
				element="//input[@name='comment_text']";

				 return element;
				
			   }
		
	
		 
		 
		 public String ReferenceType(){		
			 element="//select[@id='selectbox']";		 
			       return element;
		 }
		
	public String In_text_Citation(){
			 element="#ref_cited_txt_insert";		 
		         return element;
		 }
		 
		 
		 
		 public String ClickReferenceBar(){
	  element="//span[@class='cke_button_icon cke_button__reference_icon']";		 
		       return element;
		 }
		 
		 
		 public String reference7(){
	          element="#CIT0007 > span.edit_ref_tooltip > img";		 
		               return element;
		 }
		 
		 
		 public String reference8(){
			  element="#CIT0008 > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		 
		 
		 public String reference12(){
			  element="#CIT0012 > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		 
		 
		 public String reference14(){
			  element="#CIT0014 > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		 
	
		 
		 public String reference_8(){
			  element="#CIT0008 > mixed-citation > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		 
		 
		 public String reference24(){
			 element="#CIT0024 > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		 
		 public String reference25(){
			 element="#CIT0025 > span.edit_ref_tooltip > img";		 
				       return element;
				 }
		
		 
		 public String et_al(){
		    	
			    element="//input[@value='et al.']";
			    	return element;
				 }
		
		 
		 public String Editor_Name1() {
			 
			element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_0']";		 
		       return element;
			
		 }
		 
		 public String Editor_Name2() {
			 
		    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_1']";		 
			       return element;
				
			 }
		 
		 public String Editor_Name3() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_2']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name4() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_3']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name5() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_4']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name6() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_5']";		 
				       return element;
					
				 }

		 
		 public String Editor_Name7() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_6']";		 
				       return element;
					
				 }
		 
		 
		 public String Editor_Name8() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_7']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name9() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_8']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name10() {
			 
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_9']";		 
				       return element;
					
				 }
		 
		 public String Editor_Name11() {
			            
			    element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_10']";		 
				       return element;
					
				 }
		 
		 public String Book_Title(){
		     
			 	   element="//div[@class='col-sm-6']//div[@placeholder='Required']";
				
				return element;
			 }
		 
		 
		 public String Other_Info(){
		     
		 	   element="//div[@name='comment_text']";
			
			return element;
		 }
		 
		
		 
		 
		 
		 public String ConferenceTitle(){
			    
		 	   element="//div[@placeholder='Required']";
			
			return element;
		 }
		 
		 public String 	 Conf_Name (){
			    
		 	   element="//input[@name='conf-name_text']";
			
			return element;
		 }

		 public String ok (){
			    
		 	   element=".confirm";
			
			return element;
		 }
		 
		 public String 	 Conf_Place(){
			    
		 	   element="//input[@name='conf-loc_text']";
			
			return element;
		 }
		 
		 public String 	 Conf_Date(){
			    
		 	   element="//input[@name='conf-date_text']";
			
			return element;
		 } 
	
		 
		 public String Chapter_Title(){
		    
		 	   element="//div[@class='col-sm-12']//div[@placeholder='Required']";
			
			return element;
		 }
		 
		
		 public String Publisher(){
		     
			 	element="//input[@name='publisher-name_text']";
				
				return element;
			 }
		 
		 
		 public String City(){
		     
			 	element="//input[@name='tf:city_text']";
				
				return element;
			 }
		 
		 
		 public String Surname1(){
			
			element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_0']";		 
			       return element;
		 }
		 
		 public String given1() {		 
			
			 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_0']";
			     return element;
		 }
		 
	public String Surname2(){
		    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_1']";
			 return element;
		 }
		 
	public String given2() {
		  element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_1']";
			 return element;
		 }
	     
	public String Surname3() {
	    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_2']";
	    	  return element;
	    		 }
	    		 
	public String given3() {
	   element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_2']";
	    			 return element;
	    		 }
	    
	 public String Surname4() {
			element=".//*[@id='TextBoxesGroup']/div[1]/div[4]/div[1]/div/div[1]/input";
				 return element;
			 }
			 
	public String given4() {
				 
				 element=".//*[@id='TextBoxesGroup']/div[1]/div[4]/div[2]/div/div/input";
				 return element;
			 }

	public String Surname5() {
			element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_4']";
				 return element;
			 }
			 
	public String given5() {
				 
			element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_4']";
				 return element;
			 }
	    
	public String Surname6() {
		element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_5']";
			 return element;
		 }
		 
	public String given6() {
			 
		element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_5']";
			 return element;
		 }


	public String Surname7() {
		element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_6']";
			 return element;
		 }
		 
	public String given7() {
			 
			 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_6']";
		return element;
		 }


	public String Surname8() {
		    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_7']";
	   return element;
		 }
		 
	public String given8() {
			 
			 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_7']";
		return element;
		 }


	public String Surname9() {
		       element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_8']";
		return element;
		 }
		 
	public String given9() {
			 
			 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_8']";
			 return element;
		 }

	public String Surname10() {
		element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_9']";
			 return element;
		 }
		 
	public String given10() {
			 
		element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_9']";
			 return element;
		 }

	public String Surname11() {
		element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_10']";
			 return element;
		 }
		 
	public String given11() {
			 
		element = "//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_10']";
			 return element;
		 }
		 
	 public String AddAuthor(){
	        
	    	element = "//div[@class='col-sm-6 spaceforbutton']";
		
		return element;
	    }
	 
	 public String Ref07(){
	        
	    	element = "//ref[@id='CIT0007']";
		
		return element;
	    }
	 
	 
	 public String Ref14(){
	        
	    	element = "//ref[@id='CIT0014']";
		
		return element;
	  }
	 
	
	 public String Ref12(){
	        
	    	element = "//ref[@id='CIT0012']";
		
		return element;
	    }
	 
	 public String Ref08(){
	        
	    	element = "//ref[@id='CIT0008']";
		
		return element;
	    }
	 
	 public String Ref11(){
	        
	    	element = "//ref[@id='CIT0011']";
		
		return element;
	    }
	 public String Ref25(){
	        
	    	element = "//ref[@id='CIT0025']";
		
		return element;
	    }
	 
	 public String Collab() {
		    
		    element="//input[@name='collabgroup_collabgroup']";
			
			 return element;
		    }
		 
}
