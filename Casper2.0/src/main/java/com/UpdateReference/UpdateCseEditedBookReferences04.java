package com.UpdateReference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseEditedBookReferences04 extends UpdateBaseClass{
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;
    
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE -Check whether the Chapter title can delete and save, Check weather the Chapter title pop has displayed or not";
		 className = "UpdateCseEditedBookReferences04";    
				area = "Update Edited book Reference";
					category = "CSE Style";
					                				                   
					
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		             switc.SwitchCase(uAgent);
				
		          PomObj = new Pom();
		          
		          
	      WebElement lwe = driver.findElement(By.xpath(PomObj.ele_1()));
	 
	 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", lwe);	  						  					
		  					MoveToElement.byXpath(driver, PomObj.ele_1());	 
	                                    lwe.click();
		  						
		  					driver.switchTo().defaultContent();
		  						 
		  					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                   dropdown.selectByVisibleText("Edited Book");
                             
          	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));		
				     ele.click();ele.sendKeys("1996");
				 
				     
				     
				     ele=driver.findElement(By.xpath(PomObj.Surname1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
								ele.click(); ele.sendKeys("Preetish");      
								          
										
										
				ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
							   ele.click(); ele.sendKeys("R");  
							   
							   
							   ele=driver.findElement(By.xpath(PomObj.Surname2()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
									ele.click(); ele.sendKeys("Raj");      
									          
											
											
					ele=driver.findElement(By.xpath(PomObj.given2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
								   ele.click(); ele.sendKeys("R"); 
								    
					
				 ele=driver.findElement(By.xpath(PomObj.Editor_Name1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name1());
									ele.click(); ele.sendKeys("Ravi");      
									          
							
									
									ele=driver.findElement(By.xpath(PomObj.Chapter_Title()));
									   WaitFor.clickableOfElementByXpath(driver, PomObj.Chapter_Title());
														ele.click(); ele.sendKeys("Guns and thungs");  		
									
														   
					
						ele=driver.findElement(By.xpath(PomObj.Book_Title()));
					ele.click(); ele.sendKeys("The Piper at the Gates of Dawn");	
									
						ele=driver.findElement(By.xpath(PomObj.Year()));
							ele.click(); ele.sendKeys("1996"); 
								
							ele=driver.findElement(By.xpath(PomObj.Publisher()));
								ele.click(); ele.sendKeys("Antony King Bright");
								
								
								ele=driver.findElement(By.xpath(PomObj.City()));
								ele.click(); ele.sendKeys("Ghatsila");
								
					/*			ele=driver.findElement(By.xpath(PomObj.Volume()));
								ele.click(); ele.sendKeys("8");		*/
								

								ele=driver.findElement(By.xpath(PomObj.FirstPage()));
								ele.click(); ele.sendKeys("1");
								
								
	
								ele=driver.findElement(By.xpath(PomObj.LastPage()));
								ele.click(); ele.sendKeys("55");
		  						
								
								
		ele=driver.findElement(By.cssSelector(PomObj.Save()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
								ele.click();	
								
								
								 switc.SwitchCase(uAgent);
								
								    ele=driver.findElement(By.cssSelector(PomObj.reference12()));
						((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
					
														ele.click();		
													
						     driver.switchTo().defaultContent();
						
			          ele=driver.findElement(By.xpath(PomObj.Chapter_Title()));
							ele.click();ele.clear();
											
											            
	             ele=driver.findElement(By.cssSelector(PomObj.Save()));
											ele.click();	
    	}catch(Exception e){
            e.printStackTrace();
         }

try {
	
 
	WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
      
  	String Actualvalue =ele.getAttribute("innerHTML");
			String ExpectedValue="Please enter Chapter title !"; 
							 
					
          if(Actualvalue.contains(ExpectedValue)==true) {
								
							  status ="Pass";			    	

							
							    }else{
							    	
remark="Cse Style Edited book Chapter title has been deleted but popup didn't apper/ Cse Style Edited book is not Workin";	
								    	 
       status ="Fail"; 	   							 
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
							} 
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
			}

						

        }
    	
        
    	 
    	@Test(alwaysRun=true)
    	         	
    			public void UpdateEditerBooktest04() throws Exception {
    			         	
    		   UpdateCseEditedBookReferences04 bj = new UpdateCseEditedBookReferences04();
    			         	         bj.CSE();
    			         	
    			         	   }
    	               }