package com.UpdateReference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class UpdateCseBookReference04 extends UpdateBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
    public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->Cse -Check whether if the Publisher deleted then Reference will not Save, Check the Publisher Pop display or not";
		 className = "UpdateCseBookReference04";    
				area = "Update Book Reference";
					category = "CSE Style";
					                				                   
					
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		              switc.SwitchCase(uAgent);
				
		          PomObj = new com.Reference.Pom();
		          
		          
		          
		          
		         	   ele = driver.findElement(By.xpath(PomObj.Ele2()));
			      
		  		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		  					
		  					MoveToElement.byXpath(driver, PomObj.Ele2());    
		  					          ele.click();
		  					          
		  					          
		  					          driver.switchTo().defaultContent();
		  					          
		  					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
		  					          
		  					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
		  				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
		  					          ele.click();
		  					     
		  					          
		  		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		  		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
		                       dropdown.selectByVisibleText("Book");
		                                 
		              	   
		  				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
		  				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
		  				
		  				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
		             WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
		  					        ele.click(); ele.sendKeys("1884");
		  				          
		  					     
		  					        
		  				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
		  							ele.click(); ele.sendKeys("Zi");      
		  					          
		  							
		  							
		  					ele=driver.findElement(By.xpath(PomObj.given1()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
		  						    ele.click(); ele.sendKeys("K");  
		  						    
		  	
		  				 ele=driver.findElement(By.xpath(PomObj.Surname2()));
			  					WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
			  			ele.click(); ele.sendKeys("Qwl");      
			  					          
			  							
			  							
			  ele=driver.findElement(By.xpath(PomObj.given2()));
			  			WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
			  	ele.click(); ele.sendKeys("K"); 
		  						    
		  						    ele=driver.findElement(By.xpath(PomObj.Year()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
		  								ele.click(); ele.sendKeys("1884");



		              ele=driver.findElement(By.xpath(PomObj.Publisher()));
		         WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
		               ele.click();ele.sendKeys("Preetish"); 
		  	      

		      ele=driver.findElement(By.xpath(PomObj.Book_Title()));
		  WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
		  	ele.click(); ele.sendKeys("Heart"); 

		  	
		   	  	
		  	
		  	ele=driver.findElement(By.xpath(PomObj.City()));
		  	WaitFor.clickableOfElementByXpath(driver, PomObj.City());
		  		ele.click(); ele.sendKeys("India");
		  		
		  		
		  		
		  		
		  ele=driver.findElement(By.cssSelector(PomObj.Save()));
		  WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
		  				ele.click();   
		  				
		  						
		  				
		  				 switc.SwitchCase(uAgent);
		  				
		  	ele = driver.findElement(By.xpath(PomObj.ref1884()));
		  	      MoveToElement.byXpath(driver, PomObj.ref1884());    
		  								ele.click();
		          
		              
		  								
		  								
		  					ele = driver.findElement(By.cssSelector(PomObj.Z()));
		  					  MoveToElement.byCssSelector(driver, PomObj.Z());
		  							  	  			ele.click();	
	    
	    
	        driver.switchTo().defaultContent();
	        
	        
	        ele=driver.findElement(By.xpath(PomObj.Publisher()));
			  WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
			  	 ele.click(); ele.clear(); 


		                  
		                  ele=driver.findElement(By.cssSelector(PomObj.Save()));
		                  WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
		                  				ele.click();
		                  		             			
           }catch(Exception e){
  e.printStackTrace();
    }
  	
  	try {
			
  		 WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
  	      
      	String Actualvalue =ele.getAttribute("innerHTML");
 			String ExpectedValue="Please enter the Publisher Name !"; 
 							 
 					
 if(Actualvalue.contains(ExpectedValue)==true) {
 								
 							  status ="Pass";			    	

 							
 							    }else{
 remark="Cse Style Publisher has been deleted but popup didn't apper/ Cse Style book is not Working";	
 								    	 
           status ="Fail"; 	   							 
 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
 							    } 
 					} catch (Exception e) {
 						e.printStackTrace();
 					}finally {
 						        System.out.println(className);
 	 Ex.testdata(description, className, remark, category, area, status, uAgent);
 			}

 } 
                  
		        
  
  
 @Test(alwaysRun=true)
       	
		public void UpdateBooktest04() throws Exception {
		         	
	   UpdateCseBookReference04 bj = new UpdateCseBookReference04();
		         	         bj.CSE();
		         	
		         	   }
             }