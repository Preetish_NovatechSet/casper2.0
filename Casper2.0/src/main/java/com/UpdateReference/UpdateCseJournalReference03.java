package com.UpdateReference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseJournalReference03 extends UpdateBaseClass{
		
		
		
		
		static String remark;
		static String className;
		static String category;
		static String area;
		static String description;
	    static String status;
	    public static  Excel Ex; 
	    static com.UpdateReference.Pom PomObj;
		public static  WebElement ele;

		


			
		    
		    public void CSE() throws IOException, InterruptedException {
		    	
		    	try {
		    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "TandF AuthorEnd-->Cse -Check whether the 2 author name can delete and add 5 author name with Collab name and check the style has displayed correctly or not";
				 className = "UpdateCseJournalReference03";    
						 area = "Update Journal Reference";
							  category = "CSE Style";
							                				                   
							
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
				              System.out.println("BrowerName->"+uAgent);
				
				      Cookies cokies = new Cookies(driver);
				              cokies.cookies();
				                
				             
			           Switch switc = new Switch(driver);
				             switc.SwitchCase(uAgent);
						
				          PomObj = new Pom();
				          
				          
			 WebElement lwe = driver.findElement(By.cssSelector(PomObj.reference8()));
				  			lwe.click();
				  						
				  			
				  						 driver.switchTo().defaultContent();
				  						 
				  				ele=driver.findElement(By.xpath(PomObj.Surname1()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
											 ele.click(); ele.clear(); ele.sendKeys("Preet");      
										          
												
												
								ele=driver.findElement(By.xpath(PomObj.given1()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
											   ele.click(); ele.clear(); ele.sendKeys("N");			  				 
									
											   
				  				ele=driver.findElement(By.xpath(PomObj.Surname2()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
												ele.click(); ele.clear(); ele.sendKeys("tu");      
										          
												
												
								ele=driver.findElement(By.xpath(PomObj.given2()));
										WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
											    ele.click(); ele.clear(); ele.sendKeys("Narayan");  
										    
											    ele=driver.findElement(By.xpath(PomObj.Surname3()));
												WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
													ele.click(); ele.sendKeys("Ra");      
											          
													
													
							    ele=driver.findElement(By.xpath(PomObj.given3()));
												WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
								ele.click(); ele.sendKeys("sad");  
								
								
								for(int i=0;i<=7;i++) {
								 ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
					                              ele.click(); 
								
								}
								
								
								
								ele=driver.findElement(By.xpath(PomObj.Surname4()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
									ele.click(); ele.sendKeys("Rj");      
							          
									
									
				ele=driver.findElement(By.xpath(PomObj.given4()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
				ele.click(); ele.sendKeys("Pra"); 
								
							


				ele=driver.findElement(By.xpath(PomObj.Surname5()));
				WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
					ele.click(); ele.sendKeys("Rani");      
				      
					
					
				ele=driver.findElement(By.xpath(PomObj.given5()));
				WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
				ele.click(); ele.sendKeys("Pa"); 			 
				  						 
				  						 
				ele=driver.findElement(By.xpath(PomObj.Collab()));
				WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
				ele.click();ele.sendKeys("Preetish M");
			
				ele=driver.findElement(By.cssSelector(PomObj.Save()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
										ele.click();		
										
											  							              
		    	   }catch(Exception e){
		            e.printStackTrace();
		              }

		try {
			
			Switch switc = new Switch(driver);
		   switc.SwitchCase(uAgent);

										       
		 
		String Actualvalue =null;
		String ListOfJournal = null;
		String ExpectedValue="Preet N, tu Narayan, Ra sad, Rj Pra, Rani Pa, Preetish M. 2017. Prevention and treatment of white spot lesions in orthodontic patients. Contemp Clin Dent. 8:11�19."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.Ref11());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref11()));

			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		remark="Cse Style is incorrect while Updating the Journal reference";	
		     		    	 
			          		 status ="Fail"; 
			        }
			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		}
								

		        }
		    	
		        
		    	 
		    	@Test(alwaysRun=true)
		    	         	
		    			public void test3() throws Exception {
		    			         	
		    		   UpdateCseJournalReference03 bj = new UpdateCseJournalReference03();
		    			         	         bj.CSE();
		    			         	
		    			         	   }
		    	               }
