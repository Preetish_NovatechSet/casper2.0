package com.UpdateReference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class UpdateCseConferenceReference11 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether the 2 author name can delete 1 Red Star Surname in conference reference and save, check Weather the Surname PopUp displayed or not";
		 className = "UpdateCseConferenceReference11";    
				area = "Update Conference Reference";
					category = "CSE Style";
					                				                   
					
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		             switc.SwitchCase(uAgent);
				
		          PomObj = new Pom();
		          
		          
		          ele = driver.findElement(By.xpath(PomObj.ele_1()));
			      
			  		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			  					
			  					MoveToElement.byXpath(driver, PomObj.ele_1());    
			  					          ele.click();
		  						
		  						 driver.switchTo().defaultContent();
		  						 
		  						ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
	  					          
	  					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
	  				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
	  					          ele.click();
	  					     
	  					          
	  		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
	  		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
	                       dropdown.selectByVisibleText("Conference");
	                                 
	              	   
	  				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
	  				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));	 
	  				     ele.click();ele.sendKeys("1957");
								    
		  						 
		  						 
		  						 
		  						 
								    ele=driver.findElement(By.xpath(PomObj.Surname1()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
										ele.click(); ele.sendKeys("Reddy");      
								          
										
			ele = driver.findElement(By.xpath(PomObj.given1()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
			ele.click();
			ele.sendKeys("N");
			
			
			ele=driver.findElement(By.xpath(PomObj.Surname2()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
						ele.click(); ele.sendKeys("AA"); 			
					
				
								
						ele=driver.findElement(By.xpath(PomObj.given2()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
				ele.click(); ele.sendKeys("Mahato"); 
				

            ele = driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
			ele.click();
			ele.sendKeys("1957");
			
			
		

			ele = driver.findElement(By.xpath(PomObj.ConferenceTitle()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.ConferenceTitle());
			ele.click(); ele.sendKeys("A World of Opportunities");
				
			ele = driver.findElement(By.xpath(PomObj.Conf_Name()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Name());
			ele.click(); ele.sendKeys("One day");
												    

			ele = driver.findElement(By.xpath(PomObj.Conf_Place()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Place());
			ele.click(); ele.sendKeys("Purva");
					
		  						
		ele=driver.findElement(By.cssSelector(PomObj.Save()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
								ele.click();	
								
								
					
								
								 switc.SwitchCase(uAgent);			
	    ele=driver.findElement(By.cssSelector(PomObj.reference14()));
   WaitFor.clickableOfElementByCSSSelector(driver, PomObj.reference14());
						ele.click();							
								
								
						 driver.switchTo().defaultContent();
									

									 
							ele = driver.findElement(By.xpath(PomObj.Surname1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.clear();
		    
 
 ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
						ele.click();	
		  							              
 	}catch(Exception e){
         e.printStackTrace();
           }

try {
	 WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
   
	String Actualvalue =ele.getAttribute("innerHTML");
			String ExpectedValue="Please enter Authors !"; 
							 
					
if(Actualvalue.contains(ExpectedValue)==true) {
								
							  status ="Pass";			    	

							
							    }else{
remark="Cse Style First SurName has been deleted but popup didn't apper/ Cse Style Conference is not Working";	
								    	 
    status ="Fail"; 	   							 
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
							    } 
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
			}
}
	
        
    	 
    	@Test(alwaysRun=true)
    	         	
    			public void test11() throws Exception {
    			         	
    		UpdateCseConferenceReference11 bj = new UpdateCseConferenceReference11();
    			         	         bj.CSE();
    			         	
    			         	   }
    	               }