package com.UpdateReference;


import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import utilitys.BrowserFactory;

public class UpdateBaseClass {

	
	
	
	
	
	public static WebDriver driver;
	public static  String uAgent;

	@Parameters({ "browser" })	
	@BeforeClass
		public void setupApplication(String browser) throws Exception
		{
			
			
			switch (browser) {
				
			
			case "fireFox":
							
	driver = BrowserFactory.Setup_Grid(browser,CSEUpdateReferenceUrl.FireFoxCseUpdateRefUrl.getFireFoxUrl());			

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return typeof InstallTrigger !== 'undefined'?'firefox':''");		
	break ;	
				
								    
			case "chrome":
			
	driver = BrowserFactory.Setup_Grid(browser,CSEUpdateReferenceUrl.ChromeCseUpdateRefUrl.getChromeUrl());

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return !!window.chrome && !!window.chrome.webstore?'chrome':'';");			
						
			break ;
								    
								    
		   case "opera":	
				
	driver = BrowserFactory.Setup_Grid(browser,CSEUpdateReferenceUrl.OperaCseUpdateRefUrl.getOperaUrl());			       			         							

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return !!window.opr && !!opr.addons || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0?'opera':'';");											
					     break;	
					     
						 
						 
		   case "edge":	
					   
	driver = BrowserFactory.Setup_Grid(browser,CSEUpdateReferenceUrl.EdgeCseUpdateRefUrl.getEdgeUrl());				   					         			   				   					

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return !!window.StyleMedia?'edge':'';");				
										
					   break;	
					   
					   				 
			case "ie":		
					   
	driver = BrowserFactory.Setup_Grid(browser,CSEUpdateReferenceUrl.IECseUpdateRefUrl.getIEUrl());			   					         			   				   					

	uAgent = (String) ((JavascriptExecutor) driver).executeScript("return !!document.documentMode?'IE':'';");				
					   								
					   break;	 
			}
			
			Reporter.log("=====Application Started=====", true);
			
		} 
		
			
		
	@AfterClass
		public void closeApplication()
		{
		
				 try {
				Thread.sleep(3000);
				   driver.quit();	 
					
					
					switch (uAgent){
					case "opera":	 
		Runtime.getRuntime().exec("taskkill /f /im opera.exe");
				 }
					
					 System.out.println("Application Close");
				} catch (Exception e) {
					e.printStackTrace();
				           }
			           }
                }
