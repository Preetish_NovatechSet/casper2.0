package com.UpdateReference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseEditedBookReferences08 extends UpdateBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
    public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE -Check whether the 11 author name with et al reference can delete 2 author name from Edited-Book reference and check the style displayed correctly or not";
	    className = "UpdateCseEditedBookReferences08";    
	          area = "Update Edited book Reference";
			     category = "CSE Style";
					                				                   
					
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		              switc.SwitchCase(uAgent);
				
		              PomObj = new Pom();
		          
		          
		          
		          
		              
		    	      WebElement lwe = driver.findElement(By.xpath(PomObj.ele_1()));
		    	 
		    	 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", lwe);	  						  					
		    		  					MoveToElement.byXpath(driver, PomObj.ele_1());	 
		    	                                    lwe.click();
		  					          
		  					          
		  					          driver.switchTo().defaultContent();
		  					          
		  					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
		  					          
		  					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
		  				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
		  					          ele.click();
		  					     
		  					          
		  		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		  		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
		                       dropdown.selectByVisibleText("Edited Book");
		                                 
		              	   
		  				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
		  				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
		  				
		  				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
		             WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
		  					        ele.click(); ele.sendKeys("1884");
		  				          
		  					     
		  					        
		  				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
		  							ele.click(); ele.sendKeys("Zi");      
		  					          
		  							
		  							
		  					ele=driver.findElement(By.xpath(PomObj.given1()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
		  						    ele.click(); ele.sendKeys("K");  
		  						    
		  	
		  				 ele=driver.findElement(By.xpath(PomObj.Surname2()));
			  					WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
			  			ele.click(); ele.sendKeys("Qwl");      
			  					          
			  							
			  							
			  ele=driver.findElement(By.xpath(PomObj.given2()));
			  			WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
			  	ele.click(); ele.sendKeys("K"); 
			  	
			  	
			  	 ele=driver.findElement(By.xpath(PomObj.Surname3()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
			ele.click(); ele.sendKeys("Qwl");      
					          
							
							
ele=driver.findElement(By.xpath(PomObj.given3()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
	ele.click(); ele.sendKeys("K"); 
	
	
	for(int i=0;i<=7;i++) {
		 ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
                         ele.click(); 
		
		}
		
		
		
	ele=driver.findElement(By.xpath(PomObj.Surname4()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
			ele.click(); ele.sendKeys("Rj");      
	          
			
			
  ele=driver.findElement(By.xpath(PomObj.given4()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
            ele.click(); ele.sendKeys("Pra"); 
		
	


   ele=driver.findElement(By.xpath(PomObj.Surname5()));
       WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
           ele.click(); ele.sendKeys("Rani");      



  ele=driver.findElement(By.xpath(PomObj.given5()));
      WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
           ele.click(); ele.sendKeys("Pa"); 

  ele=driver.findElement(By.xpath(PomObj.Surname6()));
      WaitFor.clickableOfElementByXpath(driver, PomObj.Surname6());
           ele.click(); ele.sendKeys("Hj");      



ele=driver.findElement(By.xpath(PomObj.given6()));
    WaitFor.clickableOfElementByXpath(driver, PomObj.given6());
        ele.click(); ele.sendKeys("Pra"); 


ele=driver.findElement(By.xpath(PomObj.Surname7()));
   WaitFor.clickableOfElementByXpath(driver, PomObj.Surname7());
       ele.click(); ele.sendKeys("Dj");      



ele=driver.findElement(By.xpath(PomObj.given7()));
   WaitFor.clickableOfElementByXpath(driver, PomObj.given7());
         ele.click(); ele.sendKeys("Pra"); 

ele=driver.findElement(By.xpath(PomObj.Surname8()));
   WaitFor.clickableOfElementByXpath(driver, PomObj.Surname8());
         ele.click(); ele.sendKeys("Rj");      



ele=driver.findElement(By.xpath(PomObj.given8()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given8());
ele.click(); ele.sendKeys("A"); 


ele=driver.findElement(By.xpath(PomObj.Surname9()));
  WaitFor.clickableOfElementByXpath(driver, PomObj.Surname9());
      ele.click(); ele.sendKeys("Rj");      



ele=driver.findElement(By.xpath(PomObj.given9()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given9());
ele.click(); ele.sendKeys("Pra"); 



ele=driver.findElement(By.xpath(PomObj.Surname10()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
ele.click(); ele.sendKeys("Rj");      



ele=driver.findElement(By.xpath(PomObj.given10()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
ele.click(); ele.sendKeys("P");


ele=driver.findElement(By.xpath(PomObj.Surname11()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
ele.click(); ele.sendKeys("Rj");      



ele=driver.findElement(By.xpath(PomObj.given11()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
ele.click(); ele.sendKeys("P");
		  						    
		  						    ele=driver.findElement(By.xpath(PomObj.Year()));
		  						WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
		  								ele.click(); ele.sendKeys("1884");



		              ele=driver.findElement(By.xpath(PomObj.Publisher()));
		         WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
		               ele.click();ele.sendKeys("Preetish"); 
		  	      

		               
		           	ele=driver.findElement(By.xpath(PomObj.Chapter_Title()));
					   WaitFor.clickableOfElementByXpath(driver, PomObj.Chapter_Title());
										ele.click(); ele.sendKeys("Guns and thungs");  		
		               
		               
		               ele=driver.findElement(By.xpath(PomObj.Book_Title()));
				   ele.click(); ele.sendKeys("The Piper at the Gates of Dawn");	

		  	
		   	  	
		  	
		  	ele=driver.findElement(By.xpath(PomObj.City()));
		  	WaitFor.clickableOfElementByXpath(driver, PomObj.City());
		  		ele.click(); ele.sendKeys("India");
		  		
		  		ele=driver.findElement(By.xpath(PomObj.et_al()));
			  	WaitFor.clickableOfElementByXpath(driver, PomObj.et_al());
			  		ele.click(); ele.sendKeys("India");	
		  		
		  		
		  ele=driver.findElement(By.cssSelector(PomObj.Save()));
		  WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
		  				ele.click();   
		  				
		  						
		  				
		  				 switc.SwitchCase(uAgent);
		  				
		  				ele=driver.findElement(By.cssSelector(PomObj.reference25()));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
					ele.click();
		          
		              
		  								
	    
	    
	               driver.switchTo().defaultContent();
	        
	        
	            ele=driver.findElement(By.xpath(PomObj.Surname11()));
			  WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
			  	 ele.click(); ele.clear(); 
			  	 
			  	ele=driver.findElement(By.xpath(PomObj.given11()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
				   ele.click(); ele.clear(); 
			  	 
			  	 ele=driver.findElement(By.xpath(PomObj.Surname10()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
				  ele.click(); ele.clear(); 

				 ele=driver.findElement(By.xpath(PomObj.given10()));
			 WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
					  	 ele.click(); ele.clear(); 
					  	 
		                  ele=driver.findElement(By.cssSelector(PomObj.Save()));
		             WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
		                  		ele.click();
		                  		             			
           }catch(Exception e){
  e.printStackTrace();
    }
  	
  	try {
			
  		Switch switc = new Switch(driver);
        switc.SwitchCase(uAgent);
		
		
		String Actualvalue =null;
		String ListOfJournal = null;
		String ExpectedValue="Zi K, Qwl K, Qwl K, Rj Pra, Rani Pa, Hj Pra, Dj Pra, Rj A, Rj Pra, et al. 1884. Guns and thungs. In: The Piper at the Gates of Dawn. India: Preetish."; 
		 
		
		WaitFor.presenceOfElementByXpath(driver, PomObj.Ref25());
		List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref25()));
	
		
		             for(WebElement ele1:el){
		        	   Actualvalue =ele1.getText();
		                	
		             ListOfJournal+=Actualvalue + "\n";
		   	System.out.println("PKM--->"+ListOfJournal);
		         	   
					    }
		             
		     if(ListOfJournal.contains(ExpectedValue)==true) {
		        	  status ="Pass";
		        	  
		        }else{
		        	
	  remark="Edited book Cse Style is in correct while Updating/ Cse Style Edited book is not Workin";	
          		    	 
		          		 status ="Fail"; 
		        }
		     
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		        System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
	   }
                  
    }	        
  
  
 @Test(alwaysRun=true)
       	
		public void UpdateEditerBooktest08() throws Exception {
		         	
	   UpdateCseEditedBookReferences08 bj = new UpdateCseEditedBookReferences08();
		         	         bj.CSE();
		         	
		         	   }
             }