package com.UpdateReference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseEditedBookReferences02 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE - Check whether the 2 author name and 1 Editor reference can add 1 author name with 10 Editor name and check Wheather with style displayed correctly or not";
		 className = "UpdateCseEditedBookReferences02";    
				area = "Update Edited book Reference";
					category = "CSE Style";
					                				                   
					
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
			         
		              System.out.println("BrowerName->"+uAgent);
		
		      Cookies cokies = new Cookies(driver);
		              cokies.cookies();
		                
		             
	           Switch switc = new Switch(driver);
		             switc.SwitchCase(uAgent);
				
		          PomObj = new Pom();
		          
		          
	      WebElement lwe = driver.findElement(By.xpath(PomObj.ele_1()));
	 
	 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", lwe);	  						  					
		  					MoveToElement.byXpath(driver, PomObj.ele_1());	 
	                                    lwe.click();
		  						
		  					driver.switchTo().defaultContent();
		  						 
		  					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                   dropdown.selectByVisibleText("Edited Book");
                             
          	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));		
				     ele.click();ele.sendKeys("1996");
				 
				     
				     
				     ele=driver.findElement(By.xpath(PomObj.Surname1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
								ele.click(); ele.sendKeys("Preetish");      
								          
										
										
				ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
							   ele.click(); ele.sendKeys("R");  
							   
							   
							   ele=driver.findElement(By.xpath(PomObj.Surname2()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
									ele.click(); ele.sendKeys("Raj");      
									          
											
											
					ele=driver.findElement(By.xpath(PomObj.given1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
								   ele.click(); ele.sendKeys("R"); 
								    
					
				 ele=driver.findElement(By.xpath(PomObj.Editor_Name1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name1());
									ele.click(); ele.sendKeys("Ravi");      
									          
							
									
									ele=driver.findElement(By.xpath(PomObj.Chapter_Title()));
									   WaitFor.clickableOfElementByXpath(driver, PomObj.Chapter_Title());
														ele.click(); ele.sendKeys("Guns and thungs");  		
									
														   
					
						ele=driver.findElement(By.xpath(PomObj.Book_Title()));
					ele.click(); ele.sendKeys("The Piper at the Gates of Dawn");	
									
						ele=driver.findElement(By.xpath(PomObj.Year()));
							ele.click(); ele.sendKeys("1996"); 
								
							ele=driver.findElement(By.xpath(PomObj.Publisher()));
								ele.click(); ele.sendKeys("Antony King Bright");
								
								
								ele=driver.findElement(By.xpath(PomObj.City()));
								ele.click(); ele.sendKeys("Ghatsila");
								
					/*			ele=driver.findElement(By.xpath(PomObj.Volume()));
								ele.click(); ele.sendKeys("8");		*/
								

								ele=driver.findElement(By.xpath(PomObj.FirstPage()));
								ele.click(); ele.sendKeys("1");
								
								
	
								ele=driver.findElement(By.xpath(PomObj.LastPage()));
								ele.click(); ele.sendKeys("55");
		  						
								
								
		ele=driver.findElement(By.cssSelector(PomObj.Save()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
								ele.click();	
								
								
								 switc.SwitchCase(uAgent);
								
								    ele=driver.findElement(By.cssSelector(PomObj.reference12()));
						((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
					
														ele.click();		
								
														
														driver.switchTo().defaultContent();
						
														
						 ele=driver.findElement(By.xpath(PomObj.Surname3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
											ele.click(); ele.sendKeys("Ramjane");  								
														
			      
			ele = driver.findElement(By.xpath(PomObj.Editor_Name2()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name2());
			 ele.click();ele.sendKeys("Kumar"); 											
														
			   
			      ele = driver.findElement(By.xpath(PomObj.Editor_Name3()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name3());
			                     	ele.click();
				     ele.clear();ele.sendKeys("Neetish");
				     
				  	for(int i=0;i<=7;i++) {
						 ele=driver.findElement(By.xpath(PomObj.AddAuthor1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor1());
			                              ele.click(); 
						
						}
				     ele = driver.findElement(By.xpath(PomObj.Editor_Name4()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name4());
			                     	ele.click();
				     ele.clear();ele.sendKeys("Kumar");
				     
				
				     
				     ele = driver.findElement(By.xpath(PomObj.Editor_Name5()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name5());
			                     	ele.click();
				     ele.clear();ele.sendKeys("kausik");
				     
				     
				     ele = driver.findElement(By.xpath(PomObj.Editor_Name6()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name6());
			                     	ele.click();
				     ele.clear();ele.sendKeys("Vicky");
		  			
				     ele = driver.findElement(By.xpath(PomObj.Editor_Name7()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name7());
			                     	ele.click();
				     ele.clear();ele.sendKeys("Preeti");
				     
				     
				     ele = driver.findElement(By.xpath(PomObj.Editor_Name8()));
				     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name8());
			                     	ele.click();
				         ele.clear();ele.sendKeys("Preet");
				         
				         
				         ele = driver.findElement(By.xpath(PomObj.Editor_Name9()));
					     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name9());
				                     	ele.click();
					     ele.clear();ele.sendKeys("Shakti");
					     
					     ele = driver.findElement(By.xpath(PomObj.Editor_Name10()));
					     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name10());
				                     	ele.click();
					     ele.clear();ele.sendKeys("Bira");
					     
					     
					     ele = driver.findElement(By.xpath(PomObj.Editor_Name11()));
					     WaitFor.clickableOfElementByXpath(driver, PomObj.Editor_Name11());
				                     	ele.click();
					     ele.clear();ele.sendKeys("Kingfisher");
		
					     
					     
										
										
			try {
				ele = driver.findElement(By.cssSelector(PomObj.Save()));
				((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
				       ele.click();
			} catch (Exception e) {
			
				e.printStackTrace();
			}
					     
    	}catch(Exception e){
            e.printStackTrace();
              }

try {
	
	Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent);

								       
 
	String Actualvalue =null;
	String ListOfJournal = null;
	String ExpectedValue="Preetish RR, Raj, Ramjane. 1996. Guns and thungs. In: Ravi, Kumar, Neetish, Kumar, kausik, Vicky, Preeti, Preet, Shakti, Bira, Kingfisher, editors. The Piper at the Gates of Dawn. Ghatsila: Antony King Bright; p. 1�55."; 
	 
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.Ref12());
	List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref12()));

	
	             for(WebElement ele1:el){
	        	   Actualvalue =ele1.getText();
	                	
	             ListOfJournal+=Actualvalue + "\n";
	   	System.out.println("PKM--->"+ListOfJournal);
	         	   
				    }
	             
	     if(ListOfJournal.contains(ExpectedValue)==true) {
	        	  status ="Pass";
	        	  
	        }else{
	        	
remark="Cse Style is incorrect while Updating the Edited book reference/ Cse Style Edited book is not Working";	
     		    	 
	          		 status ="Fail"; 
	        }
	     
} catch (Exception e) {
	e.printStackTrace();
}finally {
	        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
}
						

        }
    	
        
    	 
    	@Test(alwaysRun=true)
    	         	
    			public void UpdateEditerBooktest02() throws Exception {
    			         	
    		   UpdateCseEditedBookReferences02 bj = new UpdateCseEditedBookReferences02();
    			         	         bj.CSE();
    			         	
    			         	   }
    	               }