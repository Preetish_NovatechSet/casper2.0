package com.UpdateReference;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseJournalReference02 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;

	


		
	    
	    public void CSE() throws IOException, InterruptedException {
	    	
	    	try {
	    		
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "TandF AuthorEnd-->CSE-Check whether the 2 author name reference can add Collab name and check the style displayed correctly or not";
			 className = "UpdateCseJournalReference02";    
					 area = "Update Journal Reference";
						  category = "CSE Style";
						                				                   
						
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
				         
			              System.out.println("BrowerName->"+uAgent);
			
			      Cookies cokies = new Cookies(driver);
			              cokies.cookies();
			                
			             
		           Switch switc = new Switch(driver);
			             switc.SwitchCase(uAgent);
					
			          PomObj = new Pom();
			          
			          
		 WebElement lwe = driver.findElement(By.cssSelector(PomObj.reference8()));
			  			lwe.click();
			  						
			  			
			  						 driver.switchTo().defaultContent();
			  						 
			  				 
									   					
			ele=driver.findElement(By.xpath(PomObj.Collab()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
			ele.click();ele.sendKeys("Preetish M");
		
			ele=driver.findElement(By.cssSelector(PomObj.Save()));
			WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
									ele.click();		
									
										  							              
	    	   }catch(Exception e){
	            e.printStackTrace();
	              }

	try {
		
		Switch switc = new Switch(driver);
	   switc.SwitchCase(uAgent);

									       
	 
	String Actualvalue =null;
	String ListOfJournal = null;
	String ExpectedValue="Khoroushi M, Kachuie M., Preetish M. 2017. Prevention and treatment of white spot lesions in orthodontic patients. Contemp Clin Dent. 8:11�19."; 
		 
		
		WaitFor.presenceOfElementByXpath(driver, PomObj.Ref08());
		List<WebElement> el=driver.findElements(By.xpath(PomObj.Ref08()));

		
		             for(WebElement ele1:el){
		        	   Actualvalue =ele1.getText();
		                	
		             ListOfJournal+=Actualvalue + "\n";
		   	System.out.println("PKM--->"+ListOfJournal);
		         	   
					    }
		             
		     if(ListOfJournal.contains(ExpectedValue)==true) {
		        	  status ="Pass";
		        	  
		        }else{
		        	
	remark="Cse Style is incorrect while Updating the Journal reference/ Cse Style Journal is not Working";	
	     		    	 
		          		 status ="Fail"; 
		        }
		     
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		        System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
	}
							

	        }
	    	
	        
	    	 
	    	@Test(alwaysRun=true)
	    	         	
	    			public void test2() throws Exception {
	    			         	
	    		   UpdateCseJournalReference02 bj = new UpdateCseJournalReference02();
	    			         	         bj.CSE();
	    			         	
	    			         	   }
	    	               }