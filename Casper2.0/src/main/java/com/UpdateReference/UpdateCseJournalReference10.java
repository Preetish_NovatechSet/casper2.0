package com.UpdateReference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class UpdateCseJournalReference10 extends UpdateBaseClass{
	
	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.UpdateReference.Pom PomObj;
	public static  WebElement ele;

	


		
	    
	    public void CSE() throws IOException, InterruptedException {
	    	
	    	try {
	    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->Cse -Check whether the First Page can delete and save, check Wheather Pop is displayed Or not";
			 className = "UpdateCseJournalReference10";    
					 area = "Update Journal Reference";
						  category = "CSE Style";
						                				                   
						  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
			              System.out.println("BrowerName->"+uAgent);
			
			      Cookies cokies = new Cookies(driver);
			              cokies.cookies();
			                
			             
		           Switch switc = new Switch(driver);
			             switc.SwitchCase(uAgent);
					
			          PomObj = new Pom();
			          
			          
		 WebElement lwe = driver.findElement(By.cssSelector(PomObj.reference7()));
			  			lwe.click();
			  						
			  			
			  						 driver.switchTo().defaultContent();
			  						 
			  											    
			  				ele=driver.findElement(By.xpath(PomObj.FirstPage()));
			  							ele.click();ele.clear();
			      
			
			ele=driver.findElement(By.cssSelector(PomObj.Save()));
			WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
									ele.click();		
									
										  							              
	    	   }catch(Exception e){
	            e.printStackTrace();
	              }
	    
	    	
	    	try {
	    	 WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
	  	      
	       	String Actualvalue =ele.getAttribute("innerHTML");
	  			String ExpectedValue="Please enter First page !"; 
	  							 
	  					
	  if(Actualvalue.contains(ExpectedValue)==true) {
	  								
	  							  status ="Pass";			    	

	  							
	  							    }else{
	  remark="Cse Style FirstPage has been deleted but popup didn't apper";	
	  								    	 
	            status ="Fail"; 	   							 
	  utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
	  							    } 
	  					} catch (Exception e) {
	  						e.printStackTrace();
	  					}finally {
	  						        System.out.println(className);
	  	 Ex.testdata(description, className, remark, category, area, status, uAgent);
	  			}

	  } 
	    	 
	    	@Test(alwaysRun=true)
	    	         	
	    			public void test10() throws Exception {
	    			         	
	    		   UpdateCseJournalReference10 bj = new UpdateCseJournalReference10();
	    			         	         bj.CSE();
	    			         	
	    			         	   }
	    	               }