package com.AuthorName;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PPLAuthorNameEdit04 extends PPL_BaseClass{
	


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	
    Pom PomObj;
	
	

public void AuthorNameEdit04() throws Exception{
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_Author name edit-->Double click the author name and empty (delete) the Given name and save, check whether save button through error or not";
		className = "PPLAuthorNameEdit04";    
			 area = "Author Name";
			      category = "Front matter, Delete GivenName";
			      
			      driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
			
						System.out.println("BrowerName->"+uAgent);			                     

								
	try {
							Cookies cokies =new Cookies(driver);
							          					              
							Switch switc = new Switch(driver);
								            
								          	              
									  /*ErsPEUrlString url = new ErsPEUrlString(driver);
											     url.TandFChangeC1toC2(uAgent);*/
											       			      
											      cokies.cookies();
											   
									           switc.SwitchCase(uAgent);     
								    	  
							PomObj=new Pom();
										    	
      WaitFor.presenceOfElementByXpath(driver, PomObj.PPLAuthorName2());						
    WebElement ele= driver.findElement(By.xpath(PomObj.PPLAuthorName2()));  
											
driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
												    
				
String value = ele.getAttribute("innerHTML");						

										
            if(value.contains(value)){

		MoveToElement.byXpath(driver,  PomObj.PPLAuthorName2());
																				    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		 "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
			                     "{arguments[0].fireEvent('ondblclick');}";
															
WaitFor.presenceOfElementByXpath(driver, PomObj.PPLAuthorName2());

((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);


driver.switchTo().defaultContent();
 

    WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
 
 Thread.sleep(5000); 
       ele1.click();
       ele1.clear();
       
       
   	WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
	WebElement ele3= driver.findElement(By.xpath(PomObj.CasperUpdate()));          
							   ele3.click();       																  
                        }
				} catch (Exception e){
								
						e.printStackTrace();
						
					}
			
			
			  
			 try {
				 
		WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Warringstatus_givenname());
				    
		WebElement ele1 =driver.findElement(By.cssSelector(PomObj.Warringstatus_givenname()));
				    String actualValue =  ele1.getAttribute("innerHTML");
				    String Expected="<strong>Warning!</strong> Givenname should be mandatory.";
				    
				    System.out.println("-->"+actualValue);
				    
				    
				    if(actualValue.contains(Expected)==true) {
				    	status ="Pass";
				    }else{
						 
            remark="Warning! didn't appear ";	
					    status ="Fail"; 
						    	  				    	
Thread.sleep(10000);  

         utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
							   							 
						if(actualValue.contains(Expected)==true){
							 	 System.out.println("E1: No issues");
							 	
						}				 	
						
				    
				    }			    			    
				    
			}catch(Exception e){
				
				e.printStackTrace();
			}finally {
				System.out.println(className);
	  Ex.testdata(description, className, remark, category, area, status, uAgent);	
			}
     }


@Test(alwaysRun = true)
		       		        
public void  AuthorNametest4() throws Throwable {

	try {
		
	    PPLAuthorNameEdit04 obj = new PPLAuthorNameEdit04();
			              obj.AuthorNameEdit04();



		}catch (Exception e){

			e.printStackTrace();
		}
   	}
}
