package com.AuthorName;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class PETandFRemoveAffiliationCitation_AuthorName02 extends TandFAuthorName_CommentBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;
	
	
	public void AuthorNameRemoveAffiLiactionEditAuthorName() throws Exception{
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "TandF_PE Author name edit-->In the edited author name right click and remove affiliation citation then check whether the edited name track present or not";
		  className = "PETandFRemoveAffiliationCitation_AuthorName02";    
				area = "Author Name";
				    category = "Remove AffiLiation Citation, Edit Given name";
				    
				    driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
					
					System.out.println("BrowerName->"+uAgent);			                     
							
	
		Cookies cokies =new Cookies(driver);
          
    
	      Switch switc = new Switch(driver);
	            
	          	              
	  /*    TandFC1toC2 url = new TandFC1toC2(driver);
			url.TandFAuthorChangeC1toC2(uAgent);*/
				       			      
				      cokies.cookies();
				   
		           switc.SwitchCase(uAgent); 
				      		    	  
				    	  
						PomObj=new Pom();   
				      
				WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
             WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName4()));  
	
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
						    
		String value = ele.getAttribute("innerHTML");
						
			if(value.contains(value)){
	
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
							MoveToElement.byXpath(driver,PomObj.AuthorName4());	      
				
							Actions action= new Actions(driver);
					WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
							action.contextClick(ele).build().perform();
									
							
				  driver.switchTo().defaultContent();		
								  		  
			   
					 Actions actions = new Actions(driver);	
					 
	  Thread.sleep(2000);

	  actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
	  actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
	  actions.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	  
	  
	  
Select Affilation = new Select(driver.findElement(By.cssSelector(PomObj.AddAffiliationCitationdropDown()))); 
	  Thread.sleep(1000);					
	 Affilation.selectByVisibleText("d");
	 
	 
	 WaitFor.presenceOfElementByXpath(driver, PomObj.Affilation_saveButton());
	 WebElement AffSave = driver.findElement(By.xpath(PomObj.Affilation_saveButton())); 
			
	              AffSave.click();
	              
	              UpperToolBar obj1 = new UpperToolBar(driver);
			            obj1.save_btn_method();    
	 
	 
	  /*****************************************************************************/
	  
	 
	 switc.SwitchCase(uAgent);
     
     
     WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
WebElement EditAuthorName= driver.findElement(By.xpath(PomObj.AuthorName4()));

driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
 
	String value1 = EditAuthorName.getAttribute("innerHTML");
	
  if(value1.contains(value1)){

MoveToElement.byXpath(driver,  PomObj.AuthorName4());
      
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
		"{arguments[0].fireEvent('ondblclick');}";
													
((JavascriptExecutor)driver).executeScript(doubleClickJS,EditAuthorName);


driver.switchTo().defaultContent();


WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  

   Thread.sleep(5000); 
      ele1.click();
      ele1.clear();
      ele1.sendKeys("Meheta");

    WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
     WebElement ele2= driver.findElement(By.cssSelector(PomObj.Save()));           
				ele2.click(); 
	 
  }
	 
	 
	 
/**********************************************************************************/	 
	 
	 
	  switc.SwitchCase(uAgent); 
	  
	   WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
      WebElement el= driver.findElement(By.xpath(PomObj.AuthorName4()));  

				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
					    
	String valu = el.getAttribute("innerHTML");
					
		if(valu.contains(valu)){

				WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
						MoveToElement.byXpath(driver,PomObj.AuthorName4());	      
			
						Actions actio= new Actions(driver);
				WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName4());
						actio.contextClick(el).build().perform();
								
						
			  driver.switchTo().defaultContent();		
							  		  
		   
				 
Thread.sleep(2000);

actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);
actions.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	  
	  
Select Affilation1 = new Select(driver.findElement(By.cssSelector(PomObj.RemoveAffiliationCitationdropDown()))); 
Thread.sleep(1000);					
Affilation1.selectByVisibleText("d"); 



	  WaitFor.presenceOfElementByXpath(driver, PomObj.Aff_DeleteButtom());
	  WebElement delete= driver.findElement(By.xpath(PomObj.Aff_DeleteButtom())); 
	 		     delete.click();
	 		     
	 
	 		     obj1.save_btn_method();   
	                        
	                   
	 					
	              }
	           }			
	 					
	 					 try {
	 						 
	 						  String actualValue = null;  
	 				    	  String expectedValue1 = null;
	 				    	  
	 				    	  					    	  
	 		Thread.sleep(4000);
	 				  
	 				           
	 				              switc.SwitchCase(uAgent); 
	 							
	 							
	 			 WaitFor.presenceOfElementByCSSSelector(driver, PomObj.givenNameTrc());
	 						    
	 	  	List<WebElement> ele3 =driver.findElements(By.cssSelector(PomObj.givenNameTrc()));
	 						
	 						String ListGivenName ="";  
	 					   
	 						for(WebElement ele4:ele3){
	 						 
	 						    actualValue =  ele4.getAttribute("innerHTML");
	 						    ListGivenName+=actualValue + "\n";
	 						    expectedValue1 = "Meheta";
	 				}
	 					 
	 						
	 				System.out.println("After save - " + ListGivenName); 
	 							
	 			if(ListGivenName.contains(expectedValue1)==true){
	 								 status ="Pass";
	 							 }else{
	 								 
	 		remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
	 								    	 
	 								 status ="Fail"; 
	 								    	  
	 								    	
	 						 
	 		Thread.sleep(10000);  
	 						 
	 		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	 									   
	 									 
	 			if(actualValue.contains(expectedValue1)==true) {
	 					System.out.println("E1: No issues");
	 									 	
	 			}
	 							       }			    	  
	 				         }catch (Exception e) {
	 							
	 							e.printStackTrace();
	 				     }finally {
	 				    	 System.out.println(className);
	 				   Ex.testdata(description, className, remark, category, area, status, uAgent); 	    	 
	 				     }
	 			     }		
	        

	
	@Parameters({ "browser" })
	@Test(alwaysRun = true)
			        
			        
public void  AuthorCommenttest4() throws Throwable {
		
			 try {		  
				 
			    			
	PETandFRemoveAffiliationCitation_AuthorName02 obj = new PETandFRemoveAffiliationCitation_AuthorName02();
			    	obj.AuthorNameRemoveAffiLiactionEditAuthorName();



			    		}catch (Exception e){

			    			e.printStackTrace();
			    		}
			    	}
             }