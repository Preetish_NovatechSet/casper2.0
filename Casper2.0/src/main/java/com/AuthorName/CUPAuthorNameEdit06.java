package com.AuthorName;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUPAuthorNameEdit06 extends CUP_BaseClass {

	
	

	static String remark;
	static String className = "CUPAuthorNameEdit06";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameEdit06() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-->Double click on author name and empty (delete) the Given name and Surname then save, check whether save button through error or not";			    
				 area = "Author Name";
				      category = "Front matter, Delete GivenName & Surname";
				    
				      
				 driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
						System.out.println("BrowerName->"+uAgent);			                     

									
			try {          
	              
			      Switch switc = new Switch(driver);
                       switc.SwitchCase(uAgent); 

					PomObj=new Pom();	      
	     
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());						
	WebElement ele= driver.findElement(By.xpath(PomObj.Vanessa()));  
												
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
													    
				String value = ele.getAttribute("innerHTML");						

											
	if(value.contains(value)){

																		
					MoveToElement.byXpath(driver,  PomObj.Vanessa());
														
													    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());

	((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);


	driver.switchTo().defaultContent();
	 

	  WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
	 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
	 
	 Thread.sleep(5000); 
	       ele1.click();
	       ele1.clear();
	
	
	
	 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
	 WebElement ele2= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
	 
	 Thread.sleep(5000); 
	       ele2.click();
	       ele2.clear();
	       
	       
	       WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
		     WebElement ele3= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
								 	ele3.click();      
																								
	                }
			      } catch (Exception e) {
				e.printStackTrace();
			  }
									
	
			
	try {
		
										 
		WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Warringstatus_givenname());
												    
		WebElement ele1 =driver.findElement(By.cssSelector(PomObj.Warringstatus_givenname()));
						String actualValue =  ele1.getAttribute("innerHTML");
		     
		String Expected="<strong>Warning!</strong> Givenname should be mandatory.";
												    
						System.out.println("-->"+actualValue);
												    
												    
					 if(actualValue.contains(Expected)==true) {
						 status="Pass";
						 
						 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
										 		 util.deleteRecFile(RecVideo);
					 
					              }else {
					            	 				     				 		 		 
					   status = "Fail";
					 		 	     
					 	MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
					 		 				 	 util.MoveRecFile(RecVideo);				 
								remark="Warning! didn't appear ";															    	 				

				utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
															   							 
			if(actualValue.contains(Expected)==true){
								System.out.println("E1: No issues");
															 	
					}				 	
														
												    
												    }
						} catch (Exception e) {
												
							e.printStackTrace();
								}finally {
        System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	    }						    
								}
	
		
	

	@Test(alwaysRun = true)
			        
			        
public void  AuthorNametest6() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);
	CUPAuthorNameEdit06 obj = new CUPAuthorNameEdit06();
			               obj.AuthorNameEdit06();


		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }

