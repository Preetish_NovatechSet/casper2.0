package com.AuthorName;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class PETandFAddComment_AuthorName01 extends TandFAuthorName_CommentBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	

	Pom PomObj;
	
	
	public void AuthorNameCommentEdit01() throws Throwable{
		
	
	{
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF_PE Author name edit-->Double click the author name and edit the Given name and save, check whether added comment present or not";
		  className = "PETandFAddComment_AuthorName01";    
				area = "Author Name";
				     category = "Front matter, Add comment";
				      
				     
					driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
						
						System.out.println("BrowerName->"+uAgent);			                     
								
		try {
		
		              
		         Cookies cokies =new Cookies(driver);
		         Switch switc = new Switch(driver);
		           cokies.cookies();
		              	   
		            switc.SwitchCase(uAgent);
		             	        
		            /*Eg2AQResponse a = new Eg2AQResponse(driver);
					    a.Aq1ClickAndResponseAllAQ();*/
		            
		      
		  /*    TandFC1toC2 url = new TandFC1toC2(driver);
				url.TandFAuthorChangeC1toC2(uAgent);*/
					       			      

				    					    	  
						PomObj=new Pom();   
				      
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName());
	
	
						 WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName()));  
	
						driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
						    
						String value = ele.getAttribute("innerHTML");
						
	
	
						if(value.contains(value)){
	
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName());
							MoveToElement.byXpath(driver,PomObj.AuthorName());	      
				   
							
							    
							
							Actions action= new Actions(driver);
					WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName());
							action.contextClick(ele).build().perform();
							
							
							
				 driver.switchTo().defaultContent();
					 Robot robot = new Robot(); 
					 
							 switch (uAgent){							   
			 case "firefox":
					    				
					Thread.sleep(3000); 
			   robot.keyPress(KeyEvent.VK_ENTER);
					    			
					    			break;								   
			 case "ie":
						    			
						Thread.sleep(3000); 
				robot.keyPress(KeyEvent.VK_ENTER);
						    			
						    		break;
					    	}
					    			
			if(uAgent.equals("chrome")||uAgent.equals("opera")) {
							
										
					WaitFor.presenceOfElementByXpath(driver, PomObj.Comment());
							
	                  WebElement comment= driver.findElement(By.xpath(PomObj.Comment()));			
	                
	                      //MoveToElement.byXpath(driver,PomObj.Comment());				                     
		         
		           Thread.sleep(5000);
       ((JavascriptExecutor)driver).executeScript("arguments[0].click();", comment);		
		           
		      
			}
		        
		       /*****************************************************************************/
		        WaitFor.presenceOfElementByCSSSelector(driver, PomObj.EditorEditor());
		      WebElement editor= driver.findElement(By.cssSelector(PomObj.EditorEditor()));	
		     	       MoveToElement.byCssSelector(driver, PomObj.EditorEditor());
		           	        MoveToElement.Click_sendkey(driver, editor, "Hi");
		     	  
		           	        
		           	        
		           	 WaitFor.presenceOfElementByXpath(driver, PomObj.SumbitDilogBox());
		   		 WebElement sumbit= driver.findElement(By.xpath(PomObj.SumbitDilogBox()));
		   		     MoveToElement.byCssSelector(driver, PomObj.EditorEditor());
		   		 
		   		     String  value1 = sumbit.getText();
		   		 
		   		 if(value1.contains("Submit")) {
		   			
		   			       sumbit.click();
		   	}
		   		 
		   		 
		   		 
		   		                switc.SwitchCase(uAgent);
		   		 
		   		         WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName());
			WebElement AuthorClick= driver.findElement(By.xpath(PomObj.AuthorName()));  
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
						   String value2 = AuthorClick.getAttribute("innerHTML");			

						   if(value2.contains(value2)){
											
				MoveToElement.byXpath(driver,  PomObj.AuthorName());
							
	String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
									"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
									"{arguments[0].fireEvent('ondblclick');}";
									
	                      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
							((JavascriptExecutor)driver).executeScript(doubleClickJS,AuthorClick);
		   		 
		   		 
		
							Thread.sleep(1000);
							 
							 driver.switchTo().defaultContent();
							 
							 
					WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
				 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
							 
								
							         ele1.click();
							         ele1.clear();
							         ele1.sendKeys("Preetish");
							         
							  
						WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
						WebElement ele2= driver.findElement(By.cssSelector(PomObj.Save()));           
							      
						         ele2.click();					
							
						         UpperToolBar obj1 = new UpperToolBar(driver);
							       obj1.save_btn_method(); 	
						 
		   		              }
						}
				}catch(Exception e){
	   e.printStackTrace();
        }



try {
	Switch switc = new Switch(driver);
	switc.SwitchCase(uAgent);
	
	
	WaitFor.presenceOfElementByXpath(driver, "//p[@class='contrib']");	
		 WebElement comment1= driver.findElement(By.xpath("//p[@class='contrib']"));	
		 
		 
		 String ActualValue = comment1.getAttribute("innerHTML");
		 String ExpectedValue = "Hi";
	          
		 if(ActualValue.contains(ExpectedValue)==true) {
			 status ="Pass";
		 }else {
			 status="Fail";
			 
			 remark="After Save Comment is missing";
			 
				Thread.sleep(10000);
						 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
						 	   
					      }    
			       if(ActualValue.contains(ExpectedValue)==true) {
						 	        System.out.println("E1: No issues");
						 	
						 	 }
} catch (Exception e) {

	e.printStackTrace();
      }finally {
    	         System.out.println(className);
   	   Ex.testdata(description, className, remark, category, area, status, uAgent);	  
            }
		 }
	}

	@Test(alwaysRun = true)
			        
			        
public void  AuthorCommenttest1() throws Throwable {
		
try {
	PETandFAddComment_AuthorName01 obj = new PETandFAddComment_AuthorName01();
			    			obj.AuthorNameCommentEdit01();



			    		}catch (Exception e){

			    			e.printStackTrace();
			    		}
			    	}

			   }


