package com.AuthorName;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUPAuthorNameAddComment01 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameAddComment01";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameAddComment() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-->Left Click on Author name click on comment then type some text and save check whether comment has added or not";			    
				 area = "Author Name";
				      category = "Front matter, Add comment on author name";
				    
				      driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
							System.out.println("BrowerName->"+uAgent);			                     

									
			try {
	
		          
	              
			       Switch switc = new Switch(driver);		   
				         switc.SwitchCase(uAgent); 

					PomObj=new Pom();	      
	     
	WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());						
        ele= driver.findElement(By.xpath(PomObj.Magorzata()));  
												
	    driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
													    
				String value = ele.getAttribute("innerHTML");						
											
	if(value.contains(value)){
																		
					MoveToElement.byXpath(driver,  PomObj.Magorzata());
														
		 Actions action = new Actions(driver);
					Thread.sleep(6000);
	WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());
				action.contextClick(ele).build().perform();
					
					
					Robot r= new Robot();
					Thread.sleep(3000);
					
					r.keyPress(KeyEvent.VK_ENTER);
					r.keyRelease(KeyEvent.VK_ENTER);
					
					
					
					
					Thread.sleep(3000);
					driver.switchTo().defaultContent();
					
					
					Thread.sleep(3000);
			  WaitFor.presenceOfElementByXpath(driver, PomObj.Comment());						
			        ele= driver.findElement(By.xpath(PomObj.Comment()));
			        
			       for(int i=0;i<2;i++){
			            Thread.sleep(2000);
			        MoveToElement.byclick(driver, ele);
			           Thread.sleep(2000);
			       }
			       
			       MoveToElement.bysendkeyWithoutclick(driver, ele, "i did it");
			        
			        
			      
					  WaitFor.presenceOfElementByXpath(driver, PomObj.Submit());						
				        ele= driver.findElement(By.xpath(PomObj.Submit()));
			                 ele.click();
			                 
			                 Thread.sleep(3000);
			                 switc.SwitchCase(uAgent);
			                
			                
		      
								 	
							UpperToolBar ob = new UpperToolBar(driver);
								  	  ob.save_btn_method();
																								
	                }
			      } catch (Exception e) {
				e.printStackTrace();
			  }
									
	
			
	try {
		Switch switc = new Switch(driver);
		  switc.SwitchCase(uAgent);
								
		
		
		WaitFor.presenceOfElementByXpath(driver, PomObj.validatecomment());   
		ele =driver.findElement(By.xpath(PomObj.validatecomment()));
				
		       String actualValue =  ele.getAttribute("innerHTML");
		       
		       System.out.println(""+actualValue);
	       String Expected="i did it";
												    
	 if(actualValue.contains(Expected)==true) {
		 status="Pass";
		 
		 MyScreenRecorder.stopRecording();
			String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
						 		 util.deleteRecFile(RecVideo);
	 
	              }else {
	            	 				     				 		 		 
	   status = "Fail";
	 		 	     
	 	MyScreenRecorder.stopRecording();
	 		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	 		 				 	 util.MoveRecFile(RecVideo);
	 		 				 	 
				remark="Comment added didnt displayed";	


				utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
															   							 
			if(actualValue.contains(Expected)==true){
								System.out.println("E1: No issues");
															 	
				     	}				 	
				  }				
	   } catch (Exception e) {
												
							e.printStackTrace();
								}finally {
        System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	    }						    
								}
	
		
	

	
	@Test(alwaysRun = true)
			        			        
public void  AuthorNameAddComment01() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);
		
	CUPAuthorNameAddComment01 obj = new CUPAuthorNameAddComment01();
			           obj.AuthorNameAddComment();


		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }

