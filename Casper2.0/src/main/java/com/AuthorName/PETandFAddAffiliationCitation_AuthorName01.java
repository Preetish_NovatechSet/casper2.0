package com.AuthorName;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PETandFAddAffiliationCitation_AuthorName01 extends TandFAuthorName_CommentBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	
	Pom PomObj;
	
	
	public void AuthorNameAddAffiLiactionEditAuthorName() throws Exception{
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "TandF_PE Author name edit-->In the edited author name right click and add affiliation citation then check whether the edited name track present or not";
		  className = "PETandFAddAffiliationCitation_AuthorName01";    
				area = "Author Name";
				    category = "Add AffiLiation Citation, Edit Given name";
				    
				   
				    driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
					
					System.out.println("BrowerName->"+uAgent);			                     
							
	
		Cookies cokies =new Cookies(driver);
          
    
	      Switch switc = new Switch(driver);
	            
	          	              
	     /* TandFC1toC2 url = new TandFC1toC2(driver);
			url.TandFAuthorChangeC1toC2(uAgent);*/
				       			      
				      cokies.cookies();
				   
		           switc.SwitchCase(uAgent); 
				      		    	  
				    	  
						PomObj=new Pom();   
				      
				WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
              WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName2()));  
	
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
						    
		String value = ele.getAttribute("innerHTML");
						
			if(value.contains(value)){
	
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
							MoveToElement.byXpath(driver,PomObj.AuthorName2());	      
				
							Actions action= new Actions(driver);
					WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
							action.contextClick(ele).build().perform();
									
							
				  driver.switchTo().defaultContent();		
								  		  
			   
					 Actions actions = new Actions(driver);	
					 
	  Thread.sleep(2000);

      actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
	  actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
	  actions.sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build().perform();
	
	  
Select Affilation = new Select(driver.findElement(By.cssSelector(PomObj.AddAffiliationCitationdropDown()))); 
Thread.sleep(1000);					
Affilation.selectByVisibleText("a");
					
 WaitFor.presenceOfElementByXpath(driver, PomObj.Affilation_saveButton());
 WebElement AffSave = driver.findElement(By.xpath(PomObj.Affilation_saveButton())); 
		
              AffSave.click();
              
             
                switc.SwitchCase(uAgent);
              
            WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
            
    WebElement EditAuthorName= driver.findElement(By.xpath(PomObj.AuthorName2()));
       
       driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
	    
		String value1 = EditAuthorName.getAttribute("innerHTML");
		
if(value1.contains(value1)){

MoveToElement.byXpath(driver,  PomObj.AuthorName2());
             
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
	 "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
				"{arguments[0].fireEvent('ondblclick');}";
														
       ((JavascriptExecutor)driver).executeScript(doubleClickJS,EditAuthorName);
       
       
       driver.switchTo().defaultContent();
  	 

  	 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
  	 WebElement ele1= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
  	 
  	 Thread.sleep(5000); 
  	       ele1.click();
  	       ele1.clear();
  	       ele1.sendKeys("Luddu");
       
  	     WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
	        WebElement ele2= driver.findElement(By.cssSelector(PomObj.Save()));           
					ele2.click();
					
					
					 UpperToolBar obj1 = new UpperToolBar(driver);
				       obj1.save_btn_method();
      
             }
	      }
											
   try{
				 
				  String actualValue = null;  
		    	  String expectedValue1 = null;
		    	  
		    	  					    	  
Thread.sleep(4000);
		  	           
		              switc.SwitchCase(uAgent); 
										
		WaitFor.presenceOfElementByCSSSelector(driver, PomObj.SurNameTrc());
				    
List<WebElement> ele1 =driver.findElements(By.cssSelector(PomObj.SurNameTrc()));
				
				String ListGivenName ="";  
			   
				for(WebElement ele3:ele1){
				 
				    actualValue =  ele3.getAttribute("innerHTML");
				    ListGivenName+=actualValue + "\n";
				    expectedValue1 = "Luddu";
		}
			 
			 System.out.println("After save - " + ListGivenName); 
					
					 if(ListGivenName.contains(expectedValue1)==true){
						 status ="Pass";
					 }else{
						 
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
						    	 
						 status ="Fail"; 
						    	  
						    	
				 
Thread.sleep(10000);  
				 
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
							   
							 
	if(actualValue.contains(expectedValue1)==true) {
			System.out.println("E1: No issues");
							 	
	}
					       }			    	  
		         }catch (Exception e) {
					
					e.printStackTrace();
		     }finally {	    	
		    	 System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);  	 
		     }
	}
		
			
	@Test(alwaysRun = true)
			        			        
public void  AuthorCommenttest4() throws Throwable {
		
try {
			    			
//JOptionPane.showMessageDialog(null, "Total frames = "+size, "AKB Testing" , JOptionPane.INFORMATION_MESSAGE);			    				    	
			    			
			PETandFAddAffiliationCitation_AuthorName01 obj = new PETandFAddAffiliationCitation_AuthorName01();
			    			obj.AuthorNameAddAffiLiactionEditAuthorName();

			    		}catch (Exception e){

			    			e.printStackTrace();
			    		}
			    	}
               }
