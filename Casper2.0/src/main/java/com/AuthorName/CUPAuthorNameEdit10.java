package com.AuthorName;



import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUPAuthorNameEdit10 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameEdit10";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameEdit10() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-->Double click on author name Edit the surname name then click on suffix select Junior. update and save, check whether surname has been trac or not and check the suffix Mr. to Junior has been updated or not";			    
			   area = "Author Name";
				    category = "Front matter, Edit surname name & Add suffix";
				    
             driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
						System.out.println("BrowerName->"+uAgent);			                     

									
			try {
				
		          	              
			      Switch switc = new Switch(driver);
			             switc.SwitchCase(uAgent); 
			           
					PomObj=new Pom();	      
	     
	 WaitFor.presenceOfElementByXpath(driver, PomObj.Monta�o_Machado());		
        ele= driver.findElement(By.xpath(PomObj.Monta�o_Machado()));  
												
	  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
													    
				String value = ele.getAttribute("innerHTML");						
											
if(value.contains(value)){

		MoveToElement.byXpath(driver,  PomObj.Monta�o_Machado());
																											    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																
	WaitFor.presenceOfElementByXpath(driver, PomObj.Monta�o_Machado());

	    ((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);

	driver.switchTo().defaultContent();
	 
	 Thread.sleep(2000);
	 
	WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
       ele= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
				
			         ele.click();
			         ele.clear();
			     	 ele.sendKeys("Man");
			         
			 Thread.sleep(3000);
			         
   ele= driver.findElement(By.cssSelector("#selectbox_suffix"));
  
         Select sel = new Select(ele);
        sel.selectByVisibleText("Junior");

	       WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
		      ele= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
								 	ele.click();   
								 	
								 	
							UpperToolBar ob = new UpperToolBar(driver);
								  	  ob.save_btn_method();
																								
	                  }

			      }catch (Exception e){
				e.printStackTrace();
			  }
									
	
			
	try {
		
		Switch switc = new Switch(driver);
		   switc.SwitchCase(uAgent);
										 
		     WaitFor.presenceOfElementByXpath(driver, PomObj.AuthornameTrack());												    
		List<WebElement> ele1 =driver.findElements(By.xpath(PomObj.AuthornameTrack()));
		
		
		WaitFor.presenceOfElementByXpath(driver, PomObj.SuffixTrack());   
		WebElement ele2 =driver.findElement(By.xpath(PomObj.SuffixTrack()));
				
		       String actualValue =  ele2.getAttribute("innerHTML");
		       
		       System.out.println(actualValue);
		       
		       String actualValue1 = null;
		       
		     
		       
	         for(WebElement ele:ele1) {
											
	 actualValue1 = actualValue1 + ele.getAttribute("innerHTML");	 
	     
	         }
      
		String Expected="Man";
		String Expected1="Junior";
												    
						
		 System.out.println("-->"+actualValue1);	
		 
												    
if(actualValue1.contains(Expected)==true && actualValue.contains(Expected1)==true) {
		 
		      status="Pass";
		 
		 MyScreenRecorder.stopRecording();
			String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
						 		 util.deleteRecFile(RecVideo);
	 
	              }else {
	            	 				     				 		 		 
	   status = "Fail";
	 		 	     
	 	MyScreenRecorder.stopRecording();
	 		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	 		 				 	 util.MoveRecFile(RecVideo);									 
				remark="Given name or surname didn't dispalyed / Suffix didn't displayed";	
														    	 
				utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	              }
	 
			     if(actualValue1.contains(Expected)==true){
				    System.out.println("E1: No issues");									 	
				   }	
			     
			     if(actualValue1.contains(Expected)==true){
					    System.out.println("E1: No issues");									 	
					  }
	                        
								  }catch(Exception e){												
							
									  e.printStackTrace();
							
								}finally {
                             System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	                            }						    
					}
	
		
	
	

	@Test(alwaysRun = true)
			        			        
public void  AuthorNametest10() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);
	CUPAuthorNameEdit10 obj = new CUPAuthorNameEdit10();
			       obj.AuthorNameEdit10();
			       
		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }

