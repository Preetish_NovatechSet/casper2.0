package com.AuthorName;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUPAuthorNameEdit07 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameEdit07";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;
	
	public void AuthorNameEdit06() throws Exception{

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-->Double click on author name click on suffix then select Ms. update and save, check whether Ms. has added or not";			    
			area = "Author Name";
				  category = "Front matter, Add suffix";
				    
				  
				  driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
							System.out.println("BrowerName->"+uAgent);			                     

									
			try {
			      Switch switc = new Switch(driver);
			            
			          switc.SwitchCase(uAgent); 

			          
					PomObj=new Pom();	      
	     
					
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());						
        ele= driver.findElement(By.xpath(PomObj.Vanessa()));  
					
        
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
													    
			String value = ele.getAttribute("innerHTML");						

											
	if(value.contains(value)){

																		
					MoveToElement.byXpath(driver,  PomObj.Vanessa());
														
													    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());

	((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);


	driver.switchTo().defaultContent();
	 
	
	Thread.sleep(3000);
/*WaitFor.presenceOfElementByXpath(driver, "//*[@id='selectbox_suffix']/option[1]");						
System.out.println("Check 0");	
*/
   ele= driver.findElement(By.cssSelector("#selectbox_suffix"));
  
   Select sel = new Select(ele);
   sel.selectByVisibleText("Ms.");

	
	       WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
		      ele= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
								 	ele.click();   
								 	
								 	
							UpperToolBar ob = new UpperToolBar(driver);
								  	  ob.save_btn_method();
																								
	                }
			      } catch (Exception e) {
				e.printStackTrace();
			  }
									
	
			
	try {
		Switch switc = new Switch(driver);
		  switc.SwitchCase(uAgent);
										 
		WaitFor.presenceOfElementByXpath(driver, PomObj.SuffixTrack());
												    
		WebElement ele1 =driver.findElement(By.xpath(PomObj.SuffixTrack()));
						String actualValue =  ele1.getAttribute("innerHTML");
		     
		String Expected="Ms.";
												    
						System.out.println("-->"+actualValue);
												    
												    
					 if(actualValue.contains(Expected)==true) {
						 status="Pass";
						 
						 MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
										 		 util.deleteRecFile(RecVideo);
					 
					              }else {
					            	 				     				 		 		 
					   status = "Fail";
					 		 	     
					 	MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
					 		 				 	 util.MoveRecFile(RecVideo);					 
								remark="Ms. didn't displayed on screen";	
														    	 
				utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
															   							 
			if(actualValue.contains(Expected)==true){
								System.out.println("E1: No issues");
															 	
					}				 	
														
												    
												    }
						} catch (Exception e) {
												
							e.printStackTrace();
								}finally {
        System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	    }						    
								}
	
		
	

	@Test(alwaysRun = true)
			        
			        
public void  AuthorNametest7() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);
	CUPAuthorNameEdit07 obj = new CUPAuthorNameEdit07();
			               obj.AuthorNameEdit06();


		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }

