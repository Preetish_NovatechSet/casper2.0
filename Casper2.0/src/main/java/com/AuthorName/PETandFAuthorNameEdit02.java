package com.AuthorName;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.FigureContain.TandFPEBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class PETandFAuthorNameEdit02 extends TandFPEBaseClass{


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;
	
	/*Double click the author name and edit the Surname and save, check edited Surname name data and tracking*/
	
	public void AuthorNameEdit02() throws IOException{
	
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "TandF_PE Author name edit-->Double click the author name and edit the Surname and save, check edited Surname name data and tracking";
			className = "PETandFAuthorNameEdit02";    
				 area = "Author Name";
				      category = "Front matter, Edit Surname";
	
				      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
			            
	            	    System.out.println("BrowerName->"+uAgent);	
	    	 try 
	    	 {
	          	  Cookies cokies =new Cookies(driver);
		            Switch switc = new Switch(driver);
	            		    	    
	           	 /*ErsPEUrlString url = new ErsPEUrlString(driver);
			           url.TandFChangeC1toC2(uAgent);*/
 
	            				 cokies.cookies();
	            				   
	            		      switc.SwitchCase(uAgent);  
		           
					    	  
							   PomObj=new Pom();    
					      
				
							    
				WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName());


			  WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName()));  

				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
							    
							String value = ele.getAttribute("innerHTML");
							
					if(value.contains(value)){

			MoveToElement.byXpath(driver,  PomObj.AuthorName());
								
							    
	String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
										"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																				
					((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);
								
							
		Thread.sleep(1000);
								 
								 driver.switchTo().defaultContent();
								 
								 
						WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
					 WebElement ele1= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
								 
								 ((JavascriptExecutor)driver).executeScript(doubleClickJS,ele1);
								         ele1.click();
								         ele1.clear();
								         ele1.sendKeys("Kumar Mahato");
								         
								  
							WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
							WebElement ele2= driver.findElement(By.cssSelector(PomObj.Save()));           
								      
							         ele2.click();    
							         
							         
							         UpperToolBar obj1 = new UpperToolBar(driver);
									       obj1.save_btn_method();       
								
														
							}
						} catch (Exception e) {
					
							e.printStackTrace();
						}
					      
					      
					      
					      
					      
					      try{
					    	
					    	  String actualValue = null;  
					    	  String expectedValue1 = null;
					    	  
					    	  					    	  
	Thread.sleep(4000);
					  
					              
					           Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent); 
								
								
					WaitFor.presenceOfElementByCSSSelector(driver, PomObj.SurNameTrc());
							    
			List<WebElement> ele1 =driver.findElements(By.cssSelector(PomObj.SurNameTrc()));
							
							String ListGivenName ="";  
						   
							for(WebElement ele:ele1){
							 
							    actualValue =  ele.getAttribute("innerHTML");
							    ListGivenName+=actualValue + "\n";
							    expectedValue1 = "Kumar Mahato";
					}
						 
						 System.out.println("After save - " + ListGivenName); 
								
								 if(ListGivenName.contains(expectedValue1)==true){
									 status ="Pass";
								 }else{
									 
	remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
									    	 
									 status ="Fail"; 
									    	  
									    	
							 
		Thread.sleep(10000);  
							 
			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
										   
										 
				if(actualValue.contains(expectedValue1)==true) {
						System.out.println("E1: No issues");
										 	
				}
								       }			    	  
					         }catch (Exception e) {
								
								e.printStackTrace();
					     }finally {
					    	 
					    	 System.out.println(className);;
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
					     }
				}
	
	
	
	@Parameters({ "browser" })
	@Test(alwaysRun = true)
			        
			        
public void  AuthorNametest2() throws Throwable {
		
	
			    		try {
			    			
				    	PETandFAuthorNameEdit02 obj = new PETandFAuthorNameEdit02();
			    			obj.AuthorNameEdit02();



			    		}catch (Exception e){

			    			e.printStackTrace();
			    		}
			    	}
	            }  


