package com.AuthorName;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFPEBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class PETandFAuthorNameEdit07 extends TandFPEBaseClass{

	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;
	
	public void AuthorNameEdit07() throws Exception{
		Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "TandF_PE Author name edit-->Double click on author name and edit the Given name and save, check whether affiliation citation label present or not";
			className = "PETandFAuthorNameEdit07";    
				 area = "Author Name";
				      category = "Front matter, Edit Given name check affiliation citation label";
				 
				      
				      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
			            
	            	    System.out.println("BrowerName->"+uAgent);	
	    	 try {
	          	  Cookies cokies =new Cookies(driver);
		            Switch switc = new Switch(driver);
	            		    	    
	           	 /*ErsPEUrlString url = new ErsPEUrlString(driver);
			           url.TandFChangeC1toC2(uAgent);*/
 
	            				 cokies.cookies();
	            				   
	            		      switc.SwitchCase(uAgent);  
	            		     
	            		      PomObj=new Pom();	
						     
	  	WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());						
		WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName2()));  
																		
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
																			    
			String value = ele.getAttribute("innerHTML");						
																	
							if(value.contains(value)){
																								
					MoveToElement.byXpath(driver,  PomObj.AuthorName2());
																				
																			    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
						"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
											"{arguments[0].fireEvent('ondblclick');}";
																						
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());

						((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);

						   	driver.switchTo().defaultContent();
							 
  WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
							 
							    Thread.sleep(5000); 
							       ele1.click();
							       ele1.clear();
							       ele1.sendKeys("Vicky");
							       
			 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
				WebElement ele3= driver.findElement(By.cssSelector(PomObj.Save()));           
									ele3.click();  
									
						 UpperToolBar obj1 = new UpperToolBar(driver);
								       obj1.save_btn_method(); 
							       
							}
					} catch (Exception e1) {
					e1.printStackTrace();
				}
						
						try {
				              
				           Switch switc = new Switch(driver);
				              switc.SwitchCase(uAgent); 
							
							 
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.Affilation());
																	    
			 WebElement ele1 =driver.findElement(By.cssSelector(PomObj.Affilation()));
							String actualValue =  ele1.getAttribute("innerHTML");
	                               String Expected="b";
																	    
							System.out.println("-->"+actualValue);
																	    
																	    
				if(actualValue.contains(Expected)==true) {
								status ="Pass";
										}else{
																			 
			remark="Affilation Is Changed / Affilication is not there";	
																			    	 
								 status ="Fail"; 
																			    	  				    	
							  Thread.sleep(10000);  

				utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
																				   							 
			if(actualValue.contains(Expected)==true){
						System.out.println("E1: No issues");
																				 	
										}				 	
																													    
							      }
							} catch (Exception e) {
																	
								e.printStackTrace();
						}finally {
			                 System.out.println(className);;
							   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
						    }														    
			    }			
						
	
	
	
	
	@Test(alwaysRun = true)
			        			        
public void  AuthorNametest7() throws Throwable {
		
	try
	{
			
	PETandFAuthorNameEdit07 obj = new PETandFAuthorNameEdit07();
			               obj.AuthorNameEdit07();


		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }