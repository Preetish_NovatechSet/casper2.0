package com.AuthorName;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PETandFAddComment_AuthorName04 extends TandFAuthorName_CommentBaseClass{
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameCommentEdit04() throws InterruptedException, AWTException, IOException{
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF_PE Author name edit-->In the edited author name right click and add comment then check whether the edited name track present or not";
			className = "PETandFAddComment_AuthorName04";    
				  area = "Author Name";
					 category = "Add Comment, Edit GivenName";
	
						     
						     driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
								
								System.out.println("BrowerName->"+ uAgent);			                     
										
			try {
				
					Cookies cokies =new Cookies(driver);
			          
		           
				      Switch switc = new Switch(driver);
				            
				          	              
				    /*  TandFC1toC2 url = new TandFC1toC2(driver);
						url.TandFAuthorChangeC1toC2(uAgent);*/
							       			      
							      cokies.cookies();
							   
					           switc.SwitchCase(uAgent);  
						      
						      		    	  
						    	  
								PomObj=new Pom();   
						      
						WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
		                WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName2()));  
			
							driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
								    
				String value = ele.getAttribute("innerHTML");
								
					if(value.contains(value)){
			
								WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
									MoveToElement.byXpath(driver,PomObj.AuthorName2());	      
						   						    
									
									Actions action= new Actions(driver);
							WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());
									action.contextClick(ele).build().perform();
										
									 driver.switchTo().defaultContent();
									
									//AddComment
		 /************************************************************************/							 
							
									 Robot robot = new Robot(); 	
									 switch (uAgent){							   
					 case "firefox":
							    				
							Thread.sleep(3000); 
					   robot.keyPress(KeyEvent.VK_ENTER);
							    			
							    			break;								   
					 case "ie":
								    			
								Thread.sleep(3000); 
						robot.keyPress(KeyEvent.VK_ENTER);
								    			
								    		break;
							    	}
							    			
					if(uAgent.equals("chrome")||uAgent.equals("opera")) {
									
										System.out.println("1");		
							WaitFor.presenceOfElementByXpath(driver, PomObj.Comment());
									System.out.println("2");
			                  WebElement comment= driver.findElement(By.xpath(PomObj.Comment()));			
			                
			                      //MoveToElement.byXpath(driver,PomObj.Comment());				                     
				         
				           Thread.sleep(5000);
		       ((JavascriptExecutor)driver).executeScript("arguments[0].click();", comment);		
				           
				      
					}
				      	    
				      
				      
				      
				      /************************/
				        WaitFor.presenceOfElementByCSSSelector(driver, PomObj.EditorEditor());
				      WebElement editor= driver.findElement(By.cssSelector(PomObj.EditorEditor()));	
				     	       MoveToElement.byCssSelector(driver, PomObj.EditorEditor());
				           	        MoveToElement.Click_sendkey(driver, editor, "Thank u");
				     	  		           	        
				           	        
				           	 WaitFor.presenceOfElementByXpath(driver, PomObj.SumbitDilogBox());
				   		 WebElement sumbit= driver.findElement(By.xpath(PomObj.SumbitDilogBox()));
				   		     MoveToElement.byCssSelector(driver, PomObj.EditorEditor());
				   		 
				   		     String  value1 = sumbit.getText();
				   		 
				   		 if(value1.contains("Submit")) {
				   			
				   			       sumbit.click();
				   	    }		     
				   }
					
					 //DobleClick On Author
/************************************************************************/
					switc.SwitchCase(uAgent);
					
					
					WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());						
					WebElement ele1= driver.findElement(By.xpath(PomObj.AuthorName2()));  
																
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
																	    
									
					String value1 = ele1.getAttribute("innerHTML");						

															
					            if(value1.contains(value1)){

							MoveToElement.byXpath(driver,  PomObj.AuthorName2());
																									    
					String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
							 "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
								                     "{arguments[0].fireEvent('ondblclick');}";
																				
					WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName2());

					((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);


					driver.switchTo().defaultContent();
					 

					   WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
					WebElement ele2= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
					 
					 Thread.sleep(5000); 
					 
					 MoveToElement.byCssSelector(driver, PomObj.Save());
					 
					 WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
					       ele2.click();
					          ele2.clear();
					             ele2.sendKeys("Yogi");
					       
					       
					       WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
					       WebElement ele3= driver.findElement(By.cssSelector(PomObj.Save())); 
					       
					        MoveToElement.byCssSelector(driver, PomObj.Save());
					        
					        WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
					        
											ele3.click();  
											
									UpperToolBar obj1 = new UpperToolBar(driver);
										       obj1.save_btn_method();
																				
					            }				
		}catch(Exception e){
			   e.printStackTrace();
		       }
		
		
		try {
			Switch switc = new Switch(driver);	
			
			 switc.SwitchCase(uAgent); 
				
			  String actualValue = null;  
  	          String expectedValue1 = null;
  	  
			    WaitFor.presenceOfElementByCSSSelector(driver, PomObj.givenNameTrc());
			    
			List<WebElement> ele1 =driver.findElements(By.cssSelector(PomObj.givenNameTrc()));
			
			String ListGivenName ="";  
   
			for(WebElement ele:ele1){
			 
			    actualValue =  ele.getAttribute("innerHTML");
			    ListGivenName+=actualValue + "\n";
			    expectedValue1 = "Yogi";
				
 }
 
 System.out.println("After save - " + ListGivenName); 
				
	if(ListGivenName.contains(expectedValue1)==true){
					 status ="Pass";
				 }else{
					 
			   remark="Insert Trac is missing";	
					    	 
						        status ="Fail"; 
					    	  		 
			 Thread.sleep(10000);  
			 
						utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
						   
				 } 
						
	               if(actualValue.contains(expectedValue1)==true) {
						 	   System.out.println("E1: No issues");				 		
			
				     }
		} catch (Exception e) {
			
			e.printStackTrace();
		}	finally {
			
			System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
		 
		  }                
		}
	
		    
	@Test(alwaysRun = true)
			       		        
public void  AuthorCommenttest4() throws Throwable {
		
			  try {
				  
			
			    PETandFAddComment_AuthorName04 obj = new PETandFAddComment_AuthorName04();
			    			obj.AuthorNameCommentEdit04();


			    		}catch (Exception e){

			    			e.printStackTrace();
			    		}
			    	}
	          }
