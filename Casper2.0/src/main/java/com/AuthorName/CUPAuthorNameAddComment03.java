package com.AuthorName;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUPAuthorNameAddComment03 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameAddComment03";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameAddComment() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
 description = "Casper-CUP_CopyEditor-->Left Click on author name then add comment then Double click the author name and edit the Surname name and save, check whether given name has trac or not & check the added comment is present or not";			    
	area = "Author Name";
	    category = "Front matter,Edit the given name";

			    
			    driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);				
				        System.out.println("BrowerName->"+uAgent);			                     
						
try {

                              
         Switch switc = new Switch(driver);                    	   
            switc.SwitchCase(uAgent);
             	        			       			    
		    					    	  
				     PomObj=new Pom();   
		      
				WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());
              WebElement ele= driver.findElement(By.xpath(PomObj.Magorzata()));  

				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
				    
				String value = ele.getAttribute("innerHTML");
				
		if(value.contains(value)){

				WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());
					MoveToElement.byXpath(driver,PomObj.Magorzata());	      
		   
					    Actions action= new Actions(driver);
				WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());
						action.contextClick(ele).build().perform();
					   		Thread.sleep(800);
											
						   driver.switchTo().defaultContent();
						       Robot robot = new Robot(); 
   		              
						       Thread.sleep(3000); 
					   robot.keyPress(KeyEvent.VK_ENTER);      
						       
						       
					   
					   Thread.sleep(3000);
					WaitFor.presenceOfElementByXpath(driver, PomObj.Comment());						
						  ele= driver.findElement(By.xpath(PomObj.Comment()));
						        
						    for(int i=0;i<2;i++){
						            Thread.sleep(2000);
						        MoveToElement.byclick(driver, ele);
						           Thread.sleep(2000);
						    }
						       
						 MoveToElement.bysendkeyWithoutclick(driver, ele, "Hello bro");
						
			WaitFor.presenceOfElementByXpath(driver, PomObj.Submit());						
			      ele= driver.findElement(By.xpath(PomObj.Submit()));
				                 ele.click();
				                 
				                 Thread.sleep(3000);
			                   switc.SwitchCase(uAgent);    
			                   
			                   
			       WaitFor.presenceOfElementByXpath(driver, PomObj.Magorzata());
			   WebElement AuthorClick= driver.findElement(By.xpath(PomObj.Magorzata()));  
			       driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
			       		String value2 = AuthorClick.getAttribute("innerHTML");			

		if(value2.contains(value2)){
			       											
			       		MoveToElement.byXpath(driver,  PomObj.Magorzata());
			       							
 String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
			       		"{arguments[0].fireEvent('ondblclick');}";
			       									
			       	  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
			      ((JavascriptExecutor)driver).executeScript(doubleClickJS,AuthorClick);
			       		   		 
			       		   				       		
			  Thread.sleep(1000);
			       							 
			       driver.switchTo().defaultContent();
			       							 			       							 
			      WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
			    WebElement ele1= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
			       							 
			       				ele1.click();
			       						ele1.clear();
			       							      ele1.sendKeys("Vicky.j");
			       							         
			       							         
			  WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
			      ele= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
						ele.click();  
			       							         
			       							         			       			
			       	UpperToolBar obj1 = new UpperToolBar(driver);
			       			obj1.save_btn_method();
			                   			                   
			            }    
					   
				 }
		
	                  	   
             switc.SwitchCase(uAgent);
	    
	           WaitFor.presenceOfElementByXpath(driver, PomObj.AuthornameTrack());												    
		   List<WebElement> ele1 =driver.findElements(By.xpath(PomObj.AuthornameTrack()));  
		  
		   		   
		   String actualValue1="";							
	                    
		          for(WebElement ele11:ele1){

      actualValue1 = actualValue1 + ele11.getAttribute("innerHTML");  
		         
		         }
		          
		          System.out.println("Trac--> "+actualValue1);
	    
	    
	WaitFor.presenceOfElementByXpath(driver, PomObj.FullAuthorNameAreaValidation());   
		  	 ele =driver.findElement(By.xpath(PomObj.FullAuthorNameAreaValidation()));    
	       
		  	  String actualValue =  ele.getAttribute("innerHTML");
	       
	       System.out.println("Comment- "+actualValue);
            
	          String Expected="Hello bro";
              String GivenName="Vicky.j";
											    
 if(actualValue.contains(Expected)==true && actualValue1.contains(GivenName)==true) {
	 status="Pass";
	 
	MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
					 		 util.deleteRecFile(RecVideo);
 
              }else {
            	 				     				 		 		 
   status = "Fail";
 		 	     
 	MyScreenRecorder.stopRecording();
 		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
 		 				 	 util.MoveRecFile(RecVideo);
 		 				 	 
			remark="Comment added didnt displayed";	


			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
              }									   							 
		if(actualValue.contains(Expected)==true){
							System.out.println("E1: No issues");									 	
			     	 }				 					
		     if(actualValue1.contains(GivenName)==true){
			                 System.out.println("E2: No issues");										 	
 	           }
       } catch (Exception e) {
											
						e.printStackTrace();
							}finally{
                   System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
                                }						    
						}
	
	
	
@Test(alwaysRun = true)
	               
public void  AuthorCommenttest03() throws Throwable {

try {
	
	     MyScreenRecorder.startRecording(uAgent,className);
	     
	CUPAuthorNameAddComment03 obj = new CUPAuthorNameAddComment03();
	    			obj.AuthorNameAddComment();



	    		}catch (Exception e){

	    			e.printStackTrace();
	    		}finally {
	    		    System.out.println(className);
	    	Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	    		    }	
	    	}
	}