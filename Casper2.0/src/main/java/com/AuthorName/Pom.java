package com.AuthorName;


public class Pom {

	
	 private static String element=null;
//	 private static List<WebElement> ele = null;

	 
	 
		public String Position() {
			
			element = "#aff_cite";
			return element;
		}
		
		public String DepartmentName() {
			 		
			 element="#department_name";		 
			       return element;
		 }
		
		public String InstitudtionName() {
			 	
			 element="#institution_name_2";		 
			       return element;
		 }
		
		public String Address() {
			 		
			 element="#aff_address";		 
			       return element;
		 }
		
		
		public String City() {
			
			 element="#aff_city";		 
		       return element;
		}
		
		
		public String State() {
			
			 element="#aff_state";		 
		       return element;
		}
		
		
		public String Post_Code() {
			
			 element="#aff_postal";		 
		       return element;
		}
		
		
		public String Country(){
			
			 element="#aff_country";		 
		       return element;
		}
		
		public String validateAffilation() {
			element="[ref-type='aff']";
			return element;
		}
	 
	 
	 public String Turgeon() {
	 
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[7]/span/span[2]";	 
	 
	           return element;
	 }
	 
	 public String Orcidvalue() {
			
			element = "//*[@id='orcidValue']";

			return element;
						
		}
	 
	 public String Validation_OrcidAuthor_name() {
			
			element = "//div[@id='displayOrcid']";

			return element;
						
		}
	 
	 
	 public String Clear() {
			
			element = "//*[@id='orcidClearButton']";
			     return element;				
		}
	 
	 
	public String Search() {
			
			element = "//*[@id='orcidButton']";

			return element;
						
		} 
	
	
	public String Add() {
		
		element = "//*[@id='saveOrcid']";

		return element;						
}

	
	
public String validation_PopUP() {
		
		element = "//div[@class='sweet-alert showSweetAlert visible']";

		    return element;						
}
	
	

public String YesAddIt() {

    element = "//button[@class='confirm']";

return element;
			
}


public String OK() {

      element = "//button[@class='confirm']";
               
return element;
			
}
	 
	 

	public String Table2_Footnote(){
		
		element = ".//*[@id='T0002']/div/div";
			
        return element;
	}
	
	public static String Table2_Footnote1(){
		
element = ".//*[@id='T0002']/div/div";
		 return element;
	}
		 
		 public static String Table2_Footnote2(){
			
 element = ".//*[@id='T0003']/div/div";
				
     return element;
	}
	
	public static String Table3_Footnote(){
			 
        element = ".//*[@id='T0003']/div/div";
				
     return element;
	}
	
	public static String Table5_Footnote(){
		 
        element = ".//*[@id='T0004']/div/div";
				
     return element;
	}
	
	
	public static String ClickOnEle_Add_Footnote(){
		 
        element = ".//*[@id='T0005']/table/tbody/tr[2]/td[2]";
				
     return element;
	}
	
	
	public static String Add_Footnote(){
		 
        element = ".//*[@id='cke_54']";
				
     return element;
	}
	
	
	public static String label(){
		 
        element = ".//*[@id='txt_footnode_heading1']";
				
     return element;
	}
	
	
	public static String Description(){
		 
        element = ".//*[@id='txt_foot_desc1']";
				
     return element;
	}
	
	
	public static String save(){
		 
        element = ".//*[@id='model_make_tblfootnote']/div/div[1]/div[2]/input[2]";
				
     return element;
	}
	
	
	
	public String save_btn_method() {		
		element = ".//*[@title='Save (Ctrl+S)']";
	     return element;
			
}

public String AuthorName() {		
	element = ".//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[1]/span[2]/span[1]";
     return element;
		
}	

public String GivenTextBox(){
	element="#up_contrib_name";
	  return element;
}


public String SurnameTextBox(){
	element="#up_contrib_surname";
	  return element;
}


public String AuthorName1(){
	element="//*[text()='Hakyemez-Paul']";
	  return element;
}

public String AuthorName2(){
	element="//*[text()='Pihlaja']";
	  return element;
}

public String Vanessa(){
	
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[3]/span/span[1]";
	  return element;
}

public String Magorzata(){
	
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[5]/span/span[1]";
	   return element;
}

public String Ranna(){
	
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[6]/span/span[1]";
	   return element;
}


public String Comment(){
	
	element="//iframe[@id='commentBoxIframe']";
	  return element;
}

public String validatecomment(){
	
	element="//*[@class='fa fa-comment']";
	  return element;
}


public String FullAuthorNameAreaValidation(){
	
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p";
	  return element;
}



public String Submit(){
	
	element="//td[@class='cke_dialog_ui_hbox_first']";
	  return element;
}



public String Gianni(){
	
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[4]/span/span[1]";
	  return element;
}




public String PPLAuthorName2(){
	element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[3]/span/span[1]";
	  return element;
}


public String AuthorName3(){
	
element="//*[text()='Michaela']";
	  
	return element;
}


public String Save(){
	element="//div[@id='model_make_aff']//input[@value='Save']";
	  return element;
}


public String validateInstitution() {
	element="//p[contains(text(),'Institution name is required field for Adding Affi')]";
	return element;
}

public String validateCity() {
	element="//p[contains(text(),'City is required field for Adding Affiliation !!')]";
	return element;
}

public String validateCountry() {
	element="//p[contains(text(),'Country is required field for Adding Affiliation !')]";
	return element;
}

public String givenNameTrc(){
	element=".given-names";
	  return element;
}



public String SurNameTrc(){
	element=".surname";
	  return element;
}

public String Warringstatus_givenname(){
	element="#status_givenname";
	  return element;
}


public String SuffixTrack(){
	element="//*[@class='suffix']";
	  return element;
}


public String Warringstatus_Surnamename(){
	element="#status_surname";
	  return element;
    }



public String Affilation(){
	element="#iframeID >article>front>article-meta>div.contrib-group>p>span:nth-child(3)>a";
	  return element;
    }

public String PPLAffilation(){
	element="//span[@class='contrib']//a[@class='xref'][contains(text(),'4')]";
	  return element;
    }


public String Remove_suffix() {
   element = "input#remove_suffix";
            return element;
}



public String Affilation1(){
	element="#iframeID >article>front>article-meta>div.contrib-group>p>span:nth-child(3)>a";
	  return element;
    }


	public String CasperUpdate(){
		element="//*[@id='btnAuthorName']";
		  return element;
	}

	
	public String AuthornameTrack() {
		element="//*[@class='name nova-edited']/ins";
          return element;
	}

	public String Monta�o_Machado(){
		
		element="//*[@id='iframeID']/article/front/article-meta/div[3]/p/span[3]/span[1]/span";
		  return element;
	}
}
