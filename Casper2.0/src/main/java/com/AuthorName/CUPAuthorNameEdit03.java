package com.AuthorName;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUPAuthorNameEdit03 extends CUP_BaseClass {
	

	static String remark;
	static String className = "CUPAuthorNameEdit03"; 
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	
	
	

public void AuthorNameEdit03() throws Exception{
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-->Double click the author name  and edit the Given name and Surname then save, check edited given name and surname data and tracking";
		   
			 area = "Author Name";
			     category = "Front matter, Edit GivenName & SurnameName";
			      
			      
			     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
		            
         	    System.out.println("BrowerName->"+uAgent);	
 	try { 
       	
	            Switch switc = new Switch(driver);
      				   
         		      switc.SwitchCase(uAgent);  
	      
				    	  
						   PomObj=new Pom();
						    	
					  WaitFor.presenceOfElementByXpath(driver, PomObj.Carlo());						
					WebElement ele= driver.findElement(By.xpath(PomObj.Carlo()));  
							
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
								    
			String value = ele.getAttribute("innerHTML");						

if(value.contains(value)){
												
						MoveToElement.byXpath(driver,  PomObj.Carlo());
														    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
							"{arguments[0].fireEvent('ondblclick');}";
											
WaitFor.presenceOfElementByXpath(driver, PomObj.Carlo());

						((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);	
													
					driver.switchTo().defaultContent();
						 						
		   WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
		 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
					 
					 Thread.sleep(5000); 
					 
				ele1.click();
					   ele1.clear();
					         ele1.sendKeys("OM");	
		
		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
	WebElement ele2= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));
						 
						 Thread.sleep(5000); 
						 
						         ele2.click();
						         ele2.clear();
						         ele2.sendKeys("rudra");	
						         
				WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
			  WebElement ele3= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
										ele3.click();      
								
						UpperToolBar obj1 = new UpperToolBar(driver);
								     obj1.save_btn_method();	
								     
								     
								}
				        } catch (Exception e){
					e.printStackTrace();
				}	
			    
			    	              
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent); 
	              
					
	              
     try {
					  String actualValue = null;  
					  String expectedValue1 = null;
					  String expectedValue2 = null;
					  
			WaitFor.presenceOfElementByXpath(driver, PomObj.AuthornameTrack());
					    
	 List<WebElement> ele1 =driver.findElements(By.xpath(PomObj.AuthornameTrack()));
					
					String ListGivenName ="";  
   
					for(WebElement ele:ele1){
					 
					    actualValue =  ele.getAttribute("innerHTML");
					    ListGivenName+=actualValue + "\n";
					    expectedValue1 = "OM";
					    expectedValue2 = "rudra";
 }
					
		
					
					System.out.println("After save - " + ListGivenName);
					
if(ListGivenName.contains(expectedValue1)==true && ListGivenName.contains(expectedValue2)){
						
 status="Pass";
	 
	 MyScreenRecorder.stopRecording();
		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
					 		 util.deleteRecFile(RecVideo);
 
              }else {
            	 				     				 		 		 
   status = "Fail";
 		 	     
 	MyScreenRecorder.stopRecording();
 		String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
 		 				 	 util.MoveRecFile(RecVideo);
 		 				 	 
            remark="Insert given name and surname Trac is missing";	
						    	 
 
		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
							   							 
  if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)){
					System.out.println("E1: No issues");
							 	
							 	}
					       }
				} catch (Exception e){
					e.printStackTrace();
				}finally {
					System.out.println(className);;
					   Ex.testdata(description, className, remark, category, area, status, uAgent);			
				   }
        }


@Test(alwaysRun = true)
		        
		        
public void  EditAuthorNameTest03() throws Throwable {
	
		
try {
	
	MyScreenRecorder.startRecording(uAgent,className);
	
	   CUPAuthorNameEdit03 obj = new CUPAuthorNameEdit03();
			                obj.AuthorNameEdit03();

		}catch (Exception e){

			e.printStackTrace();
	    	}
    	}
    }