package com.AuthorName;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.Pom;
import com.FigureContain.TandFPEBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PETandFAuthorNameEdit03 extends TandFPEBaseClass {
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	
	
	

public void AuthorNameEdit03() throws Exception{
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF_PE Author name edit-->Double click the author name and edit the Given name and Surname then save, check edited given name and surname data and tracking";
		className = "PETandFAuthorNameEdit03";    
			 area = "Author Name";
			     category = "Front matter, Edit GivenName & SurnameName";
			      
			      
			     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
		            
         	    System.out.println("BrowerName->"+uAgent);	
 	try { 
       	  Cookies cokies =new Cookies(driver);
	            Switch switc = new Switch(driver);
         		    	    
        	/* ErsPEUrlString url = new ErsPEUrlString(driver);
		           url.TandFChangeC1toC2(uAgent);*/

         				 cokies.cookies();
         				   
         		      switc.SwitchCase(uAgent);  
	      
				    	  
						   PomObj=new Pom();
						    	
					  WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName1());						
					WebElement ele= driver.findElement(By.xpath(PomObj.AuthorName1()));  
							
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
								    
			String value = ele.getAttribute("innerHTML");						

if(value.contains(value)){
												
						MoveToElement.byXpath(driver,  PomObj.AuthorName1());
														    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
							"{arguments[0].fireEvent('ondblclick');}";
											
WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorName1());

						((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);	
													
					driver.switchTo().defaultContent();
						 
						
		 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
		 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
					 
					 Thread.sleep(5000); 
					         ele1.click();
					         ele1.clear();
					         ele1.sendKeys("OM");	
					
					
					
		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
	WebElement ele2= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));
						 
						 Thread.sleep(5000); 
						 
						         ele2.click();
						         ele2.clear();
						         ele2.sendKeys("rudra");	
						         
						         
WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Save());
WebElement ele3= driver.findElement(By.cssSelector(PomObj.Save()));           
									   ele3.click();       
								
									   UpperToolBar obj1 = new UpperToolBar(driver);
								       obj1.save_btn_method();							
								}
				} catch (Exception e) {
					e.printStackTrace();
				}	
			    
			    
		
	              
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent); 
					
	              
				  try {
					  String actualValue = null;  
					  String expectedValue1 = null;
					  String expectedValue2 = null;
					  
					 WaitFor.presenceOfElementByCSSSelector(driver, PomObj.givenNameTrc());
					    
					List<WebElement> ele1 =driver.findElements(By.cssSelector(PomObj.givenNameTrc()));
					
					String ListGivenName ="";  
   
					for(WebElement ele:ele1){
					 
					    actualValue =  ele.getAttribute("innerHTML");
					    ListGivenName+=actualValue + "\n";
					    expectedValue1 = "OM";
						
 }
					
					WaitFor.presenceOfElementByCSSSelector(driver, PomObj.SurNameTrc());
					
					List<WebElement> ele2 =driver.findElements(By.cssSelector(PomObj.SurNameTrc()));
					
					String ListSurnameName ="";  
   
					for(WebElement ele:ele2){
					 
					    actualValue =  ele.getAttribute("innerHTML");
					    ListSurnameName+=actualValue + "\n";
					    expectedValue2 = "rudra";
						
 }
					
					System.out.println("After save - " + ListGivenName+"\n"+ListSurnameName);
					
					 if(ListGivenName.contains(expectedValue1)==true && ListSurnameName.contains(expectedValue2)){
						 status ="Pass";
					 }else{
						 
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
						    	 
							        status ="Fail"; 
						    	  				    	
 Thread.sleep(10000);  
 
							utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
							   							 
						if(actualValue.contains(expectedValue1)==true&&actualValue.contains(expectedValue2)) {
							 	 System.out.println("E1: No issues");
							 	
							 	}
					       }
				} catch (Exception e){
					e.printStackTrace();
				}finally {
					System.out.println(className);;
					   Ex.testdata(description, className, remark, category, area, status, uAgent);			
				   }
        }


@Test(alwaysRun = true)
		        
		        
public void  AuthorNametest3() throws Throwable {
	
		
try {
	
	   PETandFAuthorNameEdit03 obj = new PETandFAuthorNameEdit03();
			                obj.AuthorNameEdit03();

		}catch (Exception e){

			e.printStackTrace();
	    	}
    	}
    }

