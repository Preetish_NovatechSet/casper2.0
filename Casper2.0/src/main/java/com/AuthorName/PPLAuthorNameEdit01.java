package com.AuthorName;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class PPLAuthorNameEdit01 extends PPL_BaseClass {


	/*Double click the author name and edit the Given name and save, check edited given name data and tracking*/
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;

	
	
	public void AuthorNameEdit01() throws InterruptedException, IOException{
		
		try {	
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_Author name edit-->Double click the author name and edit the Given name and save, check edited given name data and tracking";
		className = "PPLAuthorNameEdit01";    
			 area = "Author Name";
			      category = "Front matter, Edit Given name and Surname";
		
			      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
		            
          	    System.out.println("BrowerName->"+uAgent);	
  	 
        	  Cookies cokies =new Cookies(driver);
	            Switch switc = new Switch(driver);
          		    	    
         	 /*ErsPEUrlString url = new ErsPEUrlString(driver);
		           url.TandFChangeC1toC2(uAgent);*/

          				 cokies.cookies();
          				   
          		      switc.SwitchCase(uAgent);  
	       
			    	  
					   PomObj=new Pom();
					    
			    WaitFor.presenceOfElementByXpath(driver, PomObj.AuthorNameSahar());

				WebElement ele= driver.findElement(By.xpath(PomObj.AuthorNameSahar()));  

				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
					    
					   String value = ele.getAttribute("innerHTML");
					
if(value.contains(value)){
										
						MoveToElement.byXpath(driver,  PomObj.AuthorNameSahar());
						
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
								"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
								"{arguments[0].fireEvent('ondblclick');}";
								
                      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
						((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);
						
					
Thread.sleep(1000);
						 
						 driver.switchTo().defaultContent();
						 
						 
					WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
				 WebElement ele1= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  
						 
							
						         ele1.click();
						         ele1.clear();
						         ele1.sendKeys("Preetish");
						         
						  
					WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
				 WebElement ele2= driver.findElement(By.xpath(PomObj.CasperUpdate()));           
						      
					         ele2.click();    
					         
					         
					         UpperToolBar obj1 = new UpperToolBar(driver);
							         obj1.save_btn_method();       
														
					     }
				} catch (Exception e) {
			
					e.printStackTrace();
			}
			      
			      
			      
			      
			      
			      try{
			    	
			    	
			    	  			    	  
			  Thread.sleep(4000);
			    
			              
			           Switch switc = new Switch(driver);
			              switc.SwitchCase(uAgent); 
						
					  String actualValue = null;  
			    	  String expectedValue1 = null;
			    	  
					     WaitFor.presenceOfElementByCSSSelector(driver, PomObj.givenNameTrc());
					    
					List<WebElement> ele1 =driver.findElements(By.cssSelector(PomObj.givenNameTrc()));
					
					String ListGivenName ="";  
				   
					for(WebElement ele:ele1){
					 
					    actualValue =  ele.getAttribute("innerHTML");
					    ListGivenName+=actualValue + "\n";
					    expectedValue1 = "Preetish";
						
				 }
				 
				 System.out.println("After save - " + ListGivenName); 
						
						 if(ListGivenName.contains(expectedValue1)==true){
							 status ="Pass";
						 }else{
							 
				remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
							    	 
								        status ="Fail"; 
							    	  
							    	
					 
					 Thread.sleep(10000);  
					 
								utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
								   
								 
								 if(actualValue.contains(expectedValue1)==true) {
								 	   System.out.println("E1: No issues");
								 	
								 	}
						       }			    	  
			         }catch (Exception e) {
						
						e.printStackTrace();
			      }finally {
			    	       System.out.println(className);;
					   Ex.testdata(description, className, remark, category, area, status, uAgent);
			      }
				
	      }
	
	
	
	@Test(alwaysRun = true)
			        			        
public void  AuthorNameTest1() {
	try   {
			    			
			    PPLAuthorNameEdit01 obj = new PPLAuthorNameEdit01();
			    			obj.AuthorNameEdit01();


			    		}catch (Exception e){
                            e.printStackTrace();
			    		}
			    	}
               }
	

