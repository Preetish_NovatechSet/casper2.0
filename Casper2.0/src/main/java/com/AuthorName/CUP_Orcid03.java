package com.AuthorName;


import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_Orcid03 extends CUP_BaseClass{

	
	static String remark;
	static String className = "CUP_Orcid03"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
    Assertion hardAssert = new Assertion();
    
    
    public void Orcid() throws Exception{
    		

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Orcid-->Go to Author name right click add orcid fill the orcid id then search, add & check whether orcid icon has been added or not";		     
		       area = "Author Name";
		            category = "Add ORCID";
		          				   
		            System.out.println("BrowerName->"+uAgent);                 
			           
		         			          		             
				       Switch switc = new Switch(driver);
				             switc.SwitchCase(uAgent);  
	                
					            PomObj=new Pom(); 
					            
	WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
WebElement Turgeon = driver.findElement(By.xpath(PomObj.Turgeon()));	 
		 
        MoveToElement.byXpath(driver,PomObj.Turgeon());	      


Actions action= new Actions(driver);
    WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
action.contextClick(Turgeon).build().perform();


 Robot robot = new Robot(); 
     Thread.sleep(2000);
 robot.keyPress(KeyEvent.VK_DOWN);
 
         Thread.sleep(3000);
              robot.keyPress(KeyEvent.VK_DOWN);
               
               action.sendKeys(Keys.ENTER).build().perform();
  
        Thread.sleep(2000);
    
        driver.switchTo().defaultContent();

   WaitFor.presenceOfElementByXpath(driver, PomObj.Orcidvalue());
WebElement valueOrcid = driver.findElement(By.xpath(PomObj.Orcidvalue()));	
                
                 MoveToElement.byclick(driver, valueOrcid);
                 Thread.sleep(2000);
  MoveToElement.bysendkeyWithoutclick(driver, valueOrcid,"0000-0002-1825-0097");
  
  
     WaitFor.presenceOfElementByXpath(driver, PomObj.Search());
  WebElement search = driver.findElement(By.xpath(PomObj.Search()));
       MoveToElement.byclick(driver, search);

       Thread.sleep(5000);
  WaitFor.presenceOfElementByXpath(driver, PomObj.Add());
  WebElement add = driver.findElement(By.xpath(PomObj.Add()));
       MoveToElement.byclick(driver, add);
		         
       
       WaitFor.presenceOfElementByXpath(driver, PomObj.validation_PopUP());
       WebElement validatedPopUP = driver.findElement(By.xpath(PomObj.validation_PopUP()));

       String value = validatedPopUP.getAttribute("innerHTML");
       
       if(value.contains("Author name mismatch")==false) {
    	   
    	    status="Fail";
     	  
           MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
                util.MoveRecFile(RecVideo);
             	  
               }else {
            	   
            	   
            	   Thread.sleep(5000);
            	   
              WaitFor.presenceOfElementByXpath(driver, PomObj.YesAddIt());
           WebElement yesadd = driver.findElement(By.xpath(PomObj.YesAddIt()));
            	            
            	          MoveToElement.byclick(driver, yesadd);
            	          
            	              Thread.sleep(5000);
            	          WaitFor.presenceOfElementByXpath(driver, PomObj.OK());
            	         WebElement OK = driver.findElement(By.xpath(PomObj.OK()));
            	                      
            	                    MoveToElement.byclick(driver, OK);	   
    	   
       }
       


                    
                    
                  UpperToolBar obj1 = new UpperToolBar(driver);
     				  obj1.save_btn_method();  
     				  
                    
                    
          
                    Thread.sleep(5000);
                    switc.SwitchCase(uAgent);
                    
              WaitFor.presenceOfElementByXpath(driver, PomObj.FullAuthorNameAreaValidation());
         WebElement Tooltip = driver.findElement(By.xpath(PomObj.FullAuthorNameAreaValidation()));
                        
                  MoveToElement.byXpath(driver,PomObj. FullAuthorNameAreaValidation());   
                  
                       String value1 = Tooltip.getAttribute("innerHTML");    
                       String Expected ="0000-0002-1825-0097</span>";
                              
                              System.out.println("Validation"+value1);
                              
                              
                              if(value1.contains(Expected)==true) {
                            		
                            			status ="Pass";
                            			
                         MyScreenRecorder.stopRecording(); 		
        String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);                        
                            		 util.deleteRecFile(RecVideo);
                              }else {
                            	  status="Fail";
                            	  
                          MyScreenRecorder.stopRecording();
        String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
                               util.MoveRecFile(RecVideo);
                           
                remark="after putting orcid in Author name add orcid didn't add / Pop up apper Author name mismatch";      
                              }      
                       }
    
	    	   	  
	
@Test(alwaysRun=true)
		
public void OrcidTest03() throws Throwable{
				     			     					
   try{
	   
	 MyScreenRecorder.startRecording(uAgent,className);
	   
	         CUP_Orcid03 obj = new CUP_Orcid03();
			         obj.Orcid();
			     
	     }catch (Exception e){			
		          e.printStackTrace();
		      }finally{
		  System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);													    
		}
    }
}