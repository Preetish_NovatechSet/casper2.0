package com.AuthorName;


import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUPAuthorNameEdit11 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameEdit11";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	
	
	public void AuthorNameEdit11() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
     description = "Casper-CUP_CopyEditor-->Double click on author name Edit Given-name and Surname-name then click on suffix select Junior. update and save, check whether surname has been trac or not and check the suffix Senior has been updated or not";			    
				 area = "Author Name";
				     category = "Front matter, Edit surname name & Add suffix";
				   
				     
				      driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
				      
			System.out.println("BrowerName->"+uAgent);			                     

									
			try {
				
		          
	              
			          Switch switc = new Switch(driver);			            		          	              							   
				           switc.SwitchCase(uAgent); 

					PomObj=new Pom();	      
	     
	WaitFor.presenceOfElementByXpath(driver, PomObj.Gianni());						
        ele= driver.findElement(By.xpath(PomObj.Gianni()));  
												
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
													    
				String value = ele.getAttribute("innerHTML");						

											
	if(value.contains(value)){

			MoveToElement.byXpath(driver,  PomObj.Gianni());
														
													    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																
	WaitFor.presenceOfElementByXpath(driver, PomObj.Gianni());

	    ((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);

	driver.switchTo().defaultContent();
	 
	 Thread.sleep(2000);
	 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.GivenTextBox());
     ele= driver.findElement(By.cssSelector(PomObj.GivenTextBox()));  				
			         ele.click();
			         ele.clear();
			     	 ele.sendKeys("Steven");
	 
	 
	 
			Thread.sleep(2000);
	WaitFor.presenceOfElementByCSSSelector(driver,PomObj.SurnameTextBox());
       ele= driver.findElement(By.cssSelector(PomObj.SurnameTextBox()));  
				
			         ele.click();
			         ele.clear();
			     	 ele.sendKeys("Van");
	
			         
			 Thread.sleep(3000);
			         
   ele= driver.findElement(By.cssSelector("#selectbox_suffix"));
  
         Select sel = new Select(ele);
        sel.selectByVisibleText("Senior");

	
	       WaitFor.presenceOfElementByXpath(driver,PomObj.CasperUpdate());
		      ele= driver.findElement(By.xpath(PomObj.CasperUpdate()));         				      
								 	ele.click();   
								 	
								 	
							UpperToolBar ob = new UpperToolBar(driver);
								  	  ob.save_btn_method();
																								
	                }
			      } catch (Exception e) {
				e.printStackTrace();
			  }
									
	
			
	try {
		
		Switch switc = new Switch(driver);
		  switc.SwitchCase(uAgent);
										 
		
		
		
		String actualValue = null;
		WaitFor.presenceOfElementByXpath(driver, PomObj.SuffixTrack());   
		List<WebElement> ele2 =driver.findElements(By.xpath(PomObj.SuffixTrack()));
				
		    for(WebElement ele:ele2) {
			
		    	actualValue =  ele.getAttribute("innerHTML");
			       		
		}
		    
		  String v=actualValue;		    
		  System.out.println(v);
				    		 
		             
		             
		         WaitFor.presenceOfElementByXpath(driver, PomObj.AuthornameTrack());												    
		   List<WebElement> ele1 =driver.findElements(By.xpath(PomObj.AuthornameTrack()));  
		  
		   
		   
		   String actualValue1="";							
	                    
		          for(WebElement ele:ele1) {

          actualValue1 = actualValue1 + ele.getAttribute("innerHTML");  
		         
		         }
		   	   		   								    	
	String Expected1="Van";
	String Expected2="Steven";
	String Expected3="Senior";
	
if(actualValue1.contains(Expected1)==true && actualValue1.contains(Expected2)==true && v.contains(Expected3)==true) {
 
status="Pass";
	 
	MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
					 util.deleteRecFile(RecVideo);
 
              }else{
            	 				     				 		 		 
 status = "Fail";
 		 	     
 	MyScreenRecorder.stopRecording();
 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
 		 			  util.MoveRecFile(RecVideo);
						
		remark="Given name or surname didn't dispalyed / Suffix didn't displayed";	
														    	 													    	  				    	

		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	}

			if(actualValue1.contains(Expected1)==true){
				System.out.println("E1: No issues");
			}	
			
			if(actualValue1.contains(Expected2)==true){
				System.out.println("E2: No issues");								 	
             }
			
			if(v.contains(Expected3)==true){
				System.out.println("E3: No issues");
			 }
		
				  			
		    
				} catch (Exception e) {												
					e.printStackTrace();
					 }finally {
        System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	                            }						    
					}
	
		
	
	

	@Test(alwaysRun = true)
			        			        
public void  AuthorNametest11() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);
	CUPAuthorNameEdit11 obj = new CUPAuthorNameEdit11();
			       obj.AuthorNameEdit11();
			       
		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }