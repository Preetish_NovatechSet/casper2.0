package com.AuthorName;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUPAuthorNameEdit08 extends CUP_BaseClass {

	
	
    public static WebElement ele=null;
	static String remark;
	static String className = "CUPAuthorNameEdit08";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;

	Pom PomObj;
	
	public void AuthorNameEdit08() throws Exception{

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		description = "Casper-CUP_CopyEditor-->Double click on author name then click on remove suffix and save, check whether Ms. has removed or not";		    
				 area = "Author Name";
				      category = "Front matter, Delete suffix";
				
				      
				 driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
							System.out.println("BrowerName->"+uAgent);			                     

									
			try {
				              
			      Switch switc = new Switch(driver);
						
			           switc.SwitchCase(uAgent); 

			           
		PomObj=new Pom();	  
		
	     
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());						
        ele= driver.findElement(By.xpath(PomObj.Vanessa()));  
												
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
													    
				String value = ele.getAttribute("innerHTML");						

											
	if(value.contains(value)){

																		
					MoveToElement.byXpath(driver,  PomObj.Vanessa());
														
													    
String doubleClickJS = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('dblclick',"+
		"true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)"+
										"{arguments[0].fireEvent('ondblclick');}";
																
	WaitFor.presenceOfElementByXpath(driver, PomObj.Vanessa());

	((JavascriptExecutor)driver).executeScript(doubleClickJS,ele);


	driver.switchTo().defaultContent();
	 
	
	Thread.sleep(3000);

   ele= driver.findElement(By.cssSelector(PomObj.Remove_suffix()));
         ele.click();							 	
								 	
							UpperToolBar ob = new UpperToolBar(driver);
								  	  ob.save_btn_method();
																								
	                  }
			      }catch (Exception e) {
				e.printStackTrace();
			 }
									
	
			
	try {
		Switch switc = new Switch(driver);
		  switc.SwitchCase(uAgent);
		  
		  
		  
		  if(driver.findElements(By.xpath(PomObj.SuffixTrack())).size() == 0){
			  
			  status="Pass";
				 
				 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
								 		 util.deleteRecFile(RecVideo);
			 
			              }else {
			            	 				     				 		 		 
			   status = "Fail";
			 		 	     
			 	MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			 		 				 	 util.MoveRecFile(RecVideo);
			 		 				 	 
		    	  remark="After removing Ms. is displayed on screen";	
			    	 							    	  				    	
					   Thread.sleep(10000);  

		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			  
			              }						 
		
					}catch (Exception e){
							  e.printStackTrace();															
							}finally {
                   System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);													    
	                         }						    
					}
	
		
	

	@Test(alwaysRun = true)
			        			        
public void  AuthorNametest8() throws Throwable {
		
	try
	{
		MyScreenRecorder.startRecording(uAgent,className);	
	CUPAuthorNameEdit08 obj = new CUPAuthorNameEdit08();
			            obj.AuthorNameEdit08();


		}catch (Exception e){

			e.printStackTrace();
		}
	 }
  }