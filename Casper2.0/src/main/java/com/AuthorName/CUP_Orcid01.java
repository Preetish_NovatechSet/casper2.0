package com.AuthorName;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_Orcid01 extends CUP_BaseClass{

	static String remark;
	static String className = "CUP_Orcid01"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
    Assertion hardAssert = new Assertion();
    
    
    public void Orcid() throws Exception{
    	
	

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Orcid-->Go to Author name right click add orcid id and click search button and check whether Author name dispaled or not";	    
		       area = "Author Name";
		            category = "Add ORCID id and Search";
		          				   
		            System.out.println("BrowerName->"+uAgent);                 
			           
				          		             
				       Switch switc = new Switch(driver);
				             switc.SwitchCase(uAgent);  
	                
					            PomObj=new Pom(); 
					            
	WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
WebElement Turgeon = driver.findElement(By.xpath(PomObj.Turgeon()));	 
		 
        MoveToElement.byXpath(driver,PomObj.Turgeon());	      


Actions action= new Actions(driver);
    WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
action.contextClick(Turgeon).build().perform();


Robot robot = new Robot(); 
     Thread.sleep(2000);
robot.keyPress(KeyEvent.VK_DOWN);
               
      Thread.sleep(2000);
robot.keyPress(KeyEvent.VK_DOWN);


        action.sendKeys(Keys.ENTER).build().perform();
  
    Thread.sleep(2000);
    
        driver.switchTo().defaultContent();

   WaitFor.presenceOfElementByXpath(driver, PomObj.Orcidvalue());
WebElement valueOrcid = driver.findElement(By.xpath(PomObj.Orcidvalue()));	
                
                 MoveToElement.byclick(driver, valueOrcid);
                 Thread.sleep(2000);
  MoveToElement.bysendkeyWithoutclick(driver, valueOrcid,"0000-0002-1825-0097");

  WaitFor.presenceOfElementByXpath(driver, PomObj.Search());
  WebElement search = driver.findElement(By.xpath(PomObj.Search()));
       MoveToElement.byclick(driver, search);
		         

       Thread.sleep(3000);
    WaitFor.presenceOfElementByXpath(driver, PomObj.Validation_OrcidAuthor_name());
WebElement author = driver.findElement(By.xpath(PomObj.Validation_OrcidAuthor_name()));
            
      String value = author.getAttribute("innerHTML");

        String Expected ="Author : Josiah Carberry";
       
        
        
        if(value.contains(Expected)==true) {       	
        	
        	    status="Pass";
        	    
        	 MyScreenRecorder.stopRecording();
       String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
        	util.deleteRecFile(RecVideo); 
        	
        }else{
        	       	
        	    status="Fail";
        	    
        	    MyScreenRecorder.stopRecording();
        String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
  		     util.MoveRecFile(RecVideo);  
        	
        remark="after putting orcid id Author name didn't dispalyed";
        	
       utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
        }        
    }
			
    
    
  	    	   	  
	
@Test(alwaysRun=true)
		
public void OrcidTest01() throws Throwable{
				     			     					
		try {
			
			MyScreenRecorder.startRecording(uAgent,className);		
			
			CUP_Orcid01 obj = new CUP_Orcid01();
			          obj.Orcid();
			          
		} catch (Exception e) {			
		e.printStackTrace();
		      }finally{
		    	  
		  System.out.println(className);;
Ex.testdata(description, className, remark, category, area, status, uAgent);

		}
    }
}
