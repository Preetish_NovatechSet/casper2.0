package com.TableContent;




import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import core.CreateNewFile;




public class WritePropertiesFile {
	
	String url=null;
	
	
	
	
	
	
	public static void FuntionWritePropertiesFile(String key,String url,String Path) throws InterruptedException {
		try {
			
			Thread.sleep(12000);
		Properties properties = new Properties();
		properties.setProperty(key,url);
		String scr = Path;
		File file = new File(scr);
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
     }
	
	
	
	
	//Author
	public static String WriteUrlFireFoxInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxTableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\FireFoxUrlGenrationForTableContain_Footnote.properties");
			return url;
}
	
	

	public static String WriteUrlChromeInPropertiesFile(String url) throws InterruptedException {
			
		
WritePropertiesFile.FuntionWritePropertiesFile("ChromeTableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\ChromeUrlGentrationForTableContain_Footnote.properties");
		return url;
		
}
	
	
	
	public static String WriteUrlOperaInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("OperaTableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\OperaUrlGenrationForTableContain_Footnote.properties");
		return url;		
		
}
	
	
	
	public static String WriteUrlEdgeInPropertiesFile(String url) throws InterruptedException{
				
WritePropertiesFile.FuntionWritePropertiesFile("EdgeTableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\EdgeTableContainUrl.properties");
		return url;			
}
	
	
	public static String WriteUrlIEInPropertiesFile(String url) throws InterruptedException{
		
		WritePropertiesFile.FuntionWritePropertiesFile("IETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\IETableContainUrl.properties");
				return url;			
		}
	
	
	
	/****************************************/
	             //Editor
	/**
	 * @throws InterruptedException *************************************/
	
	
	
	public static String WritePEUrlIEInPropertiesFile(String url) throws InterruptedException{
		
		WritePropertiesFile.FuntionWritePropertiesFile("IEPETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\IEPETableContainUrl.properties");
				return url;			
		}
	
	
	
	public static String WritePEUrlEdgeInPropertiesFile(String url) throws InterruptedException{
		
WritePropertiesFile.FuntionWritePropertiesFile("EdgePETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\EdgePETableContainUrl.properties");
		return url;
		
}
	
	

	
	public static String WritePEUrlChromeInPropertiesFile(String url) throws InterruptedException {
				
WritePropertiesFile.FuntionWritePropertiesFile("ChromePETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\ChromeUrlGenrationForPETableContain_Footnote.properties");
		return url;		
}
	
	
	
	public static String WritePEUrlFireFoxInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxPETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\FireForUrlGenrationForPETableContainFootnote.properties");
		return url;		
 }

	
	
	public static String WritePEUrlOperaInPropertiesFile(String url) throws InterruptedException{
		
WritePropertiesFile.FuntionWritePropertiesFile("OperaPETableContainUrl", url, "./Configfolder\\UrlTableContain_Footnote\\OperaUrlGenrationForPETableContain_Footnote.properties");
		return url;		
 }

	
/*****************************************************************************************/
	                            /**Author
	                             * @throws InterruptedException **/
	
	
	public static String WritePEAuthorNameCommentChrome(String url) throws InterruptedException {
		
		String path="./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_Chrome.properties";
		CreateNewFile.CreateNewPropertiesFile(path);
		WritePropertiesFile.FuntionWritePropertiesFile("ChromePEAuthorNameUrl", url, path);
		 return url;
	}
	
	
	public static String WritePEAuthorNameCommentFireFox(String url) throws InterruptedException {
		String path="./Configfolder\\AuthorNameUrl\\\\AuthorNameCommentUrl_FireFox.properties";
		CreateNewFile.CreateNewPropertiesFile(path);
		WritePropertiesFile.FuntionWritePropertiesFile("FireFoxPEAuthorNameUrl", url, path);
		 return url;
	}
	
	
	public static String WritePEAuthorNameCommentIE(String url) throws InterruptedException {
		String path="./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_IE.properties";
		
		CreateNewFile.CreateNewPropertiesFile(path);
		WritePropertiesFile.FuntionWritePropertiesFile("IEPEAuthorNameUrl", url, path);
		 return url;
	}
	
	public static String WritePEAuthorNameCommentEdge(String url) throws InterruptedException {
String path="./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_Edge.properties";
		
		CreateNewFile.CreateNewPropertiesFile(path);
		
		WritePropertiesFile.FuntionWritePropertiesFile("EdgePEAuthorNameUrl", url, path);
		 return url;
	}
}
