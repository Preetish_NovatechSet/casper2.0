package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class TandF_TableCaption08 extends TandFBaseClass{

	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
		
	/*********
	 *8. Single word select and use backspace key for delete, check tracking and data lose
	 *********/

	public  void selectSingle_word_bckspace() throws Exception {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "Casper-TandF_CopyEditor-TableCaption-->Single word select and use backspace key for delete, check tracking and data lose";
		    className = "TandF_TableCaption08";    
		         area = "Table Caption";
		            category = "Select and delete";
		             	    

		 Cookies cokies =new Cookies(driver);
            cokies.cookies();
             
          Switch switc = new Switch(driver);
             switc.SwitchCase(uAgent); 
	                 
		    
                  System.out.println("BrowerName->"+uAgent);
		
                  PomObj = new Pom();  
     try {  

WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption2());

WebElement ele=driver.findElement(By.xpath(PomObj.TableCaption2()));
driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);   


String value = ele.getText(); 	

if(value.contains(value)) {

((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
    ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");

MoveToElement.byXpath(driver, PomObj.TableCaption2());	

WaitFor.clickableOfElementByXpath(driver, PomObj.TableCaption2());

ele.click();
				
MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
							
	for(int i =0; i<48;i++){
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);

}

			
MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 2,Keys.ARROW_LEFT);

MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
			
UpperToolBar ob = new UpperToolBar(driver);
ob.save_btn_method();

//core.HandleAlert.isAlertPresentAccept(driver);
} }catch (Exception e){
e.printStackTrace();
}


try {
Thread.sleep(3000);

Switch switc1 = new Switch(driver);
switc1.SwitchCase(uAgent); 


WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption2());

WebElement ele1=driver.findElement(By.xpath(PomObj.TableCaption2()));

  String afterSave =  ele1.getAttribute("innerHTML");		    
	String expectedValue1 = "on</del>";
	String expectedValue2 = "ice-del ice-cts";			

System.out.println("After save - " +afterSave);

if(afterSave.contains(expectedValue1)&&afterSave.contains(expectedValue2)){

	 status="Pass";

}else {
				 					 					 		
	 
    status = "Fail";
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";

Thread.sleep(10000);

utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);

if(afterSave.contains(expectedValue1)==true) {
	   System.out.println("E1: No issues");
	}
if(afterSave.contains(expectedValue2)==true){
	   System.out.println("E2: No issues");
	}		   
}
}catch (Exception e){

e.getStackTrace();
}finally {
	System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
}
}
	
	

@Test(alwaysRun = true)

	public void  Test8() throws Throwable {
		  
		 try {		 
	
			TandF_TableCaption08 obj = new TandF_TableCaption08();
	                obj.selectSingle_word_bckspace();
			
	              
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
	     }
	 }
