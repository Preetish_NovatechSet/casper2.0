package com.TableContent;

import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUP_AuthorEnd_TableContain07 extends CUP_AuthorEnd_BaseCalss {

	
	static String cell1;
	static String cell2;
	static String AfterMergeCell;
	static String remark;
	static String className = "CUP_AuthorEnd_TableContain07"; 
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;

		
	    /**
		 * Multiple cell Merge and check data lose
		 **/
		

		
		
		public void merge_multipleCell() throws Exception {
			
				
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
		 description = "Casper-CUP_AuthorEditor-TableContain-->Multiple cell Merge and check data lose";   
		        	      area = "Table Contain";
			                   category = "Merge Multiple Cell";
			         			       
			          
			       Switch switc = new Switch(driver);
			       
			       CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                         C1_C2.CasperCUP(uAgent);
                         
			 	          switc.SwitchCase(uAgent);
			         
			  
			              PomObj = new Pom();
		  
		    WaitFor.presenceOfElementByXpath(driver, PomObj.CUPmerge1());
		     
		    WebElement ele =driver.findElement(By.xpath(PomObj.CUPmerge1()));
				
			              
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
	      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,390)");
	    
	      
	      for(int i=0;i<2;i++) {
			  MoveToElement.byclick(driver, ele);
			  Thread.sleep(4000);
	}
	      
	      
	      Robot robot = new Robot();
	    	 robot.delay(2);
	    	    robot.mouseMove(800, 300);
	    	      robot.delay(30);
	    robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
	    	            robot.delay(30);
	    	               robot.mouseMove(980, 300);
	    	             
	    	                 robot.delay(30);
	    	   robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK); 
	    	             
	    	             
	    	   
	    	   robot.delay(30);
	    	   robot.mouseRelease(InputEvent.BUTTON3_DOWN_MASK); 
	    	             
	    	   
	    	   
	    	        Thread.sleep(3000);
	    	        robot.keyPress(KeyEvent.VK_RIGHT);
	    	             robot.delay(30);
	    	        robot.keyRelease(KeyEvent.VK_RIGHT);
	    	        
	    	                Thread.sleep(3000);
	    	        robot.keyPress(KeyEvent.VK_ENTER);
	    	   
	                   
	                  /* Actions action=new Actions(driver);
	           	                    Thread.sleep(2000);
	               action.keyDown(Keys.LEFT_SHIFT).build().perform();
	           	    
	                   
	           	   WaitFor.presenceOfElementByXpath(driver, PomObj.CUPmerge2());     	     
	       	    WebElement ele1 =driver.findElement(By.xpath(PomObj.CUPmerge2()));
	           	    
	       	              MoveToElement.byclick(driver, ele1);
	           	    
	           	       action.contextClick(ele1).build().perform();
	           	       
	           	    action.keyUp(Keys.LEFT_SHIFT).build().perform();
	           	    
	           	   
	           	 Robot robot = new Robot();
	           	 robot.delay(1);
	           	 robot.keyPress(KeyEvent.VK_RIGHT);
	           	Thread.sleep(3000);

	  
	           	 robot.keyPress(KeyEvent.VK_ENTER);
	          	 robot.delay(1);
	          	 robot.keyRelease(KeyEvent.VK_ENTER);
	          	 robot.delay(1);
	           	*/
	         	 
	          UpperToolBar ob = new UpperToolBar(driver);
		  	         ob.save_btn_method();
	   
			
	    try {
				
				switc.SwitchCase(uAgent);
				
				     WaitFor.presenceOfElementByXpath(driver, PomObj.CUPmerge1());
					     
				   WebElement ele11 =driver.findElement(By.xpath(PomObj.CUPmerge1()));
									              
		//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
				
		     String value = ele11.getText();		           
			  
		     if(value.contains("240150")==true) {
		    	 
		    	 status="Pass";
		    		
		    		MyScreenRecorder.stopRecording();
		  String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		    		    		      			util.deleteRecFile(RecVideo); 		 	
		    		    		      					     			         				      				 	
		    		    		      					     			         				      				 	
		    		    		      					    }else{
		    		    		      					     			         				      			    	  				     			         				      			   
		    		    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
		    		          status="Fail";
		    		    		      					     			         				      				 			
		    		    		  MyScreenRecorder.stopRecording();
		    String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		    		    		      	      util.MoveRecFile(RecVideo);
		    	 
		    	 remark="Cell Didn't merge will Click on Merge-Tab / Right Click Dint't Work";
		    		
		    		    Thread.sleep(10000);
		    		
		   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		    					    						   
		    					 }
		    			    }catch (Exception e){
		    			    	
		    			e.getStackTrace();
		    		        	}finally {
		    		        		System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);
		    		        	}
		                }
		
		
		

	@Test(alwaysRun=true)

		public static void CUPTableContainTest7() throws Exception {
			
			 try {
				 MyScreenRecorder.startRecording(uAgent,className);
				 
	    CUP_AuthorEnd_TableContain07 obj = new CUP_AuthorEnd_TableContain07();
				     obj.merge_multipleCell();
				     
			    }catch (Exception e){
					e.printStackTrace();
			     }
		     }
		
		 }
		

