package com.TableContent;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableContain08 extends CUP_AuthorEnd_BaseCalss {

	
	static String remark;
	static String className = "CUP_AuthorEnd_TableContain08"; 
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;


    
    
	/*
	 * Multiple cell select and use delete key for delete
	 */

    
	
	
	public void select_multlple_CellvalueAndDelete() throws InterruptedException, IOException{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_AuthorEditor-TableContain-->Select Multiple cell word and use delete key, data lose track should be present";		      
		        area = "Table Contain";
		            category = "Select multiple cell and delete";
		           
		         PomObj = new Pom();
		       
		         
		         Switch switc = new Switch(driver);
		         
		         CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                      C1_C2.CasperCUP(uAgent);
                 
		            switc.SwitchCase(uAgent);
		 try {
			 
			 
			WaitFor.presenceOfElementByXpath(driver, PomObj.CUPSelect1());
				
			WebElement ele= driver.findElement(By.xpath(PomObj.CUPSelect1()));
			
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	    ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
	    
	 
	    MoveToElement.byXpath(driver, PomObj.CUPSelect1());
	    
	             MoveToElement.byclick(driver, ele);
	             
	             MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	    
	   for(int i=0;i<2;i++) {
	      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
	    }
	   
	   
	   Actions action = new Actions(driver);
	   action.keyDown(Keys.SHIFT).build().perform();
	
	   for(int i=0;i<2;i++) {
		      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
		    }
	   
	   
	   
	   
	   
	   WaitFor.presenceOfElementByXpath(driver, PomObj.CUPSelect2());
		
		WebElement ele1= driver.findElement(By.xpath(PomObj.CUPSelect2()));
		
		  MoveToElement.byXpath(driver, PomObj.CUPSelect2());
	    
                   MoveToElement.byclick(driver, ele1);
        
        MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
        
               action.keyUp(Keys.SHIFT).build().perform();
        
        
               MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);               
	         
	    	        
     UpperToolBar obj = new UpperToolBar(driver);
                 obj.save_btn_method();	 
		 
		    
			}catch (Exception e) {
			
			e.printStackTrace();
		  
		      }
		 
		 
		 
		 
		 try {			
				
				
				
				Thread.sleep(3000);
				
			       switc.SwitchCase(uAgent);
											
						
			WaitFor.presenceOfElementByXpath(driver,PomObj.CUPSelect1());
			
		WebElement ele1 = driver.findElement(By.xpath(PomObj.CUPSelect1()));
		
		WaitFor.presenceOfElementByXpath(driver,PomObj.CUPSelect2());
		
		WebElement ele2 = driver.findElement(By.xpath(PomObj.CUPSelect2()));
					 
		String actualValue =  ele1.getAttribute("innerHTML");
		String actualValue1 =  ele2.getAttribute("innerHTML");
		String expectedValue1 = "19</del>";
		String expectedValue2 = "ice-del ice-cts";
		String expectedValue3 = "14</del>";	
		String expectedValue4 = "ice-del ice-cts";
						 
						 System.out.println("After save - " + actualValue);
							
							
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue1.contains(expectedValue3)==true && actualValue1.contains(expectedValue4)==true){
							
status="Pass";
	
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo);
								 
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";			
			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
						   
						 }
				 if(actualValue.contains(expectedValue1)==true) {
						System.out.println("E1: No issues");
							 	}
							  
					if(actualValue.contains(expectedValue2)==true){
						System.out.println("E2: No issues");
							 	  
							 	 }
							   
					if(actualValue1.contains(expectedValue3)==true){
							  System.out.println("E3: No issues");

							     }
							 	  
					if(actualValue1.contains(expectedValue4)==true){
					     System.out.println("E4: No issues");

								  }
			              }catch (Exception e){
				
				e.printStackTrace();
			          }finally {
			        	  
					     	System.out.println(className);
			Ex.testdata(description,className,remark,category,area,status,uAgent);
					  
				   }
		        } 
		
	

	@Test(alwaysRun=true)
	
public static void CUPTableContainTest8() throws Exception {
			
	try {		
		
		 MyScreenRecorder.startRecording(uAgent,className);
			    
        CUP_AuthorEnd_TableContain08 obj = new CUP_AuthorEnd_TableContain08();
		          obj.select_multlple_CellvalueAndDelete();
	       
	} catch (Exception e) {
		e.printStackTrace();
	  }finally {
		  
		  System.out.println(className);			
	Ex.testdata(description, className, remark, category, area, status, uAgent);
	  }
   }
}
