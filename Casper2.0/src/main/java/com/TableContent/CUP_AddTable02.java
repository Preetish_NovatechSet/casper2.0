package com.TableContent;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class CUP_AddTable02 extends CUP_BaseClass{
	
	
	
	 static WebElement ele =null;
	 static String remark;
	 static String className;
	 static String category;
	 static String area;
	 static String description;
	 static String status;
	 public static Excel Ex;
	 Pom PomObj;
	   
	
	    
	    
	  
		
/**
* Delete one character with Back space key press and insert special character, tracking should be present
**/
	    
	public void Click_on_Aspirations() throws Throwable{
											
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-CUP_AuthorEnd-AddTable-->Check whether Add Table can Merge to right or not.";
		  className = "CUP_AddTable02";    
		         area = "Add Table";
		               category = "Merge table to right";
		              
	                    		                      		                                     
	                     
			      System.out.println("BrowerName->"+uAgent);                 
	           
			              
			          Cookies cokies =new Cookies(driver);
				             cokies.cookies();
				              
				       Switch switc = new Switch(driver);
				              switc.SwitchCase(uAgent);    				              
	        
	PomObj = new Pom();   
			                     
     
				          		
			WaitFor.presenceOfElementByXpath(driver, PomObj.tr5());
				          		
		 ele= driver.findElement(By.xpath(PomObj.tr5()));
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
				          	     
				      String value = ele.getText();
				          		
	if(value.contains(value)){
				          						
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		          	
	
	 MoveToElement.byXpath(driver, PomObj.tr5());	
				          	 		
	 WaitFor.clickableOfElementByXpath(driver, PomObj.tr5());
				          	            
				          	     ele.click();
				          	     
				          	 Actions action=new Actions(driver);
          	                    Thread.sleep(2000);        
		   
				          action.contextClick(ele).build().perform();
					
				          	     
				          	     
				        	 Robot robot = new Robot();
				           	 robot.delay(1);
				              Thread.sleep(3000); 
				           	 robot.keyPress(KeyEvent.VK_RIGHT);
				           	  Thread.sleep(3000);          	     
				           	 robot.keyPress(KeyEvent.VK_DOWN);	     
				              Thread.sleep(3000);  				              
				             robot.keyPress(KeyEvent.VK_ENTER);
				             
				          	     
				             
				    	
				    		 
					//switc.SwitchCase(uAgent);
					
			List<WebElement> ele= driver.findElements(By.xpath(PomObj.td1()));
		    		 int count =ele.size();
		    		 
		    		System.out.println(count); 
                          
                          
                          
                   int Expected1= 5;   
           
                   
      if(count==Expected1==true) {
           	  
   	   status="Pass";
   	   
             }else{
           	  
           	  status="Fail";
           	  
        	  remark="Unable to merge right";
        	  
        	  Thread.sleep(10000);  		
     		 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
             }
                 
           }
       }

	
	
	@Test(alwaysRun = true)
	
	public void  CUPAddTableTest2() throws Throwable {
		  
	try {
	
			 CUP_AddTable02 obj = new CUP_AddTable02();
                     obj.Click_on_Aspirations();
				                      
		}catch (Exception e){
			
			e.printStackTrace();
		     
		      }finally {
					System.out.println(className);
					Ex.testdata(description, className, remark, category, area, status, uAgent);
							}
                    }
	         }