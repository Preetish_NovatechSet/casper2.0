package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableContain11 extends PPL_BaseClass{

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;
	
	
	/**
	 * Single cell select and "Control + C" then "Control + V" in next cell, check tracking and data lose
	 **/

	

public void copyCellAndPaste() throws Exception {

		try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-PPL_CopyEditor-TableContain-->Single cell select and Control + C then Control + V in next cell, check tracking and data lose";
		className = "PPL_TableContain11";    
		     area = "Table Contain";
		         category = "Copy and paste";
		             
		     
		PomObj = new Pom();
		
		
	Cookies cokies =new Cookies(driver);
        cokies.cookies(); 
    
    Switch switc = new Switch(driver);
       switc.SwitchCase(uAgent);
		   
		   WaitFor.presenceOfElementByXpath(driver,PomObj.Protein());   

		WebElement ele= driver.findElement(By.xpath(PomObj.Protein()));
		
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
						
		String value = ele.getText();
				
				//System.out.println(value);
				
				if(value.contains(value)) {
					
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);			
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		
        MoveToElement.byXpath(driver, PomObj.Protein());
       
WaitFor.clickableOfElementByXpath(driver, PomObj.Protein());
              ele.click();
              
            MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);  
              
        MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 7, Keys.ARROW_LEFT);
              
        MoveToElement.copy(driver); 			     
			     	     
			if(uAgent.equals("IE")) {
			    	 
			 
			    	 
			    	  for(int i=0; i<=9;i++){
				 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT); 
						   }     	 
			
			    	  
			    	  Thread.sleep(1000); 
	   ((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");		
	           
					   core.HandleAlert.isAlertPresentAccept(driver);
			    	  
					    
			     }else{
			    	 
			    	 WaitFor.presenceOfElementByXpath(driver,PomObj.Helix());
						
					 WebElement ele2=driver.findElement(By.xpath(PomObj.Helix()));
			    	 
			    	 ele2.click();
			    	 
			     MoveToElement.paste(driver);
			        }     
			         
				  }	
			  
      	
		UpperToolBar ob = new UpperToolBar(driver);
				ob.save_btn_method();
				  // }
				
				switc.SwitchCase(uAgent);	
			
			 WaitFor.presenceOfElementByXpath(driver,PomObj.Helix());
				
			 WebElement ele2=driver.findElement(By.xpath(PomObj.Helix()));
			
			     
			     String actualValue =  ele2.getAttribute("innerHTML");
				 String expectedValue1 = "Protein</ins>"; 
				 String expectedValue2 = "ice-ins ice-cts";
				
			 
			System.out.println("After save - " + actualValue);
			
			
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true){
			 	
		 	status="Pass";
			    
			 }else{
				   Thread.sleep(10000);  
        	       
	      	          status ="Fail"; 
	    // ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele2);
	     		 
remark="Ctrl+C, Ctrl+v is Not working/ Insert colour is missing";	     			

         utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	         	  
			      if(actualValue.contains(expectedValue1)==true) {
			 	   System.out.println("E1: No issues");
			 	
			 	}
			  
			 	    if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  
			 	         }
			   	
			           }		
			      }catch(Exception e){
			
			e.printStackTrace();
		}finally{
	     	System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
		     }
	     }



@Test(alwaysRun=true)

public static void PPLTableContainTest11() throws Exception {

	
	 try {
	 		 

PPL_TableContain11 obj = new PPL_TableContain11();
       obj.copyCellAndPaste();
       

	} catch (Exception e) {
	
		e.printStackTrace();
	  }  
    }
}
