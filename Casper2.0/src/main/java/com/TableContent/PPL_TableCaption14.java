package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class PPL_TableCaption14 extends PPL_BaseClass{

	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
	
	
	/***
	 * 14. Insert one character and place cursor to next character then press backspace key, tracking should be present
	 * @throws Exception 
	 ***/


public void insert1Char_palcecursor_nxtChar_backspce() throws Exception {
		

Ex =  new Excel(description, className, remark, category, area, status,uAgent);
	   description = "Casper-PPL_CopyEditor-TableCaption-->Insert one character and place cursor to next character then press backspace key, tracking should be present";
			  className = "PPL_TableCaption14";    
			      area = "Table Caption";
			          category = "Insert and Delete";
			           
			  		          		
	 System.out.println("BrowerName->"+uAgent);
			    
			    Cookies cokies =new Cookies(driver);
		          cokies.cookies();
		       
		    Switch switc = new Switch(driver);
		       switc.SwitchCase(uAgent);
						  
		       PomObj = new Pom();  
			   	try {  	 
							  
				   WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
								
				WebElement ele=driver.findElement(By.xpath(PomObj.TableCaption3()));
				     				
						driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
				     				
				String value = ele.getText();
				     				
				         if(value.contains(value)) {
				        	 
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);   	 
		   ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");   
		   
						MoveToElement.byXpath(driver, PomObj.TableCaption3());	

				 WaitFor.clickableOfElementByXpath(driver, PomObj.TableCaption3());
				     			
				          ele.click();
				           
				                 			     					
				    MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				    
				    	 
				    	 for(int i=0;i<84;i++){
		     	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
		     				}
				    	 
				     
				     					
				     					
				     	MoveToElement.bysendkeyWithoutclick(driver, ele, "P");

				     					
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
				     		
				     		
				  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
				     								     						
				     						
				      UpperToolBar ob = new UpperToolBar(driver);
				     			  ob.save_btn_method();
				     				  
				     //HandleAlert.isAlertPresentAccept(driver); 
				     				      }
				          }catch (Exception e){
		                               e.printStackTrace();
				     				  }
		
			
			try {		
		Thread.sleep(3000);
					

		      switc.SwitchCase(uAgent);
				 
				WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
									
			WebElement ele1=driver.findElement(By.xpath(PomObj.TableCaption3()));
				    
				     String actualValue =  ele1.getAttribute("innerHTML");
					 String expectedValue1 = "P</ins>";
					 String expectedValue2 = "a</del>";
					 String expectedValue3 = "ice-del ice-cts";
					 String expectedValue4="ice-del ice-cts";
					 System.out.println("After save - " + actualValue);



if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
					status="Pass";
					
					 }else {
						 Thread.sleep(10000);
						    
		 		 		 
		 		 	     status = "Fail";
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
		 		 	   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
					 }
					 	  if(actualValue.contains(expectedValue1)==true) {
					 	   System.out.println("E1: No issues");
					 	}
					    if(actualValue.contains(expectedValue2)==true){
					 	   System.out.println("E2: No issues");
					 	       }
					    if(actualValue.contains(expectedValue3)==true) {
						 	   System.out.println("E3: No issues");
						 	}
					    if(actualValue.contains(expectedValue4)==true) {
						 	   System.out.println("E4: No issues");
						 	}
					   
					       }catch (Exception e) {
				    	
					    	   
					  e.printStackTrace();
									 	   
								        }finally{
				System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
								        }
                               	}


  @Test(alwaysRun = true)
public void  PPLTableCaptionTest14() throws Throwable {
	  
	 try {
		 
		      PPL_TableCaption14 obj = new PPL_TableCaption14();
                obj.insert1Char_palcecursor_nxtChar_backspce();
                
	} catch (Exception e){		
		    e.printStackTrace();
	    }
    }
}