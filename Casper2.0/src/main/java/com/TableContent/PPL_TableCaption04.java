package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableCaption04 extends PPL_BaseClass{
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;

	Pom PomObj;

		
			
		/**
		 * 4. Insert special character and place cursor to next character then press 
		 *       backspace key, tracking should be present 
		 **/
		
	public void insert_spl_chr_PlcCurserNxtchar_bckspce() throws Exception {
			
		
		
		
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableCaption-->Insert special character and place cursor to next character then press backspace key, tracking should be present";
		 className = "PPL_TableCaption04";    
			    area = "Table Caption";
			         category = "insert and deletion";                   
			                     
				              System.out.println("BrowerName->"+uAgent);          
			                  
				              Cookies cokies =new Cookies(driver);
					             cokies.cookies();
					              
			 Switch switc = new Switch(driver);
					 switc.SwitchCase(uAgent);      
					              
				   PomObj = new Pom();  
				try {    	                
				     			  
		WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption1());
		 WebElement ele=driver.findElement(By.xpath(PomObj.TableCaption1()));
				     			
				     				          
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);             
				     			            
				    String value = ele.getText();
				     					
				     		if(value.contains(value)){
				     						
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");     					 		    	
				     	MoveToElement.byXpath(driver, PomObj.Table_1_Caption());	
				     	 
			 WaitFor.clickableOfElementByXpath(driver, PomObj.Table_1_Caption());
				     						
				  ele.click();
				     						
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				     		 			 	
				     						 
				     for(int i=0;i<55;i++) {
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				     				}
				     					    				     					 	
				SymbolActionFuction obj = new SymbolActionFuction(driver);
				     		        obj.SymbolTest();
				     		                       
				     		                       				     		                       
	switc.SwitchCase(uAgent);    
				     			 	         	
	          WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption1());
			WebElement ele1=driver.findElement(By.xpath(PomObj.TableCaption1()));		    	
				     				
				     		      	      
				     		      	      
	 if(uAgent.equals("edge")) {
				     		      	    	
				   for(int i=0;i<2;i++) {
					   
			MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT);
			}
			     		    }else{
			     		    	
				for(int i=0;i<1;i++) {
				    MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT);} 
		      }				     		      	     	      	    	 
				   
	 
	MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
				     			 	    	  
				 UpperToolBar ob = new UpperToolBar(driver);
				        ob.save_btn_method();				     			  	        
				     			 	     
				     					   }
				     		       } catch (Exception e) {
				     			e.printStackTrace();
				     		 }
								
				
		try{
				     			
				  Thread.sleep(3000);
				     				
				        switc.SwitchCase(uAgent); 
				     						 
			WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption1());
				     				    		        
			WebElement val=driver.findElement(By.xpath(PomObj.TableCaption1()));	
				     								
		String afterSave =  val.getAttribute("innerHTML");
				     							     
				     			String expectedValue1 = "s</del>";
				     			String expectedValue2 = "ice-del ice-cts";
				     			String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				     			String expectedValue4 = "ice-ins ice-cts";
				     					 
			 System.out.println("After save - " + afterSave);



 if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true&& afterSave.contains(expectedValue4)==true){
				     			
				     				status = "Pass";
				     					 	
				     			}else {
				     					
				     		//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", val);		
				     				 		 
				     				 	     status = "Fail";
	remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing"; 
				     		       
				    Thread.sleep(10000);
				     		    
		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				     					     				     					     	
				     		    }
				     					 	
				  if(afterSave.contains(expectedValue1)==true){
				     	System.out.println("E1: No issues");
				     					}
				  if(afterSave.contains(expectedValue2)==true){
				     System.out.println("E2: No issues");
				     				  }
				   if(afterSave.contains(expectedValue3)==true){
				     			System.out.println("E3: No issues");
				     				 }
				   if(afterSave.contains(expectedValue4)==true){
				     			 System.out.println("E4: No issues");
				     		}
				     		    
		             }catch(Exception e){
				               e.getStackTrace();
				     	}
				     	        }

 
	
@Test(alwaysRun = true)
	
	public void  PPLTableCaptionTest4() throws Throwable {
		  
		 try {			
	
			  PPL_TableCaption04 obj = new PPL_TableCaption04();
                  obj.insert_spl_chr_PlcCurserNxtchar_bckspce();
			     
                     
		} catch (Exception e) {
			
			e.printStackTrace();
		     }finally {
			     System.out.println(className);;
	Ex.testdata(description, className, remark, category, area, status, uAgent);
			    }
          }
	 }