package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



/***************
 ***Preetish***
 ***************/

public class TandF_TableContain09 extends TandFBaseClass{

	
	static String remark;
	static String className = "TandF_TableContain09";
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;

	/**
	*Single cell select and use delete key for delete, check tracking and data lose
	**/
	
	public void selectSingleDel() throws Exception {
	   
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-TandF_CopyEditor-TableContain-->Single cell select and use delete key for delete, check tracking and data lose";    
	    area = "Table Contain";
	        category = "Select Single cell and delete";
	            
	    
	                PomObj = new Pom();
	                
	              Cookies cokies =new Cookies(driver);
		             cokies.cookies(); 
		         
		         Switch switc = new Switch(driver);
		            switc.SwitchCase(uAgent);
	                
	    try {
	    	
WaitFor.presenceOfElementByXpath(driver, PomObj.Table2_cellelement());
			WebElement ele1= driver.findElement(By.xpath(PomObj.Table2_cellelement()));	
			
				
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
	  ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
				String value = ele1.getText();
			//	System.out.println("Name of the element"+value);
				if(value.contains(value)) {
				
		 MoveToElement.byXpath(driver, PomObj.Table2_cellelement());
					ele1.click();
					               
				  
					
					  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
				 
					  MoveToElement.selectsingleelement(driver, Keys.DELETE);
				    
				UpperToolBar o = new UpperToolBar(driver);
			        o.save_btn_method();
			        
			        
				}
         		
				
					switc.SwitchCase(uAgent);
					
		WaitFor.presenceOfElementByXpath(driver, PomObj.Table2_cellelement());
				
		   WebElement ele= driver.findElement(By.xpath(PomObj.Table2_cellelement()));
				
				String actualValue =  ele.getAttribute("innerHTML"); 
				String expectedValue1 = "Ongoing</del>";
				String expectedValue2 = "ice-del ice-cts";  
				
		

   System.out.println("After save - " + actualValue);

   //System.out.println("Expected value - " + expectedValue);


 if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true){
			   
			   status="Pass";
   }
   else {
	  // ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	   
       
 	          status ="Fail"; 
		  
			
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert or Delete colour is missing";

    Thread.sleep(10000); 
			
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);   
			
	
			
			if(actualValue.contains(expectedValue1)==true) {
				System.out.println("E1: No issues");
			}
			
   if(actualValue.contains(expectedValue2)==true) {
			System.out.println("E2: No issues");
				
			              }
  
			        }
			  }catch (Exception e) {
				  e.getStackTrace();
		  }finally{
			System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);	
	
		}
	 }
	
	
	
	
	@Test(alwaysRun=true)
	public static void Test9() throws Exception {
	
		
		
		try {
					 
		TandF_TableContain09 obj = new TandF_TableContain09();
				    obj.selectSingleDel();
				    
		} catch (Exception e) {
				e.printStackTrace();
		      }
	   }
	
}

