package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_TableContain12 extends PPL_BaseClass{
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;
	
	/*public TandF_TableContain12(WebDriver ldriver) {
		this.driver=ldriver;
	 }*/


	/**
	 * Delete single cell data and insert some text/value, check insert and delete tracking present
	 **/
	

	

	
	public void DeleteSingleCellData_insertSome_textValue() throws Exception{

Ex =  new Excel(description, className, remark, category, area, status,uAgent);
   description = "Casper-PPL_CopyEditor-TableContain-->Delete single cell data and insert some text/value, check insert and delete tracking present";
	     className = "PPL_TableContain12";    
	          area = "Table Contain";
	             category = "Delete and insert";   
	                 
		
		
		
try {  
			
			PomObj = new Pom();
			
			
			Cookies cokies =new Cookies(driver);
	        cokies.cookies(); 
	    
	    Switch switc = new Switch(driver);
	       switc.SwitchCase(uAgent);
			                 
WaitFor.presenceOfElementByXpath(driver,PomObj.t28_5());
			 
       WebElement ele = driver.findElement(By.xpath(PomObj.t28_5()));
					
       driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
       
	
							
							  String value = ele.getText();
							  
							  if(value.contains(value)) {
								  
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		  
		 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
					 MoveToElement.byXpath(driver, PomObj.t28_5());
				WaitFor.clickableOfElementByXpath(driver, PomObj.t28_5());      
								  ele.click();
								 
          MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
          
          for(int i = 0;i<4;i++) {
          MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);   
          }
          
          
       for(int i = 0;i<4;i++) {
						MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
          }
          
				 MoveToElement.bysendkeyWithoutclick(driver, ele, "preetish");
					
				 
					UpperToolBar obj = new UpperToolBar(driver);
								 obj.save_btn_method();
						  
							 
							  }  
						     
							  
							 
						       switc.SwitchCase(uAgent);
							  
		           WaitFor.presenceOfElementByXpath(driver, PomObj.t28_5());
					
		           WebElement ele1 = driver.findElement(By.xpath(PomObj.t28_5()));			     
						     String actualValue =  ele1.getAttribute("innerHTML");
							 String expectedValue1 = "28.5</del>";
							 String expectedValue2 = "ice-del ice-cts";
							 String expectedValue3 = "preetish</ins>";
							 String expectedValue4="ice-ins ice-cts";
						 
							 System.out.println("After save - " + actualValue);
						
						
	if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true&&actualValue.contains(expectedValue4)==true && actualValue.contains(expectedValue3)==true){
       	
						 status="Pass";
						        
						    }else{

						    	Thread.sleep(10000);  
				        	       
				      	          status ="Fail"; 
				      	          
remark="Extra Char or Symbol may has come/ Insert or Delete trac is missing/ Insert or Delete colour is missing"; 
				  
				  utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
						  	          						 	
                 if(actualValue.contains(expectedValue1)==true) {
						 	   System.out.println("E1: No issues");
						 	
						 	}
						  
                 
						 if(actualValue.contains(expectedValue2)==true){
						 	   System.out.println("E2: No issues");
						 	  
						 	}
						  
						 
						if(actualValue.contains(expectedValue3)==true) {
						 	   System.out.println("E3: No issues");

						                }
						if(actualValue.contains(expectedValue4)==true) {
						 	   System.out.println("E4: No issues");

						                }
						          }
						     }catch (Exception e) {
					
						e.printStackTrace();
					}finally{
						
				     	System.out.println(className);
				 Ex.testdata(description,className,remark,category,area,status,uAgent);
				   
					 }
	              }
	

	@Test(alwaysRun=true)
	public static void PPLTableContainTest12() throws Exception {

		
		
		 try {
					 

	PPL_TableContain12 obj = new PPL_TableContain12();
	       obj.DeleteSingleCellData_insertSome_textValue();

		} catch (Exception e) {
		
			e.printStackTrace();
		  }	   
	   }
}
