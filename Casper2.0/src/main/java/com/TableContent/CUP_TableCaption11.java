package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_TableCaption11 extends CUP_BaseClass{

	

	static String remark;
	static String className = "CUP_TableCaption11";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;	
   
    Pom PomObj;
	
	/***
	 * PE - Delete one character with Back space key press and insert text, tracking should be present
	 * 
	 ***/

	public void delonechr_DeleteKey_insertTXT() throws Exception{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_CopyEditor-TableCaption-->Delete one character with Back space key press and insert text, tracking should be present";
	       
	       area = "Table Caption";
	           category = "Delete and insert";
	           
	
    System.out.println("BrowerName->"+uAgent);


    Cookies cokies =new Cookies(driver);
         cokies.cookies();
 
Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent); 
	
 PomObj = new Pom();  
	try {  	 
	 
		 WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
			
	           WebElement ele=driver.findElement(By.xpath(PomObj.CasperTable2()));
			
	      driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
	        
	        
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");			
		           
	      String value =ele.getText();
				
	   	if(value.contains(value)) {
						
		         		MoveToElement.byXpath(driver, PomObj.CasperTable2());	

		   WaitFor.clickableOfElementByXpath(driver, PomObj.CasperTable2());
					
			            ele.click();
					  	
			    MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
						
			    
			for(int i =0;i<21;i++) {
						MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			}
			
						
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
						
						MoveToElement.bysendkeyWithoutclick(driver, ele, "vice");
					
		    UpperToolBar ob = new UpperToolBar(driver);
				  	   ob.save_btn_method();
				  	   
				  	 //core.HandleAlert.isAlertPresentAccept(driver); 	
				  	 
				   }
	      }catch (Exception e){
					 e.printStackTrace();
	               }
				  
		
		
		//figure5  
		try {
			Thread.sleep(3000);
			
	        

	         switc.SwitchCase(uAgent); 
			
			
					 WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
						
				     WebElement ele1=driver.findElement(By.xpath(PomObj.CasperTable2()));
			    
				     
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "f</del>";
				 String expectedValue2 = "vice</ins>";
				 String expectedValue3 = "ice-del ice-cts";
				 String expectedValue4=  "ice-ins ice-cts";
		
				 System.out.println("After save - " + actualValue);
		
			
	if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true&& actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
		status="Pass";
		MyScreenRecorder.stopRecording();
		 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		    		      			util.deleteRecFile(RecVideo); 		 	
		    		      					     			         				      				 	
		    		      					     			         				      				 	
		    		      					    }else{
		    		      					     			         				      			    	  				     			         				      			   
		    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
		          status="Fail";
		    		      					     			         				      				 			
		    		  MyScreenRecorder.stopRecording();
		   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		    		      	      util.MoveRecFile(RecVideo);
		 		 	 remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
		 		
		 		 	Thread.sleep(10000);	 
		 		 	 
		 		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				    	}
				    	 
				 	
				 	if(actualValue.contains(expectedValue1)==true) {
				 	   System.out.println("E1: No issues");
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	      }
				    if(actualValue.contains(expectedValue3)==true) {
					 	   System.out.println("E3: No issues");
					 	}
					if(actualValue.contains(expectedValue4)==true){
					 	   System.out.println("E4: No issues");
					       }
				       }catch (Exception e) {
		               e.printStackTrace();
			       }finally{
			    	   System.out.println(className);
	         	 Ex.testdata(description, className, remark, category, area, status, uAgent); 	 
	                 }
		        }
	
	
	
	@Test(alwaysRun = true)

	public void  CUPTableCaptionTest11() throws Throwable {
		  
		 try {
			 MyScreenRecorder.startRecording(uAgent,className);
			 
			  CUP_TableCaption11 obj = new CUP_TableCaption11();
	                obj.delonechr_DeleteKey_insertTXT();			   
	                
		} catch (Exception e){
			
			e.printStackTrace();
		     }finally{
		    	 
		    	   System.out.println(className);
		         	 Ex.testdata(description, className, remark, category, area, status, uAgent); 	
		         	 
		                 }
   	               }
             }
