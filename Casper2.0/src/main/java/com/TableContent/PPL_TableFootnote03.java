package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_TableFootnote03 extends PPL_BaseClass{
	/**
	 * Insert special character and press delete key, tracking should be present
	 **/
	
	 
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
   
   
Pom PomObj;

	/*public TandF_PE_Footnote03(WebDriver ldriver) {
	this.driver = ldriver;
		}*/
	
	public void insertSplChar_PressDelKey() throws Exception {
				
try {
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableFootNote-->Insert special character and press delete key, tracking should be present";
	      className = "PPL_TableFootnote03";    
	          area = "TableFootnote";
	             category = "Insert and Delete";
           
	             
	           Cookies cokies =new Cookies(driver);
	             cokies.cookies();
	              
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent); 
             
                PomObj=new Pom(); 
                
                
          WaitFor.presenceOfElementByXpath(driver, Pom.PPLTable3_Footnote());
      
    		WebElement ele = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));
    		
     ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele); 				
         ((JavascriptExecutor)driver).executeScript("window.scrollBy(200,300)");
         
    		 String value = ele.getText();
    				
    				if(value.contains(value)) {
    					 
    				
      MoveToElement.byXpath(driver, Pom.PPLTable3_Footnote());
    			
                        ele.click();
    				       
    			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
    					
    					for(int i =0;i<65;i++) {
    						
    					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
    					}
    					
    					
    					SymbolActionFuction obj = new SymbolActionFuction(driver);
  		                        obj.SymbolTest();
    				    
  		          
  		                  
  		                      switc.SwitchCase(uAgent);
  		           WebElement ele1 = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));
    					
    				MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);
    				  
    					UpperToolBar ob = new UpperToolBar(driver);
    					ob.save_btn_method();
    					  					
    			}
    				   

		              switc.SwitchCase(uAgent); 
    				
   WaitFor.presenceOfElementByXpath(driver, Pom.PPLTable3_Footnote());
    	    		WebElement ele1 = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));
    				    
    	    		 String actualValue =  ele1.getAttribute("innerHTML");
    	    		 String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
    				 String expectedValue2 = "ice-del ice-cts";
    				 String expectedValue3 = "m</del>";
    			     String expectedValue4="ice-ins ice-cts";
    				 
    			     System.out.println("After save - " + actualValue);
    	
    	
    if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
    					
    					status="Pass";
    				        
    				 }else{
    					 
    					 
    				Thread.sleep(2000);	 
    					// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);	 
    					
    					 status="Fail";
    	
    	
    	remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
    				
    	      utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
    								
    				    if(actualValue.contains(expectedValue1)==true) {
    					   System.out.println("E1: No issues");
    					
    					}
    				 
    			if(actualValue.contains(expectedValue2)==true){
    					   System.out.println("E2: No issues");
    					  
    					}
    				  
    					 if(actualValue.contains(expectedValue3)==true) {
    					   System.out.println("E3: No issues");
    	
    				              }
    					  if(actualValue.contains(expectedValue3)==true) {
    							   System.out.println("E4: No issues");
    			
    						              }
    				                 }
                 }catch(Exception e) {
    	
    	           e.printStackTrace();
              }finally{
            	  System.out.println(className);
      	    	Ex.testdata(description,className,remark,category,area,status,uAgent);  	  
                      }
    			  }	
	
	
	   @Test(alwaysRun = true)
	
	public void  PPLTableFootnoteTest3() throws Throwable {
		
		try {
	
				PPL_TableFootnote03 obj = new PPL_TableFootnote03();
				       obj.insertSplChar_PressDelKey();	
				       
			} catch (Exception e) {	
				e.printStackTrace();
			     }
	          }	
	
		 }
	       

