package com.TableContent;



import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class TandF_TableCaption18 extends TandFBaseClass{

	/**
	 *Single word select and "Control + x" then "Control + v" in next next word, check delete and insert tracking and data lose
	 **/
	
   
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	

	public void Cut_ThenPaste_nxt_nxtWord() throws Exception{
		
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-TandF_CopyEditor-TableCaption-->Single word select and Control + x then Control + v in next next word, check delete and insert tracking and data lose";
		 className = "TandF_TableCaption18";    
		    	area = "Table Caption";
			    	category = "Cut and Paste";
				      
				
				
				Cookies cokies =new Cookies(driver);
		          cokies.cookies();
		       
		    Switch switc = new Switch(driver);
		       switc.SwitchCase(uAgent);
				      
		       PomObj = new Pom();  
			 
		     try {		
		   			  
		 		     WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
		 						
		 				     WebElement ele=driver.findElement(By.xpath(PomObj.TableCaption3()));
		 				   
		 				driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS) ;
					
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				
			String value = ele.getText();
			
			if(value.contains(value)) {
  
				MoveToElement.byXpath(driver, PomObj.TableCaption3());	

				WaitFor.clickableOfElementByXpath(driver, PomObj.TableCaption3());
				    
				ele.click();
				 
  
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				
				
				for(int i=0;i<58;i++) {
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
					   }
				     
				MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 11,Keys.ARROW_RIGHT);
				
				if(uAgent.equals("IE")) {
					
					Robot robot = new Robot();
			     robot.keyPress(KeyEvent.VK_CONTROL);
			     robot.keyPress(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_CONTROL); 
				
				}else {
				
					MoveToElement.cut(driver);
				
				}
				
				for(int i=0;i<18;i++) {
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
				   }
				
				if(uAgent.equals("IE")) {
					((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");			
							       core.HandleAlert.isAlertPresentAccept(driver);		
									
								}else {
								
								MoveToElement.paste(driver);
						}
 
		 UpperToolBar obj = new UpperToolBar(driver);
				obj.save_btn_method();
				
				
				  }}catch (Exception e){
					 e.getStackTrace();
				}
		
	try 
	{			
		Thread.sleep(3000);
		
    switc.SwitchCase(uAgent);
		
		 
				WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
					
				   WebElement ele1=driver.findElement(By.xpath(PomObj.TableCaption3()));
				     
				     String afterSave =  ele1.getAttribute("innerHTML");
					
							
	//String beforeSave = "Effect of biomass concentration on plateau permeate flux in dynamic filtration of T. suecica.";
					
				     String expectedValue1 = "prospective</ins>";
					 String expectedValue2 = "prospective</del>";
					 String expectedValue3 = "ice-del ice-cts";
					 String expectedValue4="ice-ins ice-cts";

				  System.out.println("After save - " + afterSave);



if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true && afterSave.contains(expectedValue4)==true){
					// if(!beforeSave.equals(actualValue) && afterSave.contains(expectedValue1) && afterSave.contains(expectedValue2)){
					 	System.out.println("-->:Pass");
					 	status="Pass";
					 	
					 	 
					 }else{
						 Thread.sleep(10000);

//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		

status = "Fail";
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);

					 }
					 	 
					 	
					 if(afterSave.contains(expectedValue1)==true) {
					 	    System.out.println("E1: No issues");
					 	}
					 if(afterSave.contains(expectedValue2)==true){
					 	    System.out.println("E2: No issues");
					 	}
					 if(afterSave.contains(expectedValue3)==true){
						 	System.out.println("E3: No issues");
						 	}
					 if(afterSave.contains(expectedValue3)==true){
					 	    System.out.println("E4: No issues");
					 	   }
					    }catch (Exception e) {
			    	
					   e.printStackTrace();
					   
						    }finally {
				 System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);
			}
    }
	
	
	
	
	
	@Test(alwaysRun = true)

	public void  Test18() throws Throwable {
		  
		 try {
			 
		
			  TandF_TableCaption18 obj = new TandF_TableCaption18();
	              obj.Cut_ThenPaste_nxt_nxtWord();
			
	   
	                
		} catch (Exception e){
			
			e.printStackTrace();
	     	     }
     	     }
	     }
