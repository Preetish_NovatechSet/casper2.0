package com.TableContent;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableContain07 extends PPL_BaseClass {


	
static String cell1;
static String cell2;
static String AfterMergeCell;
static String remark;
static String className;
static String category;
static String area;
static String description;
static String status;
public static Excel Ex;
Pom PomObj;

	
    /**
	 * Multiple cell Merge and check data lose
	 **/
	

	
	
	public void merge_multipleCell() throws Exception {
		
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	 description = "Casper-PPL_CopyEditor-TableContain-->Multiple cell Merge and check data lose";
     		  className = "PPL_TableContain07";    
	        	      area = "Table Contain";
		                   category = "Merge Multiple Cell";
		         
	    
		         Cookies cokies =new Cookies(driver);
		          cokies.cookies();
		       
		          
		       Switch switc = new Switch(driver);
		 	          switc.SwitchCase(uAgent);
		         
		  
		              PomObj = new Pom();
	  
	    WaitFor.presenceOfElementByXpath(driver, PomObj.PPLMearge());
	     
	    WebElement ele =driver.findElement(By.xpath(PomObj.PPLMearge()));
			
		              
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
    
      
                   MoveToElement.byclick(driver, ele);
      
                   
                   
                   Actions action=new Actions(driver);
           	                    Thread.sleep(2000);
               action.keyDown(Keys.LEFT_SHIFT).build().perform();
           	    
                   
           	   WaitFor.presenceOfElementByXpath(driver, PomObj.PPLMearge1());     	     
       	    WebElement ele1 =driver.findElement(By.xpath(PomObj.PPLMearge1()));
           	    
       	              MoveToElement.byclick(driver, ele1);
           	    
           	       action.contextClick(ele1).build().perform();
           	       
           	    action.keyUp(Keys.LEFT_SHIFT).build().perform();
           	    
           	   
           	 Robot robot = new Robot();
           	 robot.delay(1);
           	 Thread.sleep(3000);
           	 robot.keyPress(KeyEvent.VK_RIGHT);
           	 Thread.sleep(3000);
           	 robot.keyPress(KeyEvent.VK_ENTER);
        
           	
          	 
          	 
          	 
          UpperToolBar ob = new UpperToolBar(driver);
	  	         ob.save_btn_method();
   
		
    try {
			
			switc.SwitchCase(uAgent);
			
			     WaitFor.presenceOfElementByXpath(driver, PomObj.PPLTableValidation());
				     
			   WebElement ele11 =driver.findElement(By.xpath(PomObj.PPLTableValidation()));
								              
	//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
			
	     String value = ele11.getText();		           
		  
	     if(value.contains("21") && value.contains("22")) {
	    	 
	    	 status="Pass";
	    	 
	     }else {
	    	 
	    	 status="Fail";
	    	 
	    	 remark="Cell Didn't merge will Click on Merge-Tab / Right Click Dint't Work";
	    		
	    		    Thread.sleep(10000);
	    		
	   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	    					    						   
	    					 }
	    			    }catch (Exception e){
	    			    	
	    			e.getStackTrace();
	    		        	}finally {
	    		        		System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	    		        	}
	                }
	
	
	

@Test(alwaysRun=true)

	public static void PPLTableContainTest7() throws Exception {
		
		 try {
		 
			 
    PPL_TableContain07 obj = new PPL_TableContain07();
			     obj.merge_multipleCell();
			     
		    }catch (Exception e){
				e.printStackTrace();
		     }
	     }
	
	 }
	
