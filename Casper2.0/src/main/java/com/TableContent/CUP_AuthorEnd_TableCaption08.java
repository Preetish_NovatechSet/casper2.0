package com.TableContent;



import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableCaption08 extends CUP_AuthorEnd_BaseCalss {

	static String remark;
	static String className = "CUP_AuthorEnd_TableCaption08";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
		
    
	/*********
	 *8. Single word select and use backspace key for delete, check tracking and data lose
	 *********/
    
    

	public  void selectSingle_word_bckspace() throws Exception {
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_AuthorEditor-TableCaption-->Single word select and use backspace key for delete, check tracking and data lose";
		    area = "Table Caption";
		        category = "Select and delete";
		             	   
             
          Switch switc = new Switch(driver);
          
          
          CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                     C1_C2.CasperCUP(uAgent);
          
          
             switc.SwitchCase(uAgent); 
	                 
		    
                  System.out.println("BrowerName->"+uAgent);
		
                  PomObj = new Pom();  
     try {  

WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());

WebElement ele=driver.findElement(By.xpath(PomObj.CasperTable2()));
    driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);   


String value = ele.getText(); 	

if(value.contains(value)) {

((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");

MoveToElement.byXpath(driver, PomObj.CasperTable2());	

       WaitFor.clickableOfElementByXpath(driver, PomObj.CasperTable2());

                    ele.click();
				
MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
							
	for(int i =0; i<45;i++){
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);

}

			
MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 8,Keys.ARROW_LEFT);

       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
			
UpperToolBar ob = new UpperToolBar(driver);
      ob.save_btn_method();

//core.HandleAlert.isAlertPresentAccept(driver);
              } 
       }catch (Exception e){
          e.printStackTrace();
}


try {
     Thread.sleep(3000);

      Switch switc1 = new Switch(driver);
             switc1.SwitchCase(uAgent); 


WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());

WebElement ele1=driver.findElement(By.xpath(PomObj.CasperTable2()));

    String afterSave =  ele1.getAttribute("innerHTML");		    
	String expectedValue1 = "specific</del>";
	String expectedValue2 = "ice-del ice-cts";			

System.out.println("After save - " +afterSave);

if(afterSave.contains(expectedValue1)&&afterSave.contains(expectedValue2)){

	status="Pass";
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo);
	    		      	      
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";

Thread.sleep(10000);

          utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);

if(afterSave.contains(expectedValue1)==true) {
	   System.out.println("E1: No issues");
	}

if(afterSave.contains(expectedValue2)==true){
	   System.out.println("E2: No issues");
	          }		   
          }
     }catch (Exception e){
         e.getStackTrace();
           }finally {
	      System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
        }
}
	
	

@Test(alwaysRun = true)

	public void  CUPTableCaptionTest8() throws Throwable {
		  
		 try {		 
			 
			 MyScreenRecorder.startRecording(uAgent,className);
	
			 CUP_AuthorEnd_TableCaption08 obj = new CUP_AuthorEnd_TableCaption08();
	                obj.selectSingle_word_bckspace();
			
	              
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
	     }	
     }
