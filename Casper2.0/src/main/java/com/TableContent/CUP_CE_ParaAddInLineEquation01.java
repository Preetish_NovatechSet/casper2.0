package com.TableContent;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;


public class CUP_CE_ParaAddInLineEquation01 extends CUP_BaseClass{
	
	
	
	static String remark;
	static String className = "CUP_CE_ParaAddInLineEquation01"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	
	
	
	
	public void AddInline01() throws InterruptedException{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_CopyEditor-Click on Para then click on Edit Option go to the add section then click on equation check whether Equation type are displayed or not";			    
	    area = "Paragraph";
			category = "Add InLine Equation";
			
			
			try {
				
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
				
				System.out.println("BrowerName->"+uAgent);
				
   Cookies cokies =new Cookies(driver);
         cokies.cookies();
				
            Switch switc = new Switch(driver);
				switc.SwitchCase(uAgent);
				
				    PomObj= new Pom();		
				    
				    
				   WaitFor.presenceOfElementByXpath(driver, PomObj.Para1());
				               	
				 WebElement ele= driver.findElement(By.xpath(PomObj.Para1()));  
							   			          			        
				     String value = ele.getAttribute("innerHTML");
				       			//System.out.println("check 1 : "+value);   			       			
 		
				      if(value.contains(value)){
 		  
				 	    	
   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);           
       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	     	

 	    MoveToElement.byXpath(driver,  PomObj.Para1());
				  
				   for(int i=0;i<2;i++){
				       		    ele.click();
				       	 }
				   
				   
				            driver.switchTo().defaultContent();
				   
				WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
             WebElement ele1= driver.findElement(By.xpath(PomObj.Edit_Option()));  
				   
				   MoveToElement.byXpath(driver,  PomObj.Edit_Option());
				                   ele1.click();
				                                                 
				                      Thread.sleep(3000);
				                   
				     WaitFor.presenceOfElementByXpath(driver, PomObj.LeftPanelAdd());
				   WebElement Add= driver.findElement(By.xpath(PomObj.LeftPanelAdd()));  
				            	           				            	           
				     MoveToElement.byXpath(driver,  PomObj.LeftPanelAdd());
				                   Add.click();
				                   			                   
				      WaitFor.presenceOfElementByXpath(driver, PomObj.Equation());
				   WebElement ele11= driver.findElement(By.xpath(PomObj.Equation())); 
				        	      
				    MoveToElement.byXpath(driver,  PomObj.Equation());   
				        	     ele11.click();      
				        	           
				        	 Thread.sleep(4000);
				        	 
				   WaitFor.presenceOfElementByXpath(driver, PomObj.ListtheEquationType());
   List<WebElement> ListTheEquationTyp= driver.findElements(By.xpath(PomObj.ListtheEquationType())); 
				           
	String Actualvalue=""; 
	  
			for(WebElement List:ListTheEquationTyp){
				            	 
				      Actualvalue=Actualvalue+List.getAttribute("innerHTML");				            	 
				                      
				           }
				        
				       /* System.out.println("List Of InLine-->"+Actualvalue);*/
				        
if(Actualvalue.contains("Inline Equation")==true&&Actualvalue.contains("Display Numbered")==true&&Actualvalue.contains("Display Unnumbered")==true){

		status ="Pass";
						 	
										 	
						 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
						       util.deleteRecFile(RecVideo); 	 	
						 	
						 	
					      }else{
					    	     	  
					    	  	    
		 status="Fail";
						 	
						 	   
						MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
						 	    util.MoveRecFile(RecVideo);			
						 			
						
remark="Curser is jumping/ Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";		 
				Thread.sleep(10000);
						 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);						 	   
			}
				        			        
				      }
			    }catch(Exception e){				  
				    e.printStackTrace();
			}
      }
	
	
@Test(alwaysRun = true)	
	public void ParaAddInline01() throws IOException{
		
		try {
			
	MyScreenRecorder.startRecording(uAgent,className);	
			
	CUP_CE_ParaAddInLineEquation01 obj= new CUP_CE_ParaAddInLineEquation01();
			                obj.AddInline01();
		   }catch (Exception e){
			    e.printStackTrace();
		     }finally{
		   	  
		     	   System.out.println(className);
		   Ex.testdata(description, className, remark, category, area, status, uAgent);
		    }
		 }
    }
