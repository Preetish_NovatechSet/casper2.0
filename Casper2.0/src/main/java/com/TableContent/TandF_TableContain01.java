package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;


//Article ID-CWSE1307956


public class TandF_TableContain01 extends TandFBaseClass{

	static String remark;
	static String className = "TandF_TableContain01";  
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
   
    Pom PomObj;   
    
    
  
	
	/**
	 * Delete one character with Back space key press and insert special character, tracking should be present
	 **/
    
	public void Click_on_Aspirations() throws Throwable{
		
		try {
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-TandF_CopyEditor-TableContain-->Delete one character with Back space key press and insert special character, tracking should be present";	     
	         area = "Table Contain";
	            category = "Delete insert Special Character";
	               
                             
        
	            Cookies cokies =new Cookies(driver);
		            cokies.cookies();
		       
		    Switch switc = new Switch(driver);
		       switc.SwitchCase(uAgent);    
	            
	            
                     PomObj = new Pom();   
                     
        WaitFor.presenceOfElementByXpath(driver, PomObj.Abstractheading());         
	
      WebElement ele= driver.findElement(By.xpath(PomObj.Abstractheading()));
		
               driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
      
   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
        ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");   
			String value = ele.getText();
	
				if(value.contains(value)){
     
		MoveToElement.byXpath(driver, PomObj.Abstractheading());
		
			WaitFor.clickableOfElementByXpath(driver, PomObj.Abstractheading());
			
          MoveToElement.byclick(driver, ele);
         
          
          switch (uAgent) {
   	   
          case "edge":
   		  
        	  for(int i=0;i<1;i++){
   		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);  
   		   }
   		      break; 
   	     }
     
          
       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);

  
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
		
	      WaitFor.visibilityOfElementByWebElement(driver, ele);
	      
	      
 
 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
 
 
   SymbolActionFuction obj = new SymbolActionFuction(driver);
               obj.SymbolTest();
 
        			
            UpperToolBar obj1 = new UpperToolBar(driver); 
			        	obj1.save_btn_method();
			     
			    	
			        	
				} }catch(Exception e){
						
						e.printStackTrace();	
				} 
		
		try {	

			Thread.sleep(3000);
		
	       
	    Switch switc = new Switch(driver);
	       switc.SwitchCase(uAgent);
	       
		 WaitFor.presenceOfElementByXpath(driver, PomObj.Abstractheading());
		 
		WebElement ele1=driver.findElement(By.xpath(PomObj.Abstractheading()));
					     
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = "e</del>";
			     String expectedValue4="ice-ins ice-cts";
			     
			System.out.println("After save - " + actualValue);
			
			
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
			 	
			status ="Pass";
			  }else{
				  
				  Thread.sleep(10000);  
		             status ="Fail"; 
		
  remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
					
      utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
        
	if(actualValue.contains(expectedValue1)==true){
		 System.out.println("E1: No issues");
			}
			  
			 	  
			 if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  }
			   
			 	    if(actualValue.contains(expectedValue3)==true) {
			 	   System.out.println("E3: No issues");

			                }
			 	    if(actualValue.contains(expectedValue4)==true) {
					 	   System.out.println("E4: No issues");

					              }
			              }
		        }catch(Exception e){
			
			e.printStackTrace();
	       }finally {
	    	   System.out.println(className);
				Ex.testdata(description, className, remark, category, area, status, uAgent);
	       }
		}
	


	@Test(alwaysRun = true)

	public void  TableContainTest01() throws Throwable {
		  
		 try {
	
			TandF_TableContain01 obj = new TandF_TableContain01();
                     obj.Click_on_Aspirations();
		                    
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
        }	
   }