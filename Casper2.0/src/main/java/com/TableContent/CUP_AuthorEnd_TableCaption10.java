package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableCaption10 extends CUP_AuthorEnd_BaseCalss {

	
	static String remark;
	static String className = "CUP_AuthorEnd_TableCaption10";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;

	Pom PomObj;
  
	/****
	*10. Delete single word character and insert some text, check insert and delete tracking present
	*****/


	public void deleteSingleWordChar_AndInsert_SomeText() throws Exception {
			  
			  
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_AuthorEditor-TableCaption-->Delete single word character and insert some text, check insert and delete tracking present";			    
			  area = "Table Caption";
			       category = "Delete and insert";
			                 
			      						    
    System.out.println("BrowerName->"+uAgent);	
			                
			                 
			              Switch switc = new Switch(driver);
			                
			           CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
		                     C1_C2.CasperCUP(uAgent);
		                     
			              switc.SwitchCase(uAgent); 
			                  
			                 PomObj = new Pom();  
			                 
	try {  
						 
			    WaitFor.presenceOfElementByXpath(driver, PomObj.AddTable1());
					  		 		
			   WebElement ele=driver.findElement(By.xpath(PomObj.AddTable1()));
				       			 				       	          
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);    
				       		
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");			       					
				       					
	      String value =ele.getText();
				       					
	if(value.contains(value)){
				       					
				       MoveToElement.byXpath(driver, PomObj.AddTable1());	
				       						       		
				       		Actions action= new Actions(driver);
					WaitFor.presenceOfElementByXpath(driver, PomObj.AddTable1());
								action.contextClick(ele).build().perform();
																
						 driver.switchTo().defaultContent();
						 
						 Thread.sleep(2000);
						 
		action.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
				         
				         action.sendKeys(Keys.ENTER).build().perform();
								 
								 
				         Thread.sleep(2000);	
				         
				         WaitFor.presenceOfElementByXpath(driver, PomObj.AddText());
			  		 		
						WebElement ele1=driver.findElement(By.xpath(PomObj.AddText()));
						 
						MoveToElement.byclick(driver, ele1);
						  
		MoveToElement.bysendkeyWithoutclick(driver, ele1, "the punishment assigned to Dog");
		
		    
		    WaitFor.presenceOfElementByXpath(driver, PomObj.Tbsave());   
		        ele1=driver.findElement(By.xpath(PomObj.Tbsave()));
	                     MoveToElement.byclick(driver, ele1);
		
	                     
	                     switc.SwitchCase(uAgent);
	                     
	                            MoveToElement.byclick(driver, ele);	                     
				       				    	
				       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
				       				    	
				       				for(int i=0;i<30;i++) {
				       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
				       			}
				       				 
				       				for(int i=0;i<3;i++) {
				       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
				       			}
				       					
				       		
				       MoveToElement.bysendkeyWithoutclick(driver, ele, "preetish");
				       						 
				       			
				       		    UpperToolBar ob = new UpperToolBar(driver);
				       			  	    ob.save_btn_method();
				       		 
				       		// core.HandleAlert.isAlertPresentAccept(driver);
				       				       }
				       					}catch (Exception e){
				       					 
				       					e.printStackTrace(); 
				       			        
				                   }
				       				
				       		  
				       try {
				       Thread.sleep(3000);
				       Switch switc1 = new Switch(driver);
				       switc1.SwitchCase(uAgent); 
				       				

				   WaitFor.presenceOfElementByXpath(driver, PomObj.AddTable1());
				       	  		 		
				 WebElement ele1=driver.findElement(By.xpath(PomObj.AddTable1()));
				       			
				       String actualValue =  ele1.getAttribute("innerHTML");
				       String expectedValue1 = "the</del>";
				       String expectedValue2 = "preetish</ins>";
				       String expectedValue3 = "ice-del ice-cts";
				       String expectedValue4 = "ice-ins ice-cts";
				    System.out.println("After save - " + actualValue);

if(actualValue.contains(expectedValue1)==true&&actualValue.contains(expectedValue2)==true&&actualValue.contains(expectedValue3)==true&&actualValue.contains(expectedValue4)==true){
				    	   
				    	   status="Pass";
	MyScreenRecorder.stopRecording();
			String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				   		    		      			util.deleteRecFile(RecVideo); 		 	
				   		    		      					     			         				      				 	
				   		    		      					     			         				      				 	
				   		    		      					    }else{
				   		    		      					     			         				      			    	  				     			         				      			   
				   		    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
				   		          status="Fail";
				   		    		      					     			         				      				 			
				   		    		  MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
				   		    		      	      util.MoveRecFile(RecVideo);
				   		    		      	      
 remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
				        		 	   
				        		 	 Thread.sleep(10000);	   
				  utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				        		 	   
				        		 	   
				       if(actualValue.contains(expectedValue1)==true) {
				       			System.out.println("E1: No issues");
				       		}
				       			 	
				       if(actualValue.contains(expectedValue2)==true){
				       	 System.out.println("E2: No issues");
				       			 	    }
				       			     
				      if(actualValue.contains(expectedValue3)==true){
				       	 System.out.println("E3: No issues");
				       			        }
				      if(actualValue.contains(expectedValue4)==true){
				       	 System.out.println("E4: No issues");
				       				   }
				       			   }
				       		   }catch (Exception e){
				       	 e.getStackTrace();
				       			   
				       			}finally {
				       System.out.println(description+"\n"+className);
		Ex.testdata(description, className, remark, category, area, status, uAgent);
				       		      }
				       	      }
	
	

	@Test(alwaysRun = true)

	public void  CUPTableCaptionTest10() throws Throwable {
		  
		 try {
			 
			 MyScreenRecorder.startRecording(uAgent,className);
			 
			 CUP_AuthorEnd_TableCaption10 obj = new CUP_AuthorEnd_TableCaption10();
	                obj.deleteSingleWordChar_AndInsert_SomeText();
				          
		   }catch(Exception e){			
			      e.printStackTrace();
		          }
    	      }
          }