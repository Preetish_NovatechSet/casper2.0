package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_TableCaption03 extends CUP_BaseClass{
	
	
	
	static String remark;
	static String className = "CUP_TableCaption03";
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;

	Pom PomObj;
	
			
	
	
	/**
	 * Insert special character and press delete key, tracking should be present
	 **/
	
public void Click_on_Service() throws Exception{

		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
 description = "Casper-CUP_CopyEditor-TableCaption-->Insert special character and press delete key, tracking should be present";		    
		area = "Table Caption";
		  category = "Delete insert Special Character";
			            
				System.out.println("BrowerName->"+uAgent);                 
		           
				Cookies cokies =new Cookies(driver);
		             cokies.cookies();
		              
		           Switch switc = new Switch(driver);
		              switc.SwitchCase(uAgent); 
		              
		       PomObj = new Pom();   
		              
			           try {           
			                     
		 WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());         
				
		WebElement ele= driver.findElement(By.xpath(PomObj.CasperTable2()));
			   driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
			     
					 String value = ele.getText();
				
				if(value.contains(value)){
								
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
		((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		
			 
		MoveToElement.byXpath(driver, PomObj.CasperTable2());	
			 
		WaitFor.clickableOfElementByXpath(driver, PomObj.CasperTable2());
		     
			            ele.click();
						        	            
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);

			  
					
			for(int i=0;i<74;i++){
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				}
					 
			 
			   SymbolActionFuction obj = new SymbolActionFuction(driver);
			                    obj.SymbolTest();
			 
			                  
			               switc.SwitchCase(uAgent); 
			                    				                    
			                    
			    WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
			   WebElement ele1=driver.findElement(By.xpath(PomObj.CasperTable2()));
			   	      			
			   	      MoveToElement.byXpath(driver,PomObj.CasperTable2());
			           
			   	             
			   	     switch (uAgent) {
			             	 
				          case "edge":
				  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT); 
				              	 break;  
				                  
				       }     
			        
			  			      
			         MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);                    
			                    
			        			
			             UpperToolBar obj1 = new UpperToolBar(driver); 
						        	obj1.save_btn_method();
						 			        	
							  } 
				    }catch(Exception e){
									
							    e.getStackTrace();
							}
		try {	
								
				Thread.sleep(3000);
						    
			           switc.SwitchCase(uAgent); 
											
	   WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
	 WebElement ele1=driver.findElement(By.xpath(PomObj.CasperTable2()));
								     
			String actualValue = ele1.getAttribute("innerHTML");
			String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
			String expectedValue2 = "ice-del ice-cts";
			String expectedValue3 = "p</del>";
			String expectedValue4 = "ice-ins ice-cts";
			System.out.println("After save - " + actualValue);
						
						
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
						 	
						status ="Pass";
						
			MyScreenRecorder.stopRecording(); 
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);				      	
	        util.deleteRecFile(RecVideo);
				      	
						  }else{							  				
					             
						 status ="Fail"; 
						 						 
		MyScreenRecorder.stopRecording();
String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
					  util.MoveRecFile(RecVideo); 			
					
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
			     
	 Thread.sleep(10000);  		
	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			      			    	   
			      
						 if(actualValue.contains(expectedValue1)==true){
						 	   System.out.println("E1: No issues");
						 	}
						  
						 	  
						if(actualValue.contains(expectedValue2)==true){
						 	   System.out.println("E2: No issues");
						 	}
						   
						 if(actualValue.contains(expectedValue3)==true) {
						 	   System.out.println("E3: No issues");
                         }
						 	    
				if(actualValue.contains(expectedValue4)==true) {
					 System.out.println("E4: No issues");

								          }
						          }
					       }catch(Exception e){
						
						e.printStackTrace();
					}finally {
						
						System.out.println(className);
				
		Ex.testdata(description,className,remark,category,area,status,uAgent);
						
				       }
					}
			

			  
@Test(alwaysRun = true)
			
		public void  CUPTableCaptionTest3() throws Throwable {
				  
			 try{				 		
				    MyScreenRecorder.startRecording(uAgent,className);
					CUP_TableCaption03 obj = new CUP_TableCaption03();
		                      obj.Click_on_Service();
						                      
				} catch (Exception e) {					
					e.printStackTrace();
				    }
		       }
          }