package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;


/***************
 *  Preetish  *
 ***************/

public class TandF_TableContain05 extends TandFBaseClass{


static String remark;
static String className = "TandF_TableContain05";
static String category;
static String area;
static String description;
static String status;
public static Excel Ex;
Pom PomObj;
	
	
	/**
	 * Insert special character and place cursor to before character then press delete key, tracking should be present 
	 **/
	
	
	public void Click_on_Computer() throws Exception{

			
		try {
			
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	  description = "Casper-TandF_CopyEditor-TableContain-->Insert special character and place cursor to before character then press delete key, tracking should be present";		     
		    area = "Table Contain";
		       category = "Insert and Delete";
		 
		       
		       
		       Cookies cokies =new Cookies(driver);
		          cokies.cookies();
		       
		    Switch switc = new Switch(driver);
		       switc.SwitchCase(uAgent);    
		          
		          PomObj = new Pom();
		    
		   
	 WaitFor.presenceOfElementByXpath(driver, PomObj.Table3_Computer());
					
	 WebElement ele1=driver.findElement(By.xpath(PomObj.Table3_Computer())); 
		
	 driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);		

			String value = ele1.getText();
	
				if(value.contains(value)){
					
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);			
					 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		
						 
       MoveToElement.byXpath(driver,PomObj.Table3_Computer());
					
      WaitFor.clickableOfElementByXpath(driver, PomObj.Table3_Computer());
                    ele1.click();
                     			
		MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
				
				
					
			SymbolActionFuction obj = new SymbolActionFuction(driver);
	                 obj.SymbolTest();
	           
	              
	      	       switc.SwitchCase(uAgent);		   		    	             
	              
	              
	              WaitFor.presenceOfElementByXpath(driver, PomObj.Table3_Computer());
	            
	          WebElement ele0=driver.findElement(By.xpath(PomObj.Table3_Computer())); 		  	     
	              driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
	              
	              
	              if(uAgent.equals("edge")) {
	            	  for(int i=0;i<1;i++) {
					  		MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.ARROW_LEFT);
					  	 }	
	            	  
	              }else {
	            	  
	            	  for(int i=0;i<2;i++) {
					  		MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.ARROW_LEFT);
					  	 }
	            	  
	              }
	     	
		MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.DELETE);
				  	
				  	
			UpperToolBar obj1 = new  UpperToolBar(driver);
			       obj1.save_btn_method();
		
			      
				}} catch (Exception e) {
				e.printStackTrace();
			} 
			
				try {
					
				
					
					
					Thread.sleep(3000);
					
					Switch switc = new Switch(driver);
				       switc.SwitchCase(uAgent); 
					
		WaitFor.presenceOfElementByXpath(driver, PomObj.Table3_Computer());
				
			WebElement ele=driver.findElement(By.xpath(PomObj.Table3_Computer())); 
				
			     String actualValue =  ele.getAttribute("innerHTML");
				 String expectedValue1 = "g</del>";
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue4="ice-ins ice-cts";
			
			System.out.println("After save - " + actualValue);
			
			
			if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
			 	
			 	status="Pass";
			        
      }else{
    	  
    	       	
//    ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
    	  
          status ="Fail"; 
          
     remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing ";
     Thread.sleep(10000); 
	  
     utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver); 
	   

		
			 	
			      if(actualValue.contains(expectedValue1)==true) {
			 	   System.out.println("E1: No issues");
			 	
			 	}
			  
			 	    if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  
			 	}
			   
			 	    if(actualValue.contains(expectedValue3)==true) {
			 	   System.out.println("E3: No issues");

			                }
			 	   if(actualValue.contains(expectedValue4)==true) {
				 	   System.out.println("E4: No issues");
			          }
			     } 	
		} catch (Exception e) {
			e.printStackTrace();
		}
     }

	

	@Test(alwaysRun=true)
	public static void Test5() throws Exception {
	
		
		
		 try {
			
			 
		TandF_TableContain05 obj = new TandF_TableContain05();
                     obj.Click_on_Computer();
                        
		   } catch (Exception e) {
				e.printStackTrace();
			     }finally{
						System.out.println(className);
		Ex.testdata(description, className, remark, category, area, status, uAgent);
						   
							}
     	      }
         }