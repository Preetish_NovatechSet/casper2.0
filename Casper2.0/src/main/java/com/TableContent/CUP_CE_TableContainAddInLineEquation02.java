package com.TableContent;


import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.FigureContain.Pom;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_CE_TableContainAddInLineEquation02 extends CUP_BaseClass{
	
	
	static String remark;
	static String className = "CUP_CE_TableContainAddInLineEquation02"; 
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	Pom PomObj;
	/*private String currentDir = System.getProperty("user.dir");
	private File ActulwebElementImageFile = new File(currentDir + 
            "\\screenshots\\EGlogo.png" );*/
	
	
	
	public void AddInline02() throws Exception{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Click on Table Contain area, then click on Edit Option go to the add section then click on the equation, Select the inline Equation and add some symbol from Math and Save, check whether the inserted math symbol has displayed correctly or not";			    
			  area = "Table Contain";
				   category = "Add inline Equtaion";
					
										
						
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
						
						  System.out.println("BrowerName->"+uAgent);
						
		   Cookies cokies =new Cookies(driver);
		         cokies.cookies();
						
		       Switch switc = new Switch(driver);
						switc.SwitchCase(uAgent);
						
						    PomObj= new Pom();		
						    
						    
						   WaitFor.presenceOfElementByXpath(driver, PomObj.B3());
						               	
						 WebElement ele= driver.findElement(By.xpath(PomObj.B3()));  
									   			          			        
						     String value = ele.getAttribute("innerHTML");
						       			//System.out.println("check 1 : "+value);   			       			
		 		
						      if(value.contains(value)){
		 		  
						 	    	
		   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);           
		       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	     	

		 	    MoveToElement.byXpath(driver,  PomObj.B3());
						  
						   for(int i=0;i<2;i++){
						       		    ele.click();
						       	 }
						   
						   
						            driver.switchTo().defaultContent();
						   
						WaitFor.presenceOfElementByXpath(driver, PomObj.Edit_Option());
		             WebElement ele1= driver.findElement(By.xpath(PomObj.Edit_Option()));  
						   
						   MoveToElement.byXpath(driver,  PomObj.Edit_Option());
						                   ele1.click();
						                                                 
						                      Thread.sleep(3000);
						                   
						     WaitFor.presenceOfElementByXpath(driver, PomObj.LeftPanelAdd());
						   WebElement Add= driver.findElement(By.xpath(PomObj.LeftPanelAdd()));  
						            	           				            	           
						     MoveToElement.byXpath(driver,  PomObj.LeftPanelAdd());
						                   Add.click();
						                   			                   
						      WaitFor.presenceOfElementByXpath(driver, PomObj.Equation());
						   WebElement ele11= driver.findElement(By.xpath(PomObj.Equation())); 
						        	      
						    MoveToElement.byXpath(driver,  PomObj.Equation());   
						        	     ele11.click();   
						        	     
						        	     
											      					        	  
						    driver.switchTo().frame("equation_iframe");
						      System.out.println("Entered into frame");	    
						        	  
						 Actions act = new Actions(driver);
						        	    
		WebElement inline =driver.findElement(By.xpath(PomObj.InlineEquation()));

		new WebDriverWait(driver, 10).until(
WebDriver -> ((JavascriptExecutor) WebDriver).executeScript("return document.readyState").equals("complete"));
			 
		     act.moveToElement(inline).click(inline).build().perform();
			 	        	    
							 
		 WebElement Math =driver.findElement(By.xpath(PomObj.MathFormula1()));	      	 
		      act.moveToElement(Math).click(Math).build().perform();
		      
		      Thread.sleep(3000);
		     WebElement Math1 =driver.findElement(By.xpath(PomObj. MathFormula2()));	      	 
		         act.moveToElement(Math1).click(Math1).build().perform();
		     
		 
						 
		      
		      WebElement MathMultyp =driver.findElement(By.xpath(PomObj.MathMultiply()));	      	 
		         act.moveToElement(MathMultyp).click(MathMultyp).build().perform();
		      
		      Thread.sleep(3000);
		      WebElement MathLim =driver.findElement(By.xpath(PomObj.MathLims()));	      	 
		          act.moveToElement(MathLim).click(MathLim).build().perform();
		      
		          Thread.sleep(10000);
		          
		      driver.switchTo().defaultContent();
			  
		     WebElement SAveEQ =driver.findElement(By.xpath(PomObj.Save_Equation()));	      	 
			      act.moveToElement(SAveEQ).click(SAveEQ).build().perform();
		      		     
			      Thread.sleep(3000);
			      
			      
			      UpperToolBar ob = new UpperToolBar(driver);
		 	  	           ob.save_btn_method();
			      
switc.SwitchCase(uAgent);
  
     
   WebElement web = driver.findElement(By.xpath(PomObj.EqLogo()));


((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", web);           
       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");

       
 String Actual = web.getAttribute("innerHTML");
    System.out.println("Check1-->"+Actual);
         
    
String Expected="\\sqrt[]{}\\frac{\\partial }{\\partial }\\times\\lim_{ \\rightarrow }";


/* 
 *
 * File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
 BufferedImage fullImg = ImageIO.read(screenshot);

 BufferedImage expectedImage = ImageIO.read(new File(currentDir +"\\screenshots\\ExpectedEQlogo.png"));
 
 Point point = web.getLocation();
 
 // Get width and height of the element
 int eleWidth = web.getSize().getWidth();
 int eleHeight = 25;


 
 
//System.out.println("Wid-->"+eleWidth+"\n"+"Height-->"+eleHeight);
 

 // Crop element from viewable area's screenshot to get element's screenshot
 BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), 226, eleWidth, eleHeight);
           ImageIO.write(eleScreenshot, "png", screenshot);

           
 //Write Screenshot to a file
         FileUtils.copyFile(screenshot, ActulwebElementImageFile);
 
	
         ImageDiffer imgDiff = new ImageDiffer();
         ImageDiff diff = imgDiff.makeDiff(eleScreenshot, expectedImage);
         if(diff.hasDiff()==true)
         {
          System.out.println("Images are Not Same");
         }
         else {
          System.out.println("Images are Same");
         }*/
                         									      
 if(Actual.contains(Expected)==true) {
 
status ="Pass";
		 	
		 	
			 MyScreenRecorder.stopRecording();
String RecVideo = util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
			        util.deleteRecFile(RecVideo); 	 	
			 	
			 	
		      }else{
		    	     	  
		    	  	    
 status="Fail";
			 	
			 	   
			MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
			 	    util.MoveRecFile(RecVideo);			
			 			
			
remark="Add Equtaion didn't dislpayed properly";		 
                   Thread.sleep(10000);
                     }   
				}
	     }

	
	
	
	
	public void isjQueryLoaded(WebDriver driver) {
	    System.out.println("Waiting for ready state complete");
	    (new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver d) {
	                JavascriptExecutor js = (JavascriptExecutor) d;
	                String readyState = js.executeScript("return document.readyState").toString();
	                System.out.println("Ready State: " + readyState);
	         return (Boolean) js.executeScript("return !!window.jQuery && window.jQuery.active == 0");
	            }
	        });
	}
			
	
	
						      
	  @Test(alwaysRun = true)	
public void TableContainAddInline02() throws IOException{
						  		
						  		try {
						  			
	             MyScreenRecorder.startRecording(uAgent,className);	
						  			
	 CUP_CE_TableContainAddInLineEquation02 obj= new CUP_CE_TableContainAddInLineEquation02();
		  			    obj.AddInline02();
						  			    
						  		   }catch (Exception e){
						  			    e.printStackTrace();
						  		     }finally{
						  		   	  
						  		     	System.out.println(className);
				 Ex.testdata(description, className, remark, category, area, status, uAgent);
						  		     }
						       }			      						      	                       
	                    }