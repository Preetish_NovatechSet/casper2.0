package com.TableContent;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


/***************
 ***Preetish****
 ***************/


public class TandF_TableContain08 extends TandFBaseClass {
	
	

	static String remark;
	static String className = "TandF_TableContain08";    
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;

    
    
	/*
	 * Multiple cell select and use delete key for delete
	 */

	
    
	
public void select_multlple_CellvalueAndDelete() throws InterruptedException, IOException{
	
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-TandF_CopyEditor-TableContain-->Select Multiple cell word and use delete key, data lose track should be present";
	area = "Table Contain";
		category = "Select multiple cell and delete";
		           
		         PomObj = new Pom();
		       
		     
		         Cookies cokies =new Cookies(driver);
		             cokies.cookies(); 
		         
		         Switch switc = new Switch(driver);
		            switc.SwitchCase(uAgent);
		 try {
			 
			 
			WaitFor.presenceOfElementByXpath(driver, PomObj.cellElement1());
				
			WebElement ele= driver.findElement(By.xpath(PomObj.cellElement1()));
			
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	    ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
				String value = ele.getText();
					
				if(value.contains(value)) {
					
	    MoveToElement.byXpath(driver, PomObj.cellElement1());
	 
	 WaitFor.clickableOfElementByXpath(driver, PomObj.cellElement1());
	 
	                     ele.click();
				       	
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
															
    	 MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 24, Keys.ARROW_LEFT);
				          					     
		   MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
		        
     UpperToolBar obj = new UpperToolBar(driver);
                 obj.save_btn_method();
                		     
		}
		    
				Thread.sleep(3000);
							
			    switc.SwitchCase(uAgent);
				
			 WaitFor.presenceOfElementByXpath(driver, PomObj.cellElement1());
				
			 WebElement ele1= driver.findElement(By.xpath(PomObj.cellElement1()));
	
			 
			 WaitFor.presenceOfElementByXpath(driver, PomObj.cellElement2());
				
			 WebElement ele2= driver.findElement(By.xpath(PomObj.cellElement2()));
			 
			 String	actualValue =  ele1.getAttribute("innerHTML");
			 String actualValue1 =  ele2.getAttribute("innerHTML");
			 String expectedValue1 = "1.8</del>";
			 String expectedValue2 = "ice-del ice-cts";
			 String expectedValue3 = "1.1</del>";
			 
			 
			 System.out.println("AfterSave - "+actualValue + "\n"+actualValue1);
			 
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)&&actualValue1.contains(expectedValue3))
			 
			 {
				 status="Pass";
		        
	      }else{
	    	  
	    	  
	 //   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);  
	    	     	    	  
	          status ="Fail"; 
	          
	      remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Multiple Cell not Deleted";
	      Thread.sleep(10000);			
	      utilitys.Getscreenshot.captureScreenShot(uAgent,className, driver);	
	      
	            }    
			}catch (Exception e) {
			
			e.printStackTrace();
		  
		}
			System.out.println(className);
			
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	   
		}
	

	@Test(alwaysRun=true)
	
public static void Test8() throws Exception {
			
	try {		        
			    
        TandF_TableContain08 obj = new TandF_TableContain08();
		          obj.select_multlple_CellvalueAndDelete();
	       
	} catch (Exception e) {
		e.printStackTrace();
	  }
   }
}