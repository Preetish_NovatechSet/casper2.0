package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class CUP_AddTable01 extends CUP_BaseClass{
	
	
	
	 static WebElement ele =null;
	 static String remark;
	 static String className;
	 static String category;
	 static String area;
	 static String description;
	 static String status;
	 public static Excel Ex;
	 Pom PomObj;
	   
	
	    
	    
	  
		
/**
* Delete one character with Back space key press and insert special character, tracking should be present
**/
	    
	public void Click_on_Aspirations() throws Throwable{
											
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-CUP_AuthorEnd-AddTable-->Click on table menu bar edit the row ,  columns add some text on Caption and Check whether row , columns and table caption is added or not";
		  className = "CUP_AddTable01";    
		         area = "Add Table";
		               category = "Add row , columns, Caption";
		              
	                    		                      		                                     
	                     
			      System.out.println("BrowerName->"+uAgent);                 
	           
			              
			          Cookies cokies =new Cookies(driver);
				             cokies.cookies();
				              
				       Switch switc = new Switch(driver);
				              switc.SwitchCase(uAgent);    				              
	        
	PomObj = new Pom();   
			                     
      
				          		
			WaitFor.presenceOfElementByXpath(driver, PomObj.Para1());
				          		
		 ele= driver.findElement(By.xpath(PomObj.Para1()));
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
				          	     
				      String value = ele.getText();
				          		
	if(value.contains(value)){
				          						
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		          	
	
	 MoveToElement.byXpath(driver, PomObj.Para1());	
				          	 		
	 WaitFor.clickableOfElementByXpath(driver, PomObj.Para1());
				          	            
				          	     ele.click();
				          	     
				         driver.switchTo().defaultContent();
				   			Thread.sleep(2000);
					          		
				     WaitFor.presenceOfElementByXpath(driver, PomObj.AddTableBar());
				     				          		
				    ele= driver.findElement(By.xpath(PomObj.AddTableBar()));
				          	     
				    MoveToElement.byXpath(driver, PomObj.AddTableBar());	
          								          	            
								      ele.click();  
								          
								          
								          
					WaitFor.presenceOfElementByXpath(driver, PomObj.AddRow());
	     				          		
						ele= driver.findElement(By.xpath(PomObj.AddRow()));
									          	     
				 MoveToElement.byXpath(driver, PomObj.AddRow());	
					          								          	            
						MoveToElement.byclick(driver, ele);
									
									
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
									
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
							
				MoveToElement.bysendkeyWithoutclick(driver, ele, "5");				
				          	     
				         
				
				
				WaitFor.presenceOfElementByXpath(driver, PomObj.AddCol());
	          		
				ele= driver.findElement(By.xpath(PomObj.AddCol()));
							          	     
		 MoveToElement.byXpath(driver, PomObj.AddCol());	
			          								          	            
				MoveToElement.byclick(driver, ele);
													
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
							
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
					
		MoveToElement.bysendkeyWithoutclick(driver, ele, "6");	
				
				          	     
		
ele=driver.findElement(By.xpath(PomObj.caption()));
		MoveToElement.byXpath(driver, PomObj.caption());			 
				MoveToElement.byclick(driver, ele);
				
				
MoveToElement.bysendkeyWithoutclick(driver, ele, "It is composed of amorphous");



  ele=driver.findElement(By.xpath(PomObj.Submit()));
    MoveToElement.byXpath(driver, PomObj.Submit());			 
		   MoveToElement.byclick(driver, ele);

			   
		   
		   UpperToolBar ob = new UpperToolBar(driver);
		  	    ob.save_btn_method();
		   
		   
					
					switc.SwitchCase(uAgent);
					
 java.util.List<WebElement> allElements=driver.findElements(By.xpath(PomObj.CasperT4()));
					
             int count = allElements.size();             
               System.out.println(count);
               

				
     java.util.List<WebElement> Elements=driver.findElements(By.xpath(PomObj.CasperT4TD()));
              					
                        int count1 = Elements.size();                         
                         System.out.println(count1);

                         
                         
                      ele=driver.findElement(By.xpath(PomObj.T4()));
                         String caption =ele.getText();
                           System.out.println(caption);
                           
                           
                           
                    int Expected1= 5;   
                    int Expected2= 30;
                    String Expected3="It is composed of amorphous";      
                    
       if(count==Expected1==true && count1==Expected2==true && caption.contains(Expected3)==true) {
            	  
    	   status="Pass";
    	   
              }else{
            	  
            	  status="Fail";
            	  
            	  remark="Unable to add Row, col or Caption";
            	   Thread.sleep(10000);  		
         		 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
                }      
            }
        }

	
	
	@Test(alwaysRun = true)
	
	public void  CUPAddTableTest1() throws Throwable {
		  
	try {
	
			 CUP_AddTable01 obj = new CUP_AddTable01();
                      obj.Click_on_Aspirations();
				                      
		}catch (Exception e){
			
			e.printStackTrace();
		     
		      }finally {
					System.out.println(className);
					Ex.testdata(description, className, remark, category, area, status, uAgent);
							}
                     }
	         }