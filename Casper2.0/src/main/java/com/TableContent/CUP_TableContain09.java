package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_TableContain09  extends CUP_BaseClass{

	
	
	static String remark;
	static String className = "CUP_TableContain09";    
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;

	/**
	*Single cell select and use delete key for delete, check tracking and data lose
	**/
	
	public void selectSingleDel() throws Exception {
	   
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	   description = "Casper-CUP_CopyEditor-TableContain-->Single cell select and use delete key for delete, check tracking and data lose";	      
	         area = "Table Contain";
	            category = "Select Single cell and delete";
	            
	            System.out.println("BrowerName->"+uAgent);  
	            
	            
	                PomObj = new Pom();
	                
	              Cookies cokies =new Cookies(driver);
		             cokies.cookies(); 
		         
		         Switch switc = new Switch(driver);
		            switc.SwitchCase(uAgent);
	                
	    try {
	    	
WaitFor.presenceOfElementByXpath(driver, PomObj.CupNi());
			WebElement ele1= driver.findElement(By.xpath(PomObj.CupNi()));	
			
				
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
	  ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
				String value = ele1.getText();
			//	System.out.println("Name of the element"+value);
				if(value.contains(value)) {
				
		 MoveToElement.byXpath(driver, PomObj.CupNi());
					ele1.click();
					               
				  
					
					  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
					  
					  /*for(int i=0;i<8;i++) {
					  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_LEFT);
					  }*/
					  
			MoveToElement.selectsingleelement(driver, Keys.DELETE);
				    
			
				UpperToolBar o = new UpperToolBar(driver);
			        o.save_btn_method();
			        
			        
				}
         		
				
					switc.SwitchCase(uAgent);
					
		WaitFor.presenceOfElementByXpath(driver, PomObj.CupNi());
				
		   WebElement ele= driver.findElement(By.xpath(PomObj.CupNi()));
				
				String actualValue =  ele.getAttribute("innerHTML"); 
				String expectedValue1 = "Ni</del>";
				String expectedValue2 = "ice-del ice-cts";  
				
		

   System.out.println("After save - " + actualValue);

   //System.out.println("Expected value - " + expectedValue);


 if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true){
			   
	 status="Pass";
		
		MyScreenRecorder.stopRecording();
		 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
		    		      			util.deleteRecFile(RecVideo); 		 	
		    		      					     			         				      				 	
		    		      					     			         				      				 	
		    		      					    }else{
		    		      					     			         				      			    	  				     			         				      			   
		    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
		          status="Fail";
		    		      					     			         				      				 			
		    		  MyScreenRecorder.stopRecording();
		   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
		    		      	      util.MoveRecFile(RecVideo);
		  
			
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert or Delete colour is missing";

    Thread.sleep(10000); 
			
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);   
			
	
			
			if(actualValue.contains(expectedValue1)==true) {
				System.out.println("E1: No issues");
			}
			
   if(actualValue.contains(expectedValue2)==true) {
			System.out.println("E2: No issues");
				
			              }
  
			        }
			  }catch (Exception e) {
				  e.getStackTrace();
		  }finally{
			System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);	
	
		}
	 }
	
	
	
	
	@Test(alwaysRun=true)
	public static void CUPTableContainTest9() throws Exception {
	
		
		
		try {
			
			 MyScreenRecorder.startRecording(uAgent,className);
					 
		CUP_TableContain09 obj = new CUP_TableContain09();
				    obj.selectSingleDel();
				    
		} catch (Exception e) {
				e.printStackTrace();
		      }
	   }
	
}