package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_TableFootnote07 extends PPL_BaseClass{


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
   public static  Excel Ex;
   
Pom PomObj;
	
/*	public TandF_PE_Footnote07(WebDriver ldriver ) {
		this.driver=ldriver;
		
	}
*/	
	

		
	 public void TableFootNote() throws Exception{
	
	

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableFootNote-->Table Cell Add Footnote, check the citation and footnote text";
		  className = "PPL_TableFootnote07";    
		       area = "TableFootNote";
		            category = "Add Footnote";
		          				   
		         PomObj=new Pom(); 
		         
		    Cookies cokies =new Cookies(driver);
  	             cokies.cookies();
  	              
  	        Switch switc = new Switch(driver);
  	              switc.SwitchCase(uAgent);  
		         
	             
  	            WaitFor.presenceOfElementByXpath(driver, Pom.PPLClickOnEle_Add_Footnote());
  			  WebElement ele = driver.findElement(By.xpath(Pom.PPLClickOnEle_Add_Footnote()));
  						
  		             
  			   String value=ele.getText();
  			   
  			   if(value.contains(value)){
  				   
  				   
  		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
  		 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	   
  				MoveToElement.byXpath(driver, Pom.PPLClickOnEle_Add_Footnote());
  					 
  			                   ele.click();
  			                   
  			                   
  			    MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
  			                   
  					    
  			   /* Thread.sleep(2000);
  			    JavascriptExecutor js = (JavascriptExecutor) driver;

  					      String javaScript = "var evt = document.createEvent('MouseEvents');"
  					                      + "var RIGHT_CLICK_BUTTON_CODE = 2;"
  					                      + "evt.initMouseEvent('contextmenu', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, RIGHT_CLICK_BUTTON_CODE, null);"
  					                      + "arguments[0].dispatchEvent(evt)";

  					      js.executeScript(javaScript, ele);*/
  					      
  					      
  			           Actions action= new Actions(driver);        
  			          action.contextClick().build().perform();     
  			    
  			
  					     for(int i=0;i<=5;i++){ 
  					    	 Thread.sleep(900);
  					  action.sendKeys(Keys.ARROW_DOWN).build().perform();
  					      }
  					      			 
  				
  					  action.sendKeys(Keys.ENTER).build().perform();
  					      Thread.sleep(4000);
  					      
  	driver.switchTo().defaultContent();
  	 

  	WaitFor.presenceOfElementByXpath(driver, Pom.label());
  							
  				             
  	              WebElement ele11 = driver.findElement(By.xpath(Pom.label()));      
  					      MoveToElement.byXpath(driver, Pom.label());
  					      Thread.sleep(2000);
  					           ele11.click();
  			                   ele11.sendKeys("Preetish");
  					      
  			            
  			                
  			                WebElement ele2 = driver.findElement(By.xpath(Pom.Description()));      
  		      		  		    MoveToElement.byXpath(driver, Pom.Description());
  								  ele2.click();
  				                   ele2.sendKeys("kumar");
  			                   
  			                   
  				                   
  				          WebElement ele3 = driver.findElement(By.xpath(Pom.FootnoteSaveMenu()));      
  			      		  		MoveToElement.byXpath(driver, Pom.FootnoteSaveMenu());
  									 ele3.click();
  					                 
  					      
  								UpperToolBar obj1 = new UpperToolBar(driver); 
  				 			        	obj1.save_btn_method();
  					                   
  					                   
  					      }
  			   
  			   
  			   try{
  			 		
  			              switc.SwitchCase(uAgent);
  				
  				 WaitFor.presenceOfElementByXpath(driver, Pom.PPLClickOnEle_Add_Footnote());
  					
  	             WebElement ele1 = driver.findElement(By.xpath(Pom.PPLClickOnEle_Add_Footnote()));
  	             
  	             WebElement ele2 = driver.findElement(By.xpath("//*[@id='tfn8_NaN']"));
  		     
  	             String actualValue =  ele1.getAttribute("innerHTML");
  	 			 String expectedValue = "Preetish</a>";
  	 			
  	 			
  	 			String actualValue1 =  ele2.getAttribute("innerHTML");
  				 String expectedValue1 = "Preetish</span>";
  	 				
  	 	System.out.println("After save citation- " + actualValue +"\n"+ "Footnote- "+actualValue1);
  	 						   
  	 						   
  				
  				if(actualValue.contains(expectedValue)==true && actualValue1.contains(expectedValue1)){
  				
  					status ="Pass";
  				
  		 }else{
  					
  			 Thread.sleep(10000);  
  					   
  			    	  status ="Fail"; 
  			    	  remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	  
  			    //((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
  							
  							utilitys.Getscreenshot.captureScreenShot(uAgent,className,driver);
  				   
  		             
  				
  				 if(actualValue.contains(expectedValue)==true){
  				 	   System.out.println("E1: No issues");
  				 	
  				 	}
  				  
  				  if(actualValue1.contains(expectedValue1)==true){
  				 	   System.out.println("E2: No issues");
  				 	  
  				 	  }
  		           }
  		        }catch (Exception e){
  			          e.getStackTrace();
  		          }finally{	  
  			    System.out.println(className);
  		Ex.testdata(description, className, remark, category, area, status, uAgent);
  		         }
  		  }

	  

	  

	 
	 
	 
	 @Test(alwaysRun = true)

	 public void  PPLTableFootnoteTest7() throws Throwable {
	 	
	 	 try {


	 PPL_TableFootnote07 obj = new PPL_TableFootnote07();
	 		    obj.TableFootNote();
	 	
	 			       
	 		} catch (Exception e) {	
	 			e.printStackTrace();
	 		     }
	       }	
	 }

