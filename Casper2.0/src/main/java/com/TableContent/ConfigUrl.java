package com.TableContent;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigUrl {

static Properties pro;
	
	

public static void ReadPath(String PathofXML)
{
	
	String sourcePath = PathofXML;
	
	File src=new File(sourcePath);
	
	try 
	{
		FileInputStream fis=new FileInputStream(src);
		
		 pro=new Properties();
		
		   pro.load(fis);
		
	} catch (Exception e) 
	{
		System.out.println("Exception is "+e.getMessage());
	}
	
   }


            /**************************/
                    //Author
            /**************************/


public String geturlForFireFox()
{
	
	
ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\FireFoxUrlGenrationForTableContain_Footnote.properties");

	String Table2_Aspirations=pro.getProperty("FireFoxTableContainUrl");
	return Table2_Aspirations;
	
	
}
		


public String geturlForChrome()
{
	
ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\ChromeUrlGentrationForTableContain_Footnote.properties");

	String Table2_Aspirations=pro.getProperty("ChromeTableContainUrl");
	return Table2_Aspirations;
}

	

public String geturlForOpera()
{  
	
	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\OperaUrlGenrationForTableContain_Footnote.properties");

	String Table2_Aspirations=pro.getProperty("OperaTableContainUrl");
	return Table2_Aspirations;


}

public String geturlForEdge()
{                                                             

	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\EdgeTableContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("EdgeTableContainUrl");
	return Table2_Aspirations;
		
}



public String geturlForIE()
{                                                             

	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\IETableContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("IETableContainUrl");
	return Table2_Aspirations;
		
}


                     /**************************/
                             //Editor
                     /**************************/


public String geturlForIEPE()
{                                                             

	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\IEPETableContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("IEPETableContainUrl");
	return Table2_Aspirations;
		
}



public String geturlForOperaPE()
{  
	
	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\OperaUrlGenrationForPETableContain_Footnote.properties");

	String Table2_Aspirations=pro.getProperty("OperaPETableContainUrl");
	return Table2_Aspirations;


}


public String geturlForEdgePE()
{                                                             

	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\EdgePETableContainUrl.properties");

	String Table2_Aspirations=pro.getProperty("EdgePETableContainUrl");
	return Table2_Aspirations;
	
}



public String geturlForChromePE()
{
	
	ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\ChromeUrlGenrationForPETableContain_Footnote.properties");

	
	String Table2_Aspirations=pro.getProperty("ChromePETableContainUrl");
	return Table2_Aspirations;	
	
}



public String geturlForFireFoxPE()
{
	
ConfigUrl.ReadPath("./Configfolder\\UrlTableContain_Footnote\\FireForUrlGenrationForPETableContainFootnote.properties");

	
	String Table2_Aspirations=pro.getProperty("FireFoxPETableContainUrl");
	return Table2_Aspirations;

    }

/**************************************************************************************/
                              /***Editor AuthorName***/
/**************************************************************************************/


public String getAuthorNameUrlForChromePE()
{
	
ConfigUrl.ReadPath("./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_Chrome.properties");

	
	String Table2_Aspirations=pro.getProperty("ChromePEAuthorNameUrl");
	return Table2_Aspirations;

    }


public String getAuthorNameUrlForFireFoxPE()
{
	
ConfigUrl.ReadPath("./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_FireFox.properties");

	
	String Table2_Aspirations=pro.getProperty("FireFoxPEAuthorNameUrl");
	return Table2_Aspirations;
}


public String getAuthorNameUrlForIEPE()
{
	
ConfigUrl.ReadPath("./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_IE.properties");

	
	String Table2_Aspirations=pro.getProperty("IEPEAuthorNameUrl");
	return Table2_Aspirations;
}

public String getAuthorNameUrlForOperaPE()

{
	
ConfigUrl.ReadPath("./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_Opera.properties");

	
	String Table2_Aspirations=pro.getProperty("OperaPEAuthorNameUrl");
	return Table2_Aspirations;
}


public String getAuthorNameUrlForEdgePE()

{
	
ConfigUrl.ReadPath("./Configfolder\\AuthorNameUrl\\AuthorNameCommentUrl_Edge.properties");

	
	String Table2_Aspirations=pro.getProperty("EdgePEAuthorNameUrl");
	return Table2_Aspirations;
}

}