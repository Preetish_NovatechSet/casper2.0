package com.TableContent;


import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableContain08 extends PPL_BaseClass {
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;


    
    
	/*
	 * Multiple cell select and use delete key for delete
	 */

    
	
	
	public void select_multlple_CellvalueAndDelete() throws InterruptedException, IOException{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableContain-->Select Multiple cell word and use delete key, data lose track should be present";
		   className = "PPL_TableContain08";    
		        area = "Table Contain";
		            category = "Select multiple cell and delete";
		           
		         PomObj = new Pom();
		       
		     
		         Cookies cokies =new Cookies(driver);
		             cokies.cookies(); 
		         
		         Switch switc = new Switch(driver);
		            switc.SwitchCase(uAgent);
		 try {
			 
			 
			WaitFor.presenceOfElementByXpath(driver, PomObj.PPLTableContain7());
				
			WebElement ele= driver.findElement(By.xpath(PomObj.PPLTableContain7()));
			
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	    ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
	    
	 
	    MoveToElement.byXpath(driver, PomObj.PPLTableContain7());
	    
	             MoveToElement.byclick(driver, ele);
	             
	             MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	    
	   for(int i=0;i<2;i++) {
	      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
	    }
	   
	   
	   Actions action = new Actions(driver);
	   action.keyDown(Keys.SHIFT).build().perform();
	
	   for(int i=0;i<2;i++) {
		      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
		    }
	   
	   
	   
	   
	   
	   WaitFor.presenceOfElementByXpath(driver, PomObj.PPLTableContain8());
		
		WebElement ele1= driver.findElement(By.xpath(PomObj.PPLTableContain8()));
		
		  MoveToElement.byXpath(driver, PomObj.PPLTableContain8());
	    
                   MoveToElement.byclick(driver, ele1);
        
        MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.END);
        
               action.keyUp(Keys.SHIFT).build().perform();
        
        
               MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.DELETE);               
	         
	    	        
     UpperToolBar obj = new UpperToolBar(driver);
                 obj.save_btn_method();	 
		 
		    
			}catch (Exception e) {
			
			e.printStackTrace();
		  
		      }
		 
		 
		 
		 
		 try {			
				
				
				
				Thread.sleep(3000);
				
			       switc.SwitchCase(uAgent);
											
						
			WaitFor.presenceOfElementByXpath(driver,PomObj.PPLTableContain7());
			
		WebElement ele1 = driver.findElement(By.xpath(PomObj.PPLTableContain7()));
		
		WaitFor.presenceOfElementByXpath(driver,PomObj.PPLTableContain8());
		
		WebElement ele2 = driver.findElement(By.xpath(PomObj.PPLTableContain8()));
					 
		String actualValue =  ele1.getAttribute("innerHTML");
		String actualValue1 =  ele2.getAttribute("innerHTML");
		String expectedValue1 = "56</del>";
		String expectedValue2 = "ice-del ice-cts";
		String expectedValue3 = "15</del>";	
		String expectedValue4 = "ice-del ice-cts";
						 
						 System.out.println("After save - " + actualValue);
							
							
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue1.contains(expectedValue3)==true && actualValue1.contains(expectedValue4)==true){
							
	status="Pass";
						}else{
						   				    	  
						    	  Thread.sleep(10000);  
						    	  
	status ="Fail"; 
								 
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";			
			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
						   
						 }
				 if(actualValue.contains(expectedValue1)==true) {
						System.out.println("E1: No issues");
							 	}
							  
					if(actualValue.contains(expectedValue2)==true){
						System.out.println("E2: No issues");
							 	  
							 	 }
							   
					if(actualValue1.contains(expectedValue3)==true){
							  System.out.println("E3: No issues");

							     }
							 	  
					if(actualValue1.contains(expectedValue4)==true){
					     System.out.println("E4: No issues");

								  }
			              }catch (Exception e){
				
				e.printStackTrace();
			          }finally {
			        	  
					     	System.out.println(className);
			Ex.testdata(description,className,remark,category,area,status,uAgent);
					  
				   }
		        } 
		
	

	@Test(alwaysRun=true)
	
public static void Test8() throws Exception {
			
	try {		        
			    
        PPL_TableContain08 obj = new PPL_TableContain08();
		          obj.select_multlple_CellvalueAndDelete();
	       
	} catch (Exception e) {
		e.printStackTrace();
	  }finally {
		  
		  System.out.println(className);			
	Ex.testdata(description, className, remark, category, area, status, uAgent);
	  }
   }
}