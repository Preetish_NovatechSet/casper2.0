package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableCaption07 extends PPL_BaseClass{


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    
    Pom PomObj;
    
	
	
	/*****
	 *7. Single word select and use delete key for delete, 
	 *      check tracking and data lose
	 * @throws Exception 
	 *****/




	public  void selectSingle_word_delete() throws Exception {
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	   description = "Casper-PPL_CopyEditor-TableCaption-->Single word select and use delete key for delete, check tracking and data lose";
		   className = "PPL_TableCaption07";    
		        area = "Table Caption";
		           category = "Select and delete";
		                
		   

                        System.out.println("BrowerName->"+uAgent);
			   
                        Cookies cokies =new Cookies(driver);
   		             cokies.cookies();
   		              
   		           Switch switc = new Switch(driver);
   		              switc.SwitchCase(uAgent);        
                        
   		           PomObj = new Pom();  
   		           
      try {  
	   
		WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2_Caption());
		
       WebElement ele=driver.findElement(By.xpath(PomObj.Table_2_Caption()));
       driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
       
 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
       String value = ele.getText();
	
       
			if(value.contains(value)){
				
MoveToElement.byXpath(driver, PomObj.Table_2_Caption());	
				 
	  WaitFor.clickableOfElementByXpath(driver, PomObj.Table_2_Caption());
				
		        ele.click();
							      					         
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);					      

	  for(int i=0;i<65;i++){
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
			}
	
			
	MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 9, Keys.ARROW_RIGHT);
			
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);													
			
	    UpperToolBar ob = new UpperToolBar(driver);
	 	  	   ob.save_btn_method();			 	  	   		 	 
			}
		}catch (Exception e) {
			e.printStackTrace();
				}

					
  try
     {
	   Thread.sleep(3000);
		
             switc.SwitchCase(uAgent);   					
	   
  	WaitFor.presenceOfElementByXpath(driver, PomObj.Table_2_Caption());
                 
  	WebElement ele1=driver.findElement(By.xpath(PomObj.Table_2_Caption()));
				
   String afterSave =  ele1.getAttribute("innerHTML");
	 
	String expectedValue1 = "Secondary</del>";
	String expectedValue2 = "ice-del ice-cts";

System.out.println("After save - " + afterSave );

if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true){
		 
		status="Pass";
		 	
		 }else {
	
//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
	 		 	              
status = "Fail";

remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
  
Thread.sleep(12000);

	 	utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		
		 	if(afterSave .contains(expectedValue1)==true) {
		 	   System.out.println("E1: No issues");
		 	}
		    if(afterSave .contains(expectedValue2)==true){
		 	   System.out.println("E2: No issues");
		 	}
	     }
   }catch (Exception e) {
         	e.getStackTrace();
      }finally{
   	   System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
  }
}

	
	@Test(alwaysRun = true)
	public void  PPLTableCaptionTest7() throws Throwable {
		  
		 try {
			 	
			PPL_TableCaption07 obj = new PPL_TableCaption07();
	                obj.selectSingle_word_delete();			
	   	                
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
	    }
   }
