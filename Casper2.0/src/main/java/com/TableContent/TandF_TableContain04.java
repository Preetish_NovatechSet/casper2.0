package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;



/***************
 *  Preetish  *
 ***************/

public class TandF_TableContain04 extends TandFBaseClass {

	

	static String remark;
	static String className = "TandF_TableContain04"; 
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;
	Pom PomObj;


	
	
	/**
	 * Insert special character and place cursor to next character then press backspace key, tracking should be present
	 **/
	

	

	public void Click_on_MathReading() throws Exception{
		
			

	 try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
  description = "Casper-TandF_CopyEditor-TableContain-->Insert special character and place cursor to next character then press backspace key, tracking should be present";   
        		    area = "Table Contain";
		                  category = "Insert and Delete";
				         
				       
				       
				       Cookies cokies =new Cookies(driver);
				          cokies.cookies();
				       
				    Switch switc = new Switch(driver);
				       switc.SwitchCase(uAgent);       
				          
				       
				           
				          PomObj = new Pom();	    
				          
				          
				          System.out.println(uAgent);
				          
				      
		  WaitFor.presenceOfElementByXpath(driver, PomObj.Table2_Math_Reading());
				
		  WebElement ele=driver.findElement(By.xpath(PomObj.Table2_Math_Reading())); 		
		 
		     driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
					
					
				String value = ele.getText();
				//	System.out.println("total element- " + value);
		      if(value.contains(value)){
						
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
      ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				 	
			MoveToElement.byXpath(driver,PomObj.Table2_Math_Reading());
						
			WaitFor.clickableOfElementByXpath(driver, PomObj.Table2_Math_Reading());
						   MoveToElement.byclick(driver, ele);
		               
						    switch (uAgent) {   
         					case "edge":
       
         	    	                MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
         	    	                
         	              	     break;  
						    }
						    
						    
						MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.HOME);
			
						for(int i =0;i<3;i++){
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
			}
						
				 SymbolActionFuction obj = new SymbolActionFuction(driver);
		                             obj.SymbolTest();
		                        
		                           
		                  	       switc.SwitchCase(uAgent);        
		                             
		                             
		                  switch (uAgent) {   
		         					case "edge":
		         					
		         MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);	
		         						 
		         	              	    break;  
		         				    
		         						
		         			}
		                  
		                  
		               WaitFor.presenceOfElementByXpath(driver,PomObj.Table2_Math_Reading());        
		            WebElement ele0=driver.findElement(By.xpath(PomObj.Table2_Math_Reading())); 
		                            
		                      
		         MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.ARROW_RIGHT);	
		                          
		                          
		         WaitFor.presenceOfElementByXpath(driver,PomObj.Table2_Math_Reading());        
		      WebElement ele1=driver.findElement(By.xpath(PomObj.Table2_Math_Reading())); 
			
		          MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
					  	
					  	
			
			
					  UpperToolBar obj1 = new  UpperToolBar(driver);
				         	  obj1.save_btn_method();
				
						
					   }
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
          
          
	 try {			
						
			Switch switc = new Switch(driver);
		       switc.SwitchCase(uAgent);	
          
    WaitFor.presenceOfElementByXpath(driver,PomObj.Table2_Math_Reading());
   
	WebElement ele1=driver.findElement(By.xpath(PomObj.Table2_Math_Reading()));	
	
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "M</del>";
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue4="ice-ins ice-cts";
				 System.out.println("After save - " + actualValue);
			
			
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
			 	
			 	status="Pass";
			        
      }else{
    	  //((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);  
    	    	  
                status ="Fail"; 
                               
                Thread.sleep(10000);  
			
	 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	 			   
remark="Extra Char or Symbol may has come/ Delete or Trac is missing/ Insert or Delete colour is missing";
			
			
			   if(actualValue.contains(expectedValue1)==true){
			 	   System.out.println("E1: No issues");
			 	
			 	}
			  
			 	    if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  
			 	}
			   
			 	    if(actualValue.contains(expectedValue3)==true) {
			 	   System.out.println("E3: No issues");

			                }
			 	   if(actualValue.contains(expectedValue4)==true) {
				 	   System.out.println("E4: No issues");

				                }
			          }
		} catch (Exception e) {
			e.printStackTrace();
		      }finally{
		    	   
		    	    System.out.println(className);
		    	    
			   Ex.testdata(description,className,remark,category,area,status,uAgent);
			   
			  
		        }
	         }	 
	
	
	@Test(alwaysRun = true)

	public void  Test4() throws Throwable {
try {		 
			 
	TandF_TableContain04 obj = new TandF_TableContain04();
		       obj.Click_on_MathReading();
		
		
		 } catch (Exception e) {
				e.printStackTrace();
			     }
            }
     }