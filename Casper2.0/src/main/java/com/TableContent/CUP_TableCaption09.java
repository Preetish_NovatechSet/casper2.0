package com.TableContent;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_TableCaption09 extends CUP_BaseClass{

	
	static String remark;
	static String className = "CUP_TableCaption09";    
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;	
    Pom PomObj;
	
    
    
	/*****
	 **9. Single word select and "Control + C" then "Control + v" in next next word, check tracking and data lose
	 * @throws Exception 
	 *****/
	
	
    
	public void SelectSingle_Word_CopyPaste_nxtword() throws Exception {
		
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_CopyEditor-TableCaption-->Single word select and Control + C then Control + v in next word, check tracking and data lose";		  
		     area = "Table Caption";
		           category = "Copy and Paste";
		                   
	

		     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);   
		    	                   
				    
                 System.out.println("BrowerName->"+uAgent);	
		    	 
                 Cookies cokies =new Cookies(driver);
                         cokies.cookies();
                  
               Switch switc = new Switch(driver);
                     switc.SwitchCase(uAgent); 
                        
                  PomObj = new Pom();  
               
                  try {  
 
  		 WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
  		 		
  		WebElement ele=driver.findElement(By.xpath(PomObj.CasperTable2()));
	      			

					
String value=ele.getText();
					
		if(value.contains(value)) {
						
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	
			MoveToElement.byXpath(driver, PomObj.CasperTable2());	

			
			WaitFor.clickableOfElementByXpath(driver, PomObj.CasperTable2());
				    	
			
						ele.click();
						
						
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
						
				    	
				    		 for(int i =0;i<44;i++) {
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				    		 }
										
				
		MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 10,Keys.ARROW_RIGHT);
					
		
		if(uAgent.equals("IE")){	
			 
			
			 Robot robot = new Robot();
		     robot.keyPress(KeyEvent.VK_CONTROL);
		     robot.keyPress(KeyEvent.VK_C); 
		     robot.keyRelease(KeyEvent.VK_C); 
		     robot.keyRelease(KeyEvent.VK_CONTROL); 
		
		     
		}else{
			
			   MoveToElement.copy(driver);
			
		}
						     
						 for(int i =0;i<12;i++) {
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);  
						    }
						    
						 
						 if(uAgent.equals("IE")){	
							 
	((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");			
				core.HandleAlert.isAlertPresentAccept(driver);				
						
						 }else{    
							 
	          MoveToElement.paste(driver);
	          
				}
 
					  UpperToolBar ob = new UpperToolBar(driver);  
					           ob.save_btn_method();
					                   
					    
	             }
	      }catch(Exception e){
	    	 
	    e.printStackTrace();
	 }    	 

                  
try{

Thread.sleep(3000);

   switc.SwitchCase(uAgent); 


			 WaitFor.presenceOfElementByXpath(driver, PomObj.CasperTable2());
			 		
			WebElement ele1=driver.findElement(By.xpath(PomObj.CasperTable2()));
		
		
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "deposition</ins>";
				 String expectedValue2 = "ice-ins ice-cts";			 

		 System.out.println("After save - " + actualValue);


if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true){
	
	status="Pass";
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo);
	 	    
	    		      	      
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	 
	 	    
	 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		
		    if(actualValue.contains(expectedValue1)==true) {
		 	   System.out.println("E1: No issues");
		 	}
		    if(actualValue.contains(expectedValue2)==true){
		 	   System.out.println("E2: No issues");
		 	     }	 
		     }
     }catch(Exception e){
	 		
			    e.getStackTrace();
			    
		      }finally {
		    	  System.out.println(className);
   Ex.testdata(description, className, remark, category, area, status, uAgent);
        }
	 }
	
	
	
	
	@Test(alwaysRun = true)

	public void  CUPTableCaptionTest09() throws Throwable {
		  
		 try {
			 MyScreenRecorder.startRecording(uAgent,className);
			 
			    CUP_TableCaption09 obj = new CUP_TableCaption09();
	                obj.SelectSingle_Word_CopyPaste_nxtword();
			
	   
	                
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
	    }
  }