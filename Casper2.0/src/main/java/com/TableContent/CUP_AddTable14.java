package com.TableContent;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class CUP_AddTable14 extends CUP_BaseClass{
	
	
	
	 static WebElement ele =null;
	 static String remark;
	 static String className;
	 static String category;
	 static String area;
	 static String description;
	 static String status;
	 public static Excel Ex;
	 Pom PomObj;
	   
	
	    
	    
	  
		
/**
* Delete one character with Back space key press and insert special character, tracking should be present
**/
	    
	public void Click_on_Aspirations() throws Throwable{
											
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-CUP_AuthorEnd-AddTable-->Go to Added Table right click then click on table properties, click on header then select both and check whether first row , first column has added or not.";
		 className = "CUP_AddTable14";    
		         area = "Add table";
		               category = "Header add both";
		              
	                    		                      		                                     
	                     
			      System.out.println("BrowerName->"+uAgent);                 
	           
			              
			          Cookies cokies =new Cookies(driver);
				             cokies.cookies();
				              
				       Switch switc = new Switch(driver);
				              switc.SwitchCase(uAgent);    				              
	        
	PomObj = new Pom();   
			                     

	WaitFor.presenceOfElementByXpath(driver, PomObj.tr1());
		
	   ele= driver.findElement(By.xpath(PomObj.tr1()));
		   driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
		 
			
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
((JavascriptExecutor)driver).executeScript("window.scrollBy(250,450)");
MoveToElement.byXpath(driver, PomObj.tr1());	
	
        WaitFor.clickableOfElementByXpath(driver, PomObj.tr1());
			          	            
			          	     ele.click();
	
			          	 
            Actions action=new Actions(driver);
	                    Thread.sleep(2000);        
	   
			  action.contextClick(ele).build().perform();
	         	
	 
			  
			  Robot robot = new Robot();
	           	 robot.delay(1);
	             for(int i=0;i<4;i++) {
	           	   Thread.sleep(4000);          	     
	           	 robot.keyPress(KeyEvent.VK_DOWN);
	             }
	             robot.keyRelease(KeyEvent.VK_DOWN);
	             
	             
	             
	             Robot robot1 = new Robot();
	           	 robot1.delay(1);
	             Thread.sleep(5000);
	             robot1.keyPress(KeyEvent.VK_ENTER);
	             Thread.sleep(4000);
	             
	             
	             driver.switchTo().defaultContent();
	             
	         WaitFor.presenceOfElementByXpath(driver, PomObj.Headers());
	     		
	         Select dropdown = new Select(driver.findElement(By.xpath(PomObj.Headers())));
	             
	                   dropdown.selectByVisibleText("Both");
	                   
	                   
	                 ele=driver.findElement(By.xpath(PomObj.Submit()));
	                    MoveToElement.byXpath(driver, PomObj.Submit());			 
	               		    MoveToElement.byclick(driver, ele);
	            
	               		    
	             UpperToolBar ob = new UpperToolBar(driver);
	      		  	    ob.save_btn_method();
	      		  	    
	      		  	
	      		  	    
	      		  	    
	      		  	switc.SwitchCase(uAgent); 
	      		  	Thread.sleep(3000);
	      		  	    
	      	  WaitFor.presenceOfElementByXpath(driver, PomObj.HeaderfirstColumn());
			     		
	 	 List<WebElement> ele= driver.findElements(By.xpath(PomObj.HeaderfirstColumn()));	
	 	         int count =ele.size();
	 	         
	 	         System.out.println("ActualColumn-->"+count);
	 	         
	 	         
	 	         
	 	        
     	  WaitFor.presenceOfElementByXpath(driver, PomObj.Headerfirstrow());
		     		
	 List<WebElement> ele1= driver.findElements(By.xpath(PomObj.Headerfirstrow()));	
	         int count1 =ele1.size();
	         
	         System.out.println("ActualRow-->"+count1); 
	 	         
	 	         
	 	            int Expectedvalue1=4;
	 	            int Expectedvalue2=6;
	 	            
	 	            
	 	         try {
					if(count==Expectedvalue1==true && count1==Expectedvalue2==true){
						 status="Pass";
					 }else {
						 status="Fail";
						 remark= "Header unable to add Both";
						 
						 
						 Thread.sleep(10000);  		
				 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
					 }
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	 	                     
	     }
				    		 

			
@Test(alwaysRun = true)
	
	public void  CUPAddTableTest14() throws Throwable {
		  
	try {
	
			 CUP_AddTable14 obj = new CUP_AddTable14();
           obj.Click_on_Aspirations();
				                      
		}catch (Exception e){
			
			e.printStackTrace();
		     
		      }finally{
					System.out.println(className);
		Ex.testdata(description, className, remark, category, area, status, uAgent);
							}
                    }
	         }