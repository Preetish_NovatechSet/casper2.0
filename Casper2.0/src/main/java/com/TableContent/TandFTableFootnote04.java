package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;



public class TandFTableFootnote04 extends TandFBaseClass{
	
	
	
	/**
	 * Insert special character and place cursor to next character then press backspace key,
	 *  tracking should be present
	 **/
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
   
   
    Pom PomObj;
	

	/*public TandF_PE_Footnote04(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	
	
	
	
	public void insertSplChar_PlaceCursor2nxtChrThenPressBckspce() throws Exception {
		
	
try {
	
	Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	   description = "Casper-TandF_CopyEditor-TableFootNote-->Insert special character and place cursor to next character then press backspace key, tracking should be present";
	      className = "TandFTableFootnote04";    
	         area = "TableFootnote";
	           category = "Insert and Delete";
                   PomObj=new Pom(); 
                   
                   
                   Cookies cokies =new Cookies(driver);
  	                  cokies.cookies();
  	              
  	           Switch switc = new Switch(driver);
  	              switc.SwitchCase(uAgent);
             
  	            WaitFor.presenceOfElementByXpath(driver, Pom.Table3_Footnote());
  				WebElement ele = driver.findElement(By.xpath(Pom.Table3_Footnote()));
  					
  				String value = ele.getText();
  				
  		if(value.contains(value)) {

  ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
        ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
  MoveToElement.byXpath(driver, Pom.Table3_Footnote());
  					ele.click();
  				        
  				
  					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
  					
  					for(int i =0;i<66;i++) {
  					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
  					}
  				
  					
  					SymbolActionFuction obj = new SymbolActionFuction(driver);
  	                     obj.SymbolTest();
  				  
  	                     switc.SwitchCase(uAgent);          	                  
  	       WebElement ele1 = driver.findElement(By.xpath(Pom.Table3_Footnote()));
  				    
  				    for(int i =0;i<1;i++) {
  			MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_RIGHT);
  						}
  				    
  			MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
  				    
  				UpperToolBar ob = new  UpperToolBar(driver);
  					ob.save_btn_method();
  	
  					    
  				          } 
  		
  		
  		
  		Thread.sleep(4000);
  	
             switc.SwitchCase(uAgent);
  		
  		
  		WaitFor.presenceOfElementByXpath(driver, Pom.Table3_Footnote());
  	WebElement ele1 = driver.findElement(By.xpath(Pom.Table3_Footnote()));
  				    
  String actualValue =  ele1.getAttribute("innerHTML");

  String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
  String expectedValue2 = "ice-del ice-cts";
  String expectedValue3 = "c</del>";
  String expectedValue4="ice-ins ice-cts";
  	
  			   System.out.println("After save - " + actualValue);
  	
  	
  	if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true && actualValue.contains(expectedValue4)==true){
  					
  					status="Pass";
  				        
  				 }else{
  				 
  					status="Fail";
  		//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
  	 
  		remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
  				
  		     utilitys.Getscreenshot.captureScreenShot(uAgent,className, driver);
  											
  					
  				     if(actualValue.contains(expectedValue1)==true) {
  					   System.out.println("E1: No issues");
  					
  					}
  				 
  					 if(actualValue.contains(expectedValue2)==true){
  					   System.out.println("E2: No issues");
  					  
  					}
  				  
  					    if(actualValue.contains(expectedValue3)==true) {
  					   System.out.println("E3: No issues");
  					   
  					   if(actualValue.contains(expectedValue3)==true) {
  						   System.out.println("E3: No issues");
  					             }
  				              }
  				 }}catch (Exception e) {
          	e.printStackTrace();
                      }finally{
                   System.out.println(className);
                 Ex.testdata(description, className, remark, category, area, status, uAgent);     	   
                       }
  			    }
	
	
	
    @Test(alwaysRun = true)
	
	public void  Test4() throws Throwable {
		
		 try {
			 
				
				TandFTableFootnote04 obj = new TandFTableFootnote04();
			    obj.insertSplChar_PlaceCursor2nxtChrThenPressBckspce();
		
				       
			} catch (Exception e) {	
				e.printStackTrace();
			     }finally{
	                 System.out.println(className);
	                 Ex.testdata(description, className, remark, category, area, status, uAgent);     	   
	                       }
	          }	
	      }
	       

