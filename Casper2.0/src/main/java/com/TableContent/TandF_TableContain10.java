package com.TableContent;



import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


/***************
 ***Preetish***
 ***************/


public class TandF_TableContain10 extends TandFBaseClass {

	
	static String remark;
	static String className = "TandF_TableContain10";
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;
	
	
	/**
	 * Single cell select and use backspace key for delete, check tracking and data lose
	 * */
	
		
	public void SelectSingleCell_pressBckspce() throws Exception {
	
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-TandF_CopyEditor-TableContain-->Single cell select and use backspace key for delete, check tracking and data lose";	       
	         area = "Table Contain";
	             category = "Select and Delete";
	          	         
	            PomObj = new Pom();
	            
	            Cookies cokies =new Cookies(driver);
	             cokies.cookies(); 
	         
	         Switch switc = new Switch(driver);
	            switc.SwitchCase(uAgent);
	try {
		
WaitFor.presenceOfElementByXpath(driver,PomObj.Table2_select_cellTable());
		WebElement ele= driver.findElement(By.xpath(PomObj.Table2_select_cellTable()));
		
		 driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);	
	  
				
		String value = ele.getText();
	System.out.println("Element name -"+value);
				
				
			if(value.contains(value)){
			
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");			
     MoveToElement.byXpath(driver, PomObj.Table2_select_cellTable());
		
          WaitFor.clickableOfElementByXpath(driver, PomObj.Table2_select_cellTable());
					
				ele.click();
												 				
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
					
		MoveToElement.selectsingleelement(driver, Keys.BACK_SPACE);
					
				UpperToolBar o = new  UpperToolBar(driver);
			           o.save_btn_method();
			           
			         
					}	 
		     
			Thread.sleep(3000);
			
			switc.SwitchCase(uAgent);
			
	WaitFor.presenceOfElementByXpath(driver, PomObj.Table2_select_cellTable());
			
	   WebElement ele1= driver.findElement(By.xpath(PomObj.Table2_select_cellTable()));
			
			String actualValue =  ele1.getAttribute("innerHTML"); 
			String expectedValue1 = "Ongoing</del>";
	 
                    
		        	System.out.println("After save - " + actualValue);
	        	

  if(actualValue.contains(expectedValue1)==true){
	  
		        		   status="Pass";
		        		   
		        	}else{
		        		
		        		   Thread.sleep(10000);  
		        	       
		      	          status ="Fail"; 
		      	          
//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
		  
 remark="Extra Char or Symbol may has come/ Insert or Delete trac is missing/ Insert or Delete colour is missing";   	

                 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		     			
		     			
		     			if(actualValue.contains(expectedValue1)==true) {
		    				System.out.println("E1: No issues");
		    			
	                        }
		        	    }
		           } catch (Exception e) {
		
		e.printStackTrace();
	           }finally{
	        	   System.out.println(className);
			   Ex.testdata(description,className,remark,category,area,status,uAgent);
	           }
            }
	


	@Test(alwaysRun=true)
	public static void Test10() throws Exception {
	
		
		
		 try {
			
			
		     TandF_TableContain10 obj = new TandF_TableContain10();
                        obj.SelectSingleCell_pressBckspce();
                             
		      } catch (Exception e) {
				e.printStackTrace();
			     }
         	} 
       }
        

