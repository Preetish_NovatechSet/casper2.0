package com.TableContent;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;



public class TandF_TableCaption13 extends TandFBaseClass {

	
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    Pom PomObj;
	

  
	
	
	/*
	 * Insert one character and press delete key, tracking should be present
	 */

	

	public void insert1Char_pressDelKey() throws Exception {
		

Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-TandF_CopyEditor-TableCaption-->Insert one character and press delete key, tracking should be present";
		 className = "TandF_TableCaption13";    
		       area = "Table Caption";
		           category = "Insert and Delete";
		               
	 System.out.println("BrowerName->"+uAgent);
			  
		  Cookies cokies =new Cookies(driver);
          cokies.cookies();
       
    Switch switc = new Switch(driver);
       switc.SwitchCase(uAgent);
       
       PomObj = new Pom();  
	   	try {  	 
		  
         WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
			
	     WebElement ele=driver.findElement(By.xpath(PomObj.TableCaption3()));
		
	     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
	     
						String value = ele.getText();
					
			if(value.contains(value)){
					
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				
	MoveToElement.byXpath(driver, PomObj.TableCaption3());	

	WaitFor.clickableOfElementByXpath(driver, PomObj.TableCaption3());
					
					ele.click();   
	                       
						
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
			

		for(int i=0;i<95;i++){
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
						}
			      
					
					
		MoveToElement.bysendkeyWithoutclick(driver, ele, "L");
					 
					     MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
					
				UpperToolBar ob = new UpperToolBar(driver);
    					ob.save_btn_method();
    					
    					
    		//HandleAlert.isAlertPresentAccept(driver);
			
		      }}catch (Exception e) {
		    	e.printStackTrace();
	              }
		      
	  try {
		  
		  Thread.sleep(3000);
      
      switc.SwitchCase(uAgent);
		  
			WaitFor.presenceOfElementByXpath(driver, PomObj.TableCaption3());
			
		      WebElement ele1=driver.findElement(By.xpath(PomObj.TableCaption3()));
			    
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "L</ins>";
				 String expectedValue2 = "e</del>";
				 String expectedValue3 = "ice-del ice-cts";
				 String expectedValue4="ice-del ice-cts";

				 System.out.println("After save - " + actualValue);

if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true&& actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
				   status="Pass";
				 					 	
				 }else {
					 Thread.sleep(10000);
					    					
//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		
					  
	 		 	     status = "Fail";
	 		remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
	 		 	     utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				 }
				 	
				 if(actualValue.contains(expectedValue1)==true) {
				 	 System.out.println("E1: No issues");
				 	}
				    if(actualValue.contains(expectedValue2)==true){
				 	 System.out.println("E2: No issues");
				 	    }
				       if(actualValue.contains(expectedValue3)==true){
					 System.out.println("E3: No issues");
					 	    }
				           if(actualValue.contains(expectedValue4)==true){
					 System.out.println("E4: No issues");
					 	    }
	                   
	           	         } catch (Exception e) {
	
	           	        	e.getStackTrace();
	           	          
              }finally {            	   
   	  System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);  	
        }
 
}
	

	@Test(alwaysRun = true)

	public void  Test13() throws Throwable {
		  
		 try {
		
		
			 TandF_TableCaption13 obj = new TandF_TableCaption13();
	                obj.insert1Char_pressDelKey();
			

	                
		} catch (Exception e){
			
			e.printStackTrace();
		     }
          }
	  }
