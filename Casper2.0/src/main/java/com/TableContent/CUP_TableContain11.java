package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;
import com.page.UpperToolBar;

import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;

public class CUP_TableContain11 extends CUP_BaseClass{

	static String remark;
	static String className = "CUP_TableContain11";  
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;
	
	
	/**
	 * Single cell select and "Control + C" then "Control + V" in next cell, check tracking and data lose
	 **/

	

public void copyCellAndPaste() throws Exception {

		try {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_CopyEditor-TableContain-->Single cell select and Control + C then Control + V in next cell, check tracking and data lose";
		  
		     area = "Table Contain";
		         category = "Copy and paste";
		             
		     
		PomObj = new Pom();
		
		
	Cookies cokies =new Cookies(driver);
        cokies.cookies(); 
    
    Switch switc = new Switch(driver);
       switc.SwitchCase(uAgent);
		   
		   WaitFor.presenceOfElementByXpath(driver,PomObj.CupCopyTable());   

		WebElement ele= driver.findElement(By.xpath(PomObj.CupCopyTable()));
		
				driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
						
		String value = ele.getText();
				
				//System.out.println(value);
				
				if(value.contains(value)) {
					
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);			
	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");		
        MoveToElement.byXpath(driver, PomObj.CupCopyTable());
       
WaitFor.clickableOfElementByXpath(driver, PomObj.CupCopyTable());
              ele.click();
              
            MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);  
              
        MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 1, Keys.ARROW_LEFT);
              
        MoveToElement.copy(driver); 			     
			     	     
			if(uAgent.equals("IE")) {
			    	 
			 
			    	 
			    	  for(int i=0; i<1;i++){
				 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT); 
						   }     	 
			
			    	  
			    	  Thread.sleep(1000); 
	   ((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");		
	           
					   core.HandleAlert.isAlertPresentAccept(driver);
			    	  
					    
			     }else{
			    	 
			    	 WaitFor.presenceOfElementByXpath(driver,PomObj.CUPTable10());
						
					 WebElement ele2=driver.findElement(By.xpath(PomObj.CUPTable10()));
			    	 
			    	 ele2.click();
			    	 
			     MoveToElement.paste(driver);
			        }     
			         
				  }	
			  
      	
		UpperToolBar ob = new UpperToolBar(driver);
				ob.save_btn_method();
				  // }
				
				switc.SwitchCase(uAgent);	
			
			 WaitFor.presenceOfElementByXpath(driver,PomObj.CUPTable10());
				
			 WebElement ele2=driver.findElement(By.xpath(PomObj.CUPTable10()));
			
			     
			     String actualValue =  ele2.getAttribute("innerHTML");
				 String expectedValue1 = "9</ins>"; 
				 String expectedValue2 = "ice-ins ice-cts";
				
			 
			System.out.println("After save - " + actualValue);
			
			
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true){
			 	
status="Pass";
	
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo);
	    		      	      
	    
	     		 
remark="Ctrl+C, Ctrl+v is Not working/ Insert colour is missing";	     			

         utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	         	  
			      if(actualValue.contains(expectedValue1)==true) {
			 	   System.out.println("E1: No issues");
			 	
			 	}
			  
			 	    if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  
			 	         }
			   	
			           }		
			      }catch(Exception e){
			
			e.printStackTrace();
		}finally{
	     	System.out.println(className);
	   Ex.testdata(description, className, remark, category, area, status, uAgent);
		     }
	     }



@Test(alwaysRun=true)

public static void CUPTableContainTest11() throws Exception {

	
	 try {
		 MyScreenRecorder.startRecording(uAgent,className); 

CUP_TableContain11 obj = new CUP_TableContain11();
       obj.copyCellAndPaste();
       

	} catch (Exception e) {
	
		e.printStackTrace();
	  }  
    }
}
