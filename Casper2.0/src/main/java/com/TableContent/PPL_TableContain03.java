package com.TableContent;



import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;



public class PPL_TableContain03 extends PPL_BaseClass{
	
	

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	Pom PomObj;
    public static Excel Ex;

	
	
	/**
	 * Insert special character and press delete key, tracking should be present 
	 **/



public void Click_on_Computer() throws Exception{
	

		
	try {
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableContain-->Insert special character and press delete key, tracking should be present";
		    className = "PPL_TableContain03";    
			        area = "Table Contain";
			             category = "Insert and Delete";
			    
			             System.out.println("BrowerName->"+uAgent);  
			           
			           Cookies cokies =new Cookies(driver);
				          cokies.cookies();
				       
				    Switch switc = new Switch(driver);
				       switc.SwitchCase(uAgent);  
				       
			           
			          PomObj = new Pom();	   
		
WaitFor.presenceOfElementByXpath(driver, PomObj.PPLTableContain3());
				
   WebElement ele= driver.findElement(By.xpath(PomObj.PPLTableContain3()));		
       
      driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);		
				String value = ele.getText();
				//	System.out.println("total element- " + value);

					if(value.contains(value)){
		
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				
						
		      MoveToElement.byXpath(driver,PomObj.PPLTableContain3());
		      
		      WaitFor.clickableOfElementByXpath(driver, PomObj.PPLTableContain3());
		      
		      for(int i=0;i<2;i++) {
						  MoveToElement.byclick(driver, ele);
						  Thread.sleep(500);
		      }
		      
						  switch (uAgent) {
					   	   
				          case "edge":
				   		  
				        	  for(int i=0;i<1;i++){
				   		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);  
				   		   }
				   		      break; 
				   	            }	
					
						  MoveToElement.sendkeybyinsidevalue(driver, ele,Keys.END);
						 
		 for(int i=0;i<1;i++) {
					 MoveToElement.sendkeybyinsidevalue(driver, ele,Keys.ARROW_LEFT);
				} 
						  
				 SymbolActionFuction obj = new SymbolActionFuction(driver);
			              obj.SymbolTest();
					  	
			              
			      // WaitFor.presenceOfElementByXpath(driver, PomObj.PPLTableContain3());
			      // WebElement ele0= driver.findElement(By.xpath(PomObj.PPLTableContain3()));	
			       
			              
			             
			   	       switc.SwitchCase(uAgent);  
			              
			       switch (uAgent) {   
					case "edge":
						
	              		for(int i=0;i<1;i++){
	    	                MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
	    	                }
	              	     break;  
							
			       }
					
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
						  
						  
				UpperToolBar obj1 = new UpperToolBar(driver);
						 obj1.save_btn_method();
						 
					}
	} catch (Exception e) {
				
		RunTheExceptionCase();
		   driver.quit();
}
			 
				
			
	try {			
				
	
		
		Thread.sleep(3000);
		
	    Switch switc = new Switch(driver);
	       switc.SwitchCase(uAgent);
				
				
				
	       WaitFor.presenceOfElementByXpath(driver,PomObj.PPLTableContain3());
	
		WebElement ele1 = driver.findElement(By.xpath(PomObj.PPLTableContain3()));
			 
				 String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = "3</del>";
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 =DataProviderFactory.getConfig().getSpecialCharValidation();	
				 String expectedValue4="ice-ins ice-cts";
				 
				 System.out.println("After save - " + actualValue);
					
					
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
					 status="Pass";
				}else{
				   				    	  
				    	  Thread.sleep(10000);  
				             status ="Fail"; 
						 
		remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";			
				   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				   
				 }
					      if(actualValue.contains(expectedValue1)==true) {
					 	   System.out.println("E1: No issues");
					 	}
					  
					 	    if(actualValue.contains(expectedValue2)==true){
					 	   System.out.println("E2: No issues");
					 	  
					 	 }
					   
					 	    if(actualValue.contains(expectedValue3)==true){
					 	   System.out.println("E3: No issues");

					     }
					 	  
					 	    if(actualValue.contains(expectedValue4)==true){
						 	   System.out.println("E4: No issues");

						 }
	              }catch (Exception e){
		
		e.printStackTrace();
	          }finally {
	        	  
			     	System.out.println(className);
	Ex.testdata(description,className,remark,category,area,status,uAgent);
			  
		       }
        }



public void RunTheExceptionCase() throws Exception {
	
	try {
		PPL_TableContain03 obj = new PPL_TableContain03();
		    obj.Click_on_Computer();
	      } catch (Exception e) {
	
		   e.printStackTrace();
	 }
}





@Test(alwaysRun = true)

public void  PPLTableContainTest3() throws Throwable {
	  
	 try {
			        
		 
	     PPL_TableContain03 obj = new PPL_TableContain03();
	           obj.Click_on_Computer();
	      

	 } catch (Exception e) {
			e.printStackTrace();
		       }
            }
	    }
     
