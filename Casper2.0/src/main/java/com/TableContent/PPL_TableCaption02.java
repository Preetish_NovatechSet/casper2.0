package com.TableContent;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;

public class PPL_TableCaption02 extends PPL_BaseClass{


	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;

	Pom PomObj;
	
	
	
	
	
	/**
	 * Delete one character with delete key press and insert special character, tracking should be present
	 **/
	
public void Click_on_Service() throws Exception{


Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableCaption-->Delete one character with delete key press and insert special character, tracking should be present";
			  className = "PPL_TableCaption02";    
			         area = "Table Caption";
			               category = "Delete insert Special Character";			                                                    
		                     
				              System.out.println("BrowerName->"+uAgent);                 		           
		        
				              Cookies cokies =new Cookies(driver);
					              cokies.cookies();
					              
					           Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);             
				              
				              
					              PomObj = new Pom();   
					          	try {                
				WaitFor.presenceOfElementByXpath(driver, PomObj.Table_1_Caption());         
					          			
			 WebElement ele= driver.findElement(By.xpath(PomObj.Table_1_Caption()));	
					  driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
					          		      
					          String value = ele.getText();
					          			
					       if(value.contains(value)){
					        
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
					          							
					          		
               MoveToElement.byXpath(driver, PomObj.Table_1_Caption());	
					          		 

            WaitFor.clickableOfElementByXpath(driver, PomObj.Table_1_Caption());		     
					          		            ele.click();
					          						
					          		            
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);

					          		  
					          				
					         for(int i=0;i<60;i++){
		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
					      }
					          				
				 WaitFor.visibilityOfElementByWebElement(driver, ele);
					          			      
					 MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.DELETE);
					          		 
					          		 
					  SymbolActionFuction obj = new SymbolActionFuction(driver);
					          		      obj.SymbolTest();
					          		 
					          		        			
					          	UpperToolBar obj1 = new UpperToolBar(driver); 
					          				 obj1.save_btn_method();					          					    					          					    						          					        	
        						     } 
					       }catch(Exception e){
					          							
	        					e.printStackTrace();	
					             }
						try {	
					          				
		        switc.SwitchCase(uAgent); 
					          											
				 WaitFor.presenceOfElementByXpath(driver, PomObj.Table_1_Caption());
				 WebElement ele1=driver.findElement(By.xpath(PomObj.Table_1_Caption()));
					          							     
		 String actualValue =  ele1.getAttribute("innerHTML");
		 String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
		 String expectedValue3 = "a</del>";
					          	
			 System.out.println("After save - " + actualValue);
				          					
					          					
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue3)==true){
					          					 	
		         	 status ="Pass";
		      					 
		           }else{
					          						  					          					          				             
		        	 status ="Fail"; 
					          									          				
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
					              					          		     
			          		      Thread.sleep(10000);  		
		      utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
					          		      
					          		    	   					          		      
		if(actualValue.contains(expectedValue1)==true){
					   	   System.out.println("E1: No issues");
					  	}
					          					  					          					   
					if(actualValue.contains(expectedValue3)==true) {
					   	   System.out.println("E3: No issues");

					  		                }
					  			
					  	              }
					  	        }catch(Exception e){
					          					
					  			e.printStackTrace();
					     				}finally {
					          					
					  	System.out.println(className);
					          			
		 Ex.testdata(description,className,remark,category,area,status,uAgent);
					          					
					          	}
					        }
			



@Test(alwaysRun = true)

public void  PPLTableCaptionTest2() throws Throwable {
	  
	try {
		
		PPL_TableCaption02 obj = new PPL_TableCaption02();
                obj.Click_on_Service();
                
	} catch (Exception e) {
		
		e.printStackTrace();
	     }
     }
}