package com.TableContent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;


public class PPL_TableFootnote06 extends PPL_BaseClass{


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
   public static  Excel Ex;
   
Pom PomObj;
	 
	 

	 /**
	  *Insert special character and place cursor to before character then press backspace key, tracking should be present
	  **/
	 

	 public void TableFootNote() throws Exception{
			
		 try {
			 
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-PPL_CopyEditor-TableFootNote-->Insert special character and place cursor to before character then press backspace key, tracking should be present";
	    className = "PPL_TableFootnote06";    
			 area = "Table FootNote";
			   category = "Insert and delete";
			      
			   
			 
			
		             PomObj=new Pom(); 
		             
		         Cookies cokies =new Cookies(driver);
	  	             cokies.cookies();
	  	              
	  	           Switch switc = new Switch(driver);
	  	              switc.SwitchCase(uAgent);  
		             
		             
		        WaitFor.presenceOfElementByXpath(driver, Pom.PPLTable3_Footnote());					
		       WebElement ele = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));
						
		             
			   String value=ele.getText();
			   
			   if(value.contains(value)){
				   
				   
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		    ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	   
				MoveToElement.byXpath(driver, Pom.PPLTable3_Footnote());
					 
			                   ele.click();
							  
			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
						  
						  for(int i=0;i<30;i++) {
						  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.LEFT);
						  }
					  
			SymbolActionFuction obj = new SymbolActionFuction(driver);
                          obj.SymbolTest();	 	  
					
                          
                       switc.SwitchCase(uAgent);
                       
                          
                      WebElement ele1 = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));			  
							 
                          for(int i=0;i<1;i++){
							  MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.LEFT);
						}
							  
					   MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
					  
				UpperToolBar obj1 = new UpperToolBar(driver);
				        obj1.save_btn_method();
				        
				        
				        
					   
			        }
			   
	
	              switc.SwitchCase(uAgent); 
				
				
				 WaitFor.presenceOfElementByXpath(driver, Pom.PPLTable3_Footnote());
					
	             WebElement ele1 = driver.findElement(By.xpath(Pom.PPLTable3_Footnote()));
		     
	            String actualValue =  ele1.getAttribute("innerHTML");
	            String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
	 			String expectedValue2 = "ice-del ice-cts";
	 			String expectedValue3 = "s</del>";
	 			String expectedValue4="ice-ins ice-cts";
	 				
	 						   System.out.println("After save - " + actualValue);
		
		
		if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
		 	
		status ="Pass";
		
		
		  
		}else{
		
			
			Thread.sleep(10000);  
			   
	    	  status ="Fail"; 
	    	  remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	  
	   // ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
					
					utilitys.Getscreenshot.captureScreenShot(uAgent,className,driver);
		   
		 
		 if(actualValue.contains(expectedValue1)==true){
		 	   System.out.println("E1: No issues");
		 	
		 	}
		  
		  if(actualValue.contains(expectedValue2)==true){
		 	   System.out.println("E2: No issues");
		 	  
		 	}
		   
		  if(actualValue.contains(expectedValue3)==true) {
		 	   System.out.println("E3: No issues");

		                }
		   if(actualValue.contains(expectedValue4)==true) {
				 	   System.out.println("E4: No issues");

				              }
		             }
        }catch (Exception e){
	          e.getStackTrace();
          }finally{	  
	    System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
         }
  }
	 	 
	


	 @Test(alwaysRun = true)

	 public void  PPLTableFootnoteTest6() throws Throwable {
	 	
	 	 try {
	 	

	 			PPL_TableFootnote06 obj = new PPL_TableFootnote06();
	 		         obj.TableFootNote();
	 	
	 			       
	 		} catch (Exception e) {	
	 			e.printStackTrace();
	 		     }
	           }	
	        }
