package com.TableContent;



import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableCaption18 extends CUP_AuthorEnd_BaseCalss{

	/**
	 *Single word select and "Control + x" then "Control + v" in next next word, check delete and insert tracking and data lose
	 **/
	
   
	static String remark;
	static String className = "CUP_AuthorEnd_TableCaption18";
	static String category;
	static String area;
	static String description;
	static String status;
	public static  Excel Ex;
	Pom PomObj;
	

	public void Cut_ThenPaste_nxt_nxtWord() throws Exception{
		
	
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-CUP_AuthorEditor-TableCaption-->Single word select and Control + x then Control + v in next next word, check delete and insert tracking and data lose";		     
		   area = "Table Caption";
			    category = "Cut and Paste";
				      
		       
		    Switch switc = new Switch(driver);
		    
		    CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                  C1_C2.CasperCUP(uAgent);	
		    
		       switc.SwitchCase(uAgent);
				      
		       PomObj = new Pom();  
			 
		     try {		
		   			  
		 		 WaitFor.presenceOfElementByXpath(driver, PomObj.AddTable1());
		 						
		 		 WebElement ele=driver.findElement(By.xpath(PomObj.AddTable1()));
		 				   
		 		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS) ;
					
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				
			String value = ele.getText();
			
			if(value.contains(value)) {
  
			MoveToElement.byXpath(driver, PomObj.AddTable1());	
			
			            MoveToElement.byclick(driver, ele);
										   
					  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
					   
	for(int i=0;i<26;i++){
					MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
				}
				     
	MoveToElement.Shitselect_Ele_Left_RightArrow(driver, 10,Keys.ARROW_RIGHT);
				
			if(uAgent.equals("IE")){
					
					 Robot robot = new Robot();
			     robot.keyPress(KeyEvent.VK_CONTROL);
			     robot.keyPress(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_X); 
			     robot.keyRelease(KeyEvent.VK_CONTROL); 
				
				}else{
				
				MoveToElement.cut(driver);				
			}
				
				for(int i=0;i<19;i++) {
	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
		}
				
				if(uAgent.equals("IE")){
					
		((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");			
							core.HandleAlert.isAlertPresentAccept(driver);		
									
								}else{
								
								MoveToElement.paste(driver);
								
						}
 
		 UpperToolBar obj = new UpperToolBar(driver);
				obj.save_btn_method();
				
				  }
			
			}catch(Exception e){
			  e.getStackTrace();
		}
		
try 
	{			
		Thread.sleep(3000);
		
    switc.SwitchCase(uAgent);
		
		 
				WaitFor.presenceOfElementByXpath(driver, PomObj.AddTable1());
					
				   WebElement ele1=driver.findElement(By.xpath(PomObj.AddTable1()));
				     
				     String afterSave =  ele1.getAttribute("innerHTML");
					
							
	//String beforeSave = "Effect of biomass concentration on plateau permeate flux in dynamic filtration of T. suecica.";
					
				     String expectedValue1 = "punishment</ins>";
					 String expectedValue2 = "punishment</del>";
					 String expectedValue3 = "ice-del ice-cts";
					 String expectedValue4="ice-ins ice-cts";

				  System.out.println("After save - " + afterSave);



if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true && afterSave.contains(expectedValue4)==true){
					// if(!beforeSave.equals(actualValue) && afterSave.contains(expectedValue1) && afterSave.contains(expectedValue2)){
					 						 	
	status="Pass";
	
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo);

remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);

	}
					 	 
					 	
			if(afterSave.contains(expectedValue1)==true){
					 	    System.out.println("E1: No issues");
					 	}
			if(afterSave.contains(expectedValue2)==true){
					 	    System.out.println("E2: No issues");
					 	}
			if(afterSave.contains(expectedValue3)==true){
						 	System.out.println("E3: No issues");
						}
			if(afterSave.contains(expectedValue3)==true){
					 	    System.out.println("E4: No issues");
					 	}
			}catch (Exception e) {
			    	
			e.printStackTrace();					   
			    }finally {
				System.out.println(className);
	Ex.testdata(description, className, remark, category, area, status, uAgent);
			}
    }
	
	
		
	
	
@Test(alwaysRun = true)

	public void  CUPTableCaptionTest18() throws Throwable {
		  
		 try {
			 MyScreenRecorder.startRecording(uAgent,className);
		
			 CUP_AuthorEnd_TableCaption18 obj = new CUP_AuthorEnd_TableCaption18();
	              obj.Cut_ThenPaste_nxt_nxtWord();
			
	   	                
		}catch(Exception e){
			
			e.printStackTrace();
	     	     }
     	    }
	   }

