package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.PPL_BaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;


public class PPL_TableContain06 extends PPL_BaseClass {	
	
    	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
    Pom PomObj;
	
	
	/**
	 * Insert special character and place cursor to before character then press backspace key, tracking should be present
	 **/
		public void Click_on_Service() throws Exception{

				
			try {
				
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-PPL_CopyEditor-TableContain-->Insert special character and place cursor to before character then press backspace key, tracking should be present";
	   className = "PPL_TableContain06";    
		   area = "Table Contain";
		       category = "Insert and Delete";
			   
			    
			    
			    System.out.println(uAgent);
			    
			    
			       Cookies cokies =new Cookies(driver);
			          cokies.cookies();
			       
			          
			    Switch switc = new Switch(driver);
			       switc.SwitchCase(uAgent);  
			       
			       PomObj =new Pom();
			    
         WaitFor.presenceOfElementByXpath(driver,PomObj.PPLTableContain6());
 
				WebElement ele= driver.findElement(By.xpath(PomObj.PPLTableContain6()));
	
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);		

	
	
				String value = ele.getText();

					if(value.contains(value)){
								
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
			    
			     ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");				
	   
			    MoveToElement.byXpath(driver,PomObj.PPLTableContain6());
	      
	      WaitFor.clickableOfElementByXpath(driver, PomObj.PPLTableContain6());
					
	                    ele.click();
					
					
					   switch (uAgent) {
				   	   
				          case "edge":
				   		  
				        	  for(int i=0;i<1;i++){
				   		MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);  
				   		   }
				   		      break; 
				   	            }
					
					
						
				MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
					
				SymbolActionFuction obj = new SymbolActionFuction(driver);
                                 obj.SymbolTest();
                                 
                                 
                      
                      	       switc.SwitchCase(uAgent);         
                                 
                                                           
                                 
                                 switch (uAgent) {
              				   	   
       				          case "edge":
       				   		  
       				  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_RIGHT);
       				   		      break; 
       				   	
       				   	     }                      
                                 
                                 
               WebElement ele0= driver.findElement(By.xpath(PomObj.PPLTableContain6()));				  	    
                     driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);	
                
                     
				for(int i=0;i<1;i++) {
					MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.ARROW_LEFT);
				} 
				
				
					MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.BACK_SPACE);	
			
			}	
					
				
					UpperToolBar obj1 = new UpperToolBar(driver);
					       obj1.save_btn_method();
					       					       
				        }catch (Exception e) {
					e.printStackTrace();
			     } 
				
				try {

Thread.sleep(3000);
					
 Switch switc = new Switch(driver);
   switc.SwitchCase(uAgent); 
				
		WaitFor.presenceOfElementByXpath(driver,PomObj.PPLTableContain6());
					 
	  WebElement ele1= driver.findElement(By.xpath(PomObj.PPLTableContain6()));
								
	  String actualValue =  ele1.getAttribute("innerHTML");
		    String expectedValue1 = "1</del>";
		    String expectedValue2 = "ice-del ice-cts";
		    String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
		    String expectedValue4= "ice-ins ice-cts";
			
		    System.out.println("After Save:- "+actualValue);	
				
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
				 	
				 status="Pass";
				         
      }else{
    	 // ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
    	  
    	  
    	  status ="Fail"; 
    	 
remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
			
			
Thread.sleep(10000);  

			
			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			
	
			
				      if(actualValue.contains(expectedValue1)==true) {
				 	  System.out.println("E1: No issues");
				 	
				 	}
				  
				 	    if(actualValue.contains(expectedValue2)==true){
				 	   System.out.println("E2: No issues");
				 	  
				 	}
				   
				 	    if(actualValue.contains(expectedValue3)==true) {
				 	   System.out.println("E3: No issues");

				                }
				 	   if(actualValue.contains(expectedValue4)==true) {
					   System.out.println("E4: No issues");

					                }
				            }
			} catch (Exception e) {
		
				e.printStackTrace();
			    }finally {
			   	System.out.println(className);
		   Ex.testdata(description,className,remark,category,area,status,uAgent);

			    }
		   }
		
		
		
	
		@Test(alwaysRun=true)
		public static void PPLTableContainTest6() throws Exception {
								
			 try {
							
		PPL_TableContain06 obj = new PPL_TableContain06();
					obj.Click_on_Service();		
					
			 }catch(Exception e){
					e.printStackTrace();
				    }
	     	  }
		}