package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.TandFBaseClass;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;




public class TandF_TableCaption06 extends TandFBaseClass {


	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
	static String status;
	public static Excel Ex;

	Pom PomObj;
    
/*	public TandF_FigureModule06(WebDriver ldriver) {
		this.driver=ldriver;
	}*/
	

/*******
*6. Insert special character and place cursor to before character then press backspace key, tracking should be present
 * @throws Exception 
********/


public void insert_spl_chr_PlcCurserBefore_char_bckspce() throws Exception {
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
   description = "Casper-TandF_CopyEditor-TableCaption-->Insert special character and place cursor to before character then press backspace key, tracking should be present";
	   className = "TandF_TableCaption06";    
	       area = "Table Caption";
	          category = "insert and deletion";
	             
	
	
	

	                  
                System.out.println("BrowerName->"+uAgent);
	
                Cookies cokies =new Cookies(driver);
	             cokies.cookies();
	              
	           Switch switc = new Switch(driver);
	              switc.SwitchCase(uAgent);    
                
	              PomObj = new Pom();  
	          	try {    	                
	      	
	      	WaitFor.presenceOfElementByXpath(driver, PomObj.Table_1_Caption());
	      	
	                 WebElement ele=driver.findElement(By.xpath(PomObj.Table_1_Caption()));
	                
	                    driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);  
	                    
	      ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	            ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");   
	                    String value=ele.getText();
	                    
	      					if(value.contains(value)) {
	      						
	      	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	      	 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");			
	      	MoveToElement.byXpath(driver, PomObj.Table_1_Caption());	
	      	 
	          WaitFor.clickableOfElementByXpath(driver, PomObj.Table_1_Caption());
	      				
	      						       ele.click();
	      				
	      				
	      						       switch (uAgent){
	      					       		case "edge":
	      					      MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_DOWN);
	      					       		break;
	      					       }	       
	      						       
	      						       
	      			MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
	      				
	      			
	      				for(int i=0;i<2;i++){
	      			    	MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
	      			    }	
	      				
	      			
	      			SymbolActionFuction obj = new SymbolActionFuction(driver);
	                        obj.SymbolTest();
	      	 	                   
	      	                          switc.SwitchCase(uAgent);                 
	      	 	       
	                      WaitFor.presenceOfElementByXpath(driver, PomObj.Table_1_Caption());
	                  	
	                    WebElement ele1=driver.findElement(By.xpath(PomObj.Table_1_Caption()));
	                   		 
	                      MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.ARROW_LEFT);      
	                              
	      	 	        MoveToElement.sendkeybyinsidevalue(driver, ele1, Keys.BACK_SPACE);
	      	 	         	 	         
	      	    UpperToolBar ob = new UpperToolBar(driver);
	      	 	  	    ob.save_btn_method();
	      	 	  	     	 	  	    
	      	                   }
	      					} catch (Exception e) {
	      			e.printStackTrace();
	      	                   }
	      try
	      {
	      	 Thread.sleep(3000);

	            switc.SwitchCase(uAgent);  
	      	
	      	 
	      	       WaitFor.presenceOfElementByXpath(driver, PomObj.Table_1_Caption());  
	               WebElement val=driver.findElement(By.xpath(PomObj.Table_1_Caption()));
	      		
	                     String afterSave =  val.getAttribute("innerHTML");
	      			     
	      				 String expectedValue1 = "e</del>";
	      				 String expectedValue2 = "ice-del ice-cts";
	      				 String expectedValue3 = DataProviderFactory.getConfig().getSpecialCharValidation();
	      				 String expectedValue4="ice-ins ice-cts";
	      		 
	      		 
	      		 System.out.println("After save - " + afterSave);
	      	
	      	
	      if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && afterSave.contains(expectedValue3)==true&& afterSave.contains(expectedValue4)==true){
	      		 status="Pass";
	      		 	
	      		 }else{
	      			 
	   //   	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", val);	 
	      		
	      	 		 
	      	 	     status = "Fail";
	      	 remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";	
	        
	      	 	   Thread.sleep(10000);    
	      	 	    utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
	      			
	      		  }
	      		 	if(afterSave.contains(expectedValue1)==true) {
	      		 	   System.out.println("E1: No issues");
	      		 	}
	      		    if(afterSave.contains(expectedValue2)==true) {
	      		 	   System.out.println("E2: No issues");
	      		 	}
	      		    if(afterSave.contains(expectedValue3)==true) {
	      		 	   System.out.println("E3: No issues");
	      		     }
	      		    if(afterSave.contains(expectedValue4)==true) {
	      			 	System.out.println("E4: No issues");
	      			}
	             }catch (Exception e) {
	                   e.getStackTrace();
	          	   
	      	       }finally {
	      	    	   System.out.println(className);
	      	   Ex.testdata(description, className, remark, category, area, status, uAgent);
	              }
	        }



@Test(alwaysRun = true)

public void  Test6() throws Throwable {
	  
	 try {
		 
		      TandF_TableCaption06 obj = new TandF_TableCaption06();
                obj.insert_spl_chr_PlcCurserBefore_char_bckspce();
		
         
	} catch (Exception e) {
		
		e.printStackTrace();
	     }
    }

}
