package com.TableContent;




import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableContain13 extends CUP_AuthorEnd_BaseCalss {

	static String remark;
	static String className = "CUP_AuthorEnd_TableContain13";
	static String category;
	static String area;
	static String description;
	static String status;
    public static Excel Ex;
	

static Pom PomObj;

	
	/**
* Single cell select and "Control + x" then "Control + v" in next cell, check delete and insert tracking and data lose
	 **/

	
	public void Cut_ThenPaste_nxt_nxtWord() throws Exception{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_AuthorEditor-TableContain-->Single word select and Control + x then Control + v in next next word, check delete and insert tracking and data lose";		    
		      area = "Table Contain";
		         category = "Cut and paste";
		            
		         System.out.println("BrowerName->"+uAgent);                 
		           
		     	try {	     
		     		
		     		
		          PomObj = new Pom();  
		    
		    
		    Switch switc = new Switch(driver);
		    
		    CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                  C1_C2.CasperCUP(uAgent);
		    
		       switc.SwitchCase(uAgent);                
		            	  
		          
		            			
	            			 
WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTableContaincut());

		    WebElement ele = driver.findElement(By.xpath(PomObj.CUPTableContaincut()));
		    
		    
		    driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		    
		  	
		            					
		            String value=ele.getText();
		            
		         if(value.contains(value)) {
		          
		        	 
 ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	 
       ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");	        	 
		        	 
           MoveToElement.byXpath(driver, PomObj.CUPTableContaincut());

		    ele.click();
		            					
		    MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
		            				     
	 MoveToElement.Shitselect_Ele_Left_RightArrow(driver,3,Keys.ARROW_LEFT);
		            					
		           MoveToElement.cut(driver);
		          }
		
		         
		         
WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTableContainPaste());		

            WebElement ele0=driver.findElement(By.xpath(PomObj.CUPTableContainPaste()));
		            					
                 String value1=ele0.getText();
		            					
                 if(value1.contains(value1)) {
		            						
	MoveToElement.byXpath(driver,PomObj.CUPTableContainPaste());
		            				
		             ele0.click();
		            				
		     MoveToElement.sendkeybyinsidevalue(driver, ele0, Keys.END);
		           			
		     
		     if(uAgent.equals("IE")==true) {
		    	
		 ((JavascriptExecutor)driver).executeScript("document.execCommand('paste')");		
		           
				   core.HandleAlert.isAlertPresentAccept(driver);
				   
		             }else {
		            	 
		      				MoveToElement.paste(driver);
		        }				
		            				
		            UpperToolBar obj = new UpperToolBar(driver);
		            				obj.save_btn_method();
		            				
		            	
		            	      }
		            					
		            				
                 Thread.sleep(3000);
					
     	       switc.SwitchCase(uAgent);	
                 
					
                 
						WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTableContaincut());

     		    WebElement ele1 = driver.findElement(By.xpath(PomObj.CUPTableContaincut()));
		            				            		

     		   WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTableContainPaste());		
     		   WebElement ele2=driver.findElement(By.xpath(PomObj.CUPTableContainPaste()));
		            				    
		          String afterSave =  ele1.getAttribute("innerHTML");
		          String actualValue = ele2.getAttribute("innerHTML");
		            				
		            					//String beforeSave = "Effect of biomass concentration on plateau permeate flux in dynamic filtration of T. suecica.";
		            					
		          String expectedValue1 = "300</del>";
		          String expectedValue2 = "ice-del ice-cts";
		          String expectedValue3 = "300</ins>";
		          String expectedValue4 = "ice-ins ice-cts";
		        
		            					
		  System.out.println("after save - " + afterSave+"\n"+"actualValue - "+actualValue);



		 if(afterSave.contains(expectedValue1)==true && afterSave.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true 
		            	&& actualValue.contains(expectedValue4)==true){

		            					 
			 status="Pass";
				
				MyScreenRecorder.stopRecording();
 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
				    		      			util.deleteRecFile(RecVideo); 		 	
				    		      					     			         				      				 	
				    		      					     			         				      				 	
				    		  }else{
				    		      					     			         				      			    	  				     			         				      			   
				    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
				          status="Fail";
				    		      					     			         				      				 			
				    		  MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
				    		      	      util.MoveRecFile(RecVideo);
//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);		    
remark="Cut paste did not work/Extra Char or Symbol may has come/ Insert or Delete trac is missing";   			
		            		
		
		utilitys.Getscreenshot.captureScreenShot(uAgent,className, driver);
		            					 	 
		            	
		            		
		            		
		            		
		          if(afterSave.contains(expectedValue1)==true) {
		            		   
		        	System.out.println("E1: No issues");
		            					 	}
		          if(afterSave.contains(expectedValue2)==true){
		            		 
		        	  System.out.println("E2: No issues");
		            					 	}
		          if(actualValue.contains(expectedValue3)==true){
		            		  
		        	  System.out.println("E3: No issues");
		            						 	}
		          if(actualValue.contains(expectedValue4)==true){
		            		  
		        	  System.out.println("E4: No issues");
		            					 	   }
		        
		            					     }
		            				      }catch (Exception e) {
		            				e.printStackTrace();
		            				 driver.quit();
		            				HandleException();
		            			}finally {
		            				
		            System.out.println(className);
		  Ex.testdata(description,className,remark,category,area,status,uAgent);
		            			}     		   
		            	   }
	
	
	
	
	public void HandleException() throws Exception {
		CUP_AuthorEnd_TableContain13 obj = new CUP_AuthorEnd_TableContain13();
	       obj.Cut_ThenPaste_nxt_nxtWord();
	}
		            	  


	
	
	
	
	@Test(alwaysRun=true)
	public static void CUPTableContainTest13() throws Exception {

			
      try {
					 
    	  MyScreenRecorder.startRecording(uAgent,className);
	    CUP_AuthorEnd_TableContain13 obj = new CUP_AuthorEnd_TableContain13();
	       obj.Cut_ThenPaste_nxt_nxtWord();
	       

		} catch (Exception e) {
		
			e.printStackTrace();
		     }
	   
	    }
   }

