package com.TableContent;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.FigureContain.CUP_AuthorEnd_BaseCalss;
import com.FigureContain.CasperC1_C2;
import com.page.SymbolActionFuction;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.DataProviderFactory;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_AuthorEnd_TableContain01 extends CUP_AuthorEnd_BaseCalss {
	
	static String remark;
	static String className = "CUP_AuthorEnd_TableContain01";    
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    Pom PomObj;   
    
    
  
	
	/**
	 * Delete one character with Back space key press and insert special character, tracking should be present
	 **/
    
	public void Click_on_Aspirations() throws Throwable{
		
		try {
			
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_AuthorEditor-TableContain-->Delete one character with Back space key press and insert special character, tracking should be present";
	         area = "Table Contain";
	            category = "Delete insert Special Character";
	               
	            driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);               
        
	       
		       
		    Switch switc = new Switch(driver);
		    
		    CasperC1_C2 C1_C2 = new CasperC1_C2(driver);
                C1_C2.CasperCUP(uAgent);	
            
		       switc.SwitchCase(uAgent);    
	            
	            
                     PomObj = new Pom();   
                     
        WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTable_1());         
	
      WebElement ele= driver.findElement(By.xpath(PomObj.CUPTable_1()));
		
               
      
   ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
        ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");   
			String value = ele.getText();
	
				if(value.contains(value)){
     
		MoveToElement.byXpath(driver, PomObj.CUPTable_1());
		
			WaitFor.clickableOfElementByXpath(driver, PomObj.CUPTable_1());
			
			 for(int i=0;i<2;i++){
			MoveToElement.byclick(driver, ele);
			Thread.sleep(4000);
             }
         
               
          
       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.END);
       
       for(int i=0;i<3;i++) {
       MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.ARROW_LEFT);
       }
       
	      WaitFor.visibilityOfElementByWebElement(driver, ele);
	      
  MoveToElement.sendkeybyinsidevalue(driver, ele, Keys.BACK_SPACE);
 
 
    SymbolActionFuction obj = new SymbolActionFuction(driver);
               obj.SymbolTest();
 
        			
            UpperToolBar obj1 = new UpperToolBar(driver); 
			        	obj1.save_btn_method();
			     			        	
				} 
				
		}catch(Exception e){
						
		e.printStackTrace();	
	} 
		
		
try 
{	

	Thread.sleep(3000);
		
	       
	    Switch switc = new Switch(driver);
	       switc.SwitchCase(uAgent);
	       
		 WaitFor.presenceOfElementByXpath(driver, PomObj.CUPTable_1());
		 
		WebElement ele1=driver.findElement(By.xpath(PomObj.CUPTable_1()));
					     
			     String actualValue =  ele1.getAttribute("innerHTML");
				 String expectedValue1 = DataProviderFactory.getConfig().getSpecialCharValidation();
				 String expectedValue2 = "ice-del ice-cts";
				 String expectedValue3 = "M</del>";
			     String expectedValue4="ice-ins ice-cts";
			     
			System.out.println("After save - " + actualValue);
			
			
if(actualValue.contains(expectedValue1)==true && actualValue.contains(expectedValue2)==true && actualValue.contains(expectedValue3)==true&& actualValue.contains(expectedValue4)==true){
			 	
status="Pass";
	
	MyScreenRecorder.stopRecording();
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);      	    
	    		      			util.deleteRecFile(RecVideo); 		 	
	    		      					     			         				      				 	
	    		      					     			         				      				 	
	    		      					    }else{
	    		      					     			         				      			    	  				     			         				      			   
	    		      		// ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);   	  				     			         				      			    	  	    
	          status="Fail";
	    		      					     			         				      				 			
	    		  MyScreenRecorder.stopRecording();
	   String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    		      	      util.MoveRecFile(RecVideo); 
		
  remark="Extra Char or Symbol may has come/ Delete Trac is missing/ Insert Trac is missing";
					
      utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
        
	if(actualValue.contains(expectedValue1)==true){
		 System.out.println("E1: No issues");
			}
			  
			 	  
			 if(actualValue.contains(expectedValue2)==true){
			 	   System.out.println("E2: No issues");
			 	  }
			   
			 	    if(actualValue.contains(expectedValue3)==true) {
			 	   System.out.println("E3: No issues");

			                }
			 	    if(actualValue.contains(expectedValue4)==true) {
					 	   System.out.println("E4: No issues");

					              }
			              }
		        }catch(Exception e){
			
			e.printStackTrace();
	       }finally {
	    	   System.out.println(className);
				Ex.testdata(description, className, remark, category, area, status, uAgent);
	          }
		}
	


@Test(alwaysRun = true)

	public void  CUPTableContainTest01() throws Throwable {
		  
		 try {
	
			 MyScreenRecorder.startRecording(uAgent,className);
			 
			 CUP_AuthorEnd_TableContain01 obj = new CUP_AuthorEnd_TableContain01();
                     obj.Click_on_Aspirations();
		                    
		} catch (Exception e) {
			
			e.printStackTrace();
		     }
        }	
 }

