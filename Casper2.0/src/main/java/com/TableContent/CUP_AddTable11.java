package com.TableContent;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.FigureContain.CUP_BaseClass;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class CUP_AddTable11 extends CUP_BaseClass{
	
	
	
	 static WebElement ele =null;
	 static String remark;
	 static String className;
	 static String category;
	 static String area;
	 static String description;
	 static String status;
	 public static Excel Ex;
	 Pom PomObj;
	   
	
	    
	    
	  
		
/**
* Delete one character with Back space key press and insert special character, tracking should be present
**/
	    
	public void Click_on_Aspirations() throws Throwable{
											
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
description = "Casper-CUP_AuthorEnd-AddTable-->Check whether Add Table, column can able to delete the column or not.";
		 className = "CUP_AddTable11";    
		         area = "Add Table";
		               category = "Delete the column";
		              
	                    		                      		                                     
	                     
			      System.out.println("BrowerName->"+uAgent);                 
	           
			              
			          Cookies cokies =new Cookies(driver);
				             cokies.cookies();
				              
				       Switch switc = new Switch(driver);
				              switc.SwitchCase(uAgent);    				              
	        
	PomObj = new Pom();   
			                     

	WaitFor.presenceOfElementByXpath(driver, PomObj.tr4());
		
	   ele= driver.findElement(By.xpath(PomObj.tr4()));
		   driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS); 
		 
			
((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);				
 ((JavascriptExecutor)driver).executeScript("window.scrollBy(250,350)");
 
	         	
	 MoveToElement.byXpath(driver, PomObj.tr4());	
				          	 		
	 WaitFor.clickableOfElementByXpath(driver, PomObj.tr4());
				          	            
				          	     ele.click();
		
				          	 
	               Actions action=new Actions(driver);
   	                    Thread.sleep(2000);        
		   
				          action.contextClick(ele).build().perform();
					
				          	     
				          	     
				     	 Robot robot = new Robot();
			           	 robot.delay(1);
			             
			           	  Thread.sleep(3000);          	     
			           	 for(int i=0;i<2;i++) {
				           	  robot.keyPress(KeyEvent.VK_DOWN);	     
				              Thread.sleep(3000);  
				           	} 
			           	 robot.keyRelease(KeyEvent.VK_DOWN);
			           	 
			           	 
			           	 
			              robot.keyPress(KeyEvent.VK_RIGHT);	
			               Thread.sleep(3000);
			              robot.keyRelease(KeyEvent.VK_RIGHT); 
			              
			              
			              
			              for(int i=0;i<2;i++) {
				           	  robot.keyPress(KeyEvent.VK_DOWN);	     
				              Thread.sleep(3000);  
				           	} 
			                robot.keyRelease(KeyEvent.VK_DOWN);
			              
			              
			              
			              Robot robot1 = new Robot();
				           	 robot1.delay(1);
			                   Thread.sleep(4000);
			             robot1.keyPress(KeyEvent.VK_ENTER);
				             
			             
			             
			             
				             				          	     
				   WaitFor.presenceOfElementByXpath(driver, PomObj.tdValidation());
				     		
				 List<WebElement> ele= driver.findElements(By.xpath(PomObj.tdValidation()));			             
				            
				          int count =ele.size();
				    	
				               System.out.println(count);
				          
				          int val = 5;
				          
				      if(count==val==true){
				    	  
				    	  status="Pass";
				     	   
			            }else{
			          	  
			          	  status="Fail";
			          	  
			       	  remark="Unable to delete the column";
			       	  
			       	  
			       	    Thread.sleep(10000);  		
					 utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
			            }
				 }
				    		 

			
@Test(alwaysRun = true)
	
	public void  CUPAddTableTest11() throws Throwable {
		  
	try {
	
			 CUP_AddTable11 obj = new CUP_AddTable11();
              obj.Click_on_Aspirations();
				                      
		}catch (Exception e){
			
			e.printStackTrace();
		     
		      }finally{
					System.out.println(className);
		Ex.testdata(description, className, remark, category, area, status, uAgent);
							}
                   }
	         }