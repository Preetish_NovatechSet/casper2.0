package com.linkGenrationModule;


import java.io.File;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import com.EditGenieTest.LINK;

import core.MoveToElement;
import utilitys.util;


public class EditGenie_UrlGerationPage {

	
WebDriver driver; 
	
	@FindBy(how=How.XPATH,using="/html/body/div[2]/div[2]/div/form/div/div/div/div[2]/div[1]/div/select")
	List<WebElement> Xml_Type;
	
	
	@FindBy(how=How.XPATH,using=".//input[@id='xml_File']")
	static List<WebElement> Select_Xml_File;
	
	@FindBy(how=How.XPATH,using=".//input[@id='image_Files']")
	static List<WebElement> Select_image;

	@FindBy(how=How.XPATH,using=".//*[@id='pdf_Files']")
	static List<WebElement> pdf;
	
	@FindBy(how=How.XPATH,using=".//*[@value='Upload']")
	static List<WebElement> upload;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"alert-url\"]/h4")
	static List<WebElement> url; 
	
	@FindBy(how=How.XPATH,using=".//*[@id='btn']")
	static List<WebElement> StartProofBTN;
	
	
	@FindBy(how=How.XPATH,using="//*[@id='cookie-gotIt']")
	static List<WebElement> cookies;
	
	
	public  EditGenie_UrlGerationPage(WebDriver ldriver) {
	     this.driver=ldriver;	
	}
	


	private static void UseToGetElement(List<WebElement> Wele) {
		for(WebElement ele:Wele) {
			 
		 String	value = ele.getText();
			
			System.out.println(value);
			
			if(value.contains(value)) {
			
				ele.click();
			}	
			
		}
	}
	
	

	//XML in dinamic way
		public void Select_xml_value(String PublisherName){
			 try{
			     Select sel = new Select(LINK.Select_Journal(driver));
			     sel.selectByVisibleText(PublisherName);
			     }catch(Exception x){
			   System.out.println(x.getMessage());
			  }
		}
	
	//Folder and file list
	public void getFolderGetFilename(File folder) throws Exception {
				
	/*	File[] listOfFiles = folder.listFiles();		
		  System.out.println(listOfFiles.length);
		
		  for (int i=0; i<listOfFiles.length; i++){
			
		if(listOfFiles[i].isDirectory()) {
				String f = listOfFiles[i].toString();
				System.out.println("Folder path"+f);
				
				if(f.contains("Author")) {
					
			System.out.println("Inside XML folder: ");
					f= f.replace("\\", "/");
					
					String value = "/";
					
					File xml = new File(f+value);
					File[] fls = xml.listFiles();
					
					for(int j =0; j<xml.listFiles().length;j++) {
						System.out.println("pkm :-"+fls[j]);
					  }*/
//EditGenie_UrlGerationPage obj =  PageFactory.initElements(driver, EditGenie_UrlGerationPage.class);
						//  obj.RenameXmlCodeAKB();
					
				     
				  }
				
		/*	else if(f.contains("pdf")) {
				
				    System.out.println("Inside Pdf folder: ");
					f.replace("\\", "/");
					String value = "/";
					File pdf = new File(f+value);
					File[] fls = pdf.listFiles();
					System.out.println(fls[0]);
					
			EditGenie_UrlGerationPage obj =  PageFactory.initElements(driver, EditGenie_UrlGerationPage.class);
					  obj.pdfUpload(f+value);
					  
					
					
				}*/
				
		/*		else if(f.contains("Images")) {
					
					
					System.out.println("Inside image folder: ");
					f= f.replace("\\", "/");
					
					String value = "/";
					File image = new File(f+value);
					File[] fls = image.listFiles();
					
					for(int j = 0; j < image.listFiles().length; j++) {
					System.out.println(fls[j]);
					 }
				ImageExtention obj = PageFactory.initElements(driver,ImageExtention.class);
					obj.ImageFilter(f+value);
					
				}else{
					System.out.println("Path does not have any directories. \n Check the path...");

					
				           }*/
		             //   }
	               // }
		      //  }
	      
		
		    
	public void RenameXmlCodeAKB(String PathofXML) throws InterruptedException {
		String renamedFile=null;
		
		  System.out.println("Check");

	   String sourcePath = PathofXML;
	      
	      File folder = new File(sourcePath);

	      File[] listOfFiles = folder.listFiles();
	      
	      System.out.println("List counted successfully");
	   

	      System.out.println(listOfFiles.length);

	      for(int i=0;i<listOfFiles.length;i++){

	             if(listOfFiles[i].isFile()){

	             String fileName = listOfFiles[i].getName();
	             
	             System.out.println(fileName);

	             File oldFile = new File(sourcePath + fileName);
	             
	             System.out.println(oldFile);

	          String lastcharacters = fileName.substring(fileName.length()-9);

	          String upToNCharacters = fileName.substring(0, Math.min(fileName.length(), 12));
                       
	          String lchar = lastcharacters.substring(0, lastcharacters.length()-4);
	          System.out.println("PKMCheck--->"+lchar);
	          int lNumber = Integer.parseInt(lchar);

	          lNumber++;
	          
	          System.out.println(lNumber);

	          File newFile =new File(sourcePath+upToNCharacters+String.valueOf(lNumber)+".xml");

	          System.out.println(newFile);
	          oldFile.renameTo(newFile);
	          renamedFile = newFile.getName();
	          System.out.println("file renamed successfully");
	          
	          
	          
	  	    String source = PathofXML;
			  
		    System.out.println("File source-->"+source);

		   

		    System.out.println(renamedFile);

		   
		   Thread.sleep(2000);

		  
		    driver.findElement(By.xpath("//*[@name='xml']")).sendKeys(source+renamedFile);
		   
	             }
	             
	      }
		
	}
	
	
	/*public void renameFileXml_Upload(String path) throws Exception {
		

		//String date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
	
		
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();		
		String filename = listOfFiles[listOfFiles.length-1].getName();
		
		
		String absolutePath = path+filename;
		File dir = new File(absolutePath);		
		System.out.println("PKM-->"+dir);
		
		
		File newfile = new File(path+
				dir.getName().substring(0, dir.getName().lastIndexOf("_")));
		
		
		File newfile1 = new File(newfile+"_"+util.getCurrentDateTime()+".xml");
	     System.out.println(newfile1.getAbsolutePath());
	
		
	     if(dir.renameTo(newfile1)) {
			System.out.println(dir.getName()+" renamed to "+newfile1.getName());
		 }else{
			System.out.println("opration fail");
		}
		
		
		
	     for(WebElement ele:Select_Xml_File) {
			
			if(ele.getAttribute("id").contains("xml_File")) {
				
				ele.sendKeys(DataProviderFactory.getLoginAndpathfolderConfig().getfolderXmlPath()+"TENT1290145_54360_"+util.getCurrentDateTime()+".xml");
				System.out.println(DataProviderFactory.getLoginAndpathfolderConfig().getfolderXmlPath()+"TENT1290145_54360_"+util.getCurrentDateTime()+".xml");
			     }
	         }      
	
	     }*/
		

	
	public void pdfUpload(String path) {
		File folder =new File(path);
		File[] listOfFiles = folder.listFiles();		
		String filename = listOfFiles[listOfFiles.length-1].getName();
		
		
String absolutePath = path+filename;
		File dir = new File(absolutePath);		
		   System.out.println(dir);
		
		for(WebElement ele:pdf) {	
			if(ele.getAttribute("id").contains("pdf_Files")){
				
				String akb = dir.getPath();
    		    akb = akb.replace("\\", "/");
    		    System.out.println(akb);
				ele.sendKeys(akb);
				
			}
		}
	}
	
	
	
	
	public void ClickUpload_And_LinkGenrateUrl() throws InterruptedException {
		
		for(WebElement ele:upload) {
			
			if(ele.getAttribute("value").contains("Upload")) {
				MoveToElement.byclick(driver, ele);
				
			}
		}
		
		
		for(WebElement ele:url) {
			String value = ele.getText();
		System.out.println("Url_link_Genrate-->>> "+value);
		
		
		util.SwitchWindow(driver);
		
		Thread.sleep(12000);
	
		driver.navigate().to(value);
	
		Thread.sleep(2000);
		
	System.out.println("LandingPage "+value);
	
	try {
	
		UseToGetElement(cookies);
		
		
	    } catch (Exception e) {

	              }
	    }
		
		
		
	  for(WebElement ele:StartProofBTN) {
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
			
			String value = ele.getText();
            	
			if(value.contains(value))
            		
            		MoveToElement.byXpath(driver, ".//*[@id='btn']");
		              MoveToElement.byclick(driver, ele);
		
		             
		              utilitys.util.SwitchWindow(driver);
		              
		     
		   
		 
	       }
        }
     }  
