package com.linkGenrationModule;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;




public class ImageExtention{

	
	static BufferedImage img = null;
	WebDriver driver; 
    // File representing the folder that you select using a FileChooser
	// static final File dir = new File(DataProviderFactory.getLoginAndpathfolderConfig().getImagePath());	

    // array of supported extensions (use a List if you prefer)
    static final String[] EXTENSIONS = new String[]{
        "gif", "png", "jpg" , "bmp" , "tif"
        
    };
    // filter to identify images based on their extensions
    static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {
    	
        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };

    
    public  ImageExtention(WebDriver ldriver) {
	     this.driver=ldriver;	
	}
	
    
    
    @FindBy(how=How.XPATH,using=".//input[@id='image_Files']")
	static List<WebElement> Select_image;
    
    public void ImageFilter(String path) {
    	final File dir = new File(path);
    	String filename = null;
		String temp=null;
        if (dir.isDirectory()) { 
            for (final File f: dir.listFiles(IMAGE_FILTER)) {
            	

                try {
                    img = ImageIO.read(f);
                    // to display in your UI
                System.out.println("image: " + f.getName());
                
          for(WebElement ele:Select_image) {
        	  
        	  if(ele.getAttribute("id").contains("image_Files")) {
        	  
        	
        		    
        		    String akb = f.getPath();
        		    akb = akb.replace("\\", "/");
        		    System.out.println("Path:"+akb);
 
        		    
        		    temp = akb;
        		    if(filename==null)
               	 {
               		 filename = temp;
               	 }
               	 else
               	    {
               		 filename = filename+ "\n" + temp;
               	    }
        	      }
                } 
   WebElement img = driver.findElement(By.cssSelector("#image_Files"));
           img.sendKeys(filename);
                }catch (final IOException e) {
                    // handle errors here
                }
             }
          }
	   }
    }