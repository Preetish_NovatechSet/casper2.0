package com.linkGenrationModule;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import core.MoveToElement;
import utilitys.DataProviderFactory;

public class Link_Geration_Login {
	
	WebDriver driver; 
	
	@FindBy(how=How.XPATH,using="//*[@name='username']")
	static List<WebElement> username;
	
	@FindBy(how=How.XPATH,using="//*[@name='password']")
	static List<WebElement> password;
	
	
	@FindBy(how=How.XPATH,using=".//*[@class='btn btn-primary']")
	static List<WebElement> signinBtm;
	
	
	

public Link_Geration_Login(WebDriver ldriver) {
	     this.driver=ldriver;	
	}
	
	
	
	

public void login_Method() throws InterruptedException, AWTException {
	
	for(WebElement ele:username){
	String value = ele.getText();
	if(value.contains(value)) {
		
MoveToElement.byWebElement(driver, ele);
	
		MoveToElement.Click_sendkey(driver, ele, DataProviderFactory.getLoginAndpathfolderConfig().getusername());
		
	          }
		
	      }
	
	for(WebElement ele:password) {
		String value = ele.getText();	
	if(value.contains(value)){	
		MoveToElement.byWebElement(driver, ele);
		MoveToElement.Click_sendkey(driver, ele, DataProviderFactory.getLoginAndpathfolderConfig().getpassword());
	    }
	 }
	
	
	for(WebElement ele:signinBtm) {
		
		String value =ele.getAttribute("innerHTML");
		//System.out.println(value);
		
		
		if(value.contains(value)) {
			
			Thread.sleep(3000);
			utilitys.util.highLightElement(driver, ele);
			MoveToElement.byWebElement(driver, ele);
			MoveToElement.byclick(driver, ele);
			
	     System.out.println("Login done");		
		}		
	  }
    } 	
  }




