package com.AuthorNameAreaAddAffiliation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;


public class WritePropertiesFile {

	
	String url=null;
	
		
	public static void FuntionWritePropertiesFile(String key,String url,String Path) throws InterruptedException {
		try {
			Thread.sleep(12000);
		Properties properties = new Properties();
		properties.setProperty(key,url);
		String scr = Path;
		File file = new File(scr);
		FileOutputStream fileOut = new FileOutputStream(file);
		properties.store(fileOut, "Favorite Things");
		fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
     }
	
	
	/*********************AlphabetStyleAddAffiliation
	 * @throws InterruptedException ***********************/
	
public static String WriteUrlFireFoxInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Firefox.properties");
			return url;
}
	
	
	public static String WriteUrlChromeInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("ChromeUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Chrome.properties");
				return url;
				
		}
			
			
			
	public static String WriteUrlOperaInPropertiesFile(String url) throws InterruptedException {
				
WritePropertiesFile.FuntionWritePropertiesFile("OperaUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Opera.properties");
				return url;		
				
		}
			
			
			
			public static String WriteUrlEdgeInPropertiesFile(String url) throws InterruptedException{
						
WritePropertiesFile.FuntionWritePropertiesFile("EdgeUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Edge.properties");
				return url;			
		}
			
			
			public static String WriteUrlIEInPropertiesFile(String url) throws InterruptedException{
				
WritePropertiesFile.FuntionWritePropertiesFile("IEUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_IE.properties");
						return url;			
		}
			
			
			/***************************NumberStyleAddAffiliation
			 * @throws InterruptedException *****************************/
	
	
	public static String WriteUrlNumberStyleAddAffiliationFireFoxInPropertiesFile(String url) throws InterruptedException {
		
WritePropertiesFile.FuntionWritePropertiesFile("FireFoxUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Firefox.properties");
					return url;
		}
			
			
			public static String WriteUrlNumberStyleAddAffiliationChromeInPropertiesFile(String url) throws InterruptedException {
				
WritePropertiesFile.FuntionWritePropertiesFile("ChromeUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Chrome.properties");
						return url;
						
				}
					
					
					
public static String WriteUrlNumberStyleAddAffiliationOperaInPropertiesFile(String url) throws InterruptedException {
						
WritePropertiesFile.FuntionWritePropertiesFile("OperaUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Opera.properties");
						return url;		
						
				}
					
					
					
public static String WriteUrlNumberStyleAddAffiliationEdgeInPropertiesFile(String url) throws InterruptedException{
								
WritePropertiesFile.FuntionWritePropertiesFile("EdgeUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Edge.properties");
						return url;			
				}
					
					
	public static String WriteUrlNumberStyleAddAffiliationIEInPropertiesFile(String url) throws InterruptedException{
						
		WritePropertiesFile.FuntionWritePropertiesFile("IEUrl", url, "./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_IE.properties");
								return url;			
				}		
	       }
