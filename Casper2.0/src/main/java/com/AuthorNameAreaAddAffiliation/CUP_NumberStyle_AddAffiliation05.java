package com.AuthorNameAreaAddAffiliation;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.AuthorName.Pom;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_NumberStyle_AddAffiliation05 extends CUP_BaseClass{
	
	
	static String remark;
	static String className = "CUP_NumberStyle_AddAffiliation05";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    static Pom PomObj;
	
	
	
	public void AddAffilation05() throws Throwable{
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Number Style Add Affiliation-->Author name right click and add affiliation insert text Department Name, Address, City, State, Post Code, Country and then save, check whether save button through Institution warning error or not";		    
			area = "Author Name";
				category = "Add affiliation,Institution TextBox Should Empty";
									
		
										
			       System.out.println("BrowerName->"+uAgent);			                     
										
		              
				          Switch switc = new Switch(driver);				        						   
					           switc.SwitchCase(uAgent); 
										     
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
								    												    
					PomObj =  new Pom();  
						
		WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
	WebElement ele= driver.findElement(By.xpath(PomObj.Turgeon()));  

			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
						  	          		    
	 String value = ele.getAttribute("innerHTML");
						  	          		
		 if(value.contains(value)){
						          	
				MoveToElement.byXpath(driver,PomObj.Turgeon());	      

						 Actions action= new Actions(driver);
				WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
						  	 action.contextClick(ele).build().perform();
						  	          					
			 driver.switchTo().defaultContent();		
						  	          				  		  
		Actions actions = new Actions(driver);	
						  	          	 
		Thread.sleep(2000);

			actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
			actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
			        actions.sendKeys(Keys.ENTER).build().perform();

				  /***********************************************************************/     

			        PomObj =  new Pom();  
			        
 WebElement ele1=null;

				driver.switchTo().defaultContent();	

				 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Position());
				    ele1= driver.findElement(By.cssSelector(PomObj.Position()));  
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Position());
				ele1.click();
				   ele1.clear();
				      ele1.sendKeys("PFG");


				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.DepartmentName());
				ele1 = driver.findElement(By.cssSelector(PomObj.DepartmentName()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.DepartmentName());
				      ele1.click();
				 ele1.sendKeys("Electirical");	




				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Address());
				ele1 = driver.findElement(By.cssSelector(PomObj.Address()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Address());
				      ele1.click();
				  ele1.sendKeys("ManiyataTechPark, Roadno-62");


				  
				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.City());
				ele1 = driver.findElement(By.cssSelector(PomObj.City()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.City());
				    ele1.click();
				  ele1.sendKeys("Los vega");


				  
				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.State());
				ele1 = driver.findElement(By.cssSelector(PomObj.State()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.State());
				   ele1.click();
				 ele1.sendKeys("Calafoniya");


				 
				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Post_Code());
				ele1 = driver.findElement(By.cssSelector(PomObj.Post_Code()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Post_Code());
				   ele1.click();
				 ele1.sendKeys("560037");


				 
				WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Country());
				ele1 = driver.findElement(By.cssSelector(PomObj.Country()));
				WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Country());
				     ele1.click();
				
	Select dropdown = new Select(driver.findElement(By.cssSelector(PomObj.Country())));
    			  Thread.sleep(2000);
    			          dropdown.selectByVisibleText("India");
				  
				  
				   WaitFor.presenceOfElementByXpath(driver,PomObj.Save());
				ele1 = driver.findElement(By.xpath(PomObj.Save()));
				   WaitFor.clickableOfElementByXpath(driver, PomObj.Save());
				                    ele1.click();
				}
				
				
				 try {
		WaitFor.presenceOfElementByXpath(driver,PomObj.validateInstitution());
		WebElement affilation = driver.findElement(By.xpath(PomObj.validateInstitution()));	   
					
		     String ActualValue = affilation.getText();
		     String ExpectedValue="Institution name is required field for Adding Affiliation !!";
		
		     System.out.println("Actual value-->"+ActualValue);
		     
		    if(ActualValue.contains(ExpectedValue)==true) {

		status ="Pass";
		
		 MyScreenRecorder.stopRecording(); 		
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);                        
		    	     	util.deleteRecFile(RecVideo);

		}else {
			
		remark="Institution Field was empty Pop didn't appear";	
		 
		           status ="Fail"; 
		 
		 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
	    	      	              util.MoveRecFile(RecVideo);
		

		utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
					   
					 
		if(ActualValue.contains(ExpectedValue)==true) {
		System.out.println("E1: No issues");
					 	
					   }
		}
				} catch (Exception e) {
					
					status ="Fail"; 
					 
					 MyScreenRecorder.stopRecording();
	String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
				    	      	  util.MoveRecFile(RecVideo);
				    	      	remark="Institution Field was empty Pop didn't appear";		
				}finally{
					
					System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent); 
				}
				
			}

			
			
 @Test(alwaysRun = true)
	public void CupNumber_Style_Add_AffiliationTest05() throws Throwable {
			
				try{
					
					MyScreenRecorder.startRecording(uAgent,className); 
				
	CUP_NumberStyle_AddAffiliation05  obj =	new CUP_NumberStyle_AddAffiliation05();
					       obj.AddAffilation05();

				
						 }catch (Exception e){
							e.getStackTrace();
						 }
					 }	
		        }
