package com.AuthorNameAreaAddAffiliation;


import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import com.AuthorName.Pom;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_NumberStyle_AddAffiliation02 extends CUP_BaseClass {
	
	static String remark;
	static String className="CUP_NumberStyle_AddAffiliation02";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    static Pom PomObj;
	
	
	public void AddAffilationPositionSymbol() throws Throwable {
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Number Style Add Affiliation-->Author name right click and add affiliation position enter symbols, check whether error through or not";    
			area = "Author Name";
				 category = "add affiliation, position-Enter Symbol";
							  
			
						
						
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
								
                System.out.println("BrowerName->"+uAgent);                 
        
          
	         Switch switc = new Switch(driver);
	             switc.SwitchCase(uAgent); 
								     
		driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
					    
		         PomObj=new Pom();  
			       
			          
			          WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
			       WebElement ele= driver.findElement(By.xpath(PomObj.Turgeon()));  

			          	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
			          		    
			          String value = ele.getAttribute("innerHTML");
			          		
			          if(value.contains(value)){
		        	
			          			MoveToElement.byXpath(driver,PomObj.Turgeon());	      

			          			Actions action= new Actions(driver);
			          	WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
			          			action.contextClick(ele).build().perform();
			          					
			                   driver.switchTo().defaultContent();		
			          				  		  
			          	 Actions actions = new Actions(driver);	
			          	 
			          Thread.sleep(2000);

			          actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
			          actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
			          actions.sendKeys(Keys.ENTER).build().perform();
			          
		     /*******************************************************************************/	          
			
			          PomObj = new Pom();
		              WebElement ele1=null;

		driver.switchTo().defaultContent();	

		 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Position());
		    ele1= driver.findElement(By.cssSelector(PomObj.Position()));  
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Position());
		ele1.click();
		   ele1.clear();
		      ele1.sendKeys("@");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.DepartmentName());
		ele1 = driver.findElement(By.cssSelector(PomObj.DepartmentName()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.DepartmentName());
		      ele1.click();
		 ele1.sendKeys("Computer Science");	


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.InstitudtionName());
		ele1 = driver.findElement(By.cssSelector(PomObj.InstitudtionName()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.InstitudtionName());
		      ele1.click();
		 ele1.sendKeys("Preetish Univercity");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Address());
		ele1 = driver.findElement(By.cssSelector(PomObj.Address()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Address());
		      ele1.click();
		  ele1.sendKeys("TechPark, Roadno-62");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.City());
		ele1 = driver.findElement(By.cssSelector(PomObj.City()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.City());
		    ele1.click();
		  ele1.sendKeys("Bangalore");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.State());
		ele1 = driver.findElement(By.cssSelector(PomObj.State()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.State());
		   ele1.click();
		 ele1.sendKeys("Karnataka");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Post_Code());
		ele1 = driver.findElement(By.cssSelector(PomObj.Post_Code()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Post_Code());
		   ele1.click();
		 ele1.sendKeys("560037");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Country());
		ele1 = driver.findElement(By.cssSelector(PomObj.Country()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Country());
		     ele1.click();
		  
		     Select dropdown = new Select(driver.findElement(By.cssSelector(PomObj.Country())));
		     Thread.sleep(2000);
		               dropdown.selectByVisibleText("India");

	     
		  
		 boolean bst = CUP_NumberStyle_AddAffiliation02.isClickable();  
			
		 if(bst==true){
			 
			 remark="In Postion area- Enter Symbol Its Saved Pop Dint Appear";	
		    	 
      		  status ="Fail"; 
      		  
      		MyScreenRecorder.stopRecording();
    String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
      	                        util.MoveRecFile(RecVideo);
      		  
      		    	  
      Thread.sleep(10000);  
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		       }else{
		    	   
		     status ="Pass";
			 
		     MyScreenRecorder.stopRecording(); 		
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);                        
			                  util.deleteRecFile(RecVideo);
		     
		     
		           }		        
		        }	    
	        }      
			                   
			 
	
 private static boolean isClickable(){
	 
	 WebElement ele1;
	 
	            try{
	                  	WaitFor.presenceOfElementByXpath(driver,PomObj.Save());
	                   	 ele1 = driver.findElement(By.xpath(PomObj.Save()));
	            
	                   	 System.out.println("PKM-->"+ele1.isEnabled());
	                   WaitFor.clickableOfElementByXpath(driver, PomObj.Save());
	                    		                 ele1.click();
	                    			                                             
	                       return true;
	                    }
	                    catch (Exception e)
	                    {
	                      return false;
	                  }                 
	              }
			          
			          
			
 
			   @Test(alwaysRun = true)
			   
			public void Number_Style_Add_AffiliationTest02() throws Throwable {
			
				try{
						 
					 MyScreenRecorder.startRecording(uAgent,className);
		CUP_NumberStyle_AddAffiliation02  obj =	new CUP_NumberStyle_AddAffiliation02();
					       obj.AddAffilationPositionSymbol();

				
						 }catch (Exception e){
							e.getStackTrace();
						 }finally {	    	
				                System.out.println(className);
				     	       Ex.testdata(description, className, remark, category, area, status, uAgent);  	 
				     	               }
					 }	
				}	
