package com.AuthorNameAreaAddAffiliation;

public class Pom {
	
	
	private static String element = null;
	
	
	
	 public String Mahmoud() {
		 element="//*[text()='Mahmoud']";
		 
		 return element;
	 }
	
	
	
	public String RENEE() {
		element = "//*[text()='RENEE']";
				return element;
	}
	
	
	public String GRENON(){
		element = "//*[text()='GRENON']";
		return element;
	}
	
	
	public String SAMANTHA(){
		element = "//*[text()='SAMANTHA']";
		return element;
	}
	
	public String Position() {
		
		element = "#aff_cite";
		return element;
	}
	
	public String DepartmentName() {
		 		
		 element="#department_name";		 
		       return element;
	 }
	
	public String InstitudtionName() {
		 	
		 element="#institution_name_2";		 
		       return element;
	 }
	
	public String Address() {
		 		
		 element="#aff_address";		 
		       return element;
	 }
	
	
	public String City() {
		
		 element="#aff_city";		 
	       return element;
	}
	
	
	public String State() {
		
		 element="#aff_state";		 
	       return element;
	}
	
	
	public String Post_Code() {
		
		 element="#aff_postal";		 
	       return element;
	}
	
	
	public String Country(){
		
		 element="#aff_country";		 
	       return element;
	}
	
	
	public String Save() {
		element="//div[@class='modal-dialog bs-example-modal-lg']//div[@class='modal-body form-horizontal']//input[@value='Save']";
		return element;
	}
	
	
	public String Label() {
		element="";
		return element;
	}
	
	
	public String validateAffilation() {
		element="[ref-type='aff']";
		return element;
	}
	
	
	public String validateInstitution() {
		element="//p[contains(text(),'Institution name is required field for Adding Affi')]";
		return element;
	}
	
	
	public String validateCity() {
		element="//p[contains(text(),'City is required field for Adding Affiliation !!')]";
		return element;
	}
	
	
	public String validateCountry() {
		element="//p[contains(text(),'Country is required field for Adding Affiliation !')]";
		return element;
	}
	
	
 }
