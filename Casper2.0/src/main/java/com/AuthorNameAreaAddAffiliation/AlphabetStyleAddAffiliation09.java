package com.AuthorNameAreaAddAffiliation;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.page.UpperToolBar;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;




public class AlphabetStyleAddAffiliation09 extends AlphabetBaseClass{

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
	static com.AuthorNameAreaAddAffiliation.Pom Pom;
	
	
	
	public void AddAffilation_CheckLabel() throws Exception {
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
 description = "TandF_PE Alphabet Style Add Affiliation-->Author name right click and add affiliation insert text Department Name, Institution Name, Address, City, State, Post Code, Country and then save, check the affiliation label and text data lose or not";
		className = "AlphabetStyleAddAffiliation09";    
			area = "Author Name";
			    category = "Add affiliation, Check Label is Present or not";
			   
			    driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
				
				System.out.println("BrowerName->"+uAgent);			                     
						
         Cookies cokies =new Cookies(driver);
         Switch switc = new Switch(driver);
             	     		      
		 /*C1toC2 url = new C1toC2(driver);
		url.TandFAuthorChangeC1toC2(uAgent);*/
			       			      
			      cokies.cookies();		   
	           switc.SwitchCase(uAgent);        					     
						     
	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
			    
	          Pom =  new com.AuthorNameAreaAddAffiliation.Pom();  
	          WaitFor.presenceOfElementByXpath(driver, Pom.Mahmoud());
	          WebElement ele= driver.findElement(By.xpath(Pom.Mahmoud()));  

	          	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
	          		    
	          String value = ele.getAttribute("innerHTML");
	          		
	          if(value.contains(value)){
        	
	          			MoveToElement.byXpath(driver,Pom.Mahmoud());	      

	          			Actions action= new Actions(driver);
	          	WaitFor.presenceOfElementByXpath(driver, Pom.Mahmoud());
	          			action.contextClick(ele).build().perform();
	          					
	                   driver.switchTo().defaultContent();		
	          				  		  
	          	 Actions actions = new Actions(driver);	
	          	 
	          Thread.sleep(2000);

	          actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
	          actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
	          actions.sendKeys(Keys.ENTER).build().perform();
	          
     /*******************************************************************************/	          
	
	          Pom = new com.AuthorNameAreaAddAffiliation.Pom();
              WebElement ele1=null;

driver.switchTo().defaultContent();	

 WaitFor.presenceOfElementByCSSSelector(driver,Pom.Position());
    ele1= driver.findElement(By.cssSelector(Pom.Position()));  
WaitFor.clickableOfElementByCSSSelector(driver, Pom.Position());
ele1.click();
   ele1.clear();
      ele1.sendKeys("q");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.DepartmentName());
ele1 = driver.findElement(By.cssSelector(Pom.DepartmentName()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.DepartmentName());
      ele1.click();
 ele1.sendKeys("Computer Science");	


WaitFor.presenceOfElementByCSSSelector(driver,Pom.InstitudtionName());
ele1 = driver.findElement(By.cssSelector(Pom.InstitudtionName()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.InstitudtionName());
      ele1.click();
 ele1.sendKeys("Preetish Univercity");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.Address());
ele1 = driver.findElement(By.cssSelector(Pom.Address()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.Address());
      ele1.click();
  ele1.sendKeys("TechPark, Roadno-62");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.City());
ele1 = driver.findElement(By.cssSelector(Pom.City()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.City());
    ele1.click();
  ele1.sendKeys("Bangalore");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.State());
ele1 = driver.findElement(By.cssSelector(Pom.State()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.State());
   ele1.click();
 ele1.sendKeys("Karnataka");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.Post_Code());
ele1 = driver.findElement(By.cssSelector(Pom.Post_Code()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.Post_Code());
   ele1.click();
 ele1.sendKeys("560037");


WaitFor.presenceOfElementByCSSSelector(driver,Pom.Country());
ele1 = driver.findElement(By.cssSelector(Pom.Country()));
WaitFor.clickableOfElementByCSSSelector(driver, Pom.Country());
     ele1.click();
  ele1.sendKeys("India");
  
  WaitFor.presenceOfElementByXpath(driver,Pom.Save());
  ele1 = driver.findElement(By.xpath(Pom.Save()));
     WaitFor.clickableOfElementByXpath(driver, Pom.Save());
                      ele1.click();
                      
                      
                    UpperToolBar obj1 = new UpperToolBar(driver);
  				       obj1.save_btn_method(); 
	
	
}
	          
	          try {
		          
		            String expectedValue=null;
		            String affilationValue=null;
		          	String ListOfaffilation = "";
		          	
		          	
		                      switc.SwitchCase(uAgent);
		                      
		        WaitFor.presenceOfElementByCSSSelector(driver,Pom.validateAffilation());
	List<WebElement> affilation = driver.findElements(By.cssSelector(Pom.validateAffilation()));	   
		               
		          for(WebElement ele1:affilation) {
		          	affilationValue = ele1.getText();
		          	
		          	ListOfaffilation+=affilationValue + "\n";
		          	  
		          	       expectedValue = "f";
		          	
		          //System.out.println("After Save Affilation value-->"+ListOfaffilation);

		          }

		          if(ListOfaffilation.contains(expectedValue)==true){

		          status ="Pass";
		          	 }else{
		          		 
		          remark="Add Affiliation, Label is no present";	
		          		    	 
		          		 status ="Fail"; 
		          		    	  
		          Thread.sleep(10000);  

		          utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
		          			   
		          			 
		          if(affilationValue.contains(expectedValue)==true) {
		          System.out.println("E1: No issues");
		          			 	
		                         }
		          	       }			    	  
		          }catch (Exception e) {
		          	
		          	e.printStackTrace();
		          }finally {	    	
		                System.out.println(className);
		       Ex.testdata(description, className, remark, category, area, status, uAgent);  	 
		               }
		          
		   }


	   @Test(alwaysRun = true)
	public void Alphabet_Style_Add_AffiliationTest02() {
	
		try{
				 
				
	AlphabetStyleAddAffiliation09 obj = new  AlphabetStyleAddAffiliation09();
		           obj.AddAffilation_CheckLabel();
		
				 }catch (Exception e){
					e.getStackTrace();
				 }
			 }	
		  
		}	
	
	