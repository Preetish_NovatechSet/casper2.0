package com.AuthorNameAreaAddAffiliation;

import utilitys.DataProviderFactory;

public class StyleAddAffiliationUrlRead {
	
	
	/************************************************************************************************/	
	public static class FireFoxAlphabetStyleAddAffiliationUrl{
			
		public static String url;
			 
				 public static String getFireFoxUrl() {
				     url=  DataProviderFactory.getAlphabetStyleAddAffiliationUrl().geturlForFireFox();
				         

					 return url;
				    }
				}


	/************************************************************************************************/

	public static class IEAlphabetStyleAddAffiliationUrl{
			  public static String url;
			 
				 public static String getIEUrl() {
				         url=  DataProviderFactory.getAlphabetStyleAddAffiliationUrl().geturlForIE();
				         
					 return url;
				   }
				}

	/************************************************************************************************/	

	public static class ChromeAlphabetStyleAddAffiliationUrl {	
				
		public static  String url;
								
				 	
			public static String getChromeUrl() {
					      url=  DataProviderFactory.getAlphabetStyleAddAffiliationUrl().geturlForChrome();
					      

				    return url;
				     }  
				
			    }
			
/************************************************************************************************/
			
	public static class OperaAlphabetStyleAddAffiliationUrl {	
				
				public static  String url;
				 	
				public static String getOperaUrl() {
					      url=  DataProviderFactory.getAlphabetStyleAddAffiliationUrl().geturlForOpera();
					      
				    return url;
				     }  
				
			    }

/************************************************************************************************/

	public static class EdgeAlphabetStyleAddAffiliationUrl {	
		
		public static  String url;
		 	
		public static String getEdgeUrl() {
			      url=  DataProviderFactory.getAlphabetStyleAddAffiliationUrl().geturlForEdge();
			      
		    return url;
		     }  
		
	    }
	
	
/*******************************NumberStyleAddAffiliation*********************************/	
	             /***********************************************/
	
	
	public static class FireFoxNumberStyleAddAffiliationUrl{
			
		public static String url;
			 
				 public static String getFireFoxUrl() {
				     url=  DataProviderFactory.getNumberStyleAddAffiliationUrl().geturlForFireFox();
				         

					 return url;
				    }
				}


/************************************************************************************************/

	public static class IENumberStyleAddAffiliationUrl{
			  public static String url;
			 
				 public static String getIEUrl() {
				         url=  DataProviderFactory.getNumberStyleAddAffiliationUrl().geturlForIE();
				         
					 return url;
				   }
				}

	/************************************************************************************************/	

	public static class ChromeNumberStyleAddAffiliationUrl {	
				
		public static  String url;
								
				 	
			public static String getChromeUrl() {
					      url=  DataProviderFactory.getNumberStyleAddAffiliationUrl().geturlForChrome();
					      

				    return url;
				     }  
				
			    }
			
/************************************************************************************************/
			
	public static class OperaNumberStyleAddAffiliationUrl {	
				
				public static  String url;
				 	
				public static String getOperaUrl() {
					      url= DataProviderFactory.getNumberStyleAddAffiliationUrl().geturlForOpera();
					      
				    return url;
				     }  
				
			    }

/************************************************************************************************/

	public static class EdgeNumberStyleAddAffiliationUrl {	
		
		public static  String url;
		 	
		public static String getEdgeUrl() {
			      url=  DataProviderFactory.getNumberStyleAddAffiliationUrl().geturlForEdge();
			      
		    return url;
		     }  		
	     }
     }
