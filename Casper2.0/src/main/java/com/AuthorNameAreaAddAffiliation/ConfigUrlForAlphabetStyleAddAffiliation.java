package com.AuthorNameAreaAddAffiliation;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigUrlForAlphabetStyleAddAffiliation {

	
	static Properties pro;
	
	
	
	
	public static void ReadPath(String PathofXML)
	{
		
		String sourcePath = PathofXML;
		
		File src=new File(sourcePath);
		
		try 
		{
			FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		
		  {
			System.out.println("Exception is "+e.getMessage());
		  }
		
	   }
	
	
	
	
	public String geturlForChrome()
	{
		
		
ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Chrome.properties");
	
		String Table2_Aspirations=pro.getProperty("ChromeUrl");
		return Table2_Aspirations;
	}
	
	
	public String geturlForFireFox()
	{
		
		
ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Firefox.properties");
	
		String Table2_Aspirations=pro.getProperty("FireFoxUrl");
		return Table2_Aspirations;
	}
	
	
	public String geturlForOpera()
	{
		

ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Opera.properties");
	
		String Table2_Aspirations=pro.getProperty("OperaUrl");
		return Table2_Aspirations;
	}
	
	
	public String geturlForIE()
	{
			
ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_IE.properties");
	
		String Table2_Aspirations=pro.getProperty("IEUrl");
		return Table2_Aspirations;
	}
	
	
	public String geturlForEdge()
	{
		
ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\AlphabetStyleAddAffiliation_Edge.properties");
	
		String Table2_Aspirations=pro.getProperty("EdgeUrl");
		return Table2_Aspirations;
	}
	
/***************************NumberStyleAddAffiLiation**********************************/
	
public static class ConfigUrlForNumberStyleAddAffiliation {

		
		
			
		
		public String geturlForChrome()
		{
			
			
	ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Chrome.properties");
		
			String Table2_Aspirations=pro.getProperty("ChromeUrl");
			return Table2_Aspirations;
		}
		
		
		public String geturlForFireFox()
		{
			
			
	ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Firefox.properties");
		
			String Table2_Aspirations=pro.getProperty("FireFoxUrl");
			return Table2_Aspirations;
		}
		
		
		public String geturlForOpera()
		{
			

	ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Opera.properties");
		
			String Table2_Aspirations=pro.getProperty("OperaUrl");
			return Table2_Aspirations;
		}
		
		
		public String geturlForIE()
		{
				
	ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_IE.properties");
		
			String Table2_Aspirations=pro.getProperty("IEUrl");
			return Table2_Aspirations;
		}
		
		
		public String geturlForEdge()
		{
			
	ConfigUrlForAlphabetStyleAddAffiliation.ReadPath("./Configfolder\\AlphabetStyleAddAffiliation\\NumberStyleAddAffiliation_Edge.properties");
		
			String Table2_Aspirations=pro.getProperty("EdgeUrl");
			return Table2_Aspirations;
		}
	
	
     }	
	
}
