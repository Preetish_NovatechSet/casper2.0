package com.AuthorNameAreaAddAffiliation;



import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import com.AuthorName.Pom;
import com.FigureContain.CUP_BaseClass;
import core.MoveToElement;
import core.MyScreenRecorder;
import core.WaitFor;
import utilitys.ConstantPath;
import utilitys.Excel;
import utilitys.Switch;
import utilitys.util;



public class CUP_NumberStyle_AddAffiliation09 extends CUP_BaseClass{

	
	
	static String remark;
	static String className = "CUP_NumberStyle_AddAffiliation09";
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex;
    static Pom PomObj;
	
	
	public void AddAffilation_CheckLabel() throws Throwable {
		
		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "Casper-CUP_CopyEditor-Number Style Add Affiliation-->Author name right click and add affiliation insert text Department Name, Institution Name, Address, City, State, Post Code, Country and then save, check the affiliation label and text data lose or not";  
			area = "Author Name";
			     category = "Add affiliation, Check Label is Present or not";
							  
			     driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
					
			       System.out.println("BrowerName->"+uAgent);                 
			    
			      
			         Switch switc = new Switch(driver);
			             switc.SwitchCase(uAgent); 
										     
driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);
							    
				         PomObj=new Pom(); 
				         
				         
			          WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
			          WebElement ele= driver.findElement(By.xpath(PomObj.Turgeon()));  

			          	driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);      
			          		    
			          String value = ele.getAttribute("innerHTML");
			          		
			          if(value.contains(value)){
		        	
			          			MoveToElement.byXpath(driver,PomObj.Turgeon());	      

			          			Actions action= new Actions(driver);
			          	WaitFor.presenceOfElementByXpath(driver, PomObj.Turgeon());
			          			action.contextClick(ele).build().perform();
			          					
			                   driver.switchTo().defaultContent();		
			          				  		  
			          	 Actions actions = new Actions(driver);	
			          	 
			          Thread.sleep(2000);

			          actions.sendKeys(Keys.ARROW_DOWN).build().perform(); Thread.sleep(1000);				
			          actions.sendKeys(Keys.ARROW_RIGHT).build().perform();Thread.sleep(1000);
			          actions.sendKeys(Keys.ENTER).build().perform();
			          
		 /*******************************************************************************/	     
			          
			          PomObj=new Pom();
			          
WebElement ele1=null;

		driver.switchTo().defaultContent();	

		 WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Position());
		    ele1= driver.findElement(By.cssSelector(PomObj.Position()));  
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Position());
		ele1.click();
		   ele1.clear();
		      ele1.sendKeys("1");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.DepartmentName());
		ele1 = driver.findElement(By.cssSelector(PomObj.DepartmentName()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.DepartmentName());
		      ele1.click();
		 ele1.sendKeys("Computer Science");	


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.InstitudtionName());
		ele1 = driver.findElement(By.cssSelector(PomObj.InstitudtionName()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.InstitudtionName());
		      ele1.click();
		 ele1.sendKeys("Preetish Univercity");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Address());
		ele1 = driver.findElement(By.cssSelector(PomObj.Address()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Address());
		      ele1.click();
		  ele1.sendKeys("TechPark, Roadno-62");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.City());
		ele1 = driver.findElement(By.cssSelector(PomObj.City()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.City());
		    ele1.click();
		  ele1.sendKeys("Bangalore");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.State());
		ele1 = driver.findElement(By.cssSelector(PomObj.State()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.State());
		   ele1.click();
		 ele1.sendKeys("Karnataka");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Post_Code());
		ele1 = driver.findElement(By.cssSelector(PomObj.Post_Code()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Post_Code());
		   ele1.click();
		 ele1.sendKeys("560037");


		WaitFor.presenceOfElementByCSSSelector(driver,PomObj.Country());
		ele1 = driver.findElement(By.cssSelector(PomObj.Country()));
		WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Country());
		     ele1.click();
		  ele1.sendKeys("India");
		  
		  WaitFor.presenceOfElementByXpath(driver,PomObj.Save());
		  ele1 = driver.findElement(By.xpath(PomObj.Save()));
		     WaitFor.clickableOfElementByXpath(driver, PomObj.Save());
		                      ele1.click();
			
			
		}
			          try {
				          
				            String expectedValue=null;
				            String affilationValue=null;
				          	String ListOfaffilation = "";
				          	
				          	
				                      switc.SwitchCase(uAgent);
				                      
			WaitFor.presenceOfElementByCSSSelector(driver,PomObj.validateAffilation());
	List<WebElement> affilation = driver.findElements(By.cssSelector(PomObj.validateAffilation()));	   
				               
				          for(WebElement ele1:affilation) {
				          	affilationValue = ele1.getText();
				          	
				          	ListOfaffilation+=affilationValue + "\n";
				          	  
				          	       expectedValue = "3";
				          	
				          //System.out.println("After Save Affilation value-->"+ListOfaffilation);

				          }

				          if(ListOfaffilation.contains(expectedValue)==true){

				          status ="Pass";
				          
				          MyScreenRecorder.stopRecording(); 		
	 String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);                        
				           util.deleteRecFile(RecVideo);		          
				          
				          	 }else{
				          		 
				          remark="Add Affiliation, Label is no present";	
				          		    	 
				          		 status ="Fail"; 
				          		 
				        MyScreenRecorder.stopRecording();
String RecVideo =util.ReadtheListOfFileAndDeleteinPass(ConstantPath.PathForVideoRecord);   	 
				     		 util.MoveRecFile(RecVideo);			          		 
				          		 
				          		    	  
				          Thread.sleep(10000);  

				          utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
				          			   
				          			 
				          if(affilationValue.contains(expectedValue)==true) {
				          System.out.println("E1: No issues");
				          			 	
				                         }
				          	       }			    	  
				          }catch (Exception e) {
				          	
				          	e.printStackTrace();
				          }finally {	    	
				             System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);  	 
				               }
				         }
			
			
			
			
			   @Test(alwaysRun = true)
			public void CupNumber_Style_Add_AffiliationTest09() throws Throwable {
			
				try{
					
					MyScreenRecorder.startRecording(uAgent,className);  	
		CUP_NumberStyle_AddAffiliation09  obj =	new CUP_NumberStyle_AddAffiliation09();
					          obj.AddAffilation_CheckLabel();
				
						 }catch (Exception e){
							e.getStackTrace();
						 }
					 }	
				}

