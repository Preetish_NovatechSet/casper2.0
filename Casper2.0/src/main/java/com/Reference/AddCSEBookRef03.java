package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEBookRef03 extends JournalRefBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether 2 author name and citation, year, Publisher,Book title, City, displayed correctly with sorting Alphabets";
		 className = "AddCSEBookRef03";    
				area = "Book Reference";
					category = "CSE Style";
					                				                   
				
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					              cokies.cookies();
					                
					             
					       Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					           PomObj = new com.Reference.Pom();		
					           
				WebElement ele = driver.findElement(By.xpath(PomObj.TableCaption3()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					MoveToElement.byXpath(driver, PomObj.TableCaption3());    
					          ele.click();
					          
					          
					          driver.switchTo().defaultContent();
					          
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Book");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("2017");
				          
					     
					        
				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.sendKeys("Dulu");      
					          
							
							
					ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
						    ele.click(); ele.sendKeys("K");  
						    
						    ele=driver.findElement(By.xpath(PomObj.Surname2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
								ele.click(); ele.sendKeys("Pintu");      
						          
								
								
						ele=driver.findElement(By.xpath(PomObj.given2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
							    ele.click(); ele.sendKeys("M");  
						    
					      
						    
						    ele=driver.findElement(By.xpath(PomObj.Year()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
								ele.click(); ele.sendKeys("2017");



            ele=driver.findElement(By.xpath(PomObj.Publisher()));
       WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
             ele.click();ele.sendKeys("Preetish"); 
	      

    ele=driver.findElement(By.xpath(PomObj.Book_Title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
	ele.click(); ele.sendKeys("Artifficial"); 

	
	
	ele=driver.findElement(By.xpath(PomObj.City()));
	WaitFor.clickableOfElementByXpath(driver, PomObj.City());
		ele.click(); ele.sendKeys("India");
		
		
		
		
		
ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
			/*	UpperToolBar obj1 = new UpperToolBar(driver);
				      obj1.save_btn_method(); */
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		Switch switc = new Switch(driver);
            switc.SwitchCase(uAgent);
            
            WebElement ele = driver.findElement(By.xpath(PomObj.ref2017()));
		      
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
										
						MoveToElement.byXpath(driver, PomObj.ref2017());    
										    ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Dulu K, Pintu M. 2017. Artifficial. India: Preetish."; 
			 
			
			    WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Add Journal trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test2() throws Exception {
		         	
		         		AddCSEBookRef03 bj = new AddCSEBookRef03();
		         	           bj.CSE();
		         	
		         	   }
               }
