package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEBookRef08 extends JournalRefBaseClass {

	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj; 

    
 public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether Collab and citation, year, Book title,First Last page, City,displayed correctly with sorting Alphabets";
		 className = "AddCSEBookRef08";    
				area = "Book Reference";
					category = "CSE Style";
					                				                   
				
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					              cokies.cookies();
					                
					             
					       Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					           PomObj = new com.Reference.Pom();		
					           
				WebElement ele = driver.findElement(By.xpath(PomObj.Conclusion()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					MoveToElement.byXpath(driver, PomObj.Conclusion());    
					          ele.click();
					          
					          
					 driver.switchTo().defaultContent();
					          
	ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));     
	       MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
	WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
	           ele.click();
					     
					          
	Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		    WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                    dropdown.selectByVisibleText("Book");
                               
            	   
WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
	  ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
      WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
	      ele.click();   ele.sendKeys("2015");
				          
					     
	
					      
						 
ele=driver.findElement(By.xpath(PomObj.Collab()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
ele.click(); ele.sendKeys("Piku");  



	ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
	ele.click();   ele.sendKeys("2015");

	

    ele=driver.findElement(By.xpath(PomObj.Book_Title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
	ele.click();   ele.sendKeys("Artifficial"); 

		 ele=driver.findElement(By.xpath(PomObj.Publisher()));
	 WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
	 	ele.click();   ele.sendKeys("Gautam"); 
	
	 ele=driver.findElement(By.xpath(PomObj.City()));
WaitFor.clickableOfElementByXpath(driver, PomObj.City());
	 ele.click();  ele.sendKeys("India");
		
		
	 ele=driver.findElement(By.cssSelector(PomObj.Save()));
	 WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
	 				ele.click();
	 
	 
				
        }catch(Exception e){
              e.printStackTrace();
      }
    	
    	
    try {
    	
    	Switch switc = new Switch(driver);
        switc.SwitchCase(uAgent);
        
        WebElement ele = driver.findElement(By.xpath(PomObj.ref2015()));
	      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
									
						MoveToElement.byXpath(driver, PomObj.ref2015());    
									       ele.click();
									       
        
		String Actualvalue =null;
		String ListOfJournal = null;
		String ExpectedValue="Piku. 2015. Artifficial. India: Gautam."; 
		 
		
		WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
		List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
	
		
		             for(WebElement ele1:el){
		        	   Actualvalue =ele1.getText();
		                	
		             ListOfJournal+=Actualvalue + "\n";
		   	System.out.println("PKM--->"+ListOfJournal);
		         	   
					    }
		             
		     if(ListOfJournal.contains(ExpectedValue)==true) {
		        	  status ="Pass";
		        	  
		        }else{
		        	
	 remark="Add Journal trac is missing after Save / Alphabet Sorting is not Correct";	
          		    	 
		          		 status ="Fail"; 
		        }			     
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		        System.out.println(className);
		 Ex.testdata(description, className, remark, category, area, status, uAgent);
	             }
			 }
 
 
 @Test(alwaysRun=true)
	
	public void test8() throws Exception {
	         	
	         		AddCSEBookRef08 bj = new AddCSEBookRef08();
	         	               bj.CSE();
	         	
	         	   }
             }
