package com.Reference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEOtherRef08 extends JournalRefBaseClass{
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether 1 author name, year & OtherInfo, & Check Weather Citation Pop displayed or not";
		 className = "AddCSEOtherRef08";    
				area = "Others Reference";
					category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
				
					                    
			WebElement ele = driver.findElement(By.xpath(PomObj.T4_Col1()));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
				 MoveToElement.byXpath(driver, PomObj.T4_Col1());    
					          ele.click();
					          
					         
					          
					          driver.switchTo().defaultContent();
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));				       			
					  MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		        WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Others");
                               
            	               
					        
	    ele =driver.findElement(By.xpath(PomObj.Surname1()));
	         ele.click();ele.sendKeys("Zaber");				        
					          					
			
	         ele=driver.findElement(By.xpath(PomObj.given1()));
	          ele.click();ele.sendKeys("Q");
	          
	            
				ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
				  ele.click(); ele.sendKeys("2017");

	      

    ele=driver.findElement(By.xpath(PomObj.Other_Info()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Other_Info());
    ele.click();ele.sendKeys("Sing a song");
	
			
   ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
			/*	UpperToolBar obj1 = new UpperToolBar(driver);
				      obj1.save_btn_method(); */
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		 WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
				
		       			
 		String Actualvalue =ele.getAttribute("innerHTML");
 	String ExpectedValue="Please enter Cited Name !"; 
 			 
 		

 			
 			  if(Actualvalue.contains(ExpectedValue)==true) {
 				 
 			    	status ="Pass";			    	

 			utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);
 			    }else{
 			    	  remark="Citation TextBox Was Empty But Pop-up didn't apper";	
 				    	 
 				        status ="Fail";  							 
 						
 			    } 
 		} catch (Exception e) {
 			
 			e.printStackTrace();
 		}finally {
 	        System.out.println(className);
 	 Ex.testdata(description, className, remark, category, area, status, uAgent);
              }
         }  
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test8() throws Exception {
		         	
	   AddCSEOtherRef08 bj = new AddCSEOtherRef08();
		         	         bj.CSE();
		         	
		         	   }
               }