package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.page.UpperToolBar;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEOtherRef02 extends JournalRefBaseClass{
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether Collab and year, OtherInfo style displayed correctly with sorting Alphabets";
		 className = "AddCSEOtherRef02";    
				area = "Others Reference";
					category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
				
					                    
			WebElement ele = driver.findElement(By.xpath(PomObj.Table_3Contant()));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
				 MoveToElement.byXpath(driver, PomObj.Table_3Contant());    
					          ele.click();
					          
					         
					          
					          driver.switchTo().defaultContent();
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));				       			
					  MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		        WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Others");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("1888");
				          
					     
					        
	    
					          					
							
					ele=driver.findElement(By.xpath(PomObj.Collab()));
			   WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
						 ele.click(); ele.sendKeys("Yyyy");  
						    
	
						    
				ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
				  ele.click(); ele.sendKeys("1888");

	      

    ele=driver.findElement(By.xpath(PomObj.Other_Info()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Other_Info());
    ele.click();ele.sendKeys("Work Out");
	
			
   ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
				UpperToolBar obj1 = new UpperToolBar(driver);
				      obj1.save_btn_method(); 
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		Switch switc = new Switch(driver);
               switc.SwitchCase(uAgent);
            
               
            WebElement ele = driver.findElement(By.xpath(PomObj.ref1888()));	      
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);									
						MoveToElement.byXpath(driver, PomObj.ref1888());    
										 ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Yyyy. 1888. Work Out."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test1() throws Exception {
		         	
	   AddCSEOtherRef02 bj = new AddCSEOtherRef02();
		         	         bj.CSE();
		         	
		         	   }
               }