package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCseConferenceRef02 extends JournalRefBaseClass{
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether 1 author name and Collab, year, Conference Title, Conference Name, Conference Date, Conference Place, comment style displayed correctly";
		 className = "AddCseConferenceRef02";    
				area = "Conference Reference";
					category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
					           
					      WebElement ele = driver.findElement(By.xpath(PomObj.Table5()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					 MoveToElement.byXpath(driver, PomObj.Table5());    
					          ele.click();
					          
					          
					          driver.switchTo().defaultContent();
					          
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Conference");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("1996");
				          
					     
					        
				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.sendKeys("Rupam");      
					          
							
							
					ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
						    ele.click(); ele.sendKeys("Kumar");  
						    
						    
						    ele=driver.findElement(By.xpath(PomObj.Collab()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
							    ele.click(); ele.sendKeys("UY");    
	
						    
						    ele=driver.findElement(By.xpath(PomObj.Year()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
								ele.click(); ele.sendKeys("1996");

	      

ele=driver.findElement(By.xpath(PomObj.Conference_title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conference_title());
ele.click(); ele.sendKeys("Selenium"); 
	
ele=driver.findElement(By.xpath(PomObj.Conf_Name()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Name());
ele.click(); ele.sendKeys("Zee");
			
ele=driver.findElement(By.xpath(PomObj.Conf_Place()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Place());
	ele.click(); ele.sendKeys("UK");
	
	
			
ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
		
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		Switch switc = new Switch(driver);
            switc.SwitchCase(uAgent);
            
            WebElement ele = driver.findElement(By.xpath(PomObj.ref1996()));
		      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
										
					MoveToElement.byXpath(driver, PomObj.ref1996());    
										 ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Rupam Kumar, UY. 1996. Selenium. Zee; UK."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Add Journal trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test1() throws Exception {
		         	
	   AddCseConferenceRef02 bj = new AddCseConferenceRef02();
		         	         bj.CSE();
                          }
		         	   }