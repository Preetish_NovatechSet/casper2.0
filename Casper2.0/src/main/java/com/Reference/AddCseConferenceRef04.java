package com.Reference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class AddCseConferenceRef04 extends JournalRefBaseClass{
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
    
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether 11 author name and collab  year, Conference Title, Conference Name,Conference Place, check weather PopUp displayed or not";
		 className = "AddCseConferenceRef04";    
				area = "Conference Reference";
					category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
					           
					      WebElement ele = driver.findElement(By.xpath(PomObj.Table3()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					 MoveToElement.byXpath(driver, PomObj.Table3());    
					          ele.click();
					          
					          
					          driver.switchTo().defaultContent();
					          
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Conference");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("1997");
				          
					     
					        
				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.sendKeys("Rupam");      
					          
							
							
					ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
						    ele.click(); ele.sendKeys("Kumar");  
						    
						    
							
							   ele=driver.findElement(By.xpath(PomObj.Surname2()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
						ele.click();     ele.sendKeys("Gadha");  
												    
							ele=driver.findElement(By.xpath(PomObj.given2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
							    ele.click(); ele.sendKeys("Na");  
						    
							    ele=driver.findElement(By.xpath(PomObj.Surname3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
									ele.click(); ele.sendKeys("R");      
							          
									
									
						ele=driver.findElement(By.xpath(PomObj.given3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
						ele.click(); ele.sendKeys("Sam");  


						for(int i=0;i<=7;i++) {
						ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
						          ele.click(); 

						}



						ele=driver.findElement(By.xpath(PomObj.Surname4()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
						ele.click(); ele.sendKeys("Rja");      



						ele=driver.findElement(By.xpath(PomObj.given4()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
						ele.click(); ele.sendKeys("P"); 




						ele=driver.findElement(By.xpath(PomObj.Surname5()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
						ele.click(); ele.sendKeys("Rani");      



						ele=driver.findElement(By.xpath(PomObj.given5()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
						ele.click(); ele.sendKeys("Pa"); 

						ele=driver.findElement(By.xpath(PomObj.Surname6()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname6());
						ele.click(); ele.sendKeys("Hj");      



						ele=driver.findElement(By.xpath(PomObj.given6()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given6());
						ele.click(); ele.sendKeys("Pra"); 


						ele=driver.findElement(By.xpath(PomObj.Surname7()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname7());
						ele.click(); ele.sendKeys("Dj");      



						ele=driver.findElement(By.xpath(PomObj.given7()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given7());
						ele.click(); ele.sendKeys("Pra"); 

						ele=driver.findElement(By.xpath(PomObj.Surname8()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname8());
						ele.click(); ele.sendKeys("Raaj");      



						ele=driver.findElement(By.xpath(PomObj.given8()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given8());
						ele.click(); ele.sendKeys("A"); 


						ele=driver.findElement(By.xpath(PomObj.Surname9()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname9());
						ele.click(); ele.sendKeys("Rjas");      



						ele=driver.findElement(By.xpath(PomObj.given9()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given9());
						ele.click(); ele.sendKeys("Pra"); 



						ele=driver.findElement(By.xpath(PomObj.Surname10()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
						ele.click(); ele.sendKeys("Rj");      



						ele=driver.findElement(By.xpath(PomObj.given10()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
						ele.click(); ele.sendKeys("P");   
								
							
						ele=driver.findElement(By.xpath(PomObj.Surname11()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
						ele.click(); ele.sendKeys("Seema");      


						ele=driver.findElement(By.xpath(PomObj.given11()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
						ele.click(); ele.sendKeys("laguri");  
						
						ele=driver.findElement(By.xpath(PomObj.Collab()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Collab());
						ele.click(); ele.sendKeys("laguri"); 
	
						    
						    ele=driver.findElement(By.xpath(PomObj.Year()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
								ele.click(); ele.sendKeys("1997");

	      

ele=driver.findElement(By.xpath(PomObj.Conference_title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conference_title());
ele.click(); ele.sendKeys("Selenium"); 
	
ele=driver.findElement(By.xpath(PomObj.Conf_Name()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Name());
ele.click(); ele.sendKeys("Zee");
			
ele=driver.findElement(By.xpath(PomObj.Conf_Place()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Conf_Place());
	ele.click(); ele.sendKeys("UK");
	
	
			
ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
			
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
		      
        	String Actualvalue =ele.getAttribute("innerHTML");
			String ExpectedValue="Max authors allowed as per CSE style is 11. If more than 11, please enter first 10 authors and check et al."; 
				
			System.out.println("Preetish-->"+Actualvalue);
					
if(Actualvalue.contains(ExpectedValue)==true) {
	status ="Pass";					
	
}else{
	
	remark="CSE Style More Than 11 Author its getting Saved";	
	 
    status ="Fail"; 
  				    	

utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);	   							 
		
   } 
} catch (Exception e) {

e.printStackTrace();
}finally {
       System.out.println(className);
Ex.testdata(description, className, remark, category, area, status, uAgent);
  }
} 
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test4() throws Exception {
		         	
	   AddCseConferenceRef04 bj = new AddCseConferenceRef04();
		         	         bj.CSE();
                          }
		         	   }