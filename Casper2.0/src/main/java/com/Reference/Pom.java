package com.Reference;

public class Pom {

	
	 private static String element = null;
	 
	 
	public String Z(){
		
		element="#CIT0025 > span.edit_ref_tooltip > img";		 
	       return element;
	}
	 
	public String Ele2() {
		
		element="//div[contains(text(),'There were some limitations associated with this s')]";		 
	       return element;
	
	}
	
	
public String Ele3() {
		
		element="//td[contains(text(),'2.41 (1.47)')]";		 
	       return element;
	
	}
	
	
	
	 
	 public String Table3(){
		 element="//span[contains(text(),'Intra-class correlation coefficient results for co')]";		 
	       return element;
	 }
	 
	 
	 public String Table5(){
		 element="//span[contains(text(),'Analysis of DIAGNOdent values.')]";		 
	       return element;
	 }
	 
	
	 public String Para(){
		 element="//div[contains(text(),'This trial was approved by the Scientific Research')]";		 
	       return element;
	 }
	 
	 public String  Para1() {
		 element="//div[contains(text(),'This trial was approved by the Scientific Research')]";		 
	       return element; 
		 
	 }
	 
	  
	 public String Chapter_title() {
		 element="//div[@class='col-sm-12']//div[@placeholder='Required']";		 
	       return element; 
	 }
	 
	 public String Conference_title() {
		 element="//div[@placeholder='Required']";		 
	       return element; 
	 }
	 
	 public String Conf_Name() {
		 
		 element="//input[@name='conf-name_text']";
		          return element;
		 }
	 
public String Conf_Place() {
		 
		 element="//input[@name='conf-loc_text']";
		          return element;
		 }
	
	 
	 public String ClickReferenceBar(){
		 element="//span[@class='cke_button_icon cke_button__reference_icon']";		 
	       return element;
	 }
	
	 public String Method(){
		 element="//div[contains(text(),'This was a two-arm, split-mouth, double-blind, pla')]";		 
	       return element;
	 }
	 
public String Ele1() {
	    element="//div[@id='S002-S2005']//div[@class='p']";		 
	         return element;
	   }
	
	 
public String StatisticalAnalysis(){
		element="//div[@id='S002-S2008']//div[@class='p']";
	       return element;
	}
	 
public String ReferenceType(){		
		 element="//select[@id='selectbox']";		 
		       return element;
	 }
	
public String In_text_Citation(){
		 element="#ref_cited_txt_insert";		 
	         return element;
	 }
	 
public String Surname1(){
		 element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_0']";		 
		       return element;
	 }
	 
public String T3() {
	 element="//td[@colname='c2'][contains(text(),'T3')]";
     return element;
	
}


public String T2() {
	 element="//td[@colname='c2'][contains(text(),'T2')]";
    return element;
	
}

public String T4_Col1() {
	 element="//td[@colname='c3'][contains(text(),'251')]";
   return element;
	
}


public String given1() {		 
		 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_0']";
		     return element;
	 }
	 
public String Surname2(){
	    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_1']";
		 return element;
	 }
	 
public String given2() {
	  element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_1']";
		 return element;
	 }
     
public String Surname3() {
    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_2']";
    	  return element;
    		 }
    		 
public String given3() {
   element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_2']";
    			 return element;
    		 }
    
 public String Surname4() {
		element=".//*[@id='TextBoxesGroup']/div[1]/div[4]/div[1]/div/div[1]/input";
			 return element;
		 }
		 
public String given4() {
			 
			 element=".//*[@id='TextBoxesGroup']/div[1]/div[4]/div[2]/div/div/input";
			 return element;
		 }

public String Surname5() {
		element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_4']";
			 return element;
		 }
		 
public String given5() {
			 
		element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_4']";
			 return element;
		 }
    
public String Surname6() {
	element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_5']";
		 return element;
	 }
	 
public String given6() {
		 
	element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_5']";
		 return element;
	 }


public String Surname7() {
	element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_6']";
		 return element;
	 }
	 
public String given7() {
		 
		 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_6']";
	return element;
	 }


public String Surname8() {
	    element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_7']";
   return element;
	 }
	 
public String given8() {
		 
		 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_7']";
	return element;
	 }


public String Surname9() {
	       element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_8']";
	return element;
	 }
	 
public String given9() {
		 
		 element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_8']";
		 return element;
	 }

public String Surname10() {
	element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_9']";
		 return element;
	 }
	 
public String given10() {
		 
	element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_9']";
		 return element;
	 }

public String Surname11() {
	element="//div[@class='col-sm-5']//input[@name='authorgroup_AuthorGroup_10']";
		 return element;
	 }
	 
public String given11() {
		 
	element="//div[@class='col-sm-8']//input[@name='authorgroup_AuthorGroup_10']";
		 return element;
	 }

public String et_al(){
    	
    element="//input[@value='et al.']";
    	return element;
	 }
    
  
    public String Collab() {
    
    element="//input[@name='collabgroup_collabgroup']";
	
	 return element;
    }
    
    public String Other_Info() {
    	element="//div[@name='comment_text']";
    	
   	 return element;
    	
    }
    
    public String Year() {
        
    	element="//input[contains(@placeholder,'Example :1974 / In Press / Forthcoming')]";
	
	 return element;
    }
    
    
 public String AddAuthor(){
        
    	element="//div[@class='col-sm-6 spaceforbutton']";
	
	return element;
 }
 
 
 
 public String Book_Title(){
     
 	element="//div[@class='col-sm-6']//div[@placeholder='Required']";
	
	return element;
 }
 
 
 public String EdSurname1(){
     
	 	element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_0']";
		
		   return element;
}

 public String EdGivenName1(){
     
	   element="//div[@class='col-sm-8']//input[@name='Editor-Group_EditorGroup_0']";
		
		   return element;
} 

 
 public String EdSurname2(){
     
	 element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_1']";
		
		return element;
	 }
 
 
 public String EdGivenName2(){
     
	 element="//div[@class='col-sm-8']//input[@name='Editor-Group_EditorGroup_1']";
		
		return element;
} 
 
 public String EdSurname3(){
     
	 element="//div[@class='col-sm-5']//input[@name='Editor-Group_EditorGroup_2']";
		
		return element;
}

 
 public String EdGivenName3(){
     
	 element="//div[@class='col-sm-8']//input[@name='Editor-Group_EditorGroup_2']";
		
		return element;
} 
 

public String Edition(){
     
	 	element="//input[@name='edition_text']";
		
		return element;
	 }
 


 public String Publisher(){
     
	 	element="//input[@name='publisher-name_text']";
		
		return element;
	 }

 public String State(){
     
	 	element="//input[@name='tf:state_text']";
		
		return element;
	 }
 
 public String City(){
     
	 	element="//input[@name='tf:city_text']";
		
		return element;
	 }
 
 public String Country(){
     
	 	element="//input[@name='tf:country_text']";
		
		return element;
	 }

 
public String ArticleTitle () {
        
    	element="//div[@class='col-sm-12']//div[@placeholder='Required']";
	
	 return element;
    }
    
public String JournalTitle() {
    
	element="//div[@class='col-sm-6']//div[@placeholder='Required']";

 return element;
}


public String Volume() {
    
	element="//input[@name='volume_text']";

 return element;
}

public String Issue() {
	element="//input[@name='issue_text']";

	 return element;
	
}

public String FirstPage() {
	element="//input[@name='fpage_text']";

	 return element;
	
  }

public String Ext_link() {
	element="//input[@placeholder='Example: DOI']";

	 return element;
	
   }

public String LastPage() {
	element="//input[@name='lpage_text']";

	 return element;
	
   }
 
public String Comment() {
	element="//input[@name='comment_text']";

	 return element;
	
    }

public String Save() {
	element="#addButton";

	 return element;
	
   }


public String Popup11Author() {
	element="//p[contains(text(),'Max authors allowed as per CSE style is 11. If mor')]";

	 return element;
	
   }

public String refValidation() {
	     element="//ref";
	 return element;
	
}


public String ref2023() {
	element="//a[contains(text(),'2023')]";
	return element;
}

public String ref2024() {
	element="//a[contains(text(),'2024')]";
	return element;
}

public String ref2020() {
	element="//a[contains(text(),'2020')]";
	return element;
}

public String ref1995() {
	element="//a[contains(text(),'1995')]";
	return element;
}

public String ref1996() {
	element="//a[contains(text(),'1996')]";
	return element;
}

public String ref1997() {
element="//a[contains(text(),'1997')]";
	return element;	
}

public String ref1889(){
element="//a[contains(text(),'1889')]";
	return element;	
}

public String ref1888(){
element="//a[contains(text(),'1888')]";
	return element;	
}


public String ref1887(){
element="//a[contains(text(),'1887')]";
	return element;	
}

public String ref1886(){
element="//a[contains(text(),'1886')]";
	return element;	
}

public String ref1885(){
element="//a[contains(text(),'1885')]";
	return element;	
}
public String ref1884(){
element="//a[contains(text(),'1884')]";
	return element;	
}


public String ref2005() {
	element="//a[contains(text(),'2005')]";
	return element;	
}

public String ref2025() {
	element="//a[contains(text(),'2025')]";
	return element;
}


public String ref2012() {
	element="//a[contains(text(),'2012')]";
	return element;
}

public String ref2015() {
	element="//a[contains(text(),'2015')]";
	return element;
}

public String ref2017() {
	element="//a[contains(text(),'2017')]";
	return element;
}

public String ref2009() {
	element="//a[contains(text(),'2009')]";
	return element;
}

public String ref2008() {
	element="//a[contains(text(),'2008')]";
	 return element;
    }

public String ref2006() {
	element="//a[contains(text(),'2006')]";
	 return element;
    }


public String ref2021() {
	element="//a[contains(text(),'2021')]";
	 return element;
    }


public String ref2022() {
	element="//a[contains(text(),'2022')]";
	 return element;
    }

public String Validation() {
	element="//*[@publication-type='journal']";

	 return element;
	    }

public String ValidationBook() {
	element="//*[@publication-type='book']";

	 return element;
	    }


public String Discussion() {
	
element="//div[contains(text(),'There were some limitations associated with this s')]";

	 return element;
}

public String T4_257() {
	
element="//td[@colname='c3'][contains(text(),'257')]";

	 return element;
}


public String Para2() {
	element="//div[contains(text(),'The selection of control and treated quadrants for')]";

	 return element;
	   }

public String FigureContain() {

	element="//*[@id='F0001']/p/span[2]";
  
	  return element;
    }


public String FigureContain3() {

	element="//span[contains(text(),'Flow of participants in the clinical trial.')]";
  
	  return element;
    }


public String TableCaption3() {

	element="//span[contains(text(),'Degree of DLs according to Geiger index.')]";
  
	   return element;
    }



public String TableCaption4() {

	element="//span[contains(text(),'Analysis of DL area on digital images.')]";
  
	   return element;
    }


public String Blinding(){

	element="//*[@id='S002-S2007']/div";
  
	   return element;
    }


public String Conclusion(){

	element="//div[@id='S005']//div[@class='p']";
 	   return element;
    }


public String Table_3Contant(){

	element="//td[contains(text(),'248/10')]";
 	   return element;
    }



public String Et_AlPopUP() {

	element="//p[contains(text(),'Please check only if all the names are entered !')]";
  
	  return element;
    }

public String JournalValidation() {

	element="//p[contains(text(),'Please enter  Journal  title')]";
  
	  return element;
    }


public String PopupValidation() {
	element="//div[@class='sweet-alert showSweetAlert visible']";
	
	return element;
}


public String ref07() {
	element="//ref[@id='CIT0007']";
	
	return element;
}


public String ArticalValidationPop() {

	element="//p[contains(text(),'Please enter  Article  title !')]";
  
	  return element;
    }

public String PublisherPop() {

	element="//p[contains(text(),'Please enter the Publisher Name !')]";
  
	  return element;
    }

public String YearValidationPop() {

	element="//p[contains(text(),'Please enter Year !')]";
  
	  return element;
    }

public String citatinValidationPop() {

	element="//p[contains(text(),'Please enter Cited Name !')]";
  
	  return element;
    }


}
