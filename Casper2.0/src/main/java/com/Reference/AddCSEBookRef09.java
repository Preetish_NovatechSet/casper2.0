package com.Reference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class AddCSEBookRef09 extends JournalRefBaseClass {

	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj; 

    
 public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Without citation, Check whether 1 author name and year, Book title, volume, First Last page, City, Check weather Citation Popup display or not";
		 className = "AddCSEBookRef09";    
				area = "Book Reference";
					category = "CSE Style";
					                				                   
				
				driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					              cokies.cookies();
					                
					             
					       Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					           PomObj = new com.Reference.Pom();		
					           
				WebElement ele = driver.findElement(By.xpath(PomObj.Discussion()));			      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					MoveToElement.byXpath(driver, PomObj.Discussion());    
					          ele.click();
					          
					          
					 driver.switchTo().defaultContent();
					          
	ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));     
	       MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
	WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
	           ele.click();
					     
					          
	Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		    WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                    dropdown.selectByVisibleText("Book");
                               
            	   
				         
					      
						 
ele=driver.findElement(By.xpath(PomObj.Surname1()));
     WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
ele.click(); ele.sendKeys("Shakti");  

ele=driver.findElement(By.xpath(PomObj.given1()));
      WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
ele.click(); ele.sendKeys("Val");  



	ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
	ele.click();   ele.sendKeys("2015");

	

    ele=driver.findElement(By.xpath(PomObj.Book_Title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
	ele.click();   ele.sendKeys("Artifficial"); 

		 ele=driver.findElement(By.xpath(PomObj.Publisher()));
	 WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
	 	ele.click();   ele.sendKeys("Gautam"); 
	
	 ele=driver.findElement(By.xpath(PomObj.City()));
WaitFor.clickableOfElementByXpath(driver, PomObj.City());
	 ele.click();  ele.sendKeys("India");
		
		
	 ele=driver.findElement(By.cssSelector(PomObj.Save()));
	 WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
	 				ele.click();
	 
	 
				
        }catch(Exception e){
              e.printStackTrace();
      }
    	
    	
    try {
    	
    	   WebElement ele = driver.findElement(By.xpath(PomObj.PopupValidation()));
		      
       	String Actualvalue =ele.getAttribute("innerHTML");
			String ExpectedValue="Please enter Cited Name !"; 
							 
					
if(Actualvalue.contains(ExpectedValue)==true) {
								
							  status ="Pass";			    	

							
							    }else{
  remark="Citation Text box Was empty but warning PopUp didn't appear";	
								    	 
            status ="Fail"; 	   							 
utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
							    } 
					} catch (Exception e) {
						e.printStackTrace();
					}finally {
						        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
			}

}
 
 @Test(alwaysRun=true)
	
	public void test9() throws Exception {
	         	
	         		AddCSEBookRef09 bj = new AddCSEBookRef09();
	         	               bj.CSE();
	         	
	         	   }
             }
