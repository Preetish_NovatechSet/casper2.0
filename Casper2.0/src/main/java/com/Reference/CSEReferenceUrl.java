package com.Reference;

import utilitys.DataProviderFactory;

public class CSEReferenceUrl {
	
	
	
	
	/************************************************************************************************/	
	public static class FireFoxCSCJournalRefUrl{
			
		public static String url;
			 
				 public static String getFireFoxUrl() {
		         url=  DataProviderFactory.getCSCStyleReferanceUrl().geturlForFireFox();
				         

					 return url;
				    }
				}


	/************************************************************************************************/

	public static class IECSCJournalRefUrl{
			  public static String url;
			 
				 public static String getIEUrl() {
				         url=  DataProviderFactory.getCSCStyleReferanceUrl().geturlForIE();
				         

					 return url;
				    }
				}

	/************************************************************************************************/	

	public static class ChromeCSCJournalRefUrl {	
				
		public static  String url;
								
				 	
			public static String getChromeUrl() {
					      url= DataProviderFactory.getCSCStyleReferanceUrl().geturlForChrome();
					      

				    return url;
				     }  
				
			    }
			
	/************************************************************************************************/
			
	public static class OperaCSCJournalRefUrl {	
				
				public static  String url;
				 	
				public static String getOperaUrl() {
					      url=  DataProviderFactory.getCSCStyleReferanceUrl().geturlForOpera();
					      
				    return url;
				     }  
				
			    }

	/************************************************************************************************/

	public static class EdgeCSCJournalRefUrl {	
		
		public static  String url;
		 	
		public static String getEdgeUrl() {
			      url=  DataProviderFactory.getCSCStyleReferanceUrl().geturlForEdge();
			      
		    return url;
		     }  
		
	    }


	/************************************************************************************************/

}
