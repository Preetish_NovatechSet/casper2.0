package com.Reference;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class AddCSEJournalRef05 extends JournalRefBaseClass{
		
	
		
		static String remark;
		static String className;
		static String category;
		static String area;
		static String description;
	    static String status;
	    public static  Excel Ex; 
	    static com.Reference.Pom PomObj;
	    
	    
	    public void CSE() throws IOException {
	    	
	    	try {
	    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Add 1 author name and and Fill the empty Text-Box on Citation, year, article title, volume, First Last page, Check the Journal Title Pop is Display or not";
			className = "AddCSEJournalRef05";    
					area = "Journal Reference";
						category = "CSE Style";
						                				                   
					
						driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
						         
						              System.out.println("BrowerName->"+uAgent);
						
						      Cookies cokies = new Cookies(driver);
						      
						             cokies.cookies();
						                
						             
						             Switch switc = new Switch(driver);
						              switc.SwitchCase(uAgent);
						   
						             
						                    PomObj = new com.Reference.Pom();		
						           
						      WebElement ele = driver.findElement(By.xpath(PomObj.FigureContain3()));
						      
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
						
						 MoveToElement.byXpath(driver, PomObj.FigureContain3());    
						          ele.click();
						          
						          
						          driver.switchTo().defaultContent();
						          
						ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
						          
						          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
					WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
						          ele.click();
						     
						          
			Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
			     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
	                     dropdown.selectByVisibleText("Journal");
	                               
	            	   
					WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
					     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
					
					 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
	           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
						        ele.click(); ele.sendKeys("2012");
					          
						     
						        
					   ele=driver.findElement(By.xpath(PomObj.Surname1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
								ele.click(); ele.sendKeys("Preetish");      
						          
								
								
						ele=driver.findElement(By.xpath(PomObj.given1()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
							    ele.click(); ele.sendKeys("Kumar");  	
							   
							    
						ele=driver.findElement(By.xpath(PomObj.Year()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
						ele.click(); ele.sendKeys("2012");     
							       
						
						ele=driver.findElement(By.xpath(PomObj.ArticleTitle()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.ArticleTitle());
							ele.click(); ele.sendKeys("MEDLINE"); 
						       	      
			         								
						ele=driver.findElement(By.xpath(PomObj.Volume()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.Volume());
						ele.click(); ele.sendKeys("1.0"); 
									
						ele=driver.findElement(By.xpath(PomObj.FirstPage()));
				    WaitFor.clickableOfElementByXpath(driver, PomObj.FirstPage());
						ele.click(); ele.sendKeys("1");
											
						ele=driver.findElement(By.xpath(PomObj.LastPage()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.LastPage());
									ele.click(); ele.sendKeys("100");
									
									ele=driver.findElement(By.cssSelector(PomObj.Save()));
									WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
													ele.click();
																									 														
	    	}catch(Exception e){
		                 e.printStackTrace();
			     }
	    	
	    	
	    	
	    	
	    	try {
	    		
	    		
	    		 WebElement ele = driver.findElement(By.xpath(PomObj.JournalValidation()));
					
			       
					
					String Actualvalue =ele.getAttribute("innerHTML");
					String ExpectedValue="Please enter  Journal  title"; 
					 
				

					
					  if(Actualvalue.contains(ExpectedValue)==true) {
						
					    	status ="Pass";			    	

					
					    }else{
			  remark="Journal Text box Was empty But PopUp didn't appear";	
						    	 
						        status ="Fail"; 	   							 
			   utilitys.Getscreenshot.captureScreenShot(uAgent, className, driver);		
					    } 
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				        System.out.println(className);
				 Ex.testdata(description, className, remark, category, area, status, uAgent);
			}
									
	  
	                 }
	    
	    
	    
	    
	    
	    @Test(alwaysRun=true)
		
		public void test5() throws Exception {
		
			AddCSEJournalRef05 obj = new AddCSEJournalRef05();
		           obj.CSE();
		
		   }
	    }




