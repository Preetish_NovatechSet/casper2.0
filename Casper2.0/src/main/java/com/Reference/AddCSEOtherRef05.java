package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEOtherRef05 extends JournalRefBaseClass{
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether 11 author name, citation, year & OtherInfo, check the style has displayed correctly or not";
		 className = "AddCSEOtherRef05";    
				area = "Others Reference";
					category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
				
					                    
			WebElement ele = driver.findElement(By.xpath(PomObj.T4_Col1()));
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
				 MoveToElement.byXpath(driver, PomObj.T4_Col1());    
					          ele.click();
					          
					         
					          
					          driver.switchTo().defaultContent();
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));				       			
					  MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		        WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Others");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("1885");
				          
					     
					        
	    ele =driver.findElement(By.xpath(PomObj.Surname1()));
	         ele.click();ele.sendKeys("Zaber");				        
					          					
			
	         ele=driver.findElement(By.xpath(PomObj.given1()));
	          ele.click();ele.sendKeys("Q");
	          
	          ele =driver.findElement(By.xpath(PomObj.Surname2()));
		         ele.click();ele.sendKeys("Jorden");				        
						          					
				
		         ele=driver.findElement(By.xpath(PomObj.given2()));
		          ele.click();ele.sendKeys("T");
		          
		          
		          ele =driver.findElement(By.xpath(PomObj.Surname3()));
			         ele.click();ele.sendKeys("KK");				        
							          					
					
			         ele=driver.findElement(By.xpath(PomObj.given3()));
			          ele.click();ele.sendKeys("Yash");
			          
			          
			  		for(int i=0;i<=7;i++) {
						 ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
			                              ele.click(); 
						
						}
			          
	
			          
			          ele =driver.findElement(By.xpath(PomObj.Surname4()));
				         ele.click();ele.sendKeys("KK");				        
								          					
						
				         ele=driver.findElement(By.xpath(PomObj.given4()));
				          ele.click();ele.sendKeys("Yash");
				          
				          
				          
				          ele =driver.findElement(By.xpath(PomObj.Surname5()));
					         ele.click();ele.sendKeys("KK");				        
									          					
							
					         ele=driver.findElement(By.xpath(PomObj.given5()));
					          ele.click();ele.sendKeys("Yash");
					          
					          
					          ele =driver.findElement(By.xpath(PomObj.Surname6()));
						         ele.click();ele.sendKeys("KK");				        
										          					
								
						         ele=driver.findElement(By.xpath(PomObj.given6()));
						          ele.click();ele.sendKeys("Yash");
						          
						          
		          ele =driver.findElement(By.xpath(PomObj.Surname7()));
				         ele.click();ele.sendKeys("KK");				        
											          					
									
			         ele=driver.findElement(By.xpath(PomObj.given7()));
				          ele.click();ele.sendKeys("Yash");
							          
							          
			          ele =driver.findElement(By.xpath(PomObj.Surname8()));
						         ele.click();ele.sendKeys("KK");				        
												          					
										
			         ele=driver.findElement(By.xpath(PomObj.given8()));
					          ele.click();ele.sendKeys("Yash");
								          
								          
			          ele =driver.findElement(By.xpath(PomObj.Surname9()));
						         ele.click();ele.sendKeys("KK");				        
													          					
											
				         ele=driver.findElement(By.xpath(PomObj.given9()));
						          ele.click();ele.sendKeys("Yash");
			          
									          
                      ele =driver.findElement(By.xpath(PomObj.Surname10()));
				 ele.click();ele.sendKeys("Author KK");				        
		
 ele=driver.findElement(By.xpath(PomObj.given10()));
	       ele.click();ele.sendKeys("Ya");
			          
	       
           ele =driver.findElement(By.xpath(PomObj.Surname11()));
			 ele.click();ele.sendKeys("Author K");				        
	
ele=driver.findElement(By.xpath(PomObj.given11()));
    ele.click();ele.sendKeys("Y");
    
				ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
				  ele.click(); ele.sendKeys("1885");

	      

    ele=driver.findElement(By.xpath(PomObj.Other_Info()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Other_Info());
    ele.click();ele.sendKeys("Sing a song");
	
			
   ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				
			/*	UpperToolBar obj1 = new UpperToolBar(driver);
				      obj1.save_btn_method(); */
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		Switch switc = new Switch(driver);
               switc.SwitchCase(uAgent);
            
               
            WebElement ele = driver.findElement(By.xpath(PomObj.ref1885()));	      
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);									
						MoveToElement.byXpath(driver, PomObj.ref1885());    
										 ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Zaber Q, Jorden T, KK Yash, KK Yash, KK Yash, KK Yash, KK Yash, KK Yash, KK Yash, Author KK Ya, Author K Y. 1885. Sing a song."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test5() throws Exception {
		         	
	   AddCSEOtherRef05 bj = new AddCSEOtherRef05();
		         	         bj.CSE();
		         	
		         	   }
               }