package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;


public class AddCSEJournalRef02 extends JournalRefBaseClass {

	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
    
    
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE-Check whether 3 author name and Fill the empty Text-Box on In-text Citation, year ,article title, Journal title, volume, First Last page, check the style has been displayed correctly";
		 className = "AddCSEJournalRef02";    
				area = "Journal Reference";
					category = "CSE Style";
					                				                   
				
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
					           
					      WebElement ele = driver.findElement(By.xpath(PomObj.Para2()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					 MoveToElement.byXpath(driver, PomObj.Para2());    
					          ele.click();
					          
					          
					          driver.switchTo().defaultContent();
					          
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Journal");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("2008");
				          
					     
					        
				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.sendKeys("Preeti");      
					          
							
							
					ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
						    ele.click(); ele.sendKeys("Kumar");  
						    
						    ele=driver.findElement(By.xpath(PomObj.Surname2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
								ele.click(); ele.sendKeys("Pintu");      
						          
								
								
						ele=driver.findElement(By.xpath(PomObj.given2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
							    ele.click(); ele.sendKeys("Narayan");  
						    
							    ele=driver.findElement(By.xpath(PomObj.Surname3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
									ele.click(); ele.sendKeys("Raj");      
							          
									
									
			    ele=driver.findElement(By.xpath(PomObj.given3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
				ele.click(); ele.sendKeys("Prasad");  
			
				
ele=driver.findElement(By.xpath(PomObj.Year()));
   WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
ele.click(); ele.sendKeys("2008");     
   

ele=driver.findElement(By.xpath(PomObj.ArticleTitle()));
WaitFor.clickableOfElementByXpath(driver, PomObj.ArticleTitle());
ele.click(); ele.sendKeys("Argentina Militarizes the Drug War"); 
	      

ele=driver.findElement(By.xpath(PomObj.JournalTitle()));
WaitFor.clickableOfElementByXpath(driver, PomObj.JournalTitle());
	ele.click(); ele.sendKeys("James Bovard"); 

	
ele=driver.findElement(By.xpath(PomObj.Volume()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Volume());
ele.click(); ele.sendKeys("2"); 
	
ele=driver.findElement(By.xpath(PomObj.FirstPage()));
WaitFor.clickableOfElementByXpath(driver, PomObj.FirstPage());
ele.click(); ele.sendKeys("1");
			
ele=driver.findElement(By.xpath(PomObj.LastPage()));
WaitFor.clickableOfElementByXpath(driver, PomObj.LastPage());
	ele.click(); ele.sendKeys("90");


ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				ele.click();
				
				


           switc = new Switch(driver);
			switc.SwitchCase(uAgent);
			
			
			
			
			       WebElement ele1 = driver.findElement(By.xpath(PomObj.ref2008()));
			  
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele1);
										
							MoveToElement.byXpath(driver, PomObj.ref2008());    
										       ele1.click();
										       
			
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Preeti Kumar, Pintu Narayan, Raj Prasad. 2008. Argentina Militarizes the Drug War. James Bovard. 2:1�90."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.Validation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.Validation()));

			
			             for(WebElement ele11:el){
			        	   Actualvalue =ele11.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			             }
			       
			         System.out.println("--->"+ListOfJournal); 
			   	
			   	if(ListOfJournal.contains(ExpectedValue)==true) {
			    	  status ="Pass";
			    	  
			    }else{
			    	
remark="Add Journal area trac is missing after Save / Alphabet Sorting is not Correct";	
				    	 
			      		 status ="Fail"; 
			    }
   
             
		} catch (Exception e) {
			
System.out.println(e);
						
			if(e.equals(e)) {
				
				status="Fail";
				   
				   remark="After Adding Journal element Are not visible";
			  }
		}finally {
	        System.out.println(className);
	 Ex.testdata(description, className, remark, category, area, status, uAgent);
             }
        }             
		          
    
   @Test(alwaysRun=true)
         	
		public void test1() throws Exception {
		         	
		         		AddCSEJournalRef02 obj = new AddCSEJournalRef02();
		         	           obj.CSE();
		         	
		         	   }


}
    	
    	