package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCseEditorBookRef03 extends JournalRefBaseClass{
	
	static WebElement ele=null;
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj;
	
    public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether 11 author name with 3 Editor name, year, Chapter title,  Book title, volume, First Last page, City, displayed correctly with sorting Alphabets";
		 className = "AddCseEditorBookRef02";    
				area = "EditorBook Reference";
				    	category = "CSE Style";
					                				                   
				
			driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					      
					             cokies.cookies();
					                
					             
					             Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					                    PomObj = new com.Reference.Pom();		
					           
					       ele = driver.findElement(By.xpath(PomObj.Method()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					 MoveToElement.byXpath(driver, PomObj.Method());    
					          ele.click();
					          
					          
					          driver.switchTo().defaultContent();
					          
					ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));
					          
					          MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
				WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
					          ele.click();
					     
					          
		Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		     WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                     dropdown.selectByVisibleText("Edited Book");
                               
            	   
				WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
				     ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
				 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
           WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
					        ele.click(); ele.sendKeys("2009");
				          
					     
					        
				   ele=driver.findElement(By.xpath(PomObj.Surname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
							ele.click(); ele.sendKeys("Rupa");      
					          
							
							
					ele=driver.findElement(By.xpath(PomObj.given1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
						    ele.click(); ele.sendKeys("K");  
						    
	
						
						    ele=driver.findElement(By.xpath(PomObj.Surname2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
								ele.click(); ele.sendKeys("Rupa");      
						          
								
								
						ele=driver.findElement(By.xpath(PomObj.given2()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
							    ele.click(); ele.sendKeys("K");  
							    
							    
							ele=driver.findElement(By.xpath(PomObj.Surname3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
									ele.click(); ele.sendKeys("Rupa");      
							          
									
									
							ele=driver.findElement(By.xpath(PomObj.given3()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
								    ele.click(); ele.sendKeys("K");  
								    
								    ele=driver.findElement(By.xpath(PomObj.Surname4()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
										ele.click(); ele.sendKeys("Rupa");      
								          
										
										
								ele=driver.findElement(By.xpath(PomObj.given4()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
									    ele.click(); ele.sendKeys("K");  
									    
									    
									 ele=driver.findElement(By.xpath(PomObj.Surname5()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
											ele.click();  ele.sendKeys("Rupa");      
									          
											
											
									ele=driver.findElement(By.xpath(PomObj.given5()));
							  WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
										    ele.click();  ele.sendKeys("K");  
										    
										    
										ele=driver.findElement(By.xpath(PomObj.Surname6()));
						        WaitFor.clickableOfElementByXpath(driver, PomObj.Surname6());
												ele.click();  ele.sendKeys("Rupa");      
												          
														
														
												ele=driver.findElement(By.xpath(PomObj.given6()));
										  WaitFor.clickableOfElementByXpath(driver, PomObj.given6());
													    ele.click();  ele.sendKeys("K");  
						    
								ele=driver.findElement(By.xpath(PomObj.Surname7()));
							  WaitFor.clickableOfElementByXpath(driver, PomObj.Surname7());
												ele.click();  ele.sendKeys("Rupa");      
																		          
																				
																				
								ele=driver.findElement(By.xpath(PomObj.given7()));
							  WaitFor.clickableOfElementByXpath(driver, PomObj.given7());
											    ele.click();  ele.sendKeys("K"); 	
											    
											    
								ele=driver.findElement(By.xpath(PomObj.Surname8()));
							 WaitFor.clickableOfElementByXpath(driver, PomObj.Surname8());
												ele.click();  ele.sendKeys("Rupa");      
																          
																		
																		
										ele=driver.findElement(By.xpath(PomObj.given8()));
									 WaitFor.clickableOfElementByXpath(driver, PomObj.given8());
											    ele.click();  ele.sendKeys("K"); 									    
						    
											    
											    
						 ele=driver.findElement(By.xpath(PomObj.Surname9()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.Surname9());
										ele.click();  ele.sendKeys("Rupa");      
																          
																		
																		
					ele=driver.findElement(By.xpath(PomObj.given9()));
				 WaitFor.clickableOfElementByXpath(driver, PomObj.given9());
							 ele.click();  ele.sendKeys("K"); 							    
											    
							 			    
							 			    
							 			    
							 		ele=driver.findElement(By.xpath(PomObj.Surname10()));
								 WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
												ele.click();  ele.sendKeys("Rupa");      
															          
																	
																	
										ele=driver.findElement(By.xpath(PomObj.given10()));
									WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
											    ele.click();  ele.sendKeys("K"); 					 			    
							 			    
							 			    
					ele=driver.findElement(By.xpath(PomObj.Surname11()));
	           WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
						ele.click();  ele.sendKeys("Rupa");      
																          
																		
																		
							ele=driver.findElement(By.xpath(PomObj.given11()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
									    ele.click();  ele.sendKeys("K"); 						 			    
							 			    
							 			    
							 			    
							 			    
						    ele=driver.findElement(By.xpath(PomObj.Year()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
								ele.click(); ele.sendKeys("2009");

				
												
								ele=driver.findElement(By.xpath(PomObj.Chapter_title()));
								WaitFor.clickableOfElementByXpath(driver, PomObj.Chapter_title());
									ele.click(); ele.sendKeys("3 Secret");
								
							
							ele=driver.findElement(By.xpath(PomObj.EdSurname1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.EdSurname1());
								ele.click(); ele.sendKeys("Rock");			
									
									
							ele=driver.findElement(By.xpath(PomObj.EdGivenName1()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.EdGivenName1());
									ele.click(); ele.sendKeys("ED");		
										
                 
		ele=driver.findElement(By.xpath(PomObj.Book_Title()));
	WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
			ele.click(); ele.sendKeys("Kill a Mockingbird");			 
											 
			ele=driver.findElement(By.xpath(PomObj.Chapter_title()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Chapter_title());
				ele.click(); ele.sendKeys("Padmawati");		
			
			ele=driver.findElement(By.xpath(PomObj.Publisher()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
					ele.click(); ele.sendKeys("Nixon");	
			
			
					ele=driver.findElement(By.xpath(PomObj.Edition()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.Edition());
							ele.click(); ele.sendKeys("5 Edition");	
							
							  ele=driver.findElement(By.xpath(PomObj.FirstPage()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.FirstPage());
									ele.click(); ele.sendKeys("1");	
			
									
						ele=driver.findElement(By.xpath(PomObj.LastPage()));
					WaitFor.clickableOfElementByXpath(driver, PomObj.LastPage());
								 ele.click(); ele.sendKeys("115");					
						
											
							ele=driver.findElement(By.xpath(PomObj.City()));
						 WaitFor.clickableOfElementByXpath(driver, PomObj.City());
								 ele.click(); ele.sendKeys("Ghatsila");		
								 
						   ele=driver.findElement(By.xpath(PomObj.State()));
						WaitFor.clickableOfElementByXpath(driver, PomObj.State());
								ele.click(); ele.sendKeys("Jharkhand");			 
								 
								 ele=driver.findElement(By.xpath(PomObj.Country()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Country());
										ele.click(); ele.sendKeys("India");			
								
								ele=driver.findElement(By.xpath(PomObj.Ext_link()));
							WaitFor.clickableOfElementByXpath(driver, PomObj.Ext_link());
									ele.click(); ele.sendKeys("20-10-2009");		
											
						
				  ele=driver.findElement(By.cssSelector(PomObj.Save()));
               WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
				           ele.click();	
				
}catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
		
    		Switch switc = new Switch(driver);
            switc.SwitchCase(uAgent);
            
            WebElement ele = driver.findElement(By.xpath(PomObj.ref2009()));
		      
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
										
							MoveToElement.byXpath(driver, PomObj.ref2009());    
										       ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Rupa K. 2009. 3 SecretPadmawati. In: Rock ED, editor. Kill a Mockingbird. 5 Edition. Ghatsila, Jharkhand, Jharkhand: Nixon; p. 1�115. 20-10-2009."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Add Journal trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
                    
		        
    
    
   @Test(alwaysRun=true)
         	
		public void test3() throws Exception {
		         	
	   AddCseEditorBookRef03 bj = new AddCseEditorBookRef03();
		         	         bj.CSE();
		         	
		         	   }
               }