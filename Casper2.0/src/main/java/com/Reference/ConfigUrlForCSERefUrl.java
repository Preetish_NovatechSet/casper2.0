package com.Reference;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigUrlForCSERefUrl {
		
	
	static Properties pro;
	
	
	public static void ReadPath(String PathofXML)
	{
		
		String sourcePath = PathofXML;
		
		File src=new File(sourcePath);
		
		try 
		{
		FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		 {
			System.out.println("Exception is "+e.getMessage());
		 }
		
	   }
	
	
	
public String geturlForChrome()
	{
			
	ConfigUrlForCSERefUrl.ReadPath("./Configfolder\\CSC_JournalRefStyle\\Chrome.properties");
	
		String Table2_Aspirations=pro.getProperty("Chrome");
	 	return Table2_Aspirations;
	}
 

public String geturlForFireFox()
{
	
	ConfigUrlForCSERefUrl.ReadPath("./Configfolder\\CSC_JournalRefStyle\\Firefox.properties");
		
	  String url=pro.getProperty("FireFox");
	  return url;
}


public String geturlForIE()
{

	ConfigUrlForCSERefUrl.ReadPath("./Configfolder\\CSC_JournalRefStyle\\IE.properties");
	
String url=pro.getProperty("IE");
return url;
}


public String geturlForOpera(){

	ConfigUrlForCSERefUrl.ReadPath("./Configfolder\\CSC_JournalRefStyle\\Opera.properties");

String url=pro.getProperty("Opera");
return url;
}


public String geturlForEdge(){

	ConfigUrlForCSERefUrl.ReadPath(".//Configfolder\\CSC_JournalRefStyle\\Edge.properties");
	
String url=pro.getProperty("Edge");
return url;
     }
}
