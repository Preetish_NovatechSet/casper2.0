package com.Reference;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import core.MoveToElement;
import core.WaitFor;
import utilitys.Cookies;
import utilitys.Excel;
import utilitys.Switch;

public class AddCSEBookRef05 extends JournalRefBaseClass {
	
	
	static String remark;
	static String className;
	static String category;
	static String area;
	static String description;
    static String status;
    public static  Excel Ex; 
    static com.Reference.Pom PomObj; 

    
 public void CSE() throws IOException, InterruptedException {
    	
    	try {
    		
Ex =  new Excel(description, className, remark, category, area, status, uAgent);
	description = "TandF AuthorEnd-->CSE- Check whether 11 author name and citation, year, Book title,Publisher, City, check the style has been displayed correctly or not";
		 className = "AddCSEBookRef05";    
				area = "Book Reference";
					category = "CSE Style";
					                				                   
				
					driver.manage().timeouts().implicitlyWait(90,TimeUnit.SECONDS);                    
					         
					              System.out.println("BrowerName->"+uAgent);
					
					      Cookies cokies = new Cookies(driver);
					              cokies.cookies();
					                
					             
					       Switch switc = new Switch(driver);
					              switc.SwitchCase(uAgent);
					   
					             
					           PomObj = new com.Reference.Pom();		
					           
				WebElement ele = driver.findElement(By.xpath(PomObj.Blinding()));
					      
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
					
					MoveToElement.byXpath(driver, PomObj.Blinding());    
					          ele.click();
					          
					          
					 driver.switchTo().defaultContent();
					          
	ele=driver.findElement(By.xpath(PomObj.ClickReferenceBar()));     
	       MoveToElement.byXpath(driver, PomObj.ClickReferenceBar());   
	WaitFor.clickableOfElementByXpath(driver, PomObj.ClickReferenceBar());
	           ele.click();
					     
					          
	Select dropdown = new Select(driver.findElement(By.xpath(PomObj.ReferenceType())));
		    WaitFor.visibilityOfElementByXpath(driver, PomObj.ReferenceType());
                    dropdown.selectByVisibleText("Book");
                               
            	   
WaitFor.presenceOfElementByCSSSelector(driver, PomObj.In_text_Citation());     
	  ele=driver.findElement(By.cssSelector(PomObj.In_text_Citation()));
				
 MoveToElement.byCssSelector(driver, PomObj.In_text_Citation());
      WaitFor.clickableOfElementByCSSSelector(driver, PomObj.In_text_Citation());
	      ele.click();   ele.sendKeys("2015");
				          
					     
					        
   ele=driver.findElement(By.xpath(PomObj.Surname1()));
				WaitFor.clickableOfElementByXpath(driver, PomObj.Surname1());
      ele.click();     ele.sendKeys("Yu");      
					          
							
							
	ele=driver.findElement(By.xpath(PomObj.given1()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.given1());
	ele.click();    ele.sendKeys("K");  
						    
	
	   ele=driver.findElement(By.xpath(PomObj.Surname2()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname2());
ele.click();     ele.sendKeys("Gadha");  
						    
	ele=driver.findElement(By.xpath(PomObj.given2()));
	WaitFor.clickableOfElementByXpath(driver, PomObj.given2());
	    ele.click(); ele.sendKeys("Na");  
    
	    ele=driver.findElement(By.xpath(PomObj.Surname3()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.Surname3());
			ele.click(); ele.sendKeys("R");      
	          
			
			
ele=driver.findElement(By.xpath(PomObj.given3()));
		WaitFor.clickableOfElementByXpath(driver, PomObj.given3());
ele.click(); ele.sendKeys("Sam");  


for(int i=0;i<=7;i++) {
ele=driver.findElement(By.xpath(PomObj.AddAuthor()));
WaitFor.clickableOfElementByXpath(driver, PomObj.AddAuthor());
          ele.click(); 

}



ele=driver.findElement(By.xpath(PomObj.Surname4()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname4());
ele.click(); ele.sendKeys("Rja");      



ele=driver.findElement(By.xpath(PomObj.given4()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given4());
ele.click(); ele.sendKeys("P"); 




ele=driver.findElement(By.xpath(PomObj.Surname5()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname5());
ele.click(); ele.sendKeys("Rani");      



ele=driver.findElement(By.xpath(PomObj.given5()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given5());
ele.click(); ele.sendKeys("Pa"); 

ele=driver.findElement(By.xpath(PomObj.Surname6()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname6());
ele.click(); ele.sendKeys("Hj");      



ele=driver.findElement(By.xpath(PomObj.given6()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given6());
ele.click(); ele.sendKeys("Pra"); 


ele=driver.findElement(By.xpath(PomObj.Surname7()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname7());
ele.click(); ele.sendKeys("Dj");      



ele=driver.findElement(By.xpath(PomObj.given7()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given7());
ele.click(); ele.sendKeys("Pra"); 

ele=driver.findElement(By.xpath(PomObj.Surname8()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname8());
ele.click(); ele.sendKeys("Raaj");      



ele=driver.findElement(By.xpath(PomObj.given8()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given8());
ele.click(); ele.sendKeys("A"); 


ele=driver.findElement(By.xpath(PomObj.Surname9()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname9());
ele.click(); ele.sendKeys("Rjas");      



ele=driver.findElement(By.xpath(PomObj.given9()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given9());
ele.click(); ele.sendKeys("Pra"); 



ele=driver.findElement(By.xpath(PomObj.Surname10()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname10());
ele.click(); ele.sendKeys("Rj");      



ele=driver.findElement(By.xpath(PomObj.given10()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given10());
ele.click(); ele.sendKeys("P");   
		
	
ele=driver.findElement(By.xpath(PomObj.Surname11()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Surname11());
ele.click(); ele.sendKeys("Seema");      


ele=driver.findElement(By.xpath(PomObj.given11()));
WaitFor.clickableOfElementByXpath(driver, PomObj.given11());
ele.click(); ele.sendKeys("laguri");   
					      
						    
	ele=driver.findElement(By.xpath(PomObj.Year()));
			WaitFor.clickableOfElementByXpath(driver, PomObj.Year());
	ele.click();   ele.sendKeys("2015");

	      

    ele=driver.findElement(By.xpath(PomObj.Book_Title()));
WaitFor.clickableOfElementByXpath(driver, PomObj.Book_Title());
	ele.click();   ele.sendKeys("Artifficial"); 

		 ele=driver.findElement(By.xpath(PomObj.Publisher()));
	 WaitFor.clickableOfElementByXpath(driver, PomObj.Publisher());
	 	ele.click();   ele.sendKeys("Gautam"); 
	
	 ele=driver.findElement(By.xpath(PomObj.City()));
WaitFor.clickableOfElementByXpath(driver, PomObj.City());
	 ele.click();  ele.sendKeys("India");
		
			
    ele=driver.findElement(By.cssSelector(PomObj.Save()));
WaitFor.clickableOfElementByCSSSelector(driver, PomObj.Save());
	ele.click();
					
				
      }catch(Exception e){
    e.printStackTrace();
      }
    	
    	try {
			
       
    		Switch switc = new Switch(driver);
            switc.SwitchCase(uAgent);
            
            WebElement ele = driver.findElement(By.xpath(PomObj.ref2015()));
		      
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
										
							MoveToElement.byXpath(driver, PomObj.ref2015());    
										       ele.click();
										       
            
			String Actualvalue =null;
			String ListOfJournal = null;
			String ExpectedValue="Yu K, Gadha Na, R Sam, Rja P, Rani Pa, Hj Pra, Dj Pra, Raaj A, Rjas Pra, Rj P, Seema laguri. 2015. Artifficial. India: Gautam."; 
			 
			
			WaitFor.presenceOfElementByXpath(driver, PomObj.refValidation());
			List<WebElement> el=driver.findElements(By.xpath(PomObj.refValidation()));
		
			
			             for(WebElement ele1:el){
			        	   Actualvalue =ele1.getText();
			                	
			             ListOfJournal+=Actualvalue + "\n";
			   	System.out.println("PKM--->"+ListOfJournal);
			         	   
						    }
			             
			     if(ListOfJournal.contains(ExpectedValue)==true) {
			        	  status ="Pass";
			        	  
			        }else{
			        	
		 remark="Add Journal trac is missing after Save / Alphabet Sorting is not Correct";	
	          		    	 
			          		 status ="Fail"; 
			        }			     
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			        System.out.println(className);
			 Ex.testdata(description, className, remark, category, area, status, uAgent);
		             }
				 }
 
 
 @Test(alwaysRun=true)
	
	public void test5() throws Exception {
	         	
	         		AddCSEBookRef05 bj = new AddCSEBookRef05();
	         	               bj.CSE();
	         	
	         	   }
        }