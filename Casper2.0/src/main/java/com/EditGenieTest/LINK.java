package com.EditGenieTest;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LINK {
	
	 private static WebElement element = null;
	 
	 
	 public static WebElement Username(WebDriver driver){
		 
		    element = driver.findElement(By.name("username"));
			 
		    return element;
		 
		    }
	 
	 
	 public static WebElement Password(WebDriver driver){
		 
		    element = driver.findElement(By.name("password"));
			 
		    return element;
		 
		    }
	 
	 public static WebElement Sign_in(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//input[@value='Sign In']"));
			 
		    return element;
		 
		    }
	 
	 public static WebElement Select_Journal(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//select"));
			 
		    return element;
		 
		    }
	 
	 public static WebElement Choose_XML_file(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//input[@name='xml']"));
			 
		    return element;
		 
		    }
	 
	 
	 public static WebElement Upload(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//*[@name='command' and @value='Upload']"));
			 
		    return element;
		 
		    }
	 
	 public static WebElement Link(WebDriver driver){
		 
		    element = driver.findElement(By.cssSelector("#alert-url > h4 > a"));
			 
		    return element;
		 
		    }
	 
	

	 public static WebElement Start_Proofing(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//a[@class='btn btn-lg btn-success btn-block']"));
			 
		    return element;
		 
		    }
	
}
