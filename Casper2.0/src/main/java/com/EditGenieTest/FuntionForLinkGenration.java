package com.EditGenieTest;



import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import com.TableContent.WritePropertiesFile;
import com.linkGenrationModule.LinkGerationTest;
import utilitys.BrowserFactory;
import utilitys.ConstantPath;




/*******Preetish******/




public class FuntionForLinkGenration {


	WebDriver driver; 
	//XML path 
	private final static String  XmlProjectPath = "D:\\Edit_GenieWorkSpace\\Casper2.0\\XML";

	
    final static String TableContainFootNoteXMLPath= getXmlprojectpath()+"/FTPDownloadFileTANDF\\PNRH/";
    final static String FigureContainXMLPath= getXmlprojectpath()+"/FTPDownloadFileTANDF\\PNRH/";
    final static String AuthorName= getXmlprojectpath()+"/FTPDownloadFileTANDF\\PNRH/";
    final static String AuthorNameAlphabetStyleAddAffiliation= getXmlprojectpath()+"/FTPDownloadFileTANDF\\YJOR/";
    final static String AuthorNameNumberStyleAffiliation= getXmlprojectpath()+"/FTPDownloadFileTANDF\\TPSR/";
	final static String CSCJournalRef=  getXmlprojectpath()+"/FTPDownloadFileTANDF\\YJOR/";
	final static String PublisherName="TANDF";
	
	
    /******FigureContain*******
	  *******Author End *******/
    
	
	/*****************************Firefox**************************/
	   
	@Test(alwaysRun=true)
	public void LinkGenrationFuntionForFireFoxFigureContain() throws Exception {
				
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
					        driver.quit();
					        FireFoxFigureContain();
					}		
			   }

	
	public void FireFoxFigureContain() throws Exception {
				
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						driver.quit();
						FireFoxFigureContain();
					}		 
		       }
			
	/*************************************************************************************************************************************/	  
			
		 
		 /*******************************chrome*****************************/
			
	@Test(alwaysRun=true)

	public void LinkGenrationFuntionForChromeFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						   driver.quit();
						ChromeFigureContain();
					}		
			}

	public void ChromeFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					 driver.quit();
						ChromeFigureContain();
				}		
		 }



	/*************************************************************************************************************************************/	
			/***************IE*************/
		
	/*@Test(alwaysRun=true)

	public void LinkGenrationFuntionForIEFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
						
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
			com.FigureContain.WritePropertiesFile.WriteUrlIEInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						 driver.quit();
						 IEFigureContain();
					 }		
	            }
		
	public void IEFigureContain() throws Exception {
		try {
			driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
			
			LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
			
			String TestLinkUrl= driver.getCurrentUrl();
			com.FigureContain.WritePropertiesFile.WriteUrlIEInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			 driver.quit();
			 IEFigureContain();
		}		
	}*/
		
	/*************************************************************************************************************************************/	
			/*****************Edge******************/

	/*	@Test(alwaysRun=true)
			public void LinkGenrationFuntionForEdgeFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
						
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WriteUrlEdgeInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						driver.quit();
					 EdgeFigureContain();
						
					}		
			}
			

		public void EdgeFigureContain() throws Exception {
			
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
					
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WriteUrlEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					 EdgeFigureContain();
				}		
				
		}*/
		
	//*************************************************************************************************************************************//	             
			
			             /******************* Figure Contain  *******************/
		                   /*****************  Editor End    ******************/
			
		 /**firefox**/
	
	@Test(alwaysRun=true)
			public void LinkGenrationFuntionForFireFoxPEFigureContain() throws Exception {
			   try {
						driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WritePEUrlFireFoxInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						    driver.quit();
						FireFoxPEFigureContain();
					}		
		      }
		public void FireFoxPEFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WritePEUrlFireFoxInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					     driver.quit();
					FireFoxPEFigureContain();				
				}		
	       }
		
	//*************************************************************************************************************************************//	
		
	/**chrome**/

	@Test(alwaysRun=true)
			public void LinkGenrationFuntionForChromePEFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WritePEUrlChromeInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						   driver.quit();
						ChromePEFigureContain();
					     }	
					}
		
		
		  public void ChromePEFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WritePEUrlChromeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					    driver.quit();
					 ChromePEFigureContain();
				     }	
		    }
		
		
		
	//*************************************************************************************************************************************//			
		
		
		/**Edge**/	    
	/*
	@Test(alwaysRun=true)
		public void LinkGenrationFuntionForEdgePEFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",VersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
						com.FigureContain.WritePropertiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						   driver.quit();
						EdgePEFigureContain();
					}			
			}

			
			
			
	public void EdgePEFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",VersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.FigureContain.WritePropertiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					EdgePEFigureContain();
				}			
		}*/
		

			
	//*************************************************************************************************************************************//		
			
			/****IE****/

	/*	
	@Test(alwaysRun=true)

	public void LinkGenrationFuntionForIEPEFigureContain() throws Exception {
					try {
						driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
						LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
						
						String TestLinkUrl= driver.getCurrentUrl();
	com.FigureContain.WritePropertiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
					} catch (Exception e) {
						driver.quit();
						IEPEFigureContain();
					}	
				}

	public void IEPEFigureContain() throws Exception {
				try {
					driver = BrowserFactory.Setup_Grid("Chrome",IEVersionControlurl);
					LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,FigureContainXMLPath);
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.FigureContain.WritePropertiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					IEPEFigureContain();
				}			
	     }*/
			
		    
		    
		/**   Add Comment Edit Author name 
		  * @throws Exception *****************************************************************************************/
		
	/**chrome**/
		
	@Test(alwaysRun=true)
		    public void LinkGenrationAuthorNameChrome() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentChrome(TestLinkUrl);
				} catch (Exception e) {
					  driver.quit();
					AuthorNameChrome();
				}
		    }
		    
		    public void AuthorNameChrome() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentChrome(TestLinkUrl);
				} catch (Exception e) {
					  driver.quit();
					AuthorNameChrome();
				}
		    	
		    }

		
	/*************************************************************************************************************************************/	   
		                                        /**firefox**/
		
		
		@Test(alwaysRun=true)
		    public void LinkGenrationAuthorNameFireFox() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentFireFox(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					AuthorNameFireFox();
				}
		    	
		    }
		    
		  
		    public void AuthorNameFireFox() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentFireFox(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					AuthorNameFireFox();
				}
		    	
		    }

		
	//*************************************************************************************************************************************//	    
		                         /***********Edge***********/	
		
		
	/*@Test(alwaysRun=true)
		    public void LinkGenrationAuthorNameEdge() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentEdge(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					AuthorNameEdge();
				}
		    	
		    }
		    
		  public void AuthorNameEdge() throws Exception {
			    	try {
						driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.IEVersionControlurl); 
						
						 LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,AuthorName);			
						
						String TestLinkUrl= driver.getCurrentUrl();
						WritePropertiesFile.WritePEAuthorNameCommentEdge(TestLinkUrl);
					} catch (Exception e) {
						driver.quit();
						AuthorNameEdge();
					}
			    	
			    }
		    	   */  

	/***********IE***********/	     

/*
		@Test(alwaysRun=true)
		    public void LinkGenrationAuthorNameIE() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorName);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					WritePropertiesFile.WritePEAuthorNameCommentIE(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					AuthorNameIE();
					
				}
		    	
		    }
			
		     public void AuthorNameIE() throws Exception {
			    	try {
						driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.IEVersionControlurl); 
						
						 LinkGerationTest url = new LinkGerationTest(driver);
						url.LoginLinkGerationTest(PublisherName,AuthorName);			
						
						String TestLinkUrl= driver.getCurrentUrl();
						WritePropertiesFile.WritePEAuthorNameCommentIE(TestLinkUrl);
					} catch (Exception e) {
						driver.quit();
						AuthorNameIE();
						
					}
			    	
			    }
*/
		    
		//*************************************************************************************************************************************//	    
		    
		    /***********************AlphabetStyleAddAffiliation***************************/
		                        /************YJOR XML*************/
			
		    /******chrome*****/	                            
	   
	@Test(alwaysRun=true)
		    public void LinkGenrationAlphabetStyleAddAffiliationChrome() throws Exception {
		   try
		   {
		   
		    	driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl); 
		    	
		    	 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
				
				String TestLinkUrl= driver.getCurrentUrl();
			   com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
		   }
		   catch(Exception e)
		   {
			   driver.quit();
			   AlphabetStyleAddAffiliationChrome();
		   }
		 }


	  public void AlphabetStyleAddAffiliationChrome() throws Exception {
		   try
		   {
		   
		    driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
		    	
		    	 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
				
				String TestLinkUrl= driver.getCurrentUrl();
			   com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
		   }
		   catch(Exception e)
		   {
			   driver.quit();
			   AlphabetStyleAddAffiliationChrome(); 
		   }
		}
	      
		     
//*************************************************************************************************************************************//  
	                          /******firefox*****/ 
	  
	  
	  @Test(alwaysRun=true)
	public void LinkGenrationAlphabetStyleAddAffiliationFireFox() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					AlphabetStyleAddAffiliationFireFox();
				}
		    }
		    
	public void AlphabetStyleAddAffiliationFireFox() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome",ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					AlphabetStyleAddAffiliationFireFox();
				}
		    	
		  }
		  
	//*************************************************************************************************************************************//	    
		                      /*******Edge*******/    
	/*
	@Test(alwaysRun=true)
		    public void LinkGenrationAlphabetStyleAddAffiliationEdge() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					AlphabetStyleAddAffiliationEdge();
				}
		    	
		  }

	public void AlphabetStyleAddAffiliationEdge() throws Exception{
		try {
			driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
			
			 LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
			
			String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlEdgeInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			driver.quit();
			AlphabetStyleAddAffiliationEdge();
		}
		
	}	 */

	//*************************************************************************************************************************************//


	/*******IE*******/ 	    
	/*
	@Test(alwaysRun=true)
		    public void LinkGenrationAlphabetStyleAddAffiliationIE() throws Exception{
		      	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	 	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					AlphabetStyleAddAffiliationIE();
				}	      	
		   }
			
	public void AlphabetStyleAddAffiliationIE() throws Exception{
	  	try {
			driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
			
			 LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,AuthorNameAlphabetStyleAddAffiliation);			
			
			String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlIEInPropertiesFile(TestLinkUrl);
		        }catch (Exception e){
			   driver.quit();
			AlphabetStyleAddAffiliationIE();
		} 	
	}*/

		
	//*************************************************************************************************************************************//

		    /***********************NumberStyleAddAffiliation***************************/
		                                 /*TPSR XML*/
		                        /*****************************/
		    
		    /**chrome**/

		     
@Test(alwaysRun=true)

	public void LinkGenrationNumberStyleAddAffiliationChrome() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	   com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationChromeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					NumberStyleAddAffiliationChrome();
				}
		    }

	 public void NumberStyleAddAffiliationChrome() throws Exception {
	   	try {
				driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
				
				 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
				
				String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationChromeInPropertiesFile(TestLinkUrl);
			} catch (Exception e) {
				   driver.quit();
				NumberStyleAddAffiliationChrome();
			}
	   }
	

	//*************************************************************************************************************************************//	    
	                                               /**firefox**/
		  
	
		     @Test(alwaysRun=true)
	 public void LinkGenrationNumberStyleAddAffiliationFireFox() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationFireFoxInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					NumberStyleAddAffiliationFireFox();
				}
		    	
		  }
		    
		    
	 public void NumberStyleAddAffiliationFireFox() throws Exception {
		   	    	try {
		   				driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
		   				
		   				 LinkGerationTest url = new LinkGerationTest(driver);
		   				url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
		   				
		   				String TestLinkUrl= driver.getCurrentUrl();
		   com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationFireFoxInPropertiesFile(TestLinkUrl);
		   			} catch (Exception e) {
		   			   driver.quit();
		   				NumberStyleAddAffiliationFireFox();
		   			}
		   	  }
	
		     
	//*************************************************************************************************************************************// 
	                                        //**************Edge******************//
	/*	          
		 @Test(alwaysRun=true)
		    public void LinkGenrationNumberStyleAddAffiliationEdge() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					NumberStyleAddAffiliationEdge();
				}
		    	
		  }
		  
		  
		  public void NumberStyleAddAffiliationEdge() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					NumberStyleAddAffiliationEdge();
				}
		    	
		  }*/
	//*************************************************************************************************************************************//	  
		        //**************IE******************//    
		  
		/*   @Test(alwaysRun=true)
	public void LinkGenrationNumberStyleAddAffiliationIE() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
				NumberStyleAddAffiliationIE();
				}	    	
		   }
		    
		    
	public void NumberStyleAddAffiliationIE() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,AuthorNameNumberStyleAffiliation);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.AuthorNameAreaAddAffiliation.WritePropertiesFile.WriteUrlNumberStyleAddAffiliationIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
				NumberStyleAddAffiliationIE();
				}	    	
		  }
		*/    
		 
		           /**********************CSEAddRef****************************/
		                      /************YJOR XML*************/
		    
		 /**chrome**/

	     
	@Test(alwaysRun=true)
		public void LinkGenrationCSCJournalRefChrome() throws Exception {
		    	try {
			driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					    url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					    String TestLinkUrl= driver.getCurrentUrl();
	 com.Reference.WritePropetiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					CSCJournalRefChrome();
				}
		    }
		   
	public void CSCJournalRefChrome() throws Exception {
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					    url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					    String TestLinkUrl= driver.getCurrentUrl();
	com.Reference.WritePropetiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					CSCJournalRefChrome();
				}
		    }
		   
		    
		/**firefox**/	    

	
		     @Test(alwaysRun=true)
	public void LinkGenrationCSCJournalRefFireFox() throws Exception {
		    
		    	try {
			driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					String TestLinkUrl= driver.getCurrentUrl();
	com.Reference.WritePropetiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					   driver.quit();
					CSCJournalRefFireFox();
				 }
		   }

	public void CSCJournalRefFireFox() throws Exception {
	    
	    	try {
			driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 
				
				 LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
				
				String TestLinkUrl= driver.getCurrentUrl();
	com.Reference.WritePropetiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
			} catch (Exception e) {
				   driver.quit();
				CSCJournalRefFireFox();
			   }
	      }
		       
	
		     
		  /***************edge*********************/        

	/*
	  @Test(alwaysRun=true)
		    public void LinkGenrationCSCJournalRefEdge() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.Reference.WritePropetiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					CSCJournalRefEdge();
				}
		    	
		  }
		  public void CSCJournalRefEdge() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.Reference.WritePropetiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					CSCJournalRefEdge();
				}
		    	
		  }
		
	*/	
		    
		  /****************IE******************/
		 
	/*
		     @Test(alwaysRun=true)
		    public void LinkGenrationCSCJournalRefIE() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.Reference.WritePropetiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					CSCJournalRefIE();
				}	
		 }
		    
		    public void CSCJournalRefIE() throws Exception{
		    	try {
					driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 
					
					 LinkGerationTest url = new LinkGerationTest(driver);
					url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			
					
					String TestLinkUrl= driver.getCurrentUrl();
					com.Reference.WritePropetiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
				} catch (Exception e) {
					driver.quit();
					CSCJournalRefIE();
				}
		    	
		 }
	*/	    

		  /**********************CSEUpadteRef****************************/
		             /************YJOR XML*************/
	/**chrome**/
			
		   
		 @Test(alwaysRun=true)
	public void LinkGenrationCseUpdateRefChrome() throws Exception {
		try {
			driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 

			LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

			String TestLinkUrl= driver.getCurrentUrl();

			com.UpdateReference.WritePropetiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			         driver.quit();
			       CseUpdateRefChrome();
		       }
	     }
		
		 
	public void CseUpdateRefChrome() throws Exception {
		try {
				driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 

				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

				String TestLinkUrl= driver.getCurrentUrl();

				com.UpdateReference.WritePropetiesFile.WriteUrlChromeInPropertiesFile(TestLinkUrl);
			}catch (Exception e){
				       driver.quit();
				    CseUpdateRefChrome();
			       }
		     }
	

	/**firefox**/

		   
	@Test(alwaysRun=true)
	    public void LinkGenrationCseUpdateRefFireFox() throws Exception {

		try {
			
	driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 

			LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

			String TestLinkUrl= driver.getCurrentUrl();
			com.UpdateReference.WritePropetiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			   driver.quit();
			CseUpdateRefFireFox();
			}
	   }
		
	public void CseUpdateRefFireFox() throws Exception {

	try{
		driver = BrowserFactory.Setup_Grid("Chrome", ConstantPath.VersionControlurl); 

		LinkGerationTest url = new LinkGerationTest(driver);
		url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

		String TestLinkUrl= driver.getCurrentUrl();
		com.UpdateReference.WritePropetiesFile.WriteUrlFireFoxInPropertiesFile(TestLinkUrl);
	} catch (Exception e) {
		        driver.quit(); 
		      CseUpdateRefFireFox();
		    }
	   }
	
		     
	//*************************************************************************************************************************************//
		    
		                        /******IE******/    
		    /*
	 @Test(alwaysRun=true)
		    public void LinkGenrationCseUpdateRefIE() throws Exception{
		    try {
				driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 

				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

				String TestLinkUrl= driver.getCurrentUrl();
				com.UpdateReference.WritePropetiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
			} catch (Exception e) {
				driver.quit();
				CseUpdateRefIE();
			    }
		    }
		  
		 public void CseUpdateRefIE() throws Exception{
		    try {
				driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 

				LinkGerationTest url = new LinkGerationTest(driver);
				url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

				String TestLinkUrl= driver.getCurrentUrl();
				com.UpdateReference.WritePropetiesFile.WritePEUrlIEInPropertiesFile(TestLinkUrl);
			} catch (Exception e) {
				driver.quit();
				CseUpdateRefIE();
			    }
		   }
	*/
		     
	//*************************************************************************************************************************************//
		  
		  /******Edge******/
		
	/*
	@Test(alwaysRun=true)
		public void LinkGenrationCseUpdateRefEdge() throws Exception{
		try {
			driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 

			LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

			String TestLinkUrl= driver.getCurrentUrl();
			com.UpdateReference.WritePropetiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			
			CseUpdateRefEdge();
	    	}
		}

	public void CseUpdateRefEdge() throws Exception{
		try {
			driver = BrowserFactory.Setup_Grid("Chrome", IEVersionControlurl); 

			LinkGerationTest url = new LinkGerationTest(driver);
			url.LoginLinkGerationTest(PublisherName,CSCJournalRef);			

			String TestLinkUrl= driver.getCurrentUrl();
			com.UpdateReference.WritePropetiesFile.WritePEUrlEdgeInPropertiesFile(TestLinkUrl);
		} catch (Exception e) {
			
			CseUpdateRefEdge();
	    	}
		}
	*/
		         
		@AfterMethod(alwaysRun=true)
			public void TearDown(){
				try {
					Thread.sleep(6000);
					   driver.quit();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}


		public static String getXmlprojectpath() {
			return XmlProjectPath;
		}
		}	