package com.page;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.WaitFor;


public class UpperToolBar {

	
	WebDriver driver;
	Pom PomObj ;
	static  String uAgent;
	
	public UpperToolBar(WebDriver ldriver) {
		this.driver=ldriver;
	}
	
		
public void save_btn_method() throws Exception{
     
	
	
	Thread.sleep(6000);
	
	driver.switchTo().defaultContent();
         
	PomObj = new Pom();
	
	
	WaitFor.presenceOfElementByXpath(driver, PomObj.save_btn_method());
		
		WebElement ele = driver.findElement(By.xpath(PomObj.save_btn_method()));	
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
		String value = ele.getText();
			
		
			
			if(value.contains(value)) {
				
				
				
		try {
						   			
				
				
			MoveToElement.byXpath(driver, PomObj.save_btn_method());
				System.out.println("Save--->" + value);
				//JavascriptExecutor js = (JavascriptExecutor)driver;
				     //js.executeScript("scroll(250, 0)");
				       ele.click();
				System.out.println("check2--->" + value);    
				 			       
				       
		} catch (Exception e) {
			MoveToElement.byXpath(driver, PomObj.save_btn_method());
			System.out.println("Save--->" + value);
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
	MoveToElement.byXpath(driver, PomObj.save_btn_method());
			       ele.click();
			       Thread.sleep(2000);
		}
		
		
		}else{
			
	MoveToElement.byXpath(driver, PomObj.save_btn_method());	
		
		 WaitFor.clickableOfElementByWebElement(driver, ele);	

				ele.click();
				
				Thread.sleep(2000);
			  }
		   }
	    
	
	
	
	
	
	
	
	

	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[1]/a")
	static List<WebElement> Bold;
	//Bold_module
		public void boldMethod() {
			
			for(WebElement ele:Bold ) {
				String value = ele.getAttribute("data-original-title");
				System.out.println("PKM-"+value);
				
				if(value.contains("Bold")) {
				
				MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[1]/a");
				ele.click();
				}
			}
			
		}	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[2]/a")
	static List<WebElement> italic;
	//Italic_tab
	public void ItalicMethod() {
	
		for(WebElement ele:italic ) {
			String value = ele.getAttribute("data-original-title");
			System.out.println("PKM-"+value);
			
			if(value.contains("Italic")) {
			
			MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[2]/a");
			ele.click();
	
			}
	     }
      }
	
	
	

	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[3]/a")
	static List<WebElement> underline ;
	
	
	//underline tab
	public void underlineMethod() {
		for(WebElement ele:italic ) {
			String value = ele.getAttribute("data-original-title");
			System.out.println("PKM-"+value);
			
			if(value.contains("Underline")) {
			
			MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[3]/a");
		        ele.click();
			}
		}
	 }
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[4]/a")
	static List<WebElement> subscript ;
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[5]/a")
	static List<WebElement> superscript ;
	
	
	//subscript tab
	public void subScriptA2Method() {
		for(WebElement ele:subscript) {
			String value = ele.getAttribute("data-original-title");
			System.out.println("PKM-"+value);
			
			if(value.contains("Subscript")) {
			
			MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[4]/a");
		        ele.click();
			    }
	        }
		}
	
	//superScrit tab
	public void superScriptA2Method() {
		for(WebElement ele:superscript) {
			String value = ele.getAttribute("data-original-title");
			System.out.println("PKM-"+value);
			
			if(value.contains("Superscript")) {
			
			MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[5]/a");
		        ele.click();
			   }
	        }
		}
	
	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[6]/a")
	static List<WebElement> comment ;
	//comment tab
		public void commentMethod() {
			for(WebElement ele:comment) {
				String value = ele.getAttribute("data-original-title");
				System.out.println("PKM-"+value);
				
				if(value.contains("Comment")) {
				
				MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[6]/a");
			        ele.click();
		
			  }
			}
		}
	
	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/a")
	static List<WebElement> citation;
	//hover to citation tab
public void CitationMethod() {
	
	for(WebElement ele:citation){
			String value = ele.getAttribute("data-toggle");
			System.out.println("PKM-"+value);
			
			if(value.contains("dropdown")) {
			
			MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/a");
	
          }
        }
    }
	

@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/ul/li[1]/a")
	static List<WebElement> table_Citation;
	
	@FindBy(how=How.XPATH,using=".//*[@id='ul_tableList']/li[1]/a")
	static List<WebElement> Table1;
	
	@FindBy(how=How.XPATH,using=".//*[@id='ul_tableList']/li[2]/a")
	static List<WebElement> Table2;
	
	@FindBy(how=How.XPATH,using=".//*[@id='ul_tableList']/li[3]/a")
	static List<WebElement> Table3;
	
	@FindBy(how=How.XPATH,using=".//*[@id='ul_tableList']/li[4]/a")
	static List<WebElement> Table4;
	
	@FindBy(how=How.XPATH,using=".//*[@id='ul_tableList']/li[5]/a")
	static List<WebElement> Table5;
	
	// add table citation tab
	public void AddTableCitation() {
		
		for(WebElement ele:table_Citation){
		
			String value = ele.getText();
		System.out.println("PKM-"+value);
		
		if(value.contains("Add Table Citation")) {
		
		MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/ul/li[1]/a");

	    }
	  }
	 }


	public void hover_ToCitationTable1() {
		
		for(WebElement ele:table_Citation){
		
			String value = ele.getText();
		System.out.println("PKM-"+value);
		
		if(value.contains("Table I")) {
		
		MoveToElement.byXpath(driver, ".//*[@id='ul_tableList']/li[1]/a");

	    }
	  }
	 }


	public void hover_ToCitationTable2() {
		
		for(WebElement ele:table_Citation){
		
			String value = ele.getText();
		System.out.println("PKM-"+value);
		
		if(value.contains("Table II")) {
		
		MoveToElement.byXpath(driver, ".//*[@id='ul_tableList']/li[2]/a");

	    }
	   }
	 }
	
	
	
	
	
	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/ul/li[2]/a")
	static List<WebElement> Add_Figure_Citation;

	//Add figure citation menu
	public void AddFigureCitation() {
		
		for(WebElement ele:table_Citation){
		
			String value = ele.getText();
		System.out.println("PKM-"+value);
		
		if(value.contains("Add Figure Citation")) {
		
		MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[9]/ul/li[2]/a");

	   }
	  }
	 }
	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@data-original-title='Find & Replace']")
	static List<WebElement> find_replace;
	
	//Find And Replace
	public void Find_Replace() {
		for(WebElement ele:find_replace) {
			
			String value = ele.getAttribute("data-original-title");
			
			if(value.contains("Find & Replace")) {
				MoveToElement.byXpath(driver, ".//*[@data-original-title='Find & Replace']");
				MoveToElement.byclick(driver, ele);  
			}
			
		}
	}
	
	
	
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[11]/a")
	static List<WebElement> attachment;
	
	//find attachment
	public void Attachment_Method() {
		
		for(WebElement ele:attachment){
			
			String value = ele.getAttribute("data-original-title");
		System.out.println("PKM-"+value);
		
		if(value.contains("Attachment")) {
		
		MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[11]/a");
	    MoveToElement.byclick(driver, ele);  
		   }
	     }	
	   }
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/a")
	static List<WebElement>Ref;
	//hover to reference bar
	public void hoverToReferenceMethod() {
		
		
		WaitFor.presenceOfElementByXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/a");
		for(WebElement ele:Ref){
			
			String value = ele.getAttribute("href");
		System.out.println("PKM-"+value);
		
		if(value.contains("javascript:void(0);")) {
		
			WaitFor.presenceOfElementByXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/a");
		MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/a");
	   
		   }
		
	     }
			
	   }
	
	@FindBy(how=How.XPATH,using="//*/a[contains(text(),'Add Reference')]")
	static List<WebElement>addRef;

	public void AddRefereance() {
		
		
		WaitFor.presenceOfElementByXpath(driver, "//*/a[contains(text(),'Add Reference')]");
		for(WebElement ele:addRef){
			
			String value = ele.getText();
		System.out.println("PKM Reference--> "+value);
		
		if(value.contains(value)) {
		
			
	MoveToElement.byXpath(driver, "//*/a[contains(text(),'Add Reference')]");
		MoveToElement.byclick(driver, ele);
	   
		   }
		
	     }
			
	   }
	
	
	
	@FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/ul/li[2]/a")
	static List<WebElement> AddCrossCitation;
	
	
	 //hover to citation bar
	 public void HoverToAdd_Cross_Citation_MethodClick() {
	 	
	 	for(WebElement ele:AddCrossCitation){
	 		
	 		String value = ele.getAttribute("ineerHTML");
	 	System.out.println("PKM-"+value);
	 	
	 	if(value.contains("Add Cross Citation")) {
	 	
	 	MoveToElement.byXpath(driver, ".//*[@id='bs-example-navbar-collapse-1']/ul[1]/li[8]/ul/li[2]/a");
	 	MoveToElement.byclick(driver, ele);
	    
	 	    }
	 	  }
	    }
	 
	 
	 @FindBy(how=How.XPATH,using=".//*[@id='bs-example-navbar-collapse-1']/ul[2]/li[5]/button")
		static List<WebElement> FinshAndSave;
          //Finsh and save
	 public void Finsh_And_Save() {
		 
		 for(WebElement ele:FinshAndSave){
		
			 String value = ele.getText();
			 if(value.contains(value)) {
				 MoveToElement.byXpath(driver, " .//*[@id='bs-example-navbar-collapse-1']/ul[2]/li[5]/button");
				 JavascriptExecutor executor = (JavascriptExecutor)driver;
				 executor.executeScript("arguments[0].click()", ele);
			 }		 		 
		 }
	 }	 
}


	
