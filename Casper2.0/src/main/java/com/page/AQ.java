package com.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import core.MoveToElement;


public class AQ {

	WebDriver driver;
	
	
	@FindBy(how=How.XPATH,using="html/body/div[39]/ul/li[2]/div[1]/div[1]/font/b")
	static List<WebElement> aq;
	
	
	
	public AQ(WebDriver ldriver) {
		this.driver=ldriver;
	}
	
	
public void click_on_AQ_Tab() {
		
		for(WebElement ele:aq ) {
			String value = ele.getAttribute("innerHTML");
			
			if(value.contains("AQ")) {
			
	   ele.click();
			}
		}
		
    }	



@FindBy(how=How.XPATH,using=".//*[@id='authorquerypanel']/rref[1]/div/center/font")
static List<WebElement> Authorquery;
public void click_on_Author_query() {
		for(WebElement ele:Authorquery) {
			
			String value= ele.getAttribute("innerHTML");
			
			
			if(value.contains("Author Query &nbsp;AQ1")) {
				
				
				MoveToElement.byXpath(driver, ".//*[@id='authorquerypanel']/rref[1]/div/center/font");
				
				MoveToElement.byclick(driver, ele);
				
				
			}
			
		}
    }



@FindBy(how=How.XPATH,using=".//*[@id='temp_authquery_0']")
static List<WebElement> AQ1;
public void click_on_AQ1() {
		for(WebElement ele:AQ1) {
			
			String value= ele.getText();
			
			if(value.contains("AQ1")) {
				
				
				MoveToElement.byXpath(driver, ".//*[@id='temp_authquery_0']");
				
				MoveToElement.byclick(driver, ele);
				
				
			  }
			}
         }
		

@FindBy(how=How.XPATH,using=".//*[@id='txt_author_answer']")
static List<WebElement> txt_author_answer;
public void txt_Author(String writeSomeTxt) {
for(WebElement ele:txt_author_answer) {
 			
 			String value= ele.getAttribute("onkeypress");
 			
 			if(value.contains("return event.keyCode != 13;")) {
 				
 				
 				MoveToElement.byXpath(driver, ".//*[@id='txt_author_answer']");
 				
 				MoveToElement.byclick(driver, ele);
 				
 				MoveToElement.Click_sendkey(driver, ele, writeSomeTxt);
    }
   }
  }


 @FindBy(how=How.XPATH,using=".//*[@id='btn_aq_save']")
         static List<WebElement> save_btn;

public void clickSave_tab() {
for(WebElement ele:save_btn) {
 			
 			String value= ele.getAttribute("innerHTML");
 			
 			if(value.contains("Save")) {
 				
 				
 				MoveToElement.byXpath(driver, ".//*[@id='btn_aq_save']");
 				
 				MoveToElement.byclick(driver, ele);
 				
         }
      
      }
   
   }


 }