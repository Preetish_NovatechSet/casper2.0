package com.page;


import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import core.MoveToElement;
import core.WaitFor;



public class SideToolBar {

	WebDriver driver;
	int count;
	int count2;
public SideToolBar(WebDriver ldriver) {
	this.driver=ldriver;
}

//Tables
@FindBy(how=How.XPATH,using=".//div[@class='menu-icon']//i[@class='fa fa-table']")
List<WebElement> table;

public void table() {
	
  
	for(WebElement ele : table) {
	 
		String value = ele.getAttribute("class");
		
		//System.out.println("pkm"+value);
	   
		if(value.contains("fa fa-table")) {
			MoveToElement.byXpath(driver,".//div[@class='menu-icon']//i[@class='fa fa-table']");
	        MoveToElement.byclick(driver, ele);   
		}
      }
    } 



//Author query in dynamic way
@FindBy(how=How.CSS,using="[class='unwrap-it']")
List<WebElement> Authorquery;

@FindBy(how=How.CSS,using="#temp_authquery_0")
List<WebElement> AQ1ToClick;

@FindBy(how=How.XPATH,using=".//*[@id='chk_aq_r']")
List<WebElement>AQAnswer;

@FindBy(how=How.CSS,using="#btn_aq_next")
List<WebElement>ClickOnNXTBottom;

@FindBy(how=How.CSS,using="button[class='btn btn-default btn-sm'][value='final submit']")
List<WebElement> FinshandSbmit;


public void click_on_Author_queryAndResponse() throws InterruptedException {

	
	 try {
		count=Authorquery.size();
System.out.println("PKM count---"+count);
		
count2=AQ1ToClick.size();
System.out.println("Pkm-->"+count2);

try {
	
	for(WebElement ele:AQ1ToClick) {
		String value =ele.getText();
		if(value.contains(value)) {
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);	
		}
	}
	
		
	count2=AQ1ToClick.size();
System.out.println("Pkm-->"+count2);


WaitFor.presenceOfElementByCSSSelector(driver, "[class='unwrap-it']");
	for(WebElement ele:Authorquery){
	
	String js = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";	    
	((JavascriptExecutor) driver).executeScript(js, ele);
			   
	       String value = ele.getText();
		//System.out.println("Prob"+value);
			if(value.contains(value)) {
				
				MoveToElement.byCssSelector(driver, "#temp_authquery_0");
			        	MoveToElement.byclick(driver, ele);
			       break;
			       
			      }
			}
} catch (Exception e) {

	System.out.println(e.getStackTrace());
}

				
		for(int j=0;j< Authorquery.size();j++) {
						
		for(WebElement AnswerTheAQ:AQAnswer){
							
							String value1=AnswerTheAQ.getText();
			//			System.out.println("Prob0"+value1);	
							if(value1.contains(value1)) {
								
					MoveToElement.byXpath(driver, ".//*[@id='chk_aq_r']");
						MoveToElement.byclick(driver, AnswerTheAQ);
					   MoveToElement.bysendkeyWithoutclick(driver, AnswerTheAQ,  "preetish anwer the aq");
		                     
						break;
						         }
		              }
					for(WebElement NXTbtm:ClickOnNXTBottom){
						String value2=NXTbtm.getText();
				//		System.out.println("Prob1"+value2);
							if(value2.contains(value2)) {
							MoveToElement.byCssSelector(driver, "#btn_aq_next");
							Thread.sleep(900);
							MoveToElement.byclick(driver, NXTbtm);
							break;
					        }  
					}
		     
		}

		
		
for(WebElement SaveBTM:FinshandSbmit){
		
		String value2=SaveBTM.getText();
		    if(value2.contains(value2)){
		   MoveToElement.byCssSelector(driver, "button[class='btn btn-default btn-sm'][value='final submit']");
		MoveToElement.byclick(driver, SaveBTM);
		Thread.sleep(2000);
		core.HandleAlert.isAlertPresentAccept(driver);
	
		                }
			       }
	} catch (Exception e) {
		e.printStackTrace();
	               }   	
         }
}