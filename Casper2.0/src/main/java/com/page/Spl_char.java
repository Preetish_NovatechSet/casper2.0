package com.page;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import core.MoveToElement;

public class Spl_char {
	
	
WebDriver driver;	

	
	@FindBy(how=How.XPATH,using=".//*[@id='letter']/div/div[69]/code")
	static List<WebElement> spl_char; 
	
	
	public Spl_char(WebDriver ldriver) {
		this.driver=ldriver;
	}
	
	
	public void Click_spl_char(){

		int count=spl_char.size();
		System.out.println(count);
		
		for(WebElement ele:spl_char){

			String value = ele.getText();
			System.out.println("total element are- " + value);
			
		if(value.contains("Č")) {
			MoveToElement.byXpath(driver, ".//*[@id='letter']/div/div[69]/code");
		     ele.click();

           }
		}	
	 }
  }
		
