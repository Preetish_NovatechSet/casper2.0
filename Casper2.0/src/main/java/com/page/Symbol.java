package com.page;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import com.FigureContain.Pom;
import core.MoveToElement;
import core.WaitFor;
import utilitys.util;



public class Symbol {

	
 WebDriver driver;
 Pom Pomobj;
 
	
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[1]/a")
  static List<WebElement> Greek; 
  
  @FindBy(how=How.CSS,using="[href='#letter']")
  static List<WebElement> Letter;
  
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[3]/a")
  static List<WebElement> Arrow;
  
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[4]/a")
  static List<WebElement> Mathematics;
  
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[5]/a")
  static List<WebElement> Geometric;
  
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[6]/a")
  static List<WebElement>Currency;
  
  @FindBy(how=How.XPATH,using=".//*[@id='li_symbol']/ul/li/ul/li[7]/a")
  static List<WebElement>Others;
  
  @FindBy(how=How.CSS,using="[data-original-title='Latin Small Letter sharp S']")
  static List<WebElement> SplChar;
  
 
  
  public  Symbol(WebDriver ldriver) {
	  this.driver=ldriver;
  }
  

public void click_On_Symbol() {
	try {
          driver.switchTo().defaultContent();
	
		         Pomobj = new Pom();
		      
		   WaitFor.presenceOfElementByXpath(driver, Pomobj.click_On_Symbol());
	   
		   WebElement ele =driver.findElement(By.xpath(Pomobj.click_On_Symbol()));
	    
	   WaitFor.visibilityOfElementByXpath(driver, Pomobj.click_On_Symbol());
	   
		String value1 = ele.getText();
			
	//System.out.println("Name of element--"+value1);
		
		if(value1.contains(value1)) {
			
			 MoveToElement.byXpath(driver,Pomobj.click_On_Symbol());
			
		WaitFor.clickableOfElementByWebElement(driver, ele);
			
			  MoveToElement.byclick(driver, ele);
			
			} 
		} catch (Exception e) {
                    	e.printStackTrace();
                    
		          }
			   }
	                  
	 


 
 public void click_On_Greek() {

WaitFor.presenceOfElementByXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[1]/a");
	for(WebElement ele:Greek) {
		
		WaitFor.visibilityOfElementByWebElement(driver, ele);
		       String value = ele.getText();
		 //  System.out.println("Name of the element is---"+value);
	
		if(value.contains("Greek")) {
			WaitFor.clickableOfElementByWebElement(driver, ele);
			MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[1]/a");
   		 }  
	  }
   }

 
 
 
public void click_On_Letter() {

	WaitFor.presenceOfElementByXpath(driver,Pomobj.click_On_Letter());
             
	   WebElement ele=driver.findElement(By.xpath(Pomobj.click_On_Letter()));
	
	WaitFor.visibilityOfElementByWebElement(driver, ele);
		
		       String value = ele.getText();
		  // System.out.println("Name of the element is---"+value);
	
		if(value.contains(value)) {			
			
			Select drpCountry = new Select(driver.findElement(By.xpath(Pomobj.click_On_Letter())));
			
			drpCountry.selectByVisibleText("Latin-1 Supplement");
			
					
   /* ((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		
			 MoveToElement.byCssSelector(driver,Pomobj.click_On_Letter());*/
						
		}  
	  }
    
  

 public void click_On_Arrow() {
	

		for(WebElement ele:Arrow) {
			       String value = ele.getText();
			 //  System.out.println("Name of the element is---"+value);
		
			if(value.contains("Arrow")) {
				MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[3]/a");
				
			}  
		  }
	    }
  
 public void click_On_Mathematics() {
	

		for(WebElement ele: Mathematics) {
			       String value = ele.getText();
			 //  System.out.println("Name of the element is---"+value);
		
			if(value.contains("Mathematics")) {
				MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[4]/a");
				
			}  
		  }
	    }
  
 public void click_On_Geometric() {


		for(WebElement ele:Geometric) {
			       String value = ele.getText();
			 //  System.out.println("Name of the element is---"+value);
		
			if(value.contains("Geometric")) {
				
				MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[5]/a");			
			}  
		  }
	    }
  
 public void click_On_Currency() {

		for(WebElement ele:Currency) {
			       String value = ele.getText();
			 //  System.out.println("Name of the element is---"+value);
		
			if(value.contains("Currency")) {
				MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[6]/a");			
			   }  
		   }
	    }
 
 
 public void click_On_Others() {

		for(WebElement ele:Others) {
			       String value = ele.getText();
			 //  System.out.println("Name of the element is---"+value);
		
			if(value.contains("Others")) {
				MoveToElement.byXpath(driver, ".//li[@id='li_symbol']/ul/li/ul/li[7]/a");			
			   }  
		   }
	    }
 
 
 
 
 
  public void click_On_Splchar() throws Exception {
	  
	 Thread.sleep(3000);
	
	  WaitFor.presenceOfElementByCSSSelector(driver,Pomobj.click_On_Splchar());
	  
	  WebElement ele=driver.findElement(By.cssSelector(Pomobj.click_On_Splchar()));
		     
		  String value = ele.getText();
	 //System.out.println("Element name--"+value);
	  
	  if (value.contains(value)) {
		 	  	
		  MoveToElement.byCssSelector(driver,Pomobj.click_On_Splchar());
	
	              util.highLightElement(driver, ele);
	       
	     ((JavascriptExecutor)driver).executeScript("arguments[0].click();", ele);
	  
	   }  
    }
  
  
  public void InsertBottom() {
	  

      
		WaitFor.presenceOfElementByXpath(driver, Pomobj.Insertbotton());
		   
		WebElement  ele=driver.findElement(By.xpath(Pomobj.Insertbotton()));
		    
		        MoveToElement.byXpath(driver, Pomobj.Insertbotton());
		                MoveToElement.byclick(driver, ele);
	     } 
   }