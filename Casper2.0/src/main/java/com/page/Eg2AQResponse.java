package com.page;




import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.FigureContain.Pom;
import core.MoveToElement;
import core.WaitFor;




public class Eg2AQResponse {
	
	
	WebDriver driver;
	static Pom PomObj;
	public static WebElement ele =null;
	
	public Eg2AQResponse(WebDriver ldriver){
		this.driver=ldriver;
	}
	
		



	public void Aq1ClickAndResponseAllAQ() throws Throwable{
		
				
try {
		
	  	
			 PomObj= new Pom();		
			
	WaitFor.presenceOfElementByXpath(driver, PomObj.AQAll());
			
			List<WebElement> count= driver.findElements(By.xpath(PomObj.AQAll()));
			    int AQ_List=count.size();
		System.out.println(AQ_List);
			
			

			
 ele= driver.findElement(By.xpath(PomObj.AQ1()));  
						
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);
				
				
				MoveToElement.byXpath(driver, PomObj.AQ1());
		WaitFor.presenceOfElementByXpath(driver, PomObj.AQ1());
		                ele.click();
		        
			
			
			   Thread.sleep(6000);
			int iCount = 0;
			    for(int i=0;i<AQ_List;i++){
			    	
			    driver.switchTo().defaultContent();
			  
			    iCount++;
			    
			    	
			    WaitFor.presenceOfElementByXpath(driver, PomObj.EditAQ());
				
			   
			    WebElement CheckBox= driver.findElement(By.xpath(PomObj.EditAQ()));   
			    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			    
			    
			    
			    String value1 =CheckBox.getText();
				System.out.println("CheckBox--->"+value1);
				
				if(value1.contains(value1)){
					
					MoveToElement.byXpath(driver, PomObj.EditAQ());
					((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+CheckBox.getLocation().y+")");	
					   CheckBox.click();
					   
					   Thread.sleep(2000);
					   
			
				  } 
				 
			
					 WaitFor.presenceOfElementByXpath(driver, PomObj.AQnxtBottom());
					WebElement nxt= driver.findElement(By.xpath(PomObj.AQnxtBottom()));   
					    
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					    
					
					
					
						String value2="";
					    value2 =nxt.getText();
					    
						System.out.println("nxt--->"+value2);
						
						
						if(value2 != null && !value2.isEmpty() && iCount< AQ_List){
							
							if(value2.contains(value2)){
								
								MoveToElement.byXpath(driver, PomObj.AQnxtBottom());
								
								nxt.click();	
						
							continue;
							
						}else{
		      	
							break;					
						 }
						
						
			    }		
					
			    				
			    	    
				
						
				WebElement saveAQButton= driver.findElement(By.xpath(PomObj.btn_aq_save()));   
							    						
					MoveToElement.byXpath(driver, PomObj.btn_aq_save());				      
				WaitFor.presenceOfElementByXpath(driver, PomObj.btn_aq_save());
									saveAQButton.click();	
								
									Thread.sleep(10000);
								
						          
								} 
			             
			    		    
		
			    WaitFor.presenceOfElementByXpath(driver, PomObj.Submit());
			  
				WebElement sumbit= driver.findElement(By.xpath(PomObj.Submit()));   
					    
				driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		    
							
							MoveToElement.byXpath(driver, PomObj.Submit());
							
							sumbit.click();	
					
							Thread.sleep(5000);
			    
			    			
					
			  try {
				  
				WaitFor.presenceOfElementByXpath(driver, PomObj.Yes_Submit());
						  
				WebElement sumbit1= driver.findElement(By.xpath(PomObj.Yes_Submit()));
						
				  driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
						
					        	 MoveToElement.byXpath(driver, PomObj.Yes_Submit());
						        
						        WaitFor.clickableOfElementByXpath(driver, PomObj.Yes_Submit());
						                  
						        sumbit1.click(); 
						        Thread.sleep(20000);
						
			    }catch(Exception e){
				
				WaitFor.presenceOfElementByXpath(driver, PomObj.Yes_Submit());
				  
				WebElement sumbit1= driver.findElement(By.xpath(PomObj.Yes_Submit()));
					driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					     
						        WaitFor.clickableOfElementByXpath(driver, PomObj.Yes_Submit());
						                  
						        sumbit1.click();  	          
						        	 
						       Thread.sleep(20000);
						
			           }
			
				/*	 WaitFor.presenceOfElementByXpath(driver, PomObj.EditorLink());
						WebElement EditorLinkobj= driver.findElement(By.xpath(PomObj.EditorLink()));   
							    
							    String value4 =EditorLinkobj.getText();
							
							//System.out.println("EditorLink->"+value4);	
								
							if(value4.contains(value4)){
									
									MoveToElement.byXpath(driver, PomObj.EditorLink());
									//EditorLinkobj.click();		
									
									
									//driver.switchTo().defaultContent();
								   
							}*/	
							
							
					
					
		        }catch (Exception e) {
		
			e.printStackTrace();
		       }
		
	       } 
	
	
	
	
//Ers	
	
public void ErsAq1ClickAndResponseAllAQ(){
		
		
		try {
				
			
					 PomObj= new Pom();		
					
			WaitFor.presenceOfElementByXpath(driver, PomObj.AQAll());
					
					List<WebElement> count= driver.findElements(By.xpath(PomObj.AQAll()));
					    int zize=count.size();
				System.out.println("Total AQ-->"+zize);
					
					
		WaitFor.presenceOfElementByXpath(driver, PomObj.Q1());
					
		WebElement ele= driver.findElement(By.xpath(PomObj.Q1()));  
					
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);		
						
					String value =ele.getText();
					
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(false);", ele);		
					
					
					if(value.contains(value)){
								
						MoveToElement.byXpath(driver, PomObj.Q1());
							
						Thread.sleep(5000);
					    ele.click();
					    
					    
					}
					
					
					int iCount = 0;
					    for(int i=0;i<zize;i++){

					    driver.switchTo().defaultContent();
					  
					    iCount++;
					    
					    	
					    WaitFor.presenceOfElementByXpath(driver, PomObj.EditAQ());
						
					   
					    WebElement CheckBox= driver.findElement(By.xpath(PomObj.EditAQ()));   
					    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					    
					    
					    
					    String value1 =CheckBox.getText();
						System.out.println("CheckBox--->"+value1);
						
						if(value1.contains(value1)){
							
							MoveToElement.byXpath(driver, PomObj.EditAQ());
							((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+CheckBox.getLocation().y+")");	
							   CheckBox.click();
							   
							   Thread.sleep(2000);
							   
					
						  } 
						 
				
							
							 WaitFor.presenceOfElementByXpath(driver, PomObj.AQnxtBottom());
							WebElement nxt= driver.findElement(By.xpath(PomObj.AQnxtBottom()));   
							    
							driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
							    
							
							
							
								String value2="";
							    value2 =nxt.getText();
							    
								System.out.println("nxt--->"+value2);
								
								
								if(value2 != null && !value2.isEmpty() && iCount< zize){
									
									if(value2.contains(value2)){
										
										MoveToElement.byXpath(driver, PomObj.AQnxtBottom());
										
										nxt.click();	
								
									continue;
									
								}else{
				      	
									break;					
								 }
								
								
					    }		
							
					    				
					    	    
							WaitFor.presenceOfElementByXpath(driver, PomObj.btn_aq_save());
								
						WebElement saveAQButton= driver.findElement(By.xpath(PomObj.btn_aq_save()));   
									    
							driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
						    
									    String value3 =saveAQButton.getText();
									
									System.out.println("Save&Sumbit->"+value3);	
										
									if(value3.contains(value3)){
										
							MoveToElement.byXpath(driver, PomObj.btn_aq_save());
							      
						WaitFor.clickableOfElementByXpath(driver, PomObj.btn_aq_save());
						
											saveAQButton.click();	
										
											Thread.sleep(5000);
										
								          
										} 
					             }  
					    
					    
					   
					    
					    
					    
					    
				
					    WaitFor.presenceOfElementByXpath(driver, PomObj.Submit());
					  
						WebElement sumbit= driver.findElement(By.xpath(PomObj.Submit()));   
							    
						driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
				    
							    String value3 =sumbit.getText();
							
							System.out.println("Sumbit->"+value3);	
								
							if(value3.contains(value3)){
									
									MoveToElement.byXpath(driver, PomObj.Submit());
									
									sumbit.click();	
							
									Thread.sleep(6000);
					    
							}
					    			
							
					  try {
						  
						WaitFor.presenceOfElementByXpath(driver, PomObj.Yes_Submit());
								  
						WebElement sumbit1= driver.findElement(By.xpath(PomObj.Yes_Submit()));
								driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
								
							        	 MoveToElement.byXpath(driver, PomObj.Yes_Submit());
								        
								        WaitFor.clickableOfElementByXpath(driver, PomObj.Yes_Submit());
								                  
								        sumbit1.click(); 
								        Thread.sleep(6000);
								
					    }catch(Exception e){
						
						WaitFor.presenceOfElementByXpath(driver, PomObj.Yes_Submit());
						  
						WebElement sumbit1= driver.findElement(By.xpath(PomObj.Yes_Submit()));
							driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
							     
								        WaitFor.clickableOfElementByXpath(driver, PomObj.Yes_Submit());
								                  
								        sumbit1.click();  	          
								        	 
								       Thread.sleep(12000);
								
					           }
					
						/*	 WaitFor.presenceOfElementByXpath(driver, PomObj.EditorLink());
								WebElement EditorLinkobj= driver.findElement(By.xpath(PomObj.EditorLink()));   
									    
									    String value4 =EditorLinkobj.getText();
									
									//System.out.println("EditorLink->"+value4);	
										
									if(value4.contains(value4)){
											
											MoveToElement.byXpath(driver, PomObj.EditorLink());
											//EditorLinkobj.click();		
											
											
											//driver.switchTo().defaultContent();
										   
									}*/	
									
									
							
							
				        }catch (Exception e) {
				
					e.printStackTrace();
				       }
				
			       }
	
	
	
	
	
	
	
	
	   
    }
   

