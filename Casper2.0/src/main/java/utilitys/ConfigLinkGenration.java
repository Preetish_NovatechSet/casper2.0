package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigLinkGenration {
   
	Properties pro;
	

public ConfigLinkGenration()
{
	
	File src=new File("./Configfolder/VersionControlLink.properties");
	
	try 
	{
		FileInputStream fis=new FileInputStream(src);
		
		 pro=new Properties();
		
		   pro.load(fis);
		
	} catch (Exception e) 
	{
		System.out.println("Exception is "+e.getMessage());
	}
	
   }

public String getversion8()
{
	String v8=pro.getProperty("linkGenrationV8");
	return v8;
}

public String getversion7()
{
	String v7=pro.getProperty("linkGenrationV7");
	return v7;
}

public String getversion6()
{
	String v6=pro.getProperty("linkGenrationV6");
	return v6;
}

public String getversion5()
{
	String v5=pro.getProperty("linkGenrationV5");
	return v5;
}
public String getversion4()
{
	String v4=pro.getProperty("linkGenrationV4");
	return v4;
}
public String getversion3()
{
	String v3=pro.getProperty("linkGenrationV3");
	return v3;
}
public String getversion2()
{
	String v2=pro.getProperty("linkGenrationV2");
	return v2;
}



public String getdev()
{
	String v2=pro.getProperty("LinkGenrationDev");
	return v2;
}


public String getEG2UAT()
{
	String v2=pro.getProperty("LinkGenrationUAT");
	return v2;
}


public String getEG2IE()
{
	String v2=pro.getProperty("LinkGenrationIE");
	return v2;
}


public String getEG2LiveLink()
{
	String v2=pro.getProperty("EG2LiveLink");
	return v2;
}

public String getDevCasperv3()
{
	String v2=pro.getProperty("devCasperv3");
	return v2;
}

public String getDevCasper()
{
	String v2=pro.getProperty("devCasper");
	return v2;
}

public String getDevCasperUAT()
{
	String v2=pro.getProperty("devCasperUAT");
	return v2;
}

public String getLiveCasper() {
	
	String v2=pro.getProperty("LiveCasper");
	return v2;
	
}

}