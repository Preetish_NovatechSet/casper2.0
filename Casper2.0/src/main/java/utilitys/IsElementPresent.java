package utilitys;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IsElementPresent {
	
	WebDriver driver;
	
	public IsElementPresent(WebDriver driver) {
		this.driver=driver;
		
	}
	
	
	public boolean isElementPresent(By element) {
		   try {
		       driver.findElement(element);
		       return true;
		   } catch (NoSuchElementException e) {
		       return false;
		   }
		}

}
