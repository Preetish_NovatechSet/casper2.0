package utilitys;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zip {

	
	
public static void CreateZip() throws IOException {
		

String output = "E:\\AutomationReport.zip";

String source = "E:\\EG2 Screenshot\\";


System.out.println("Zip Archive Example");
        System.out.println();
System.out.println("Output to Zip : " + output);
        System.out.println();
        
List<String> fileList = getAllFileList(new File(source));

        zip(output, fileList);

}
		
		
		
/**
 *   @preetish
 *   
 * Zip the list received as parameter
 * 
 * @param zipFile
 *            output ZIP file location
 */


public static void zip(String zipFile, List<String> fileList) {

	
byte[] buffer = new byte[1024];

try {
    FileOutputStream fos = new FileOutputStream(zipFile);
        
    ZipOutputStream zos = new ZipOutputStream(fos);
   
        zos.setMethod(ZipOutputStream.DEFLATED);
    
    for (String file : fileList) {
    	
    	File srcFile = new File(file);   	   	  
    	    	      	 	
    System.out.println("File added: " + file);

    zos.putNextEntry(new ZipEntry(srcFile.getName()));

    FileInputStream in = new FileInputStream(file);

    int len;
        while ((len = in.read(buffer)) > 0) {
        zos.write(buffer, 0, len);
           }

    in.close();
    }

    zos.closeEntry();

    zos.close();

    System.out.println();
    System.out.println("Done!");
} catch (IOException ex) {
    ex.printStackTrace();
     }
}


/**
 *      List file or directory
 * 
 **/


public static List<String> getAllFileList(File file) {

List<String> result = new ArrayList<String>();
/** add the file only */
if (file.isFile()) {
    result.add(file.getAbsolutePath());
    /** add all files from the folder */
} else if (file.isDirectory()) {
    File[] files = file.listFiles();
    for (File f : files) {
    result.addAll(getAllFileList(f));
    }
}
return result;
   }		
}
