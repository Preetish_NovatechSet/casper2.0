package utilitys;


import com.AuthorNameAreaAddAffiliation.ConfigUrlForAlphabetStyleAddAffiliation;
import com.AuthorNameAreaAddAffiliation.ConfigUrlForAlphabetStyleAddAffiliation.ConfigUrlForNumberStyleAddAffiliation;
import com.FigureContain.ConfigUrlForFigureContain;
import com.Reference.ConfigUrlForCSERefUrl;
import com.TableContent.ConfigUrl;
import com.UpdateReference.ConfigUrlForCSEUpdateRefUrl;




public class DataProviderFactory 
{

	
	public static ConfigDataProvider getConfig()
	{
		ConfigDataProvider config=new ConfigDataProvider();
		return config;	
	}
	
	
	public static ConfigloginDataAndPath getLoginAndpathfolderConfig()
	{
		ConfigloginDataAndPath config=new ConfigloginDataAndPath();
		return config;	
	}
	
	public static ConfigLinkGenration getLinkVersion()
	{
	ConfigLinkGenration config=new ConfigLinkGenration();
		return config;
	}	
	
	public static ConfigWriteExcelPath getWritePath() {
		ConfigWriteExcelPath config = new ConfigWriteExcelPath();
		return config;
	}
	
	
	public static ConfigUrl getXpathPath() {
		ConfigUrl config = new ConfigUrl();
		return config;
	}
	

	public static ConfigUrl geturl() {
		ConfigUrl config = new ConfigUrl();
		return config;

	}
	public static ConfigUrlForFigureContain getFigureContainUrl() {
		ConfigUrlForFigureContain config =new ConfigUrlForFigureContain();
		return config;
		
	}
	
	
	
	public static ConfigUrlForAlphabetStyleAddAffiliation getAlphabetStyleAddAffiliationUrl() {

ConfigUrlForAlphabetStyleAddAffiliation config = new ConfigUrlForAlphabetStyleAddAffiliation();
		
		return config;
	}
	
	
	public static ConfigUrlForNumberStyleAddAffiliation getNumberStyleAddAffiliationUrl() {
		ConfigUrlForNumberStyleAddAffiliation config =new ConfigUrlForNumberStyleAddAffiliation();
		return config;
	}
	
		
	public static ConfigUrlForCSERefUrl getCSCStyleReferanceUrl() {
			
		ConfigUrlForCSERefUrl config =new ConfigUrlForCSERefUrl();
			 return config;
        }
	
	
	public static ConfigUrlForCSEUpdateRefUrl getCSCStyleUpdateReferanceUrl() {
		
		ConfigUrlForCSEUpdateRefUrl config =new ConfigUrlForCSEUpdateRefUrl();
			 return config;
        }
	}