package utilitys;


import java.io.IOException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.testng.annotations.Test;


public class SendEmail {

	
	
	@Test
	public void AutomaticSendMail() throws IOException {
		
          Zip.CreateZip();
		    SendMail();		
	}
	
	
	
protected static void SendMail() {      
	    
      Properties props = new Properties();
      
      props.put("mail.smtp.host", "smtp-mail.outlook.com");
      props.put("mail.smtp.port", "587");
      props.put("mail.smtp.starttls.enable","true");
      props.put("mail.smtp.auth", "true");

		
      
      Session session = Session.getDefaultInstance(props,
				 
				new javax.mail.Authenticator(){
 
					protected PasswordAuthentication getPasswordAuthentication(){
 
					return new PasswordAuthentication(DataProviderFactory.getLoginAndpathfolderConfig().EmailUser(), DataProviderFactory.getLoginAndpathfolderConfig().EmailPass());
 
					}
 
				});
 
		try {
 
			// Create object of MimeMessage class
			Message message = new MimeMessage(session);
 
			// Set the from address
			message.setFrom(new InternetAddress(DataProviderFactory.getLoginAndpathfolderConfig().EmailUser()));
            
			// Set the recipient address
			message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("antonykingbright@novatechset.com"));
			message.setRecipients(Message.RecipientType.CC,InternetAddress.parse("gautamp@novatechset.com"));
			
            
                        // Add the subject link
			     message.setSubject("EG2.0 Automation Report");
 		
			  BodyPart messageBodyPart = new MimeBodyPart();
			  
	         // Now set the actual message
messageBodyPart.setText("Hi Antony" + "\n" + "\n"+"Please find the attached Automation Testing Report of EG2.0." + "\n" + "\n" + "-Preetish");

	         // Create a multipar message
	         Multipart multipart = new MimeMultipart();

	         // Set text message part
	       multipart.addBodyPart(messageBodyPart);

	         // Part two is attachment
	         messageBodyPart = new MimeBodyPart();
	         String filename = "E:\\AutomationReport.zip";
	         DataSource source = new FileDataSource(filename);
	       
	         String ActualFileName= source.getName();
	         
	         messageBodyPart.setDataHandler(new DataHandler(source));
	         
	         messageBodyPart.setFileName(ActualFileName);
	         
	         multipart.addBodyPart(messageBodyPart);

	         // Send the complete message parts
	         message.setContent(multipart);
	         
	         System.out.println("=====Email Sent=====");
	         // Send message
	         Transport.send(message);

	         System.out.println("Sent message successfully....");
	  
	      } catch (MessagingException e){
	            throw new RuntimeException(e);
	      }
	  }
  }	
