package utilitys;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

//Preetish Kumar Mahato

public class BrowserFactory {

	public static WebDriver driver;
	static DesiredCapabilities capabilities = null;
	static Capabilities cap;

	public BrowserFactory(WebDriver driver) {
		BrowserFactory.driver = driver;

	}

	@SuppressWarnings("deprecation")
	@BeforeClass
	@Parameters({ "browser" })

	public static WebDriver Setup_Grid(String browser, String url) throws Exception {

		if (browser.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", DataProviderFactory.getConfig().getFireFoxGekoPath());
			capabilities = DesiredCapabilities.firefox();
			capabilities.setCapability("marionette", true);
			capabilities.setBrowserName("firefox");
			capabilities.setAcceptInsecureCerts(false);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.getVersion();
			capabilities.setJavascriptEnabled(true);

			// capabilities.setCapability(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY,System.setProperty("webdriver.firefox.marionette",
			// DataProviderFactory.getConfig().getFireFoxGekoPath()));

			/*
			 * ProfilesIni profile = new ProfilesIni(); FirefoxProfile myprofile =
			 * profile.getProfile("preetish");
			 * myprofile.setPreference("javascript.enabled",true);
			 */

			driver = new FirefoxDriver(capabilities);

		}

		else if (browser.equalsIgnoreCase("chrome")) {
			System.out.println("Chrome Browser Start :-->");

			capabilities = DesiredCapabilities.chrome();
			capabilities.setBrowserName("chrome");
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			capabilities.setCapability(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
					System.setProperty("webdriver.chrome.driver", DataProviderFactory.getConfig().getChromePath()));

			driver = new ChromeDriver(capabilities);

			driver.manage().window().maximize();
		}

		else if (browser.equalsIgnoreCase("edge")) {

			capabilities = DesiredCapabilities.edge();
			capabilities.getBrowserName();
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY,
					System.setProperty("webdriver.edge.driver", "./driver\\MicrosoftWebDriver.exe"));

			driver = new EdgeDriver(capabilities);

			driver.manage().window().maximize();

		}

		else if (browser.equalsIgnoreCase("ie")) {

			// System.setProperty("webdriver.ie.driver",DataProviderFactory.getConfig().getIEPath());

			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY,
					System.setProperty("webdriver.ie.driver", DataProviderFactory.getConfig().getIEPath()));
			// capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setJavascriptEnabled(true);
			driver = new InternetExplorerDriver(capabilities);

			driver.manage().window().maximize();
		}

		else if (browser.equalsIgnoreCase("safari")) {

			driver = new SafariDriver();

		}

		else if (browser.equalsIgnoreCase("opera")) {

			System.setProperty("webdriver.opera.driver", "./driver\\operadriver.exe");

			OperaOptions options = new OperaOptions();
			options.setBinary(new File("C:\\Program Files\\Opera\\launcher.exe"));

			capabilities = DesiredCapabilities.operaBlink();
			capabilities.setBrowserName("opera");
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);

			browser = capabilities.getBrowserName();
			System.out.println(browser);

			// capabilities.setCapability(OperaOptions.CAPABILITY, options);

			/*
			 * capabilities.setCapability(OperaDriverService.OPERA_DRIVER_EXE_PROPERTY,
			 * System.setProperty("webdriver.opera.driver",
			 * DataProviderFactory.getConfig().getOperaPath()));
			 * capabilities.setCapability("opera.autostart ",true);
			 */

			driver = new OperaDriver(options);

			driver.manage().window().maximize();

		}

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// ((JavascriptExecutor)driver).executeScript("window.location = \'"+url+"\'");
		driver.get(url);

		// driver.manage().deleteAllCookies();

		new WebDriverWait(driver, 20).until(WebDriver -> ((JavascriptExecutor) WebDriver)
				.executeScript("return document.readyState").equals("complete"));

		return driver;

	}
}