package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigWriteExcelPath {
	Properties pro;
	

	public ConfigWriteExcelPath()
	{
		
		File src=new File("./Configfolder/WriteExcelReportPath.properties");
		
		try 
		{
			FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		{
			System.out.println("Exception is "+e.getMessage());
		}
		
	   }
	
	public String getFigureContainReportPath()
	{
		String FigureReport=pro.getProperty("FigureContainReportPath");
		return FigureReport;
	}
	
	public String getFTP_XML_ReportPath()
	{
		String FTP_XMLReport=pro.getProperty("Ftp_xmlReportPath");
		return FTP_XMLReport;
	}
	
	public String getParagraphReport()
	{
		String Report=pro.getProperty("paragraph23_10_17");
		return Report;
	}
	
}
