package utilitys;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ConfigloginDataAndPath {

	Properties pro;
	
	public ConfigloginDataAndPath()
	{
		
		File src=new File("./Configfolder/Login_xmlPath.properties");
		
		try 
		{
			FileInputStream fis=new FileInputStream(src);
			
			 pro=new Properties();
			
			   pro.load(fis);
			
		} catch (Exception e) 
		{
			System.out.println("Exception is "+e.getMessage());
		}
		
	   }
	
	public String getusername()
	{
		String usernm=pro.getProperty("username");
		return usernm;
	}
		
		
	public String getpassword()
	{
		String passwd=pro.getProperty("password");
		return passwd;
	}
	
	public String getfolderXmlPath()
	{
		String path=pro.getProperty("xmlFolderPath");
		return path;
	}
	
	
	public String getImagePath()
	{
		String path=pro.getProperty("imageFolderPath");
		return path;
	}
	public String getpdfPath()
	{
		String path=pro.getProperty("pdfpath");
		return path;
	}
	public String Path()
	{
		String path=pro.getProperty("path");
		return path;
	}
	
	public String EmailUser()
	{
		String path=pro.getProperty("EmailUser");
		return path;
	}
	
	public String EmailPass()
	{
		String path=pro.getProperty("EmailPass");
		return path;
	}
}
